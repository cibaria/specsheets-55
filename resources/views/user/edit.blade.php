@extends('layouts.admin')


@section('content')
    <div id="specsheets">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>A User
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/user/">Users</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

    {{--errors--}}
    @if ( count( $errors ) > 0 )
        <div class="alert alert-dismissible alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

    <h1><i class='fa fa-user'></i>Edit User</h1>
        {{ Form::model($user, array('method' => 'PATCH','files' => true,'image/save' => 'image/save', 'route' => array('user.update', $user->id))) }}
        {{ Form::hidden('username', $user->username) }}
        {{ Form::hidden('email', $user->email) }}
        {{ Form::hidden('password', $user->password) }}
        {{ Form::hidden('group', $user->group) }}
        <fieldset>

{{--    {{ Form::model($user, ['role' => 'form', 'url' => '/user/' . $user->id, 'method' => 'PUT']) }}--}}

    <div class="form-group">
        {{ Form::label('username', 'Username') }}
        {{ Form::text('username', null,['placeholder' => 'Username', 'class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
            <div class="form-group">
                {{ Form::label('email', 'Email') }}
                {{ Form::text('email', null,['placeholder' => 'Email', 'class' => 'form-control']) }}
            </div>
    <div class="form-group">
        {{ Form::label('password', 'New Password') }}
        {{ Form::password('password', ['placeholder' => 'New Password', 'class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('group') }}
            {{ Form::select('group', [$user->group, '100' => 'Admin', '50' => 'Employee', '10' => 'Guest'], $user->group, ['placeholder' => 'Role', 'class' => 'form-control']) }}

    </div>


    <div class='form-group'>
        {{ Form::submit('Update', ['class' => 'btn btn-primary']) }}
    </div>

    {{ Form::close() }}

</div>

@stop