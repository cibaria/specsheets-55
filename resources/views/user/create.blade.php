@extends('layouts.admin')


@section('content')

    {{--errors--}}
    @if ( count( $errors ) > 0 )
        <div class="alert alert-dismissible alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

    <h1><i class='fa fa-user'></i> Add User</h1>

    {{ Form::open(['image/save', 'route' => 'user.store','files' => true, 'class' => 'form-horizontal']) }}


    <div class="form-group">
        {{ Form::label('username') }}
        {{ Form::text('username', null,['placeholder' => 'Username', 'class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('email') }}
        {{ Form::text('email', null, ['placeholder' => 'email', 'class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('password') }}
        {{ Form::text('password', null, ['placeholder' => 'Password', 'class' => 'form-control']) }}
    </div>


    <div class="form-group">
        {{ Form::label('group') }}
        {{ Form::select('group', ['100' => 'Admin', '50' => 'Employee', '10' => 'Guest', '20' => 'Accounting'], true, ['placeholder' => 'Group', 'class' => 'form-control']) }}
    </div>


    <div class='form-group'>
        {{ Form::submit('Add', ['class' => 'btn btn-default']) }}
    </div>

    {{ Form::close() }}



@stop