@extends('layouts.admin')


@section('content')


    <h1><i class="fa fa-users"></i> User Administration</h1>
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
                <a href="{{ url('/') . "/user/create/" }}" class="btn btn-primary navbar-btn">Create A User</a>

            </nav>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">

 <thead>
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Group</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>

            @foreach($users as $user)
            <tr>
            <td>{{ $user->username }}</td>
            <td>{{ $user->email }}</td>
                @if($user->group == 100)
             <td>Admin</td>
                    @elseif($user->group == 50)
                    <td>Employee</td>
                    @elseif($user->group == 10)
                    <td>Guest</td>
                    @elseif($user->group == 20)
                    <td>Accounting</td>
                @endif

            <td>
            <a href="/user/{{ $user->id }}/edit" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>
               {{ Form::open(['url' => '/user/' . $user->id, 'method' => 'DELETE']) }}
               {{ Form::submit('Delete', ['class' => 'btn btn-danger'])}}
               {{ Form::close() }}

            </td>
            </tr>

            @endforeach
     </tbody>

        </table>
    </div>




@stop