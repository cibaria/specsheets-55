@extends('layouts.admin')

@section('content')

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Dashboard <small>Statistics Overview</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row

        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="fa fa-info-circle"></i>  <strong>Like SB Admin?</strong> Try out <a href="http://startbootstrap.com/template-overviews/sb-admin-2" class="alert-link">SB Admin 2</a> for additional features!
                </div>
            </div>
        </div>
         /.row -->

        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-comments fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">


                                <div class="huge">{{ $recievingappointmentlogs }}</div>
                                <div>Total Active Receiving Appointment Logs</div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->group != 10)
                    <a href="/receivingappointmentlogs/">
                        <div class="panel-footer">
                    
                    
                            <span class="pull-left">View Details</span>
                            
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tasks fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $lotlogs }}</div>
                                <div>Total Log Logs</div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->group != 10)
                    <a href="/lotlogs/">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tasks fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $specsheets }}</div>
                                <div>Total Specsheets</div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->group != 10)
                    <a href="/specsheets/">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-support fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $nutritionfacts }}</div>
                                <div>Total Nutrition Facts</div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->group != 10)
                    <a href="/nutritionfacts/">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tasks fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $customer_complaints }}</div>
                                <div>Total Customer Complaints</div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->group != 10)
                    <a href="/customercomplaints/">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-comments fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $product_holds }}</div>
                                <div>Total Product Holds</div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->group != 10)
                    <a href="/productholds/">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                    @endif
                </div>
            </div>
        </div>





<div class="row">

{{-- product holds --}}
<div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i>Latest Product Holds</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">

                    @foreach($product_hold as $hold)
                        <a href="#" class="list-group-item">
                            
                            <i class="fa fa-fw fa-check"></i> {{ $hold->id + "500" }} | {{ $hold->description }} | <strong>{{ $hold->hold_starting_date }}</strong>
                        </a>
                    @endforeach
                </div>
                    
                    @if(Auth::user()->group != 10)
                <div class="text-right">
                    <a href="/productholds/">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                @endif
            </div>
        </div>
    </div>

{{-- customer complaints --}}
<div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i>Latest Customer Complaints</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">

                    @foreach($customer_complaint as $complaint)
                        <a href="#" class="list-group-item">
                            
                            <i class="fa fa-fw fa-check"></i> {{ $complaint->order_number }} | {{ $complaint->corrective_action_by }} | <strong>{{ $complaint->customer_name }}</strong>
                        </a>
                    @endforeach
                </div>
                    
                    @if(Auth::user()->group != 10)
                <div class="text-right">
                    <a href="/receivingappointmentlogs/">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                @endif
            </div>
        </div>
    </div>

    {{--receiving appointment logs--}}
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Latest Receiving Appointment Logs</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">

                    @foreach($appointments as $appointment)
                        <a href="#" class="list-group-item">
                          
                            <i class="fa fa-fw fa-check"></i> {{ $appointment->id }} {{ $appointment->conf_name }}  <strong>{{ $appointment->vendor }}</strong>
                        </a>
                    @endforeach
                </div>
                    
                    @if(Auth::user()->group != 10)
                <div class="text-right">
                    <a href="/receivingappointmentlogs/">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                @endif
            </div>
        </div>
    </div>

    {{--lot logs--}}
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Latest Lot Logs</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">

                    @foreach($lots as $lot)
                        <a href="#" class="list-group-item">
                          
                            <i class="fa fa-fw fa-check"></i>{{ $lot->lot }} <strong>{{ $lot->so }}</strong>
                        </a>
                    @endforeach
                </div>
                @if(Auth::user()->group != 10)
                <div class="text-right">
                    <a href="/lotlogs/">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                @endif
            </div>
        </div>
    </div>

    {{--specsheets--}}
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Latest Specsheets</h3>
                    </div>
                    <div class="panel-body">
                        <div class="list-group">

                            @foreach($specs as $spec)
                            <a href="#" class="list-group-item">
                          
                                <i class="fa fa-fw fa-check"></i> {{ $spec->title }}
                            </a>
                            @endforeach
                        </div>
                     @if(Auth::user()->group != 10)
                        <div class="text-right">
                            <a href="/specsheets/">View Details <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

    {{--nutrition facts--}}
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Latest Nutrition Facts</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">

                    @foreach($nutritions as $nutrition)
                        <a href="#" class="list-group-item">
                          
                            <i class="fa fa-fw fa-check"></i> {{ $nutrition->title }}
                        </a>
                    @endforeach
                </div>
                @if(Auth::user()->group != 10)
                <div class="text-right">
                    <a href="/nutritionfacts/">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                @endif
            </div>
        </div>
    </div>





        </div>
        <!-- /.row -->

    </div>

@stop