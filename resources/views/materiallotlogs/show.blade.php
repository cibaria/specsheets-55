@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Material Lot Log - Lot# {{ $materiallotlog->lot }}
                            @if(Auth::user()->group == 100 || Auth::user()->group == 50) 
                            <a href="/materiallotlogs/label/{{ $materiallotlog->id}}" class="btn btn-primary">View Label</a>
                            @endif
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/materiallotlogs/">Material Lot Logs</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Lot:</strong> {{ $materiallotlog->lot }}
                            </li>
                            <li class="list-group-item">
                                <strong>Ref#:</strong> {{ $materiallotlog->reference_number }}
                            </li>
                            <li class="list-group-item">
                                <strong>SO#</strong> {{ $materiallotlog->so }}
                            </li>
                            <li class="list-group-item">
                                <strong>Old Lot:</strong> {{ $materiallotlog->original_lot }}
                            </li>
                            <li class="list-group-item">
                                <strong>Description:</strong> {{ $materiallotlog->oil_type }}
                            </li>
                            <li class="list-group-item">
                                <strong>Supplier:</strong> {{ $materiallotlog->supplier }}
                            </li>
                            <li class="list-group-item">
                                <strong>Origin:</strong> {{ $materiallotlog->origin }}
                            </li>
                            <li class="list-group-item">
                                <strong>Quantity:</strong> {{ $materiallotlog->qty }}
                            </li>
                            <li class="list-group-item">
                                <strong>Quantity Per Layer:</strong> {{ $materiallotlog->quantity_per_layer }}
                            </li>
                            <li class="list-group-item">
                                <strong>Quantity Per Pallet:</strong> {{ $materiallotlog->quantity_per_pallet }}
                            </li>
                            <li class="list-group-item">
                                <strong>Size:</strong> {{ $materiallotlog->size }}
                            </li>
                            <li class="list-group-item">
                                <strong>Customer:</strong> {{ $materiallotlog->customer }}
                            </li>
                            <li class="list-group-item">
                                <strong>Date:</strong> {{ $materiallotlog->date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Note:</strong> {{ $materiallotlog->notes1 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Note:</strong> {{ $materiallotlog->notes2 }}
                            </li>    <li class="list-group-item">
                                <strong>Note:</strong> {{ $materiallotlog->notes3 }}
                            </li>
                        </ul>

                        <hr/>


                    </div>
                </div>
            </div>
        </div>
@stop