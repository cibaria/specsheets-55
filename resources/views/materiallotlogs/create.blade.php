@extends('layouts.admin')

@section('content')
    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span> A New Material Lot Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/materiallotlogs/">Material Lot Logs</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'materiallotlogs.store', 'class' => 'form-horizontal']) }}
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('lot', 'Lot') }}
                                {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot']) }}
                            </div>
                             <div class="form-group">
                                {{ Form::label('reference_number', 'Ref#') }}
                                {{ Form::text('reference_number', null,['class' => 'form-control', 'placeholder' => 'Ref#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('original_lot', 'Old Lot') }}
                                {{ Form::text('original_lot', null,['class' => 'form-control', 'placeholder' => 'Old Lot']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('so', 'SO#') }}
                                {{ Form::text('so', null,['class' => 'form-control', 'placeholder' => 'SO#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oil_type', 'Description') }}
                                {{ Form::text('oil_type', null,['class' => 'form-control', 'placeholder' => 'Description']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('supplier', 'Supplier') }}
                                {{ Form::text('supplier', null,['class' => 'form-control', 'placeholder' => 'Supplier']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('origin', 'Origin') }}
                                {{ Form::text('origin', null,['class' => 'form-control', 'placeholder' => 'Origin']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('qty', 'QTY') }}
                                {{ Form::text('qty', null,['class' => 'form-control', 'placeholder' => 'QTY']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('quantity_per_layer', 'Quantity Per Layer') }}
                                {{ Form::text('quantity_per_layer', null,['class' => 'form-control', 'placeholder' => 'Quantity Per Layer']) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('quantity_per_pallet', 'Quantity Per Pallet') }}
                                {{ Form::text('quantity_per_pallet', null,['class' => 'form-control', 'placeholder' => 'Quantity Per Pallet']) }}
                            </div>                             
                            <div class="form-group">
                                {{ Form::label('size', 'Size') }}
                                {{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Size']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('customer', 'Customer') }}
                                {{ Form::text('customer', null,['class' => 'form-control', 'placeholder' => 'Customer']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                {{ Form::text('date', null,['class' => 'form-control', 'id' => 'datepicker', 'placeholder' => 'Date']) }}
                            </div>

                            <hr/>
                            <h2>Notes</h2>
                            <div class="form-group">
                                {{ Form::label('notes1', 'Note') }}
                                {{ Form::textarea('notes1', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes2', 'Note') }}
                                {{ Form::textarea('notes2', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes3', 'Note') }}
                                {{ Form::textarea('notes3', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>

                            {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            // $('#datatable').DataTable({
            //     paging:true,
            //     "order": [[ 10, "asc" ]],
            //     // scrollY:400,
            //     deferRender:true,
            //     // scroller:true,
            //     columnDefs: [
            //                     { targets: 'no-sort', orderable: false }
            //                 ],

            // });
        });
     </script>

    @stop