@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span> Product Hold # <?php $hold_number = $producthold->id + "500" ?> {{ $hold_number }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/productholds/">Product Holds</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
            @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                <div class="col-lg-8">
                  {{ Form::model($producthold, array('method' => 'PATCH', 'route' => array('productholds.update', $producthold->id))) }}
                    <fieldset>
                  <div class="form-group">
                        {{ Form::label('hold_starting_date', 'Hold Starting Date') }}
                        {{ Form::text('hold_starting_date', null,['class' => 'form-control', 'placeholder' => 'Hold Starting Date', 'id' => 'datepicker1']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('description', 'Description') }}
                        {{ Form::text('description', null,['class' => 'form-control', 'placeholder' => 'Description']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('classification_of_hold', 'Classification of Hold') }}
                        {{ Form::select('classification_of_hold',[$producthold->classification_of_hold, 'Specifications Non-Conformance' => 'Specifications Non-Conformance', 'Post Return Inspection' => 'Post Return Inspection', 'Damage Inspection' => 'Damage Inspection', 'Other' => 'Other'], '', ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('hold_by', 'Hold By') }}
                        {{ Form::text('hold_by', null,['class' => 'form-control', 'placeholder' => 'Hold By']) }}
                    </div>                    
                    <div class="form-group">
                        {{ Form::label('reason_for_hold', 'Reason For Hold') }}
                        {{ Form::textarea('reason_for_hold', null,['class' => 'form-control', 'placeholder' => 'Reason For Hold', 'rows' => '8', 'cols' => '8']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('action_required_before_release', 'Action Required Before Release') }}
                        {{ Form::textarea('action_required_before_release', null,['class' => 'form-control', 'placeholder' => 'Action Required Before Release', 'rows' => '8', 'cols' => '8']) }}
                    </div>
                    <div class="form-group">
                    {{ Form::label('notes', 'Note') }}
                    {{ Form::textarea('notes', null,['cols' => '8', 'rows' => '8', 'class' => 'form-control', 'placeholder' => 'Notes']) }}
                    </div>
<hr>
                    <div class="form-group">
                        {{ Form::label('released', 'Released?') }}
                        {{ Form::checkbox('released', 1, ['class' => 'form-control']) }}
                    </div>
                    <div class="form" id="released_notes">
                        {{ Form::label('hold_release_date', 'Hold release Date') }}
                        {{ Form::text('hold_release_date', null, ['class' => 'form-control', 'placeholder' => 'Hold Release Date', 'id' => 'datepicker'] )}}
                        <br>

                        {{ Form::label('person_releasing', 'Person Releasing') }}
                        {{ Form::select('person_releasing',[$producthold->person_releasing, 'JG' => 'JG', 'Kenya?' => 'Kenya?'], '', ['class' => 'form-control']) }}
                        <br>
                        {{ Form::label('released_notes', 'Released Notes') }}
                        {{ Form::textarea('released_notes', null, ['cols' => '8', 'rows' => '8', 'class' => 'form-control', 'placeholder' => 'Released Notes', 'id' => 'released_notes']) }}
                    </div>

                    {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                    </fieldset>
                {{ Form::close() }}

            </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop

@section('footer')
<script>
$('#datepicker').datepicker();

//check if it's checked
if($('#released').prop('checked')) {
    $('#released_notes').show();
} else {
    $('#released_notes').hide();
}
//once it's checked show/hide
$("#released").change(function() {
    if(this.checked) {
        $("#released_notes").show();
    } else {
        $("#released_notes").hide();
    }
});
</script>
@stop