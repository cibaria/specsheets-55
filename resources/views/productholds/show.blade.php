@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Product Hold #<?php $hold_number = $producthold->id + "500"; ?> {{ $hold_number }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/productholds/">Product Holds</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Hold Number:</strong> <?php $hold_number = $producthold->id + "500"; ?>
                                {{ $hold_number }}  
                            </li>
                            <li class="list-group-item">
                                <strong>Hold by:</strong> {{ $producthold->hold_by }}
                            </li>
                            <li class="list-group-item">
                                <strong>Hold Starting Date:</strong> {{ $producthold->hold_starting_date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Description:</strong> {{ $producthold->description }}
                            </li>

                            <li class="list-group-item">
                                <strong>Classification Of Hold:</strong> {{ $producthold->classification_of_hold }}
                            </li>
                        
                            <li class="list-group-item">
                                <strong>Reason For Hold:</strong> {{ $producthold->reason_for_hold }}
                            </li>
                            <li class="list-group-item">
                                <strong>Action Required For Release:</strong> {{ $producthold->action_required_before_release }}
                            </li>
                            <li class="list-group-item">
                                <strong>Notes:</strong> {{ $producthold->notes}}
                            </li>
                            <li class="list-group-item">
                                <strong>Released: </strong>
                                @if($producthold->released == 1)
                                    Yes
                                    @elseif($producthold->released == 0)
                                    No
                                    @else

                                @endif
                            </li>

                            @if($producthold->released == 1)
                            <li class="list-group-item">
                                <strong>Hold Released Date: </strong> {{ $producthold->hold_release_date}}
                            </li>
                            <li class="list-group-item">
                                <strong>Person Releasing: </strong> {{ $producthold->person_releasing }}
                            </li>
                            <li class="list-group-item">
                                <strong>Hold Release Notes: </strong> {{ $producthold->released_notes }}
                            </li>
                            @else

                            @endif
                        </ul>

                        <hr/>
                        <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                            <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($producthold->created_at))->diffForHumans(); ?>
                        </div>
                        <div class="alert alert-dismissible alert-warning">
                            <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($producthold->updated_at))->diffForHumans(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@stop