@extends('layouts.admin')

@section('content')

<div id="rmas">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Product Hold
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Product Hold
                </li>
            </ol>
        </div>
    </div>

    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
                {{ link_to_route('productholds.create', 'Create A New Product Hold',null, ['class' => 'btn btn-primary navbar-btn']) }}
                <a href="/productholds/released" class="btn btn-success navbar-btn">Released</a>
                                {{--search--}}

{{--                 <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class=" search form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-primary" data-sort="rmas">Submit</button>
                </form> --}}
            </nav>
        </div>
    </div>

    <table id="datatable" class="table table-stripped table-responsive table-hover">
        <thead>
        <tr>
           <td><strong>Hold Start Date</strong></td>
           <td><strong>Hold By</strong></td>
           <td><strong>Description</strong></td>
           <td><strong>Reason For Hold</strong></td>
           {{-- <td><strong>Status</strong></td> --}}
           <td></td>
        </tr>
        </thead>
        <tbody class="list">
        @foreach($productholds as $producthold)
            @if($producthold->released != 1)
            <tr>
             <td>{{ $producthold->hold_starting_date }}</td>
             <td>{{ $producthold->hold_by }}</td>
             <td>{{ $producthold->description }}</td>
             <td>{{ $producthold->reason_for_hold }}</td>
             {{-- <td>{{ $producthold->status }}</td> --}}
             <td class="no-sort">
                 <div class="btn-group" role="group" aria-label="">
                     {{ link_to_route('productholds.show', 'View', [$producthold->id],['class' => 'btn btn-success']) }}
                     @if(Auth::user()->group == 100)
                     {{ link_to_route('productholds.edit', 'Edit', [$producthold->id],['class' => 'btn btn-warning']) }}

                     {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('productholds.destroy', $producthold->id))) }}
                     {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                     {{ Form::close() }}
                     @endif
                 </div>
             </td>
            </tr>
            @endif
        @endforeach

        </tbody>
    </table>


<ul class="pagination"></ul>
</div>
    @stop

{{--end content--}}


{{--footer--}}
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop

