<?php //echo Html::anchor('saleschart/edit/'.$saleschart->id, 'Edit'); ?>
<br />

<div class="container">
    <div style="float:left; width:150px;">
        <!--    logo & Left-->
        {{ HTML::image('images/cibaria-logo.jpg') }}
    </div>
    <div style="float:left; width:300px; padding-top:25px;">
        <h1 style="font-size:35px;"><?php echo $saleschart->month; ?> 2014 Sales</h1>
        <p style="color:#d34343; text-align: center;font-size:28px; font-weight: bold;padding-top: 15px;">$

            <?php if($saleschart->revenue >= 12000000)
            {
                echo $saleschart->revenue, '00,000,000';
            }
            ?>

            <!--            --><?php
            //            //format revenue
            //            if($saleschart->revenue  >= 1000000 ) {
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif ($saleschart->revenue >= 1200000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 2000000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 2200000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 3000000) {
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 3200000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 4000000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 4200000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 5000000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 5200000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 6000000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 6200000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 7000000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 7200000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 8000000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 8200000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 9000000){
            //                echo $saleschart->revenue, '0,000,000');
            //            } elseif($saleschart->revenue >= 9200000){
            //                echo $saleschart->revenue, '0,000,000');
            //            }elseif ($saleschart->revenue >= 10000000){
            //                echo $saleschart->revenue, '00,000,000');
            //            } elseif($saleschart->revenue >= 10000000){
            //                echo $saleschart->revenue, '00,000,000');
            //            }elseif($saleschart->revenue >= 11000000){
            //                echo $saleschart->revenue, '00,000,000');
            //            }elseif($saleschart->revenue >= 12000000){
            //                echo $saleschart->revenue, '00,000,000');
            //            }
            //
            //            else{
            //                echo $saleschart->revenue;
            //            }
            ?>

        </p>
        <p style="text-align: center;padding-top:10px;"><span style="color:#0f6aa3; font-weight: bold; font-size: 12px;">VS. 2013</span>
            <!--           arrow-->
            <?php if($saleschart->arrow == 1)
            {
            echo "<span style='color:green; font-weight:bold;'><strong>" . "&#x2191;" . "</strong></span>";
            } else {
            echo "<span style='color:red; font-weight:bold;'><strong>" . "&#x2193;" . "</strong></span>";
            } ?>
            <!--            goal-->
            <strong>%<?php echo $saleschart->goal_percentage; ?></strong></p>
        <br />
        <p style="text-align: center;"><strong><?php echo $saleschart->message; ?></strong></p>

    </div><!-- end Logo & left -->

    <!--oil pour & right-->
    <div style="float:right"><?php echo HTML::image('images/oilpour.jpg'); ?></div>
</div><!-- end -->


<div class="container" >
    <?php

    //chart
    switch($saleschart->revenue) {
    case ($saleschart->revenue >= 1000000000):
    echo "<h1 style='color:red; font-size:42px;'>You have put too many zero's in</h1>";
    break;
    case ($saleschart->revenue >= 25000000):
    echo "<div class='background48'>
                       <div style='float:left; padding-top:25px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 24200000):
    echo "<div class='background47'>
                       <div style='float:left; padding-top:45px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 24000000):
    echo "<div class='background46'>
                       <div style='float:left; padding-top:65px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 23200000):
    echo "<div class='background45'>
                       <div style='float:left; padding-top:80px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 23000000):
    echo "<div class='background44'>
                       <div style='float:left; padding-top:100px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 22000000):
    echo "<div class='background42'>
                       <div style='float:left; padding-top:135px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 21500000):
    echo "<div class='background41'>
                       <div style='float:left; padding-top:155px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 21000000):
    echo "<div class='background40'>
                       <div style='float:left; padding-top:170px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 20500000):
    echo "<div class='background39'>
                       <div style='float:left; padding-top:190px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 20000000):
    echo "<div class='background38'>
                       <div style='float:left; padding-top:205px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 19200000):
    echo "<div class='background37'>
                       <div style='float:left; padding-top:225px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 19000000):
    echo "<div class='background36'>
                       <div style='float:left; padding-top:245px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 18200000):
    echo "<div class='background35'>
                       <div style='float:left; padding-top:265px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 18000000):
    echo "<div class='background34'>
                       <div style='float:left; padding-top:280px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 17200000):
    echo "<div class='background33'>
                       <div style='float:left; padding-top:300px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 17000000):
    echo "<div class='background32'>
                       <div style='float:left; padding-top:320px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 16200000):
    echo "<div class='background31'>
                       <div style='float:left; padding-top:340px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000'. "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 16000000):
    echo "<div class='background30'>
                       <div style='float:left; padding-top:360px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 15200000):
    echo "<div class='background29'>
                       <div style='float:left; padding-top:375px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 15000000):
    echo "<div class='background28'>
                       <div style='float:left; padding-top:395px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case($saleschart->revenue >= 14200000):
    echo "<div class='background27'>
                       <div style='float:left; padding-top:410px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 14000000):
    echo "<div class='background26'>
                       <div style='float:left; padding-top:430px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 132000000):
    echo "<div class='background25'>
                       <div style='float:left; padding-top:450px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 13000000):
    echo "<div class='background24'>
                       <div style='float:left; padding-top:468px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 12200000):
    echo "<div class='background23'>
                       <div style='float:left; padding-top:485px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 12000000):
    echo "<div class='background22'>
                       <div style='float:left; padding-top:505px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 11200000):
    echo "<div class='background21'>
                       <div style='float:left; padding-top:520px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 11000000):
    echo "<div class='background20'>
                       <div style='float:left; padding-top:540px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 10200000):
    echo "<div class='background19'>
                       <div style='float:left; padding-top:560px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 10000000):
    echo "<div class='background18'>
                       <div style='float:left; padding-top:580px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '00,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 9200000):
    echo "<div class='background17'>
                       <div style='float:left; padding-top:595px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 9000000):
    echo "<div class='background16'>
                       <div style='float:left; padding-top:615px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 8200000):
    echo "<div class='background15'>
                       <div style='float:left; padding-top:630px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 8000000):
    echo "<div class='background14'>
                       <div style='float:left; padding-top:650px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 7200000):
    echo "<div class='background13'>
                       <div style='float:left; padding-top:670px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 7000000):
    echo "<div class='background12'>
                       <div style='float:left; padding-top:690px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 6200000):
    echo "<div class='background11'>
                       <div style='float:left; padding-top:708px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 6000000):
    echo "<div class='background10'>
                       <div style='float:left; padding-top:725px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 5200000);
    echo "<div class='background9'>
                       <div style='float:left; padding-top:745px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 5000000):
    echo "<div class='background8'>
                       <div style='float:left; padding-top:760px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 4200000):
    echo "<div class='background7'>
                       <div style='float:left; padding-top:780px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 4000000):
    echo "<div class='background6'>
                       <div style='float:left; padding-top:800px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 3200000):
    echo "<div class='background5'>
                       <div style='float:left; padding-top:820px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 3000000):
    echo "<div class='background4'>
                       <div style='float:left; padding-top:840px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'> $" . $saleschart->revenue, '0,000,000' . "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 2200000):
    echo "<div class='background3'>
                       <div style='float:left; padding-top:855px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'> $" . $saleschart->revenue, '0,000,000' .  "</span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 2000000):
    echo "<div class='background2'>
                       <div style='float:left; padding-top:875px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'> $" .  $saleschart->revenue, '0,000,000' . " </span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 1200000):
    echo "<div class='background1'>
                       <div style='float:left; padding-top:895px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" .  $saleschart->revenue, '0,000,000' . " </span></div>
                        </div>";
    break;
    case ($saleschart->revenue >= 1000000):
    echo "<div class='background1'>
                       <div style='float:left; padding-top:895px;padding-left:10px;'><span style='font-weight:bold; color:#d34343'>$" .  $saleschart->revenue, '0,000,000' . " </span></div>
                        </div>";
    break;
    default: echo HTML::image('images/background.jpg');
    }
    //end chart
    ?>
    <br />
    <p style="text-align: center;">As Of <?php echo $saleschart->month; ?></p>
</div>



