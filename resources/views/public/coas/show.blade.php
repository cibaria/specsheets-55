@extends('layouts.publiccoas')

@section('content')


{{-- top --}}
<div style="text-align:center;">
        <img src="{{ url('/') . "/images/cibaria-logo.jpg" }}" alt="" width="178" height="157"/><br />
        <span style="font-size:14px;"><strong>TEL: (951) 823 - 8490 - FAX: (951) 823 - 8495</strong></span><br />
        <span style="font-size:14px;"><strong>705 Columbia Ave Riverside, CA 92507</strong></span><br/>
        <br/>
        <br/>
        <h2 style="font-size:26px;"><u>CERTIFICATE OF ANALYSIS</u></h2>
        <h2 style="font-size:26px;">{{ strtoupper($coa->description) }}</h2>
    </div>
{{-- end top --}}
<br />
<br />

<div class="center">
<table width="100%">
    @if(!empty($coa->coa_template))
        <tr>
            <td>Description:</td>
            <td width="50px"></td>
            <?php
            $template = str_replace("_", " ", $coa->coa_template);
            ?>
            <td>{{ ucwords($template) }}, {{ ucfirst($coa->coa->coa_description) }}</td>
        <tr>
            @endif

		@if(!empty($coa->lot))
		<tr>
		<td>Lot Number:</td>
		<td width="50px"></td>
		<td>{{ $coa->lot }}</td>
		<tr>
		@endif

		@if(!empty($coa->origin))
		<tr>
		<td>Origin:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->origin) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->manf_date))
		<td>Manf Date:</td>
		<td width="50px"></td>
		<td>{{ $coa->coa->manf_date }}</td>
		</tr>
		@endif

		@if(!empty($coa->expiration_date))
		<td>Exp Date:</td>
		<td width="50px"></td>
		<td>{{ $coa->expiration_date }}</td>
		</tr>
		@endif

<tr>
	<td height="50px"></td>
</tr>
{{-- results --}}


		@if(!empty($coa->coa->absorbtion_in_ultra_violet))
		<td>Absorbtion in Ultra Violet:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->absorbtion_in_ultra_violet) }}</td>
		</tr>
		@endif

            @if(!empty($coa->coa->flavor_and_odor))
                <td>Flavor and Odor:</td>
                <td width="50px"></td>
                <td>{{ ucfirst($coa->coa->flavor_and_odor) }}</td>
                </tr>
            @endif

		@if(!empty($coa->coa->k268))
		<td>k268:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->k268) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->deltak))
		<td>Delta K:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->deltak) }}</td>
		</tr>
		@endif
		@if(!empty($coa->coa->nm232))
		<td>232nm:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->nm232) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->acid_value))
		<td>Acid Value:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->acid_value) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->acidity))
		<td>Acidity:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->acidity) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->additives))
		<td>Additives:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->additives) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->alcohol))
		<td>Alcohol:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->alcohol) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->aom))
		<td>AOM:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->aom) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->appearance))
		<td>Appearance @ 25 degree C:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->appearance) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->arachidic_acid))
		<td>Arachidic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->arachidic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->aroma))
		<td>Aroma:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->aroma) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->ash))
		<td>Ash:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->ash) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->behenic_acid))
		<td>Behenic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->behenic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->beta_sitosterol_delta5_avenasterol))
		<td>Beta Sitosterol Delta5 Avenasterol:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->beta_sitosterol_delta5_avenasterol) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->brassicasterol))
		<td>Brassicasterol:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->brassicasterol) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->brix))
		<td>Brix:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->brix) }}</td>
		</tr>
		@endif

		{{-- fatty acids --}}

		@if(!empty($coa->coa->C3_0))
		<td>Propionic acid C3:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C3_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C4_0))
		<td>Butyric acid C4:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C4_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C5_0))
		<td>Valeric acid C5:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C5_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C6_0))
		<td>Caproic acid C6:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C6_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C7_0))
		<td>Enanthic acid C7:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C7_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C8_0))
		<td>Caprylic acid C8:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C8_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C9_0))
		<td>Pelargonic acid C9:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C9_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C10_0))
		<td>Capric acid C10:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C10_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C11_0))
		<td>Undecylic acid C11:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C11_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C12_0))
		<td>Lauric acid C12:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C12_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C13_0))
		<td>Tridecylic acid C13:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C13_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C14_0))
		<td>Myristic acid C14:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C14_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C15_0))
		<td>Pentadecylic acid C15:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C15_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C16_0))
		<td>Palmitic acid C16:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C16_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C16_1))
		<td>Palmitoliec acid C16:1:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C16_1) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C17_0))
		<td>Margaric acid C17:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C17_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C17_1))
		<td>Heptadecenoic acid C17:1:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C17_1) }}</td>
		</tr>
		@endif


		@if(!empty($coa->coa->C18_0))
		<td>Stearic acid C18:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C18_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C18_1))
		<td>Oleic acid C18:1:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C18_1) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->trans_c_18_1))
		<td>Alpha Linolenic C18:3:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->trans_c_18_1) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->c18_1t))
		<td>C18:1T:</td>
		<td width="50px"></td>
		<td><{{ ucfirst($coa->coa->c18_1t) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C18_2))
		<td>Linoleic acid C18:2:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C18_2) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C18_3))
		<td>Alpha Linolenic C18:3:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C18_3) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->c18_2t_c18_3t))
		<td>C18:2T + C18:3T:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->c18_2t_c18_3t) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C19_0))
		<td>Nonadecylic acid C19:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C19_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C20_0))
		<td>Arachidic acid C20:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C20_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C20_1))
		<td>Eicosenic Acid C20:1:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C20_1) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C21_0))
		<td>Heneicosylic acid C21:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C21_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C22_0))
		<td>Behenic acid C22:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C22_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C23_0))
		<td>Tricosylic acid C23:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C23_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C24_0))
		<td>Lignoceric acid C24:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C24_0) }}</td>
		</tr>
		@endif

            @if(!empty($coa->coa->C24_1))
                <td>Trans Linoleic + Linolenic C24:1</td>
                <td width="50px"></td>
                <td>{{ ucfirst($coa->coa->C24_1) }}</td>
                </tr>
            @endif

		@if(!empty($coa->coa->C25_0))
		<td>Pentacosylic acid C25:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C25_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C26_0))
		<td>Cerotic acid C26:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C26_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C27_0))
		<td>Heptacosylic acid C27:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C27_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C28_0))
		<td>Montanic acid C28:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C28_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C29_0))
		<td>Nonacosylic acid C29:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C29_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C30_0))
		<td>Melissic acid C30:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C30_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C31_0))
		<td>Henatriacontylic acid C31:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C31_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C32_0))
		<td>Lacceroic acid C32:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C32_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C33_0))
		<td>Psyllic acid C33:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C33_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C34_0))
		<td>Geddic acid C34:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C34_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C35_0))
		<td>Ceroplastic acid C35:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C35_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C36_0))
		<td>Hexatriacontylic acid C36:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C36_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C37_0))
		<td>Heptatriacontanoic acid C37:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C37_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->C38_0))
		<td>Octatriacontanoic acid C38:0:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->C38_0) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->campestanol_campesterol))
		<td>Campestanol+Campesterol:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->campestanol_campesterol) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->capric_acid))
		<td>Capric Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->capric_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->capronic_acid))
		<td>Capronic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->capronic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->caprylic_acid))
		<td>Caprylic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->caprylic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->cholesterol))
		<td>Cholesterol:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->cholesterol) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->cold_test))
		<td>Cold Test:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->cold_test) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->color))
		<td>Color:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->color) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->color_gardner))
		<td>Color Gardner:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->color_gardner) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->color_lovibond))
		<td>Color Lovibond:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->color_lovibond) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->color_red))
		<td>Color Lovibond Red:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->color_red) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->blue))
		<td>Color Blue:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->blue) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->neutral))
		<td>Color Neutral:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->neutral) }}</td>
		</tr>
		@endif


		@if(!empty($coa->coa->color_visual))
		<td>Color Visual:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->color_visual) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->color_yellow))
		<td>Color Lovibond Yellow:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->color_yellow) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->neutral))
		<td>Color Neutral:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->neutral) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->compesterol))
		<td>Compesterol:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->compesterol) }}</td>
		</tr>
		@endif
		@if(!empty($coa->coa->copper))
		<td>Copper:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->copper) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->delta7_stigmasternol))
		<td>Delta7 Stigmasternol:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->delta7_stigmasternol) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->density_20))
		<td>Density 20:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->density_20) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->developed_alcohol_degree))
		<td>Developed Alcohol Degree:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->developed_alcohol_degree) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->dextros_equivalent))
		<td>Dextros Equivalent:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->dextros_equivalent) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->eicosanedienoic_acid))
		<td>Eicosanedienoic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->eicosanedienoic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->eicosaneteetraenoic_acid))
		<td>Eicosaneteetraenoic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->eicosaneteetraenoic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->erucic_acid))
		<td>Erucic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->erucic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->fatty_acid))
		<td>Fatty Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->fatty_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->ffa))
		<td>FFA (% Oleic Acid):</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->ffa) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->flavor))
		<td>Flavor:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->flavor) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->free_acidity))
		<td>Free Acidity:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->free_acidity) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->free_fatty_acids))
		<td>Free Fatty Acids:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->free_fatty_acids) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->free_so2))
		<td>Free SO2:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->free_so2) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->free_sulpphurous_anhydride))
		<td>Free Sulpphurous Anhydride:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->free_sulpphurous_anhydride) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->fructose))
		<td>Fructose:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->fructose) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->gadoleic_acid))
		<td>Gadoleic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->gadoleic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->glucose))
		<td>Glucose:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->glucose) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->grains))
		<td>Grains:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->grains) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->heavy_metals_as_lead))
		<td>Heavy Metals as Lead:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->heavy_metals_as_lead) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->heptadacanoic_acid))
		<td>Heptadacanoic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->heptadacanoic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->heptadecenoic_acid))
		<td>Heptadecenoic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->heptadecenoic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->hydroxyl_value))
		<td>Hydroxyl Value:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->hydroxyl_value) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->identification_a))
		<td>Identification A:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->identification_a) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->identification_b))
		<td>Identification B:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->identification_b) }}</td>
		</tr>
		@endif


		@if(!empty($coa->coa->identification_c))
		<td>Identification C:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->identification_c) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->insoluble_impurities))
		<td>Insoluble Impurities:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->insoluble_impurities) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->iodine_value))
		<td>Iodine Value:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->iodine_value) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->iodine_value_ri))
		<td>Iodine Value RI:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->iodine_value_ri) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->iron))
		<td>Iron:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->iron) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->iv_wijs))
		<td>iv_wijs:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->iv_wijs) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->lauric_acid))
		<td>Lauric Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->lauric_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->lead))
		<td>Lead:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->lead) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->lignoceric_acid))
		<td>Lignoceric Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->lignoceric_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->linoleic_acid))
		<td>Linoleic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->linoleic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->linolenic_acid))
		<td>Linolenic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->linolenic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->m_i))
		<td>m_i:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->m_i) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->maltose))
		<td>Maltose:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->maltose) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->melting_point))
		<td>Melting Point Deg C:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->melting_point) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->minor))
		<td>Minor:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->minor) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->moisture))
		<td>Moisture:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->moisture) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->moisture_and_impurities))
		<td>Moisture and Impurities %:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->moisture_and_impurities) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->moisture_and_sediment_content))
		<td>Moisture and Sediment Content:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->moisture_and_sediment_content) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->moisture_level))
		<td>Moisture level:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->moisture_level) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->myistoleic_acid))
		<td>Myistoleic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->myistoleic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->myristic))
		<td>Myristic:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->myristic) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->nervonic_acid))
		<td>Nervonic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->nervonic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->octadecatetraen_acid))
		<td>Octadecatetraen Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->octadecatetraen_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->odor))
		<td>Odor:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->odor) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->oleic_acid))
		<td>Oleic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->oleic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->optical_rotation))
		<td>Optical Rotation:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->optical_rotation) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->osi))
		<td>OSI:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->osi) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->other_carbohydrates))
		<td>Other Carbohydrates:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->other_carbohydrates) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->palmitic_acid))
		<td>Palmitic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->palmitic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->palmitoliec_acid))
		<td>Palmitoliec Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->palmitoliec_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->peroxide_value))
		<td>Peroxide Value meq/kg:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->peroxide_value) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->ph))
		<td>Ph:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->ph) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->pounds_per_gallon))
		<td>Pounds Per Gallon:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->pounds_per_gallon) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->pv))
		<td>PV:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->pv) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->refactive_index))
		<td>Refactive Index:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->refactive_index) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->refractive_index))
		<td>Refractive Index:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->refractive_index) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->rifractometric_at_20))
		<td>rifractometric_at_20:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->rifractometric_at_20) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->salt))
		<td>Salt:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->salt) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->saponification))
		<td>Saponification:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->saponification) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->saponification_value))
		<td>Saponification Value:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->saponification_value) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->saturated_fatty_acids))
		<td>Saturated Fatty Acids:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->saturated_fatty_acids) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->smoke_point))
		<td>Smoke Point:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->smoke_point) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->so2))
		<td>SO2:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->so2) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->specific_gravity))
		<td>Specific Gravity:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->specific_gravity) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->specific_weight))
		<td>Specific Weight:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->specific_weight) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->specific_weight_as_baumic_degrees))
		<td>Specific Weight as Baumic Degrees:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->specific_weight_as_baumic_degrees) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->starch))
		<td>Starch:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->starch) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->stearic_acid))
		<td>Stearic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->stearic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->stigmasterol))
		<td>Stigmasterol:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->stigmasterol) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->taste))
		<td>Taste:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->taste) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->tbhq))
		<td>tbhq:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->tbhq) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->total_acidity_as_acetic_acid))
		<td>Total Acidity as Acetic Acid:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->total_acidity_as_acetic_acid) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->total_dry_extract))
		<td>Total Dry Extract:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->total_dry_extract) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->total_monounsaturated))
		<td>Total Monounsaturated:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->total_monounsaturated) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->total_polyunsaturated))
		<td>Total Polyunsaturated:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->total_polyunsaturated) }}</td>
		</tr>
		@endif
		@if(!empty($coa->coa->total_saturated))
		<td>Total Saturated:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->total_saturated) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->total_so2))
		<td>Total SO2:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->total_so2) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->total_sterols))
		<td>Total Sterols:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->total_sterols) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->total_sulphurous_anhydride))
		<td>Total Sulphurous Anhydride:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->total_sulphurous_anhydride) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->trans_c_18_1))
		<td>trans_c_18_1:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->trans_c_18_1) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->trans_c_18_2))
		<td>trans_c_18_2:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->trans_c_18_2) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->trans_fatty_acids))
		<td>trans_fatty_acids:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->trans_fatty_acids) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->turbidity))
		<td>turbidity:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->turbidity) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->unsaponifiable_matter))
		<td>Unsaponifiable Matter:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->unsaponifiable_matter) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->vitamin_e))
		<td>Vitamin E:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->vitamin_e) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->zinc))
		<td>Zinc:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->zinc) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->k268))
		<td>k268:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->k268) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->k270))
		<td>K270:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->k270) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->im))
		<td>IM:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->im) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->pph))
		<td>PPH:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->pph) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->moi))
		<td>MOI:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->moi) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->ind))
		<td>IND:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->ind) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->bit225))
		<td>BIT(225):</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->bit225) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->ppp))
		<td>PPP:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->ppp) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->dag))
		<td>DAG:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->dag) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->defects))
		<td>Defects:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->defects) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->fruitness))
		<td>Fruitness:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->fruitness) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->bitterness))
		<td>Bitterness:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->bitterness) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->pungency))
		<td>Pungency:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->pungency) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->campesterol))
		<td>Campesterol:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->campesterol) }}</td>
		</tr>
		@endif

		@if(!empty($coa->coa->difference_ecn42))
		<td>Triglecerides Difference ECN42:</td>
		<td width="50px"></td>
		<td>{{ ucfirst($coa->coa->difference_ecn42) }}</td>
		</tr>
		@endif



</table>

<hr>
		<p style="font-weight:bold;"><i>This is an electronically generated document and is valid without signature.</i></p>
</div>

  


@stop
