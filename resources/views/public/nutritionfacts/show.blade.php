@extends('layouts.publicnutritionfacts')

@section('content')

    <div class="container">

    </div>



    <!-- turn canvas into .png -->
    <script type="text/javascript">
        function capture() {
            $('#target').html2canvas({
                onrendered: function (canvas) {
                    //Set hidden field's value to image data (base-64 string)
                    $('#img_val').val(canvas.toDataURL("image/png"));
                    //Submit the form manually
                    document.getElementById("myForm").submit();
                }
            });
        }
    </script>

    <div id="target" style="width:250px;padding-left:50px">
    <div class="conatainer">
        <h2 style="width:250px; text-align:center;"><?php echo $nutritionfact->title; ?></h2>
        <table width="250px" border="1px solid black" cellpadding="5" cellspacing="0">
            <tr>
                <td><h1 style="text-align:center;">Nutrition Facts</h1>
                <hr>
                   
                    <span style="font-size:10pt;"><?php echo $nutritionfact->servings_per_container; ?> Servings Per Container </span><br />
                    <div style="padding-bottom:5px;">
                    <span style="font-size:10pt;font-weight:bold;float:left;">Serving Size </span>
                    <span style="font-size:10pt;font-weight:bold;float:right;"><?php echo $nutritionfact->serving_size; ?></span>
                    </div>
                    <div style="height:13px; background-color:black; border-1px solid black;margin-top:15px;"></div>
                    <span style="font-size: 10pt;font-weight:bold;">Amounts Per Serving</span><br />
                    <div>
                    <span style="font-size: 16pt; float:left;margin-bottom:2px; margin-top:5px; font-weight:bold;">Calories &nbsp;</span>
                    <span style="font-size: 22pt; float:right; font-weight:bold; margin-bottom: 5px;"><?php echo $nutritionfact->calories; ?></span><br />
</div>
<div style="height:5px;"></div>

                    <div id="nutritionoines" style="height:3px; background-color:black; border-1px solid black;margin-top:10px;margin-bottom:5px;"></div>
                    <span style="float:right; font-weight:bold; font-size:8pt;margin-left:100px;">% Daily Value*</span>

                    <div style="height:3px; background-color:black; border-2px solid black; margin-bottom:3px;margin-top:22px;"></div>
                    <span style="text-align:left;font-weight:bold;font-size: 8pt;">Total Fat <?php echo $nutritionfact->total_fat_grams; ?>g</span><span style="float:right;font-size: 8pt; font-weight:bold;"><?php echo $nutritionfact->total_fat_percent; ?>%</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px;margin-bottom:3px;"></div>
                    <span style="padding-left:25px; text-align:left;font-size: 8pt; font-weight:bold;">Saturated Fat <?php echo $nutritionfact->saturated_fat_grams; ?>g</span><span style="float:right;font-size: 8pt; font-weight:bold;"><?php echo $nutritionfact->saturated_fat_percentage; ?>%</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-bottom:3px;margin-top:3px;"></div>
                    <span style="padding-left:25px; text-align:left;font-size: 8pt; font-weight:bold;">Trans Fat <?php echo $nutritionfact->trans_fat; ?>g</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px;margin-bottom:3px;"></div>
                    <span style="text-align:left;font-size: 8pt; font-weight:bold;">Cholesterol <?php echo $nutritionfact->cholesterol_grams; ?>mg</span><span style="float:right;font-size: 8pt; font-weight:bold;"><?php echo $nutritionfact->cholesterol_percentage; ?>%</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px; margin-bottom:3px;"></div>
                    <span style="text-align:left;font-size: 8pt; font-weight:bold;">Sodium <?php echo $nutritionfact->sodium_grams; ?>mg</span><span style="float:right;font-size: 8pt; font-weight:bold;"><?php echo $nutritionfact->sodium_percentage; ?>%</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px; margin-bottom:3px;"></div>
                    <span style="text-align:left;font-size: 8pt; font-weight:bold;">Total Carbohydrates <?php echo $nutritionfact->total_carbohydrates_grams; ?>g</span><span style="float:right;font-size: 8pt; font-weight:bold;"><?php echo $nutritionfact->total_carbohydrates_percentage; ?>%</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px; margin-bottom:3px;"></div>
                    <span style="padding-left:25px;text-align:left;font-size: 8pt; font-weight:bold;">Dietary Fiber <?php echo $nutritionfact->dietary_fiber_grams; ?>g</span><span style="float:right;font-size: 8pt; font-weight:bold;"><?php echo $nutritionfact->dietary_fiber_percentage; ?>%</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px; margin-bottom:3px;"></div>
                    <span style="padding-left:25px;text-align:left;font-size: 8pt; font-weight:bold;">Sugars <?php echo $nutritionfact->sugars_grams; ?>g</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px; margin-bottom:3px;"></div>
                    <span style="padding-left:25px;text-align:left;font-size: 8pt; font-weight:bold;">Added Sugars <?php echo $nutritionfact->added_sugars; ?>g</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px; margin-bottom:3px;"></div>

                    <span style="text-align:left;font-size: 8pt; font-weight:bold;">Protein <?php echo $nutritionfact->protein_grams; ?>g</span>

                    <div style="height:13px; background-color:black; border-1px solid black;"></div>

                    <span style="text-align:left;font-size: 8pt; font-weight:bold;">Vitamin D <?php echo $nutritionfact->vitamin_d_grams; ?>mcg</span><span style="float:right;font-size: 8pt; font-weight:bold;"><?php echo $nutritionfact->vitamin_d_percentage; ?>%</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px; margin-bottom:3px;"></div>
                    <span style="text-align:left;font-size: 8pt; font-weight:bold;">Calcium <?php echo $nutritionfact->calcium_grams; ?>mg</span><span style="float:right;font-size: 8pt; font-weight:bold;"><?php echo $nutritionfact->calcium_percentage; ?>%</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px; margin-bottom:3px;"></div>
                    <span style="text-align:left;font-size: 8pt; font-weight:bold;">Iron <?php echo $nutritionfact->iron_grams; ?>mg</span><span style="float:right;font-size: 8pt; font-weight:bold;"><?php echo $nutritionfact->iron_percentage; ?>%</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px; margin-bottom:3px;"></div>
                    <span style="text-align:left;font-size: 8pt; font-weight:bold;">Potassium <?php echo $nutritionfact->potassium_grams; ?>mg</span><span style="float:right;font-size: 8pt; font-weight:bold;"><?php echo $nutritionfact->potassium_percentage; ?>%</span>

                    <div style="height:3px; background-color:black; border-1px solid black;margin-top:3px; margin-bottom:3px;"></div>

                    <span style="font-weight:bold; font-size:6pt;">*The % Daily Value tells you how much a nutrient in a serving of food contributes to a daily diet. 2,000 calories a day is used for general nutrition advice.</span>
                </td>
            </tr>

        </table>
        <br />
        <h3 style="width:250px;">Ingredients: <?php echo $nutritionfact->ingredients; ?></h3>

    </div>

    <div class="noshow">
        <br />
        <hr />

            {{--<input type="submit" value="Print Nutrition Label" onclick="window.print();" class="btn btn-success"/>--}}
            {{--<input type="submit" value="Download" onclick="capture();" />--}}
            <form method="POST" enctype="multipart/form-data" action="/public/nutritionfacts/download" id="myForm">
                {{ csrf_field() }}
                <input type="hidden" name="img_val" id="img_val" value="" />
            </form>

        </div>
    </div>
    @stop