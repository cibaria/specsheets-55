@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span>A PO ETA
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/poetas/">PO ETA</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'poetas.store', 'class' => 'form-horizontal']) }}

                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('vendor_name', 'Vendor Name') }}
                                {{ Form::text('vendor_name', null,['class' => 'form-control',  'placeholder' => 'Vendor Name']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('order_date', 'Order Date') }}
                                {{ Form::text('order_date', null,['class' => 'form-control', 'id' => 'datepicker1', 'placeholder' => 'Order Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('item_description', 'Item Description') }}
                                {{ Form::text('item_description', null,['class' => 'form-control', 'placeholder' => 'Item Description']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('po', 'PO#') }}
                                {{ Form::text('po', null,['class' => 'form-control', 'placeholder' => 'PO#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('delivery_request_date', 'Delivery Request Date') }}
                                {{ Form::text('delivery_request_date', null,['class' => 'form-control', 'id' => 'datepicker', 'placeholder' => 'Delivery Request Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('quantity_ordered', 'Quantity Ordered') }}
                                {{ Form::text('quantity_ordered', null,['class' => 'form-control', 'placeholder' => 'Quantity Ordered']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('eta', 'ETA') }}
                                {{ Form::text('eta', null,['class' => 'form-control', 'placeholder' => 'ETA']) }}
                            </div>
                            @if(Auth::user()->username == 'purchasing')
                            <div class="form-group">
                                {{ Form::label('price', 'Price') }}
                                {{ Form::text('price', null,['class' => 'form-control', 'placeholder' => 'Price']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('amount', 'Amount') }}
                                {{ Form::text('amount', null,['class' => 'form-control', 'placeholder' => 'Amount']) }}
                            </div>
                            @endif
                            <div class="form-group">
                                {{ Form::label('notes', 'Notes') }}
                                {{ Form::textarea('notes', null,['class' => 'form-control', 'placeholder' => 'Notes', 'rows' => '8', 'cols' => '8']) }}
                            </div>

                            {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop

@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
        });
    </script>

    @stop