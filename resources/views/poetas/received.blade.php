@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    PO ETA
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > PO ETA
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    @if(Auth::user()->group != 20)
                    {{ link_to_route('poetas.index', '<< Back To PO ETA',null, array('class' => 'btn btn-warning navbar-btn')) }}
                    @endif
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Vendor Name</strong></td>
                <td><strong>Order Date</strong></td>
                <td><strong>Item Description</strong></td>
                <td><strong>PO#</strong></td>
                <td><strong>Delivery Request Date</strong></td>
                <td><strong>Quantity Ordered</strong></td>
                <td><strong>ETA</strong></td>
                @if(Auth::user()->username == 'purchasing')
                <td><strong>Price</strong></td>
                <td><strong>Amount</strong></td>
                @endif
                <td><strong>Notes</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($poetas as $poeta)

            <tr>
            <td>{{ $poeta->vendor_name }}</td>
            <td>{{ $poeta->order_date }}</td>
            <td>{{ $poeta->item_description }}</td>
            <td>{{ $poeta->po }}</td>
            <td>{{ $poeta->delivery_request_date }}</td>
            <td>{{ $poeta->quantity_ordered }}</td>
            <td>{{ $poeta->eta }}</td>
                @if(Auth::user()->username == 'purchasing')
            <td>{{ $poeta->price }}</td>
            <td>{{ $poeta->amount }}</td>
                @endif
            <td>{{ $poeta->notes }}</td>
                    <td class="no-sort">
                        <div class="btn-group" role="group" aria-label="">
                            @if(Auth::user()->username == 'jkoser')
                            {{ link_to_route('poetas.edit', 'Edit', [$poeta->id],['class' => 'btn btn-warning']) }}
                            {{ Form::open(array('method' => 'DELETE','style' => 'display:inline', 'route' => array('poetas.destroy', $poeta->id))) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            @endif
        
                            {{ Form::close() }}
                        </div>


                </tr>
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
    @stop


{{--footer--}}
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop