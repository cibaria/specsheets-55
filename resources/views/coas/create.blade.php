@extends('layouts.admin')

@section('content')
    <div id="coas">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span>A COA
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/coas/">COA</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>
        {{-- navbar --}}
        {{--<nav class="navbar navbar-default">--}}

        {{--</nav>--}}
        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($coa, array('method' => 'PATCH', 'route' => array('coas.store', $coa->id))) }}
{{--                        {{ Form::open(['route' => 'coas.store', 'class' => 'form-horizontal']) }}--}}
                        <ul class="list-group">
                            <li class="list-group-item">
                                <?php
                                $template = str_replace("_", " ", $coa->coa_template);
                                ?>
                                <strong>Master Template: </strong> {{ ucwords($template) }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lot: </strong> {{ $coa->lot }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lot Log Description: </strong> {{ $coa->description }}
                            </li>
                            <li class="list-group-item">
                                <strong>Expiration Date: </strong> {{ $coa->expiration_date}}
                            </li>
                            @if(!is_null($coa->coa))
                                <li class="list-group-item">
                                    <strong>Manf Date: </strong> {{ $coa->coa->manf_date}}
                                </li>
                                @else
                                <li class="list-group-item">
                                    <strong>Manf Date: </strong>
                                </li>
                                @endif
                            <li class="list-group-item">
                                <strong>Origin: </strong> {{ $coa->origin}}
                            </li>
                            </ul>
                            {{ Form::close() }}





                            <form method="post" action="{{ url('/') }}/coas/create_coa/{{ $coa->id }}" accept-charset="UTF-8">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <h2>COA Information</h2>
                                @switch($coa->coa_template)
                                @case('apricot_kernel')
                                @include('coas.form.coa_names.apricot_kernel')
                                @break

                                @case('almond')
                                @include('coas.form.coa_names.almond')
                                @break

                                @case('avocado')
                                @include('coas.form.coa_names.avocado')
                                @break

                                @case('babassu')
                                @include('coas.form.coa_names.babassu')
                                @break

                                @case('butters')
                                @include('coas.form.coa_names.butters')
                                @break

                                @case('canola')
                                @include('coas.form.coa_names.canola')
                                @break

                                @case('castor')
                                @include('coas.form.coa_names.castor')
                                @break

                                @case('coconut')
                                @include('coas.form.coa_names.coconut')
                                @break

                                @case('corn')
                                @include('coas.form.coa_names.corn')
                                @break

                                @case('cottonseed')
                                @include('coas.form.coa_names.cottonseed')
                                @break

                                @case('flaxseed')
                                @include('coas.form.coa_names.flaxseed')
                                @break

                                @case('hemp')
                                @include('coas.form.coa_names.hemp')
                                @break

                                @case('jojoba')
                                @include('coas.form.coa_names.jojoba')
                                @break

                                @case('macadamia')
                                @include('coas.form.coa_names.macadamia')
                                @break

                                @case('olive_oil')
                                @include('coas.form.coa_names.olive_oil')
                                @break

                                @case('olive_oil_extra_virgin_flavored')
                                @include('coas.form.coa_names.olive_oil_extra_virgin_flavored')
                                @break

                                @case('olive_oil_fused')
                                @include('coas.form.coa_names.olive_oil_fused')
                                @break

                                @case('olive_oil_pure_infused')
                                @include('coas.form.coa_names.olive_oil_pure_infused')
                                @break

                                @case('palm_kernel')
                                @include('coas.form.coa_names.palm_kernel')
                                @break

                                @case('peanut')
                                @include('coas.form.coa_names.peanut')
                                @break

                                @case('rice_bran')
                                @include('coas.form.coa_names.rice_bran')
                                @break

                                @case('safflower')
                                @include('coas.form.coa_names.safflower')
                                @break

                                @case('sesame')
                                @include('coas.form.coa_names.sesame')
                                @break

                                @case('soybean')
                                @include('coas.form.coa_names.soybean')
                                @break

                                @case('sunflower')
                                @include('coas.form.coa_names.sunflower')
                                @break
                                @endswitch
                            <div class="form-group">
                                {{ Form::label('coa_description', 'COA Description') }}
                                {{ Form::text('coa_description', null,['class' => 'form-control', 'placeholder' => 'COA Description', 'id' => 'coa_description']) }}
                            </div>

                            @switch($coa->coa_template)
                                @case('apricot_kernel')
                                @include('coas.form.template.apricot_kernel')
                                @break

                                @case('almond')
                                @include('coas.form.template.almond')
                                @break

                                @case('avocado')
                                @include('coas.form.template.avocado')
                                @break

                                @case('babassu')
                                @include('coas.form.template.babassu')
                                @break

                                @case('butters')
                                @include('coas.form.template.butters')
                                @break

                                @case('canola')
                                @include('coas.form.template.canola')
                                @break

                                @case('castor')
                                @include('coas.form.template.castor')
                                @break

                                @case('coconut')
                                @include('coas.form.template.coconut')
                                @break

                                @case('corn')
                                @include('coas.form.template.corn')
                                @break

                                @case('cottonseed')
                                @include('coas.form.template.cottonseed')
                                @break

                                @case('flaxseed')
                                @include('coas.form.template.flaxseed')
                                @break

                                @case('hemp')
                                @include('coas.form.template.hemp')
                                @break

                                @case('jojoba')
                                @include('coas.form.template.jojoba')
                                @break

                                @case('macadamia')
                                @include('coas.form.template.macadamia')
                                @break

                                @case('olive_oil')
                                @include('coas.form.template.olive_oil')
                                @break

                                @case('olive_oil_extra_virgin_flavored')
                                @include('coas.form.template.olive_oil_extra_virgin_flavored')
                                @break

                                @case('olive_oil_fused')
                                @include('coas.form.template.olive_oil_fused')
                                @break

                                @case('olive_oil_pure_infused')
                                @include('coas.form.template.olive_oil_pure_infused')
                                @break

                                @case('palm_kernel')
                                @include('coas.form.template.palm_kernel')
                                @break

                                @case('peanut')
                                @include('coas.form.template.peanut')
                                @break

                                @case('rice_bran')
                                @include('coas.form.template.rice_bran')
                                @break

                                @case('safflower')
                                @include('coas.form.template.safflower')
                                @break

                                @case('sesame')
                                @include('coas.form.template.sesame')
                                @break

                                @case('soybean')
                                @include('coas.form.template.soybean')
                                @break

                                @case('sunflower')
                                @include('coas.form.template.sunflower')
                                @break
                            @endswitch



                        {{ Form::submit('Create', ['class' => 'btn btn-default', 'style' => 'float:right', 'autocomplete' => 'off']) }}
                            </form>

                    </div>
                    <!--end row-->
                </div>

                {{-- right --}}
                <!--end row-->
            </div>
        </div><!--end container fluid-->

    </div>
@stop

@section('footer')
    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            //take the value of the coa_name dropdown and put it into the coa_description input box while leaving the option to edit the input box / Per Susan
            //TODO:Refactor
            $('#coa_name').change(function () {
                $(this).find("option").each(function () {
                    var o = $('#coa_name option:selected').text();
                    //debug the dropdown value
//                    console.log(o);
                    $('#coa_description').val(o);
                    $('#coa_name').hide();
                });

//                // hide input fields when they are hidden
//                $("div").filter(":hidden").children("input[type='text']").attr("disabled", "disabled");
//                $("div").filter(":hidden").children("textarea[class='form-control']").attr("disabled", "disabled");

            });



        });


    </script>

@stop