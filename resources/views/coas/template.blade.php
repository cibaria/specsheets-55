@extends('layouts.admin')

@section('content')

    <div id="coas">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    COA <span class="alert alert-dismissible alert-info" style="padding-right:15px;">Master Templates</span>
                </h1>
                <p>COA Master Templates are created when the COA template checkbox is checked within the individual <a href="/lotlogs/">Lot Logs</a> edit screen.</p>
                <p>Active COA's is a duplicate of a COA Master Template. Active COA's is what you would send to a customer or edit.</p>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > COA
                    </li>
                </ol>
            </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <a href="/coas/" class="btn btn-warning navbar-btn">COA's</a>

                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Lot Number</strong></td>
                <td><strong>Lot log Description</strong></td>
                <td><strong>Manf Date</strong></td>
                <td><strong>Expiration Date</strong></td>
                <td><strong>Origin</strong></td>
                <td><strong>Master Template</strong></td>
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">

            @foreach($coas as $coa)
            @if(!is_null($coa->coa))
                @if($coa->coa->master_template == 'yes')

                <tr>
                    <td class="isdone"><a href="/lotlogs/{{ $coa->id }}" style="background-color: #800080;border-radius: 20px; padding:5px; color:white;font-weight:bold;text-decoration: none;">{{ $coa->lot }}</a></td>
                    <td>{{ $coa->description}}</td>
                    <td>
                        @if(!is_null($coa->coa))
                            {{ $coa->coa->manf_date }}
                        @endif
                    </td>
                    <td>{{ $coa->expiration_date }}</td>
                    <td>{{ $coa->origin }}</td>

                    <?php
                    $template = str_replace("_", " ", $coa->coa_template);
                        echo "<td>";
                        echo ucwords($template);
                        echo "</td>";

                    ?>

                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            <a href="/coas/create/{{ $coa->id }}" class="btn btn-primary">Create A New COA</a>
                        </div>
                    </td>
                </tr>
                @endif
                @endif
            @endforeach

            </tbody>
        </table>

        <ul class="pagination"></ul>
    </div>
@stop
@section('footer')
    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                    { targets: 'no-sort', orderable: false }
                ]
            });
        });
    </script>

@stop

