<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'rbd' => 'RBD',
       'ho_organic' => 'HO, Organic',
       'organic' => 'Organic',
       ], null, ['class' => 'form-control']) }}
</div>