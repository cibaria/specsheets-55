<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'toasted' => 'Toasted',
       'toasted_organic' => 'Toasted Organic',
       'clear' => 'Clear',
       ], null, ['class' => 'form-control']) }}
</div>