<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'rbd' => 'RBD',
       'rbd_with_tbhq' => 'RBD With TBHQ',
       ], null, ['class' => 'form-control']) }}
</div>
