<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'ho_expeller_pressed_non_gmo' => 'HO, Expeller Pressed, NON-GMO',
       'organic' => 'Organic',
       ], null, ['class' => 'form-control']) }}
</div>