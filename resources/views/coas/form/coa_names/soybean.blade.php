<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'rbd' => 'RBD',
       'rbd_organic' => 'RBD, Organic',
       'rbd_non_gmo' => 'RBD NON-GMO',
       'winterized' => 'Winterized',
       ], null, ['class' => 'form-control']) }}
</div>