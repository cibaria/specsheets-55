<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'rbd' => 'RBD',
       'extra_virgin' => 'Extra Virgin',
       'organic' => 'Organic',
       ], null, ['class' => 'form-control']) }}
</div>