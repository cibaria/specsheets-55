<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'mango' => 'Mango',
       'shea_refined' => 'Shea Refined',
       'shea_refined_organic' => 'Shea Refined, Organic',
       'cocoa_butter_deodorized' => 'Cocoa Butter, Deodorized',
       'cocoa_butter_natrual_prime' => 'Cocoa Butter Natural/Prime'
       ], null, ['class' => 'form-control']) }}
</div>
