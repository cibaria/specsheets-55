<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'golden_expeller_pressed_refined' => 'Golden, Expeller Pressed, Refined',
       'organic' => 'Organic',
       ], null, ['class' => 'form-control']) }}
</div>
