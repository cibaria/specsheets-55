<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'mo_rbd_non_gmo' => 'MO, RBD, NON-GMO',
       'ho_rbd_non_gmo' => 'HO, RBD, NON-GMO',
       'ho_rbd_non_gmo_organic' => 'HO, RBD, NON-GMO, Organic',
       ], null, ['class' => 'form-control']) }}
</div>