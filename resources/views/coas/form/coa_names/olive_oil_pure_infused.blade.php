<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'basil' => 'Basil',
       'black_pepper' => 'Black Pepper',
       'chipotle' => 'Chipotle',
       'garlic' => 'Garlic',
       'habanero' => 'Habanero',
       'jalapeno' => 'Jalapeno',
       'lemon_pepper' => 'Lemon Pepper',
       'oregano' => 'Oregano',
       'roasted_chili' => 'Roasted Chili',
       'rosemary' => 'Rosemary',
       'scallion' => 'Scallion',
       ], null, ['class' => 'form-control']) }}
</div>