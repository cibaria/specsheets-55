<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'expeller_pressed_non_gmo' => 'Expeller Pressed, Non-GMO',
       'rbd' => 'RBD',
       'expeller_pressed_rbd' => 'Expeller Pressed, RBD',
       ], null, ['class' => 'form-control']) }}
</div>