<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'extra_virgin' => 'Extra Virgin',
       'extra_virgin_organic' => 'Extra Virgin, Organic',
       'extra_virgin_specialty' => 'Extra Virgin, Specialty',
       'pomace' => 'Pomace',
       'pure' => 'Pure',
       'virgin' => 'Virgin',
       ], null, ['class' => 'form-control']) }}
</div>
