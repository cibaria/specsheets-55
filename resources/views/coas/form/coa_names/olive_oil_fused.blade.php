<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'lemon_grass_fused' => 'Lemon Grass Fused',
       'garlic_and_roasted_chili' => 'Garlic and Roasted Chili',
       'fused' => 'Fused',
       'garlic_cilantro_fused' => 'Garlic Cilantro Fused',
       'garlic_mushroom_fused' => 'Garlic Mushroom Fused',
       'rosemary_lavender_fused' => 'Rosemary Lavender Fused',
       'citrus_habanero_fused' => 'Citrus Habanero Fused',
       'herbs_d_provence_fused' => 'Herbs D Provence Fused',
       ], null, ['class' => 'form-control']) }}
</div>
