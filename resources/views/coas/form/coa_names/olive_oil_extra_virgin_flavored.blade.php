<div class="form-group" id="coa_name">
    {{ Form::label('coa_name', 'COA Name') }}
    {{ Form::select('coa_name',['Please Select',
       'cranberry_walnut' => 'Cranberry Walnut',
       'natural_black_truffle' => 'Natural Black Truffle',
       'natural_blood_orange' => 'Natural Blood Orange',
       'natural_butter' => 'Natural Butter',
       'natural_florida_orange' => 'Natural Florida Orange',
       'natural_meyer_lemon' => 'Natural Meyer Lemon',
       'natural_persian_lime' => 'Natural Persian Lime',
       'natural_rhubarb' => 'Natural Rhubarb',
       'natural_sage_and_onion' => 'Natural Sage and Onion',
       'natural_smoked_hickory' => 'Natural Smoked Hickory',
       'natural_sundried_tomato' => 'Natural Sundried Tomato',
       'natural_tangerine' => 'Natural Tangerine',
       'natural_vanilla_bean' => 'Natural Vanilla Bean',
       'natural_white_truffle' => 'Natural White Truffle',
       ], null, ['class' => 'form-control']) }}
</div>
