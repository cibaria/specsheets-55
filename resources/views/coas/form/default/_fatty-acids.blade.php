<div class="form-group">
    {{ Form::label('C12_0', 'C12:') }}
    {{ Form::text('C12_0', null,['class' => 'form-control',  'placeholder' => 'C12:']) }}
</div>
<div class="form-group">
    {{ Form::label('C14_0', 'C14:0 Myristic Acid ') }}
    {{ Form::text('C14_0', null,['class' => 'form-control',  'placeholder' => 'C14:0 Myristic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C14_1', 'C14:1') }}
    {{ Form::text('C14_1', null,['class' => 'form-control',  'placeholder' => 'C14:1']) }}
</div>
<div class="form-group">
    {{ Form::label('C16_0', 'C16: Palmitic Acid') }}
    {{ Form::text('C16_0', null,['class' => 'form-control',  'placeholder' => 'C16: Palmitic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C16_1', 'C16:1 Palmitoleic Acid') }}
    {{ Form::text('C16_1', null,['class' => 'form-control',  'placeholder' => 'C16:1 Palmitoleic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C17_0', 'C17:0 Heptadacanoic Acid') }}
    {{ Form::text('C17_0', null,['class' => 'form-control',  'placeholder' => 'C17:0 Heptadacanoic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C17_1', 'C17:1') }}
    {{ Form::text('C17_1', null,['class' => 'form-control',  'placeholder' => 'C17:1']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_0', 'C18:0 Stearic Acid') }}
    {{ Form::text('C18_0', null,['class' => 'form-control',  'placeholder' => 'C18:0 Stearic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_1', 'C18:1 Oleic Acid') }}
    {{ Form::text('C18_1', null,['class' => 'form-control',  'placeholder' => 'C18:1 Oleic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_2', 'C18:2 Linoleic Acid') }}
    {{ Form::text('C18_2', null,['class' => 'form-control',  'placeholder' => 'C18:2 Linoleic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_3', 'C18:3 Linolenic Acid') }}
    {{ Form::text('C18_3', null,['class' => 'form-control',  'placeholder' => 'C18:3 Linolenic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C20_0', 'C20:0 Arachidic Acid') }}
    {{ Form::text('C20_0', null,['class' => 'form-control',  'placeholder' => 'C20:0 Arachidic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C20_1', 'C20:1 Behenic Acid') }}
    {{ Form::text('C20_1', null,['class' => 'form-control',  'placeholder' => 'C20:1 Behenic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C22_0', 'C22:0 Behenic Acid') }}
    {{ Form::text('C22_0', null,['class' => 'form-control',  'placeholder' => 'C22:0 Behenic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C24_0', 'C24:0 Trans Oleic') }}
    {{ Form::text('C24_0', null,['class' => 'form-control',  'placeholder' => 'C24:0 Trans Oleic']) }}
</div>
<div class="form-group">
    {{ Form::label('C24_1', 'C24:1 Trans Linoleic + Linolenic') }}
    {{ Form::text('C24_1', null,['class' => 'form-control',  'placeholder' => 'C24:1 Trans Linoleic + Linolenic']) }}
</div>