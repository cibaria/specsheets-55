<div class="form-group">
    {{ Form::label('description', 'Description') }}
    {{ Form::text('description', null,['class' => 'form-control',  'placeholder' => 'Description']) }}
</div>
<div class="form-group">
    {{ Form::label('lot', 'Lot') }}
    {{ Form::text('lot', null,['class' => 'form-control',  'placeholder' => 'Lot']) }}
</div>
<div class="form-group">
    {{ Form::label('origin', 'Origin') }}
    {{ Form::text('origin', null,['class' => 'form-control',  'placeholder' => 'Origin']) }}
</div>
<div class="form-group">
    {{ Form::label('manf_date', 'Manf Date') }}
    {{ Form::text('manf_date', null,['class' => 'form-control',  'placeholder' => 'Manf Date', 'id' => 'datepicker1', 'autocomplete' => 'off']) }}
</div>
<div class="form-group">
    {{ Form::label('expiration_date', 'Expiration Date') }}
    {{ Form::text('expiration_date', null,['class' => 'form-control',  'placeholder' => 'Expiration Date', 'id' => 'datepicker', 'autocomplete' => 'off']) }}
</div>