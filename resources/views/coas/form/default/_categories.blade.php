<div id="categories">

<div class="row">
    <div class="col-xs-6 col-md-4">
        <div class="form-group">
            {{ Form::label('', 'Template') }}
            <div class="controls">
                {{ Form::select('',[
                '' => 'Choose',
                'oils-pure' => 'Olive Oil, Pure',
                'canola' => 'Canola',
                'castor-oil' => 'Castor Oil',
                'cocoa-butter' => 'Cocoa Butter',
                'coconut' => 'Coconut',
                'macadamia-nut' => 'Macadamia Nut',
                'olive-oil-extra-virgin' => 'Olive Oil, Extra Virgin',
                'olive-oil-pure' => 'Olive Oil, Pure',
                'palm' => 'Palm',
                'safflower' => 'Safflower',
                'sesame-oil' => 'Sesame Oil',
                'shea-butter' => 'Shea Butter',
                'soybean' => 'Soybean',
                'sunflower' => 'Sunflower',
                'vinegar' => 'Vinegar',
                'walnut' => 'Walnut',
                ], '', ['class' => 'form-control', 'id' => 'template_category']) }}

            </div>
        </div>
    </div>
</div>
{{--end row 1--}}

</div><!--end categories-->