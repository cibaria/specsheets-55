<a href="/coas/create/" class="btn btn-primary" style="float:right;">Reset</a>

@if(isset($coa) && url('/') . "/coas/" . $coa->id . "/edit/" )
    {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
@else
    {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}
@endif

