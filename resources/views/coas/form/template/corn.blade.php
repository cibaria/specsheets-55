<div class="form-group">
    {{ Form::label('flavor', 'Flavor') }}
    {{ Form::text('flavor', $coa->coa->flavor,['class' => 'form-control', 'placeholder' => 'Flavor']) }}
</div>
<div class="form-group">
    {{ Form::label('color_red', 'Color Lovibond Red') }}
    {{ Form::text('color_red', $coa->coa->color_red,['class' => 'form-control', 'placeholder' => 'Color Lovibond Red']) }}
</div>
<div class="form-group">
    {{ Form::label('color_yellow', 'Color Lovibond Yellow') }}
    {{ Form::text('color_yellow', $coa->coa->color_yellow,['class' => 'form-control', 'placeholder' => 'Color Lovibond Yellow']) }}
</div>
<div class="form-group">
    {{ Form::label('aroma', 'Aroma') }}
    {{ Form::text('aroma', $coa->coa->aroma,['class' => 'form-control', 'placeholder' => 'Aroma']) }}
</div>
<div class="form-group">
    {{ Form::label('free_fatty_acids', 'Free Fatty Acid') }}
    {{ Form::text('free_fatty_acids', $coa->coa->free_fatty_acids,['class' => 'form-control', 'placeholder' => 'Free Fatty Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('moisture_and_impurities', 'Moisture And Impurities') }}
    {{ Form::text('moisture_and_impurities', $coa->coa->moisture_and_impurities,['class' => 'form-control', 'placeholder' => 'Moisture And Impurities']) }}
</div>
<div class="form-group">
    {{ Form::label('peroxide_value', 'Peroxide Value meq/kg') }}
    {{ Form::text('peroxide_value', $coa->coa->peroxide_value,['class' => 'form-control', 'placeholder' => 'Peroxide Value meq/kg']) }}
</div>
<div class="form-group">
    {{ Form::label('iodine_value', 'Iodine Value') }}
    {{ Form::text('iodine_value', $coa->coa->iodine_value,['class' => 'form-control', 'placeholder' => 'Iodine Value']) }}
</div>
<div class="form-group">
    {{ Form::label('melting_point', 'Melting Point') }}
    {{ Form::text('melting_point', $coa->coa->melting_point,['class' => 'form-control', 'placeholder' => 'Melting Point']) }}
</div>
<div class="form-group">
    {{ Form::label('additives', 'Additives') }}
    {{ Form::text('additives', $coa->coa->additives,['class' => 'form-control', 'placeholder' => 'Additives']) }}
</div>
<div class="form-group">
    {{ Form::label('cold_test', 'Cold Test') }}
    {{ Form::text('cold_test', $coa->coa->cold_test,['class' => 'form-control', 'placeholder' => 'Cold Test']) }}
</div>