
<div class="form-group">
    {{ Form::label('appearance', 'Appearance @ 25 degree C') }}
    {{ Form::text('appearance', $coa->coa->appearance,['class' => 'form-control', 'placeholder' => 'Appearance @ 25 degree C']) }}
</div>
<div class="form-group">
    {{ Form::label('color_red', 'Color Lovibond Red') }}
    {{ Form::text('color_red', $coa->coa->color_red,['class' => 'form-control', 'placeholder' => 'Color Lovibond Red']) }}
</div>
<div class="form-group">
    {{ Form::label('color_yellow', 'Color Lovibond Yellow') }}
    {{ Form::text('color_yellow', $coa->coa->color_yellow,['class' => 'form-control', 'placeholder' => 'Color Lovibond Yellow']) }}
</div>
<div class="form-group">
    {{ Form::label('flavor_and_odor', 'Flavor and Odor') }}
    {{ Form::text('flavor_and_odor', $coa->coa->flavor_and_odor,['class' => 'form-control', 'placeholder' => 'Flavor and Odor']) }}
</div>
<div class="form-group">
    {{ Form::label('free_fatty_acids', 'Free Fatty Acids') }}
    {{ Form::text('free_fatty_acids', $coa->coa->free_fatty_acids,['class' => 'form-control', 'placeholder' => 'Free Fatty Acids']) }}
</div>
<div class="form-group">
    {{ Form::label('iodine_value', 'Iodine Value') }}
    {{ Form::text('iodine_value', $coa->coa->iodine_value,['class' => 'form-control', 'placeholder' => 'Iodine Value']) }}
</div>
<div class="form-group">
    {{ Form::label('lead', 'Lead') }}
    {{ Form::text('lead', $coa->coa->lead,['class' => 'form-control', 'placeholder' => 'Lead']) }}
</div>
<div class="form-group">
    {{ Form::label('moisture', 'Moisture') }}
    {{ Form::text('moisture', $coa->coa->moisture,['class' => 'form-control', 'placeholder' => 'Moisture']) }}
</div>
<div class="form-group">
    {{ Form::label('peroxide_value', 'Peroxide Value') }}
    {{ Form::text('peroxide_value', $coa->coa->peroxide_value,['class' => 'form-control', 'placeholder' => 'Peroxide Value']) }}
</div>
<div class="form-group">
    {{ Form::label('refractive_index', 'Refractive Index') }}
    {{ Form::text('refractive_index', $coa->coa->refractive_index,['class' => 'form-control', 'placeholder' => 'Refractive Index']) }}
</div>
<div class="form-group">
    {{ Form::label('unsaponifiable_matter', 'Unsaponifiable Matter') }}
    {{ Form::text('unsaponifiable_matter', $coa->coa->unsaponifiable_matter,['class' => 'form-control', 'placeholder' => 'Unsaponifiable Matter']) }}
</div>
<div class="form-group">
    {{ Form::label('C16_0', 'C16: Palmitic acid') }}
    {{ Form::text('C16_0', $coa->coa->C16_0,['class' => 'form-control',  'placeholder' => 'C16: Palmitic acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_0', 'C18:0 C18:0 Stearic Acid') }}
    {{ Form::text('C18_0', $coa->coa->C18_0,['class' => 'form-control',  'placeholder' => 'C18:0 C18:0 Stearic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_1', 'C18:1 Oleic acid') }}
    {{ Form::text('C18_1', $coa->coa->C18_1,['class' => 'form-control',  'placeholder' => 'C18:1 Oleic acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_2', 'C18:2 Linoleic acid') }}
    {{ Form::text('C18_2', $coa->coa->C18_2,['class' => 'form-control',  'placeholder' => 'C18:2 Linoleic acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_3', 'C18:3 Linolenic Acid') }}
    {{ Form::text('C18_3', $coa->coa->C18_3,['class' => 'form-control',  'placeholder' => 'C18:3 Linolenic Acid']) }}
</div>