<div class="form-group">
    {{ Form::label('free_fatty_acids', 'Free Fatty Acid') }}
    {{ Form::text('free_fatty_acids', $coa->coa->ffa,['class' => 'form-control', 'placeholder' => 'Free Fatty Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('peroxide_value', 'Peroxide Value (meg 0/kg)') }}
    {{ Form::text('peroxide_value', $coa->coa->peroxide_value,['class' => 'form-control', 'placeholder' => 'Peroxide Value (meg 0/kg)']) }}
</div>
<div class="form-group">
    {{ Form::label('C14_0', 'C14:0 Myristic Acid ') }}
    {{ Form::text('C14_0', $coa->coa->C14_0,['class' => 'form-control',  'placeholder' => 'C14:0 Myristic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C16_0', 'C16: Palmitic Acid') }}
    {{ Form::text('C16_0', $coa->coa->C16_0,['class' => 'form-control',  'placeholder' => 'C16: Palmitic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C16_1', 'C16:1 Palmitoleic Acid') }}
    {{ Form::text('C16_1', $coa->coa->C16_1,['class' => 'form-control',  'placeholder' => 'C16:1 Palmitoleic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C17_0', 'C17:0 Heptadacanoic Acid') }}
    {{ Form::text('C17_0', $coa->coa->C17_0,['class' => 'form-control',  'placeholder' => 'C17:0 Heptadacanoic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C17_1', 'C17:1') }}
    {{ Form::text('C17_1', $coa->coa->C17_1,['class' => 'form-control',  'placeholder' => 'C17:1']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_0', 'C18:0 Stearic Acid') }}
    {{ Form::text('C18_0', $coa->coa->C18_0,['class' => 'form-control',  'placeholder' => 'C18:0 Stearic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_1', 'C18:1 Oleic Acid') }}
    {{ Form::text('C18_1', $coa->coa->C18_1,['class' => 'form-control',  'placeholder' => 'C18:1 Oleic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_2', 'C18:2 Linoleic Acid') }}
    {{ Form::text('C18_2', $coa->coa->C18_2,['class' => 'form-control',  'placeholder' => 'C18:2 Linoleic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C18_3', 'C18:3 Linolenic Acid') }}
    {{ Form::text('C18_3', $coa->coa->C18_3,['class' => 'form-control',  'placeholder' => 'C18:3 Linolenic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C20_0', 'C20:0 Arachidic Acid') }}
    {{ Form::text('C20_0', $coa->coa->C20_0,['class' => 'form-control',  'placeholder' => 'C20:0 Arachidic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C20_1', 'C20:1 Behenic Acid') }}
    {{ Form::text('C20_1', $coa->coa->C20_1,['class' => 'form-control',  'placeholder' => 'C20:1 Behenic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C22_0', 'C22:0 Behenic Acid') }}
    {{ Form::text('C22_0', $coa->coa->C22_0,['class' => 'form-control',  'placeholder' => 'C22:0 Behenic Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('C24_0', 'C24:0 Trans Oleic') }}
    {{ Form::text('C24_0', $coa->coa->C24_0,['class' => 'form-control',  'placeholder' => 'C24:0 Trans Oleic']) }}
</div>
<div class="form-group">
    {{ Form::label('C24_1', 'C24:1 Trans Linoleic + Linolenic') }}
    {{ Form::text('C24_1', $coa->coa->C24_1,['class' => 'form-control',  'placeholder' => 'C24:1 Trans Linoleic + Linolenic']) }}
</div>
<div class="form-group">
    {{ Form::label('cholesterol', 'Cholesterol') }}
    {{ Form::text('cholesterol', $coa->coa->cholesterol,['class' => 'form-control',  'placeholder' => 'Cholesterol']) }}
</div>
<div class="form-group">
    {{ Form::label('brassicasterol', 'Brassicasterol') }}
    {{ Form::text('brassicasterol', $coa->coa->brassicasterol,['class' => 'form-control',  'placeholder' => 'Brassicasterol']) }}
</div>
<div class="form-group">
    {{ Form::label('campesterol', 'Campesterol') }}
    {{ Form::text('campesterol', $coa->coa->campesterol,['class' => 'form-control',  'placeholder' => 'Campesterol']) }}
</div>
<div class="form-group">
    {{ Form::label('stigmasterol', 'Stigmasterol') }}
    {{ Form::text('stigmasterol', $coa->coa->stigmasterol,['class' => 'form-control',  'placeholder' => 'Stigmasterol']) }}
</div>