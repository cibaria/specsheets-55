@extends('layouts.admin')

@section('content')
    <div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>COA {{
                    $coa->lot }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/coas/">COA</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($coa, array('method' => 'PATCH', 'route' => array('coas.update', $coa->id))) }}
                        <div class="form-group">
                            {{ Form::label('coa_active', 'COA Active') }}
                            @if($coa->coa_active == 'yes')
                                {{ Form::checkbox('coa_active','yes', true)}}
                            @else
                                {{ Form::checkbox('coa_active','yes', false)}}
                            @endif
                        </div>
                        <div class="form-group" id="template">
                            {{ Form::label('coa_template', 'COA Template')}}
                            @if(!is_null($coa->coa_template))
                                {{ Form::select('coa_template',[$coa->coa_template, 'oils' => 'Oils', 'vinegars' => 'Vinegars'], $coa->coa_template, ['class' => 'form-control']) }}
                            @else
                                {{ Form::select('coa_template',['' => 'Please Select', 'oils' => 'Oils', 'vinegars' => 'Vinegars'],null, ['class' => 'form-control']) }}
                            @endif
                        </div>
                        <div class="form-group">
                            {{ Form::label('lot', 'Lot') }}
                            {{ Form::text('lot',null, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('description', 'Description') }}
                            {{ Form::text('description',null, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('expiration_date', 'Expiration Date') }}
                            {{ Form::text('expiration_date',null, ['class' => 'form-control', 'placeholder' => 'Expiration Date', 'id' => 'datepicker1', 'autocomplete' => 'off']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('manf_date', 'Manf Date') }}

                            @if(!is_null($coa->coa))
                                <input type="text" name="manf_date" value="<?php echo $coa->coa->manf_date; ?>"  id="datepicker" autocomplete="off" class="form-control"/>

                                @else
                                <input type="text" name="manf_date"  id="datepicker" autocomplete="off" class="form-control"/>

                            @endif

                        </div>
                        <div class="form-group">
                            {{ Form::label('origin', 'Origin') }}
                            {{ Form::text('origin',null, ['class' => 'form-control', 'placeholder' => 'Origin']) }}
                        </div>

                        {{ Form::submit('Update', ['class' => 'btn btn-default', 'style' => 'float:right', 'autocomplete' => 'off']) }}

                        {{ Form::close() }}
                    </div>
                    <!--end row-->
                </div>

                {{-- right --}}
                <!--end row-->
            </div>
        </div>
        <!--end col lg 8 -->
    </div><!--end container fluid-->

    </div>
@stop
@section('footer')
    <script>
        $(function () {
            $("#datepicker").datepicker();
            $("#datepicker1").datepicker();
        });
    </script>

@stop