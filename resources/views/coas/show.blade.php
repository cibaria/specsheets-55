@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>COA
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/coas/">COA</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>
<div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <a href="{{ url('/') . "/public/coas/" . $coa->id }}" class="btn btn-primary navbar-btn">View Public COA</a>

                </nav>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Lot:</strong> {{ $coa->lot }}
                            </li>

                            <li class="list-group-item">
                                <strong>Description:</strong> {{ $coa->description }}
                            </li>
                            <li class="list-group-item">
                                <strong>Manf date:</strong> {{ $coa->manf_date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Expiration Date:</strong> {{ $coa->expiration_date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Origin:</strong> {{ $coa->origin }}
                            </li>
                            <li class="list-group-item">
                                <strong>PO Number:</strong> {{ $coa->po_number }}
                            </li>
                            <li class="list-group-item">
                                <strong>Item Number:</strong> {{ $coa->item_number }}
                            </li>
                            <li class="list-group-item">
                                <strong>Capronic Acid:</strong> {{ $coa->capronic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Caprylic Acid:</strong> {{ $coa->caprylic_acid}}
                            </li>
                            <li class="list-group-item">
                                <strong>Capric Acid:</strong> {{ $coa->capric_acid}}
                            </li>
                            <li class="list-group-item">
                                <strong>Lauric Acid:</strong> {{ $coa->lauric_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Myristic:</strong> {{ $coa->myristic}}
                            </li>
                            <li class="list-group-item">
                                <strong>Myistoleic Acid:</strong> {{ $coa->myistoleic_acid}}
                            </li>
                            <li class="list-group-item">
                                <strong>Palmitic Acid:</strong> {{ $coa->palmitic_acid}}
                            </li>
                            <li class="list-group-item">
                                <strong>Palmitoliec Acid:</strong> {{ $coa->palmitoliec_acid}}
                            </li>
                            <li class="list-group-item">
                                <strong>Heptadacanoic Acid:</strong> {{ $coa->heptadacanoic_acid}}
                            </li>
                            <li class="list-group-item">
                                <strong>Heptadecenoic Acid:</strong> {{ $coa->heptadecenoic_acid}}
                            </li>
                            <li class="list-group-item">
                                <strong>Stearic Acid:</strong> {{ $coa->stearic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Oleic Acid:</strong> {{ $coa->oleic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Linoleic Acid:</strong> {{ $coa->linoleic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Linolenic Acid:</strong> {{ $coa->linolenic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Octadecatetraen Acid:</strong> {{ $coa->octadecatetraen_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Arachidic Acid:</strong> {{ $coa->arachidic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Gadoleic Acid:</strong> {{ $coa->gadoleic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Eicosanedienoic Acid:</strong> {{ $coa->eicosanedienoic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Eicosaneteetraenoic:</strong> {{ $coa->eicosaneteetraenoic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Behenic Acid:</strong> {{ $coa->behenic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>Erucic acid:</strong> {{ $coa->erucic_acid}}
                            </li>
                            <li class="list-group-item">
                                <strong>Lignoceric Acid:</strong> {{ $coa->lignoceric_acid}}
                            </li>
                            <li class="list-group-item">
                                <strong>Nervonic Acid:</strong> {{ $coa->nervonic_acid}}
                            </li>
                            <li class="list-group-item">
                                <strong>Minor:</strong> {{ $coa->minor}}
                            </li>
                            <li class="list-group-item">
                                <strong>Additives:</strong> {{ $coa->additives}}
                            </li>
                            <li class="list-group-item">
                                <strong>AOM:</strong> {{ $coa->aom}}
                            </li>
                            <li class="list-group-item">
                                <strong>Ash:</strong> {{ $coa->ash}}
                            </li>
                            <li class="list-group-item">
                                <strong>Absorbtion in Ultra Violet:</strong> {{ $coa->absorbtion_in_ultra_violet }}
                            </li>
                            <li class="list-group-item">
                                <strong>Acid Value:</strong> {{ $coa->acid_value }}
                            </li>
                            <li class="list-group-item">
                                <strong>Acidity:</strong> {{ $coa->acidity }}
                            </li>
                            <li class="list-group-item">
                                <strong>Alcohol:</strong> {{ $coa->alcohol }}
                            </li>
                            <li class="list-group-item">
                                <strong>Appearance:</strong> {{ $coa->appearance }}
                            </li>
                            <li class="list-group-item">
                                <strong>Aroma:</strong> {{ $coa->aroma }}
                            </li>
                            <li class="list-group-item">
                                <strong>Brix:</strong> {{ $coa->brix }}
                            </li>
                            <li class="list-group-item">
                                <strong>Copper:</strong> {{ $coa->copper }}
                            </li>
                            <li class="list-group-item">
                                <strong>Cold Test:</strong> {{ $coa->cold_test }}
                            </li>
                            <li class="list-group-item">
                                <strong>Color:</strong> {{ $coa->color }}
                            </li>
                            <li class="list-group-item">
                                <strong>Color Gardner:</strong> {{ $coa->color_gardner }}
                            </li>
                            <li class="list-group-item">
                                <strong>Color Lovibond:</strong> {{ $coa->color_lovibond }}
                            </li>
                            <li class="list-group-item">
                                <strong>Color Blue:</strong> {{ $coa->blue }}
                            </li>
                            <li class="list-group-item">
                                <strong>Color neutral:</strong> {{ $coa->neutral }}
                            </li>
                            <li class="list-group-item">
                                <strong>Color Red:</strong> {{ $coa->color_red }}
                            </li>
                            <li class="list-group-item">
                                <strong>Color Yellow:</strong> {{ $coa->color_yellow }}
                            </li>
                            <li class="list-group-item">
                                <strong>Color Visual:</strong> {{ $coa->color_visual }}
                            </li>
                            <li class="list-group-item">
                                <strong>Developed alcohol degree:</strong> {{ $coa->developed_alcohol_degree }}
                            </li>
                            <li class="list-group-item">
                                <strong>Dextros equivalent:</strong> {{ $coa->dextros_equivalent }}
                            </li>
                            <li class="list-group-item">
                                <strong>Density 20:</strong> {{ $coa->density_20 }}
                            </li>
                            <li class="list-group-item">
                                <strong>FFA:</strong> {{ $coa->ffa }}
                            </li>
                            <li class="list-group-item">
                                <strong>Free sulpphurous anhydride:</strong> {{ $coa->free_sulpphurous_anhydride }}
                            </li>
                            <li class="list-group-item">
                                <strong>Fructose:</strong> {{ $coa->fructose }}
                            </li>
                            <li class="list-group-item">
                                <strong>Flavor:</strong> {{ $coa->flavor }}
                            </li>
                            <li class="list-group-item">
                                <strong>Free acidity:</strong> {{ $coa->free_acidity }}
                            </li>
                            <h2>Fatty Acids</h2>
                            <li class="list-group-item">
                                <strong>Fatty acids:</strong> {{ $coa->fatty_acids }}
                            </li>
                            <li class="list-group-item">
                                <strong>Free fatty acids:</strong> {{ $coa->free_fatty_acids }}
                            </li>
                            <li class="list-group-item">
                                <strong>Saturated fatty acids:</strong> {{ $coa->saturated_fatty_acids }}
                            </li>
                            <li class="list-group-item">
                                <strong>trans fatty acids:</strong> {{ $coa->trans_fatty_acids }}
                            </li>
                            <li class="list-group-item">
                                <strong>Propionic acid C3:0:</strong> {{ $coa->C3_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Butyric acid C4:0:</strong> {{ $coa->C4_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Valeric acid C5:0:</strong> {{ $coa->C5_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Caproic acid C6:0:</strong> {{ $coa->C6_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Enanthic acid C7:0:</strong> {{ $coa->C7_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Caprylic acid C8:0:</strong> {{ $coa->C8_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Pelargonic acid C9:0:</strong> {{ $coa->C9_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Capric acid C10:0:</strong> {{ $coa->C10_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Undecylic acid C11:0:</strong> {{ $coa->C11_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lauric acid C12:0:</strong> {{ $coa->C12_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Tridecylic acid C13:0:</strong> {{ $coa->C13_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Myristic acid C14:0:</strong> {{ $coa->C14_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Pentadecylic acid C15:0:</strong> {{ $coa->C15_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Palmitic acid C16:0:</strong> {{ $coa->C16_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Palmitoliec acid C16:1:</strong> {{ $coa->C16_1 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Margaric acid C17:0:</strong> {{ $coa->C17_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Heptadecenoic acid C17:1:</strong> {{ $coa->C17_1 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Stearic acid C18:0:</strong> {{ $coa->C18_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Oleic acid C18:1:</strong> {{ $coa->C18_1 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Linoleic acid C18:2:</strong> {{ $coa->C18_2 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Alpha Linolenic C18:3:</strong> {{ $coa->C18_3 }}
                            </li>

                            <li class="list-group-item">
                                <strong>C18:1T:</strong> {{ $coa->c18_1t }}
                            </li>
                            <li class="list-group-item">
                                <strong>C18:2T + C18:3T:</strong> {{ $coa->c18_2t_c18_3t }}
                            </li>
                            <li class="list-group-item">
                                <strong>Trans C 18:1:</strong> {{ $coa->trans_c_18_1 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Trans C 18:2:</strong> {{ $coa->trans_c_18_2 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Nonadecylic acid C19:0:</strong> {{ $coa->C19_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Arachidic acid C20:0:</strong> {{ $coa->C20_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Eicosenic Acid C20:1:</strong> {{ $coa->C20_1 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Heneicosylic acid C21:0:</strong> {{ $coa->C21_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Behenic acid C22:0:</strong> {{ $coa->C22_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Tricosylic acid C23:0:</strong> {{ $coa->C23_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lignoceric acid C24:0:</strong> {{ $coa->C24_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Pentacosylic acid C25:0:</strong> {{ $coa->C25_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Cerotic acid C26:0:</strong> {{ $coa->C26_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Heptacosylic acid C27:0:</strong> {{ $coa->C27_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Montanic acid C28:0:</strong> {{ $coa->C28_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Nonacosylic acid C29:0:</strong> {{ $coa->C29_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Melissic acid C30:0:</strong> {{ $coa->C30_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Henatriacontylic acid C31:0:</strong> {{ $coa->C31_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lacceroic acid C32:0:</strong> {{ $coa->C32_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Psyllic acid C33:0:</strong> {{ $coa->C33_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Geddic acid C34:0:</strong> {{ $coa->C34_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Ceroplastic acid C35:0:</strong> {{ $coa->C35_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Hexatriacontylic acid C36:0:</strong> {{ $coa->C36_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Heptatriacontanoic acid C37:0:</strong> {{ $coa->C37_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Octatriacontanoic acid C38:0:</strong> {{ $coa->C38_0 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Campestanol+Campesterol:</strong> {{ $coa->campestanol_campesterol }}
                            </li>


                        </div><!--end row-->
                    </div>

                    {{-- right --}}
            <div class="col-md-6">
                 <li class="list-group-item">
                                <strong>Free so2:</strong> {{ $coa->free_so2 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Grains:</strong> {{ $coa->grains }}
                            </li>
                            <li class="list-group-item">
                                <strong>Glucose:</strong> {{ $coa->glucose }}
                            </li>
                            <li class="list-group-item">
                                <strong>Heavy metals as lead:</strong> {{ $coa->heavy_metals_as_lead }}
                            </li>
                            <li class="list-group-item">
                                <strong>Hydroxyl value:</strong> {{ $coa->hydroxyl_value }}
                            </li>
                            <li class="list-group-item">
                                <strong>I.V. (Wijs):</strong> {{ $coa->iv_wijs }}
                            </li>
                            <li class="list-group-item">
                                <strong>Iron:</strong> {{ $coa->iron }}
                            </li>
                            <li class="list-group-item">
                                <strong>Identification A:</strong> {{ $coa->identification_a }}
                            </li>
                            <li class="list-group-item">
                                <strong>Identification B:</strong> {{ $coa->identification_b }}
                            </li>
                            <li class="list-group-item">
                                <strong>Identification C:</strong> {{ $coa->identification_c }}
                            </li>
                            <li class="list-group-item">
                                <strong>Insoluble impurities:</strong> {{ $coa->insoluble_impurities }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lead:</strong> {{ $coa->lead }}
                            </li>
                            <li class="list-group-item">
                                <strong>Maltose:</strong> {{ $coa->maltose }}
                            </li>
                            <li class="list-group-item">
                                <strong>m_i:</strong> {{ $coa->m_i }}
                            </li>
                            <li class="list-group-item">
                                <strong>Melting point:</strong> {{ $coa->melting_point }}
                            </li>
                            <li class="list-group-item">
                                <strong>Moisture:</strong> {{ $coa->moisture }}
                            </li>
                            <li class="list-group-item">
                                <strong>Moisture and impurities:</strong> {{ $coa->moisture_and_impurities }}
                            </li>
                            <li class="list-group-item">
                                <strong>Moisture and sediment content:</strong> {{ $coa->moisture_and_impurities }}
                            </li>
                            <li class="list-group-item">
                                <strong>Moisture level:</strong> {{ $coa->moisture_level }}
                            </li>
                            <li class="list-group-item">
                                <strong>Odor:</strong> {{ $coa->odor }}
                            </li>
                            <li class="list-group-item">
                                <strong>OSI:</strong> {{ $coa->osi }}
                            </li>
                            <li class="list-group-item">
                                <strong>Other carbohydrates:</strong> {{ $coa->other_carbohydrates }}
                            </li>

                            <li class="list-group-item">
                                <strong>Optical rotation:</strong> {{ $coa->optical_rotation }}
                            </li>
                            <li class="list-group-item">
                                <strong>PV:</strong> {{ $coa->pv }}
                            </li>
                            <li class="list-group-item">
                                <strong>Peroxide Value:</strong> {{ $coa->peroxide_value }}
                            </li>
                            <li class="list-group-item">
                                <strong>Pounds per gallon:</strong> {{ $coa->pounds_per_gallon }}
                            </li>
                            <li class="list-group-item">
                                <strong>Rifractometric at 20:</strong> {{ $coa->rifractometric_at_20 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Refractive index:</strong> {{ $coa->refractive_index }}
                            </li>
                            <li class="list-group-item">
                                <strong>Smoke point:</strong> {{ $coa->smoke_point }}
                            </li>
                            <li class="list-group-item">
                                <strong>Specific gravity:</strong> {{ $coa->specific_gravity }}
                            </li>
                            <li class="list-group-item">
                                <strong>Specific weight:</strong> {{ $coa->specific_weight }}
                            </li>
                            <li class="list-group-item">
                                <strong>Starch:</strong> {{ $coa->starch }}
                            </li>
                            <li class="list-group-item">
                                <strong>Salt:</strong> {{ $coa->salt }}
                            </li>
                            <li class="list-group-item">
                                <strong>Saponification value:</strong> {{ $coa->saponification_value }}
                            </li>

                            <li class="list-group-item">
                                <strong>specific weight as baumic degrees:</strong> {{ $coa->specific_weight_as_baumic_degrees }}
                            </li>
                            <li class="list-group-item">
                                <strong>SO2:</strong> {{ $coa->so2 }}
                            </li>
                            <li class="list-group-item">
                                <strong>TBHQ:</strong> {{ $coa->tbhq }}
                            </li>
                            <li class="list-group-item">
                                <strong>total acidity as acetic acid:</strong> {{ $coa->total_acidity_as_acetic_acid }}
                            </li>
                            <li class="list-group-item">
                                <strong>total dry extract:</strong> {{ $coa->total_dry_extract }}
                            </li>
                            <li class="list-group-item">
                                <strong>Total monounsaturated:</strong> {{ $coa->total_monounsaturated }}
                            </li>
                            <li class="list-group-item">
                                <strong>Total polyunsaturated:</strong> {{ $coa->total_polyunsaturated }}
                            </li>
                            <li class="list-group-item">
                                <strong>Total saturated:</strong> {{ $coa->total_saturated }}
                            </li>
                            <li class="list-group-item">
                                <strong>total sulphurous anhydride:</strong> {{ $coa->total_sulphurous_anhydride }}
                            </li>
                            <li class="list-group-item">
                                <strong>Trans C 18:1:</strong> {{ $coa->trans_c_18_1 }}
                            </li>
                            <li class="list-group-item">
                                <strong>Trans C 18:2:</strong> {{ $coa->trans_c_18_2 }}
                            </li>
                            <li class="list-group-item">
                                <strong>C18:1T:</strong> {{ $coa->c18_1t }}
                            </li>
                            <li class="list-group-item">
                                <strong>C18:2T + C18:3T:</strong> {{ $coa->c18_2t_c18_3t }}
                            </li>
                            <li class="list-group-item">
                                <strong>Taste:</strong> {{ $coa->taste }}
                            </li>
                            <li class="list-group-item">
                                <strong>Total SO2:</strong> {{ $coa->total_so2 }}
                            </li>

                            <li class="list-group-item">
                                <strong>Turbidity:</strong> {{ $coa->turbidity }}
                            </li>
                            <li class="list-group-item">
                                <strong>Unsaponifiable matter:</strong> {{ $coa->unsaponifiable_matter }}
                            </li>
                            <li class="list-group-item">
                                <strong>Vitamin e:</strong> {{ $coa->vitamin_e }}
                            </li>
                            <li class="list-group-item">
                                <strong>Zinc:</strong> {{ $coa->zinc }}
                            </li>
                            <li class="list-group-item">
                                <strong>PH:</strong> {{ $coa->ph }}
                            </li>
                            <li class="list-group-item">
                                <strong>Cholesterol:</strong> {{ $coa->cholesterol }}
                            </li>
                            <li class="list-group-item">
                                <strong>Brassicasterol:</strong> {{ $coa->brassicasterol }}
                            </li>
                            <li class="list-group-item">
                                <strong>Compesterol:</strong> {{ $coa->compesterol }}
                            </li>
                            <li class="list-group-item">
                                <strong>Stigmasterol:</strong> {{ $coa->stigmasterol }}
                            </li>
                            <li class="list-group-item">
                                <strong>Delta7 stigmasternol:</strong> {{ $coa->delta7_stigmasternol }}
                            </li>
                            <li class="list-group-item">
                                <strong>delta5-23stigmastadienol + clerosterol +
                                        sitostanol + delta5-24-stigmastadienol
                                        :</strong> {{ $coa->beta_sitosterol_delta5_avenasterol }}
                            </li>
                            <li class="list-group-item">
                                <strong>Total sterols:</strong> {{ $coa->total_sterols }}
                            </li>
                            <li class="list-group-item">
                                <strong>Iodine value:</strong> {{ $coa->iodine_value }}
                            </li>
                            <li class="list-group-item">
                                <strong>Iodine Value - Ri:</strong> {{ $coa->iodine_value_ri }}
                            </li>
                            <li class="list-group-item">
                                <strong>Refactive index:</strong> {{ $coa->refactive_index }}
                            </li>
                            <li class="list-group-item">
                                <strong>Saponification:</strong> {{ $coa->saponification }}
                            </li>
                            <li class="list-group-item">
                                <strong>Delta k:</strong> {{ $coa->deltak }}
                            </li>
                            <li class="list-group-item">
                                <strong>232nm:</strong> {{ $coa->nm232 }}
                            </li>
                            <li class="list-group-item">
                                <strong>k268:</strong> {{ $coa->k268 }}
                            </li>
                            <li class="list-group-item">
                                <strong>k270:</strong> {{ $coa->k270 }}
                            </li>
                            <li class="list-group-item">
                                <strong>IM:</strong> {{ $coa->im }}
                            </li>
                            <li class="list-group-item">
                                <strong>PPH:</strong> {{ $coa->pph }}
                            </li>
                            <li class="list-group-item">
                                <strong>MOI:</strong> {{ $coa->moi }}
                            </li>
                            <li class="list-group-item">
                                <strong>IND:</strong> {{ $coa->ind }}
                            </li>
                            <li class="list-group-item">
                                <strong>BIT(225):</strong> {{ $coa->bit225 }}
                            </li>
                            <li class="list-group-item">
                                <strong>PPP:</strong> {{ $coa->ppp }}
                            </li>
                            <li class="list-group-item">
                                <strong>DAG:</strong> {{ $coa->dag }}
                            </li>
                            <li class="list-group-item">
                                <strong>Defects:</strong> {{ $coa->defects }}
                            </li>
                            <li class="list-group-item">
                                <strong>Fruitness:</strong> {{ $coa->fruitness }}
                            </li>
                            <li class="list-group-item">
                                <strong>Bitterness:</strong> {{ $coa->bitterness }}
                            </li>
                            <li class="list-group-item">
                                <strong>Pungency:</strong> {{ $coa->pungency }}
                            </li>
                            <li class="list-group-item">
                                <strong>Campesterol:</strong> {{ $coa->campesterol }}
                            </li>
                            <li class="list-group-item">
                                <strong>Triglecerides Difference ECN42:</strong> {{ $coa->difference_ecn42 }}
                            </li>

                        </ul>


                    </div>
                </div>
            </div>
        </div>
@stop