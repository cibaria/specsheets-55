@extends('layouts.admin')

@section('content')

<div id="coas">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                COA
            </h1>
            <p>A COA is created from a <a href="/coas/templates">Master Template</a>. In order to create a COA you must click on the <a href="/coas/templates">Master Templates</a> button, then select "Create A COA" from the <a href="/coas/templates">Master Template</a> that you choose.</p>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > COA
                </li>
            </ol>
        </div>
    </div>

    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
           <a href="/coas/templates" class="btn btn-info navbar-btn">Master Templates</a>

            </nav>
        </div>
    </div>

        <table id="datatable" class="table table-stripped">
        <thead>
        <tr>
            <td><strong>Lot Number</strong></td>
           <td><strong>COA Description</strong></td>
           <td><strong>Manf Date</strong></td>
           <td><strong>Expiration Date</strong></td>
            <td><strong>Origin</strong></td>
            <td><strong>Master Template</strong></td>
           <td class="no-sort"></td>
        </tr>
        </thead>
        <tbody class="list">

        @foreach($coas as $coa)
            @if(!is_null($coa->coa))
        @if($coa->coa->master_template == 'no')
            <tr>
                <td class="isdone"><a href="/lotlogs/{{$coa->id}}" style="background-color: #ff7518;border-radius: 20px; padding:5px; color:white;font-weight:bold;text-decoration: none;">{{ $coa->lot }}</a></td>
                <td>
                    @if(!is_null($coa->coa))
                        <?php
                                $template = str_replace("_", " ", $coa->coa_template);
                        ?>
                        {{ ucwords($template) }}, {{ $coa->coa->coa_description}}
                    @endif
                </td>
                <td>
                    @if(!is_null($coa->coa))
                        {{ $coa->coa->manf_date }}
                    @endif
                </td>
                <td>{{ $coa->expiration_date }}</td>
                <td>{{ $coa->origin }}</td>
                <?php
                $template = str_replace("_", " ", $coa->coa_template);
                echo "<td>";
                echo ucwords($template);
                echo "</td>";

                ?>
             <td>
                 <div class="btn-group" role="group" aria-label="">
                    <a href="/public/coas/{{ $coa->id }}" class="btn btn-technology">Public COA</a>

                     @if(Auth::user()->group == 100)
                     {{ link_to_route('coas.edit', 'Edit', [$coa->id],['class' => 'btn btn-warning']) }}

                     {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('coas.destroy', $coa->id))) }}
                     {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                     {{ Form::close() }}
                     @endif
                 </div>
             </td>
            </tr>
            @endif
            @endif
        @endforeach

        </tbody>
    </table>


<ul class="pagination"></ul>
</div>
    @stop
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ]
            });
        });
    </script>

    @stop

