@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>COA {{
                    $coa->lot }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/coas/">COA</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($coa, array('method' => 'PATCH', 'route' => array('coas.update', $coa->id))) }}

                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Lot: </strong> {{ $coa->lot }}
                            </li>
                            <li class="list-group-item">
                                <strong>Expiration Date: </strong> {{ $coa->expiration_date}}
                            </li>
                            <li class="list-group-item">
                                <strong>Origin: </strong> {{ $coa->origin}}
                            </li>
                            <?php
                            $template = str_replace( "_", " ", $coa->coa_template );
                            ?>
                            <li class="list-group-item">
                                <strong>Master Template:</strong> {{ ucwords($template) }}
                            </li>
                            <h2>Edit</h2>
                        </ul>

                        @if(!is_null($coa->coa))

                            <div class="form-group">
                                {{ Form::label('coa_description', 'COA Description') }}
                                <?php
                                $template = str_replace( "_", " ", $coa->coa_template );
                                ?>
                                <div style="display: block; width:100%;">
                                    {{ ucwords($template) }}, <input type="text" name="coa_description"
                                                                     value="<?php echo $coa->coa->coa_description; ?>"
                                                                     style="
                                                                       /*display: inline;*/
                                                                       /*width: 88%;*/
                                                                       height: 43px;
                                                                       padding: 10px 18px;
                                                                       font-size: 15px;
                                                                       line-height: 1.42857143;
                                                                       color: #333333;
                                                                       background-color: #ffffff;
                                                                       background-image: none;
                                                                       border: 1px solid #cccccc;
                                                                        border-radius: 0;"
                                            />
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                {{ Form::label('manf_date', 'Manf Date') }}
                                <input type="text" name="manf_date" value="<?php echo $coa->coa->manf_date; ?>"
                                       id="datepicker" autocomplete="off" class="form-control"/>
                            </div>
                        @else
                            <div class="form-group">
                                {{ Form::label('coa_description', 'COA Description') }}
                                <input type="text" name="coa_description" class="form-control"/>
                            </div>
                            <div class="form-group">
                                {{ Form::label('manf_date', 'Manf Date') }}
                                <input type="text" name="manf_date" id="datepicker" autocomplete="off"
                                       class="form-control"/>
                            </div>

                        @endif

                        @switch($coa->coa_template)
                        @case('apricot_kernel')
                        @include('coas.form.template.apricot_kernel')
                        @break

                        @case('almond')
                        @include('coas.form.template.almond')
                        @break

                        @case('avocado')
                        @include('coas.form.template.avocado')
                        @break

                        @case('babassu')
                        @include('coas.form.template.babassu')
                        @break

                        @case('butters')
                        @include('coas.form.template.butters')
                        @break

                        @case('canola')
                        @include('coas.form.template.canola')
                        @break

                        @case('castor')
                        @include('coas.form.template.castor')
                        @break

                        @case('coconut')
                        @include('coas.form.template.coconut')
                        @break

                        @case('corn')
                        @include('coas.form.template.corn')
                        @break

                        @case('cottonseed')
                        @include('coas.form.template.cottonseed')
                        @break

                        @case('flaxseed')
                        @include('coas.form.template.flaxseed')
                        @break

                        @case('hemp')
                        @include('coas.form.template.hemp')
                        @break

                        @case('jojoba')
                        @include('coas.form.template.jojoba')
                        @break

                        @case('macadamia')
                        @include('coas.form.template.macadamia')
                        @break

                        @case('olive_oil')
                        @include('coas.form.template.olive_oil')
                        @break

                        @case('olive_oil_extra_virgin_flavored')
                        @include('coas.form.template.olive_oil_extra_virgin_flavored')
                        @break

                        @case('olive_oil_fused')
                        @include('coas.form.template.olive_oil_fused')
                        @break

                        @case('olive_oil_pure_infused')
                        @include('coas.form.template.olive_oil_pure_infused')
                        @break

                        @case('palm_kernel')
                        @include('coas.form.template.palm_kernel')
                        @break

                        @case('peanut')
                        @include('coas.form.template.peanut')
                        @break

                        @case('rice_bran')
                        @include('coas.form.template.rice_bran')
                        @break

                        @case('safflower')
                        @include('coas.form.template.safflower')
                        @break

                        @case('sesame')
                        @include('coas.form.template.sesame')
                        @break

                        @case('soybean')
                        @include('coas.form.template.soybean')
                        @break

                        @case('sunflower')
                        @include('coas.form.template.sunflower')
                        @break
                        @endswitch

                        {{ Form::submit('Update', ['class' => 'btn btn-default', 'style' => 'float:right', 'autocomplete' => 'off']) }}

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
<<<<<<< HEAD
        <!--end col lg 8 -->
    </div><!--end container fluid-->

=======
    </div>
>>>>>>> coa_update_v2
    </div>
@stop
@section('footer')
    <script>
        $(function () {
            $("#datepicker").datepicker();
            $("#datepicker1").datepicker();
        });
    </script>
@stop