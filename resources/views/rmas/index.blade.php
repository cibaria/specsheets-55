@extends('layouts.admin')

@section('content')

<div id="rmas">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                RMA
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > RMA
                </li>
            </ol>
        </div>
    </div>

    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
                {{ link_to_route('rmas.create', 'Create A New RMA',null, ['class' => 'btn btn-primary navbar-btn']) }}                {{--search--}}

                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class=" search form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-primary" data-sort="rmas">Submit</button>
                </form>
            </nav>
        </div>
    </div>

    <table class="table table-stripped table-responsive table-hover">
        <thead>
        <tr>
           <td><strong>RMA#</strong></td>
           <td><strong>Date</strong></td>
           <td><strong>Request Received By</strong></td>
           <td></td>
        </tr>
        </thead>
        <tbody class="list">
        @foreach($rmas as $rma)
            <tr>
             <td class="rmanumber">{{ $rma->rma_number }}</td>
             <td>{{ $rma->date }}</td>
             <td>{{ $rma->request_received_by }}</td>
             <td>
                 <div class="btn-group" role="group" aria-label="">
                     {{ link_to_route('rmas.show', 'View', [$rma->id],['class' => 'btn btn-success']) }}
                     @if(Auth::user()->group == 100)
                     {{ link_to_route('rma.edit', 'Edit', [$rma->id],['class' => 'btn btn-warning']) }}

                     {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('rmas.destroy', $specsheet->id))) }}
                     {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                     {{ Form::close() }}
                     @endif
                 </div>
             </td>
            </tr>
        @endforeach

        </tbody>
    </table>


<ul class="pagination"></ul>
</div>
    @stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'rmanumber'],
            // pagination
            page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var rmaList = new List('rmas', options);
    </script>
    @stop

