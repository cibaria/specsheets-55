@extends('layouts.admin')

@section('content')

    <div id="rmas">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span>A RMA
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/rmas/">RMA</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10">
                    <div class="col-lg-20">
                        {{ Form::open(['image/save', 'route' => 'specsheets.store','files' => true, 'class' => 'form-horizontal']) }}
                        <fieldset>
                        <div class="form-group">
                        	{{ Form::label('rma_number', 'RMA#') }}
                        	{{ Form::text('rma_number', null, ['class' => 'form-control', 'placeholder' => 'RMA#']) }}
                        </div>

                            <div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                   {{ Form::text('date', null,['class' => 'form-control', 'placeholder' => 'Date', 'id' => 'datepicker']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('request_received_by', 'Request Received By') }}
                                {{ Form::text('request_received_by', null,['class' => 'form-control', 'placeholder' => 'Request Received By']) }}
                            </div>
<h2>Customer Details</h2>

<div class="container-fluid">
            <div class="row">
                <div class="col-md-6" style="padding-left:0px;">
                    <div class="col-lg-8" style="padding-left:0px;">

                    <div class="form-group">
                    	{{ Form::label('company', 'Company') }}
                    	{{ Form::text('company', null, ['class' => 'form-control', 'placeholder' => 'Company']) }}
                    </div>
                            <div class="form-group">
                                {{ Form::label('company_address1', 'Address') }}
                                {{ Form::text('company_address1', null,['class' => 'form-control', 'placeholder' => 'Address']) }}
                                {{ Form::text('company_address2', null, ['class' => 'form-control', 'placeholder' => 'Address'])}}
                            </div>
                            <div class="form-group">
                            	{{ Form::label('city', 'City') }}
                            	{{ Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City']) }}
                            </div>




                            </div>
                            </div>

 						<div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('contact', 'Contact') }}
                                {{ Form::text('contact', null,['class' => 'form-control', 'placeholder' => 'Contact']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('contact_phone', 'Phone') }}
                                {{ Form::text('contact_phone', null,['class' => 'form-control', 'placeholder' => 'Phone']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('email', 'Email') }}
                                {{ Form::text('email', null,['class' => 'form-control', 'placeholder' => 'Email']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('state', 'State') }}
                                {{ Form::text('state', null,['class' => 'form-control', 'placeholder' => 'State']) }}
                            </div>

                            </div><!--col-md-6-->   
                            </div><!--end Customer Details-->
</div>
                            <h2>Product Details</h2>
                            
						<div class="row">
						  <div class="col-md-4">
						  <div class="form-group">
						  	{{ Form::label('issued_by', 'Issued By') }}
						  	{{ Form::text('issued_by', null, ['class' => 'form-control', 'placeholder' => 'Issued By'])}}
						  </div>
						  <div class="form-group">
						  	{{ Form::label('issued_on', 'Issued On') }}
						  	{{ Form::text('issued_on', null, ['class' => 'form-control', 'placeholder' => 'Issued On']) }}
						  </div>
						  <div class="form-group">
						  	{{ Form::label('good_until', 'Good Until') }}
						  	{{ Form::text('good_until', null, ['class' => 'form-control', 'placeholder' => 'Good Until']) }}
						  </div>
						  </div>

						  <div class="col-md-4">
						  <div class="form-group">
						  	{{ Form::label('restocking_fee', '15% Restocking Fee') }}
						  	{{ Form::text('restocking_fee', null, ['class' => 'form-control', 'placeholder' => 'Restocking Fee'])}}
						  </div>
						  <div class="form-group">
						  	{{ Form::label('return_received_on', 'Return Received On') }}
						  	{{ Form::text('return_received_on', null, ['class' => 'form-control', 'placeholder' => 'Return Received On']) }}
						  </div>
						  <div class="form-group">
						  	{{ Form::label('return_received_by', 'Return Received By') }}
						  	{{ Form::text('return_received_by', null, ['class' => 'form-control', 'placeholder' => 'Return Received By']) }}
						  </div>

						  </div>

						  <div class="col-md-4">
<div class="form-group">
						  	{{ Form::label('credit_amount', 'Credit Amount') }}
						  	{{ Form::text('credit_amount', null, ['class' => 'form-control', 'placeholder' => 'Credit Amount'])}}
						  </div>
						  <div class="form-group">
						  	{{ Form::label('credit_issued_by', 'Credit Issued By') }}
						  	{{ Form::text('credit_issued_by', null, ['class' => 'form-control', 'placeholder' => 'Credit Issued By']) }}
						  </div>
						  <div class="form-group">
						  	{{ Form::label('credit_issued_on', 'Credit Issued On') }}
						  	{{ Form::text('credit_issued_on', null, ['class' => 'form-control', 'placeholder' => 'Credit Issued On']) }}
						  </div>						  	
						  <div class="form-group">
						  	{{ Form::label('replacement_sent', 'Replacement Sent') }}
						  	{{ Form::text('replacement_sent', null, ['class' => 'form-control', 'placeholder' => 'Replacement Sent']) }}
						  </div>						  	
						  </div>
						</div>
						<div class="row">
                    		<div class="col-lg-20">
								<div class="form-group">
									{{ Form::label('credit_return_authorized_yes_by', 'Credit/Return Authroized YES') }}
									{{ Form::label('credit_return_authorized_yes_by', 'By') }}
									{{ Form::text('credit_return_authorized_yes_by', null, ['class' => 'form-control', 'placeholder' => 'By']) }}
								</div>
							</div>
						</div>	



                        </fieldset>
                        <hr/>
                        <div class="form-group">
                        {{ Form::submit('Create', array('class' => 'btn btn-default')) }}
                        </div>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end specsheets-->
    @stop