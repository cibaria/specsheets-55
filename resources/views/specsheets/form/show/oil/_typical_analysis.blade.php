<h2>Typical Analysis</h2>

<li class="list-group-item">
    <strong>Free Fatty Acid: </strong> {{ $specsheet->typical_fatty_acid }}
</li>
<li class="list-group-item">
    <strong>Moisture: </strong> {{ $specsheet->typical_moisture2 }}
</li>
<li class="list-group-item">
    <strong>Peroxide Value: </strong> {{ $specsheet->typical_peroxide }}
</li>
<li class="list-group-item">
    <strong>Iodine Value: </strong> {{ $specsheet->typical_iodine }}
</li>
<li class="list-group-item">
    <strong>Saponification Value: </strong> {{ $specsheet->typical_saponification }}
</li>
<li class="list-group-item">
    <strong>Anisidine Value: </strong> {{ $specsheet->typical_anisidine }}
</li>
<li class="list-group-item">
    <strong>Cold Test: </strong> {{ $specsheet->typical_cold }}
</li>
<li class="list-group-item">
    <strong>Refractive Index: </strong> {{ $specsheet->typical_refractive }}
</li>
<li class="list-group-item">
    <strong>Specific Gravity: </strong> {{ $specsheet->typical_gravity }}
</li>
<li class="list-group-item">
    <strong>Oil Stability Index(OSI): </strong> {{ $specsheet->typical_stability }}
</li>
<li class="list-group-item">
    <strong>Smoke Point: </strong> {{ $specsheet->typical_smoke }}
</li>
<li class="list-group-item">
    <strong>Additives: </strong> {{ $specsheet->organoleptic_appearance }}
</li>