<h2>Typical Fatty</h2>
<li class="list-group-item">
    <strong>Myristic Acid: </strong> {{ $specsheet->typical_myristic }}
</li>
<li class="list-group-item">
    <strong>Palmitic Acid: </strong> {{ $specsheet->typical_palmitic }}
</li>
<li class="list-group-item">
    <strong>Palmitoleic Acid: </strong> {{ $specsheet->typical_palmitoleic }}
</li>
<li class="list-group-item">
    <strong>Heptadecanoic Acid: </strong> {{ $specsheet->typical_heptadecanoic }}
</li>
<li class="list-group-item">
    <strong>Heptadecenoic Acid: </strong> {{ $specsheet->typical_heptadecenoic }}
</li>
<li class="list-group-item">
    <strong>Stearic Acid: </strong> {{ $specsheet->typical_stearic }}
</li>
<li class="list-group-item">
    <strong>Oleic Acid: </strong> {{ $specsheet->typical_oleic }}
</li>
<li class="list-group-item">
    <strong>Linoleic Acid: </strong> {{ $specsheet->typical_linoleic }}
</li>
<li class="list-group-item">
    <strong>Linolenic Acid: </strong> {{ $specsheet->typical_linolenic }}
</li>
<li class="list-group-item">
    <strong>Arachidic Acid: </strong> {{ $specsheet->typical_arachidic }}
</li>
<li class="list-group-item">
    <strong>Behenic Acid: </strong> {{ $specsheet->typical_behenic }}
</li>
<li class="list-group-item">
    <strong>Erucic Acid: </strong> {{ $specsheet->typical_erucic }}
</li>
<li class="list-group-item">
    <strong>Lignoceric Acid: </strong> {{ $specsheet->typical_lignoceric }}
</li>