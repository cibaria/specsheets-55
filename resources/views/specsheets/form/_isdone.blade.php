<div class="form-group">
{{ Form::label('isdone', 'Complete?') }}
{{ Form::select('isdone',['' => 'Yes/No', 'Yes' => 'Yes', 'No' => 'No'], '', ['class' => 'form-control']) }}
</div>