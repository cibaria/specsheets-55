<h2>Organoleptic Characteristics</h2>
    <div class="form-group">
        {{ Form::label('organoleptic_color', 'Color:') }}
        {{ Form::text('organoleptic_color', null,['class' => 'form-control', 'placeholder' => 'Color']) }}
    </div>
<div class="form-group">
    {{ Form::label('organoleptic_odor', 'Bouquet/Odor:') }}
    {{ Form::text('organoleptic_odor', null,['class' => 'form-control', 'placeholder' => 'Bouquet/Odor']) }}
</div>
<div class="form-group">
    {{ Form::label('organoleptic_flavor', 'Taste/Flavor:') }}
    {{ Form::text('organoleptic_flavor', null,['class' => 'form-control', 'placeholder' => 'Taste/Flavor']) }}
</div>
<div class="form-group">
    {{ Form::label('organoleptic_appearance', 'Appearance/Texture:') }}
    {{ Form::text('organoleptic_appearance', null,['class' => 'form-control', 'placeholder' => 'Appearance/Texture']) }}
</div>