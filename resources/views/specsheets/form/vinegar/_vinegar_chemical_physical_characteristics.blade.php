<h2>Chemical & Physical Characteristics:</h2>
<div class="form-group">
    {{ Form::label('vinegar_acidity', 'Total Acidity:') }}
    {{ Form::text('vinegar_acidity', null,['class' => 'form-control', 'placeholder' => 'Total Acidity']) }}
</div>
<div class="form-group">
    {{ Form::label('vinegar_ph', 'PH:') }}
    {{ Form::text('vinegar_ph', null,['class' => 'form-control', 'placeholder' => 'PH']) }}
</div>
<div class="form-group">
    {{ Form::label('vinegar_gravity', 'Specific Gravity:') }}
    {{ Form::text('vinegar_gravity', null,['class' => 'form-control', 'placeholder' => 'Specific Gravity']) }}
</div>
<div class="form-group">
    {{ Form::label('vinegar_matters', 'Foreign Matters:') }}
    {{ Form::text('vinegar_matters', null,['class' => 'form-control', 'placeholder' => 'Foreign Matters']) }}
</div>
<div class="form-group">
    {{ Form::label('vinegar_metals', 'Heavy Metals:') }}
    {{ Form::text('vinegar_metals', null,['class' => 'form-control', 'placeholder' => 'Heavy Metals:']) }}
</div>
<div class="form-group">
    {{ Form::label('vinegar_dry', 'Total Dry Extract:') }}
    {{ Form::text('vinegar_dry', null,['class' => 'form-control', 'placeholder' => 'Total Dry Extract']) }}
</div>
<div class="form-group">
    {{ Form::label('vinegar_sulphurous', 'Total Sulphurous Anhydride:') }}
    {{ Form::text('vinegar_sulphurous', null,['class' => 'form-control', 'placeholder' => 'Total Sulphurous Anhydride']) }}
</div>
<div class="form-group">
    {{ Form::label('ashes', 'Ash:') }}
    {{ Form::text('ashes', null,['class' => 'form-control', 'placeholder' => 'Ash']) }}
</div>
<div class="form-group">
    {{ Form::label('chemical_grain', 'Grain:') }}
    {{ Form::text('chemical_grain', null,['class' => 'form-control', 'placeholder' => 'Grain']) }}
</div>
<div class="form-group">
    {{ Form::label('chemical_alcohol', 'Residual Alcohol:') }}
    {{ Form::text('chemical_alcohol', null,['class' => 'form-control', 'placeholder' => 'Residual Alcohol']) }}
</div>
<div class="form-group">
    {{ Form::label('chemical_sugar_free_extract', 'Sugar Free Dry Extract:') }}
    {{ Form::text('chemical_sugar_free_extract', null,['class' => 'form-control', 'placeholder' => 'Sugar Free Dry Extract']) }}
</div>
<div class="form-group">
    {{ Form::label('vinegar_density', 'Density:') }}
    {{ Form::text('vinegar_density', null,['class' => 'form-control', 'placeholder' => 'Density']) }}
</div>
<div class="form-group">
    {{ Form::label('chemical_brix', 'Brix:') }}
    {{ Form::text('chemical_brix', null,['class' => 'form-control', 'placeholder' => 'Brix']) }}
</div>
<div class="form-group">
    {{ Form::label('chemical_alcohol_degree', 'Developed Alcohol Degree:') }}
    {{ Form::text('chemical_alcohol_degree', null,['class' => 'form-control', 'placeholder' => 'Developed Alcohol Degree']) }}
</div>
<div class="form-group">
    {{ Form::label('chemical_reduced_dry_extract', 'Reduced Dry Extract:') }}
    {{ Form::text('chemical_reduced_dry_extract', null,['class' => 'form-control', 'placeholder' => 'Reduced Dry Extract']) }}
</div>
<div class="form-group">
    {{ Form::label('reducing_sugars', 'Reducing Sugars:') }}
    {{ Form::text('reducing_sugars', null,['class' => 'form-control', 'placeholder' => 'Reducing Sugars']) }}
</div>
<div class="form-group">
    {{ Form::label('chemical_rif', 'Rifractometric at 20°C:') }}
    {{ Form::text('chemical_rif', null,['class' => 'form-control', 'placeholder' => 'Rifractometric at 20°C']) }}
</div>