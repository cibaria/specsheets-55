
    <h2>Nutrition Facts</h2>
    <br/>
    <table width="100%" border="1" bordercolor="#97b854" cellpadding="5" cellspacing="0">
        <tr bgcolor="#97b854" style="padding:10px; text-align:center;color:white;">
            <td>Nutrient</td>
            <td>Unit</td>
            <td>Value per 100g</td>
            <td>Value per 100ml</td>
        </tr>
        <tr>
            <td>Total Fat</td>
            <td>g</td>
            <td> {{ Form::text('vinegar_nutrition_fat_100g', null, array('placeholder' => 'Total Fat 100g')) }}</td>
            <td> {{ Form::text('vinegar_nutrition_fat_100ml', null, array('placeholder' => 'Total Fat 100ml')) }}</td>
        </tr>
        <tr>
            <td>Sodium</td>
            <td>mg</td>
            <td>{{ Form::text('vinegar_nutrition_sodium_100g', null, array('placeholder' => 'Sodium 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_sodium_100ml', null, array('placeholder' => 'Sodium 100ml')) }}</td>
        </tr>
        <tr>
            <td>Total Carbohydrates</td>
            <td>g</td>
            <td> {{ Form::text('vinegar_nutrition_carb_100g', null, array('placeholder' => 'Carbohydrates 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_carb_100ml', null, array('placeholder' => 'Carbohydrates 100ml')) }}</td>
        </tr>
        <tr>
            <td>Sugars</td>
            <td>g</td>
            <td>{{ Form::text('vinegar_nutrition_sugars_100g', null, array('placeholder' => 'Sugars 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_sugars_100ml', null, array('placeholder' => 'Sugars 100ml')) }}</td>
        </tr>
        <tr>
            <td>Protein</td>
            <td>g</td>
            <td>{{ Form::text('vinegar_nutrition_protein_100g', null, array('placeholder' => 'Proteins 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_protein_100ml', null, array('placeholder' => 'Proteins 100ml')) }}</td>
        </tr>
        <tr>
            <td>Kcal</td>
            <td>kcal</td>
            <td>{{ Form::text('vinegar_nutrition_kcal_100g', null, array('placeholder' => 'Kcal 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_kcal_100ml', null, array('placeholder' => 'Kcal 100ml')) }}</td>
        </tr>
        <tr>
            <td>Calories</td>
            <td>cal</td>
            <td> {{ Form::text('vinegar_nutrition_calories_100g', null, array('placeholder' => 'Calories 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_calories_100ml', null, array('placeholder' => 'Calories 100ml')) }}</td>
        </tr>
        <tr>
            <td>Calories From Fat</td>
            <td>cal</td>
            <td>{{ Form::text('vinegar_nutrition_calories_fat_100g', null, array('placeholder' => 'Calories From Fat 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_calories_fat_100ml', null, array('placeholder' => 'Calories From Fat 100ml')) }}</td>
        </tr>
        <tr>
            <td>Vitamin A</td>
            <td>%</td>
            <td>{{ Form::text('vinegar_nutrition_vitamin_a_100g', null, array('placeholder' => 'Vitamin A 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_vitamin_a_100ml', null, array('placeholder' => 'Vitamin A 100ml')) }}</td>
        </tr>
        <tr>
            <td>Vitamin C</td>
            <td>%</td>
            <td>{{ Form::text('vinegar_nutrition_vitamin_c_100g', null, array('placeholder' => 'Vitamin C 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_vitamin_c_100ml', null, array('placeholder' => 'Vitamin C 100ml')) }}</td>
        </tr>
        <tr>
            <td>Calcium</td>
            <td>%</td>
            <td> {{ Form::text('vinegar_nutrition_calcium_100g', null, array('placeholder' => 'Calcium 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_calcium_100ml', null, array('placeholder' => 'Calcium 100ml')) }}</td>
        </tr>
        <tr>
            <td>Iron</td>
            <td>%</td>
            <td> {{ Form::text('vinegar_nutrition_iron_100g', null, array('placeholder' => 'Irong 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_iron_100ml', null, array('placeholder' => 'Iron 100ml')) }}</td>
        </tr>
        <tr>
            <td>Cholesterol</td>
            <td>mg</td>
            <td>{{ Form::text('vinegar_nutrition_cholesterol_100g', null, array('placeholder' => 'Cholesterol 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_cholesterol_100ml', null, array('placeholder' => 'Cholesterol 100ml')) }}</td>
        </tr>
        <tr>
            <td>Fiber</td>
            <td>g</td>
            <td>{{ Form::text('vinegar_nutrition_fiber_100g', null, array('placeholder' => 'Fiber 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_fiber_100ml', null, array('placeholder' => 'Fiber 100ml')) }}</td>
        </tr>
        <tr>
            <td>Moisture</td>
            <td>g</td>
            <td>{{ Form::text('vinegar_nutrition_moisture_100g', null, array('placeholder' => 'Moisture 100g')) }}</td>
            <td>{{ Form::text('vinegar_nutrition_moisture_100ml', null, array('placeholder' => 'Moisture 100ml')) }}</td>
        </tr>
    </table>
