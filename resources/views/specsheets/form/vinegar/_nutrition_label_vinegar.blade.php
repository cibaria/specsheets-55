<h2>Nutrition Label</h2>
<div class="form-group">
    {{ Form::label('nutrition', 'Nutrition') }}
    {{--this is where the file upload goes--}}
</div>

<div class="form-group">
    {{ Form::label('nutrition_fact1', 'Nutrition Fact 1') }}
    {{ Form::text('nutrition_fact1', null,['class' => 'form-control', 'placeholder' => 'Nutrition Fact 1']) }}
</div>
<div class="form-group">
    {{ Form::label('nutrition_fact2', 'Nutrition Fact 2') }}
    {{ Form::text('nutrition_fact2', null,['class' => 'form-control', 'placeholder' => 'Nutrition Fact 2']) }}
</div>
<div class="form-group">
    {{ Form::label('nutrition_fact3', 'Nutrition Fact 3') }}
    {{ Form::text('nutrition_fact3', null,['class' => 'form-control', 'placeholder' => 'Nutrition Fact 3']) }}
</div>