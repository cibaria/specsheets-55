<h2>USDA NDB (National Nutrition Database)</h2>
    <br/>
    <table width="100%" border="1" bordercolor="#97b854" cellpadding="5" cellspacing="0">
        <tr bgcolor="#97b854" style="padding:10px; text-align:center;color:white;">
            <td>Nutrient</td>
            <td>Unit</td>
            <td>Value per 100.0g</td>
            <td>Tbsp 13.5g</td>
        </tr>
        <tr>
            <td colspan="4" bgcolor="#d6e3bc"> Proximates</td>
        </tr>
        <tr>
            <td>Water</td>
            <td>g</td>
            <td> {{ Form::text('water_value',null,['placeholder'=> 'Water Value']) }}</td>
            <td> {{ Form::text('water_tbsp',null,['placeholder'=> 'Water tbsp']) }}</td>
        </tr>
        <tr>
            <td>Energy</td>
            <td>kcal</td>
            <td>{{ Form::text('energy_value',null,['placeholder'=> 'energy Value']) }}</td>
            <td>{{ Form::text('energy_tbsp',null,['placeholder'=> 'energy tbsp']) }}</td>
        </tr>
        <tr>
            <td>Protein</td>
            <td>g</td>
            <td> {{ Form::text('protein_value',null,['placeholder'=> 'protein Value']) }}</td>
            <td>{{ Form::text('protein_tbsp',null,['placeholder'=> 'protein tbsp']) }}</td>
        </tr>
        <tr>
            <td>Total lipid (fat)</td>
            <td>g</td>
            <td>{{ Form::text('totallipid_value',null,['placeholder'=> 'totallipid Value']) }}</td>
            <td>{{ Form::text('totallipid_tbsp',null,['placeholder'=> 'totallipid tbsp']) }}</td>
        </tr>
        <tr>
            <td>Carbohydrate, by difference</td>
            <td>g</td>
            <td>{{ Form::text('carbohydrate_value',null,['placeholder'=> 'carbohydrate Value']) }}</td>
            <td>{{ Form::text('carbohydrate_tbsp',null,['placeholder'=> 'carbohydrate tbsp']) }}</td>
        </tr>
        <tr>
            <td>Fiber, total dietary</td>
            <td>g</td>
            <td>{{ Form::text('fiber_value',null,['placeholder'=> 'fiber Value']) }}</td>
            <td>{{ Form::text('fiber_tbsp',null,['placeholder'=> 'fiber tbsp']) }}</td>
        </tr>
        <tr>
            <td>Sugars, total</td>
            <td>g</td>
            <td> {{ Form::text('sugars_value',null,['placeholder'=> 'sugars Value']) }}</td>
            <td>{{ Form::text('sugars_tbsp',null,['placeholder'=> 'sugars tbsp']) }}</td>
        </tr>
        <tr>
            <td colspan="4" bgcolor="#d6e3bc">Minerals</td>
        </tr>
        <tr>
            <td>Calcium, Ca</td>
            <td>mg</td>
            <td>{{ Form::text('calcium_value',null,['placeholder'=> 'calcium Value']) }}</td>
            <td>{{ Form::text('calcium_tbsp',null,['placeholder'=> 'calcium tbsp']) }}</td>
        </tr>
        <tr>
            <td>Iron, Fe</td>
            <td>mg</td>
            <td>{{ Form::text('iron_value',null,['placeholder'=> 'iron Value']) }}</td>
            <td>{{ Form::text('iron_tbsp',null,['placeholder'=> 'iron tbsp']) }}</td>
        </tr>
        <tr>
            <td>Magnesium, Mg</td>
            <td>mg</td>
            <td>{{ Form::text('magnesium_value',null,['placeholder'=> 'magnesium Value']) }}</td>
            <td>{{ Form::text('magnesium_tbsp',null,['placeholder'=> 'magnesium tbsp']) }}</td>
        </tr>
        <tr>
            <td>Phosphorus, P</td>
            <td>mg</td>
            <td> {{ Form::text('phosphorus_value',null,['placeholder'=> 'phosphorus Value']) }}</td>
            <td>{{ Form::text('phosphorus_tbsp',null,['placeholder'=> 'phosphorus tbsp']) }}</td>
        </tr>
        <tr>
            <td>Potassium, K</td>
            <td>mg</td>
            <td> {{ Form::text('potassium_value',null,['placeholder'=> 'potassium Value']) }}</td>
            <td>{{ Form::text('potassium_tbsp',null,['placeholder'=> 'potassium tbsp']) }}</td>
        </tr>
        <tr>
            <td>Sodium, Na</td>
            <td>mg</td>
            <td>{{ Form::text('sodium_value',null,['placeholder'=> 'sodium Value']) }}</td>
            <td>{{ Form::text('sodium_tbsp',null,['placeholder'=> 'sodium tbsp']) }}</td>
        </tr>
        <tr>
            <td>Zinc, Zn</td>
            <td>mg</td>
            <td>{{ Form::text('zinc_value',null,['placeholder'=> 'zinc Value']) }}</td>
            <td>{{ Form::text('zinc_tbsp',null,['placeholder'=> 'zinc tbsp']) }}</td>
        </tr>
        <tr>
            <td colspan="4" bgcolor="#d6e3bc">Vitamins</td>
        </tr>
        <tr>
            <td>Vitamin C, total ascorbic acid</td>
            <td>mg</td>
            <td>{{ Form::text('vitaminc_value',null,['placeholder'=> 'vitaminc Value']) }}</td>
            <td>{{ Form::text('vitaminc_tbsp',null,['placeholder'=> 'vitaminc tbsp']) }}</td>
        </tr>
        <tr>
            <td>Thiamin</td>
            <td>mg</td>
            <td>{{ Form::text('thiamin_value',null,['placeholder'=> 'thiamin Value']) }}</td>
            <td>{{ Form::text('thiamin_tbsp',null,['placeholder'=> 'thiamin tbsp']) }}</td>
        </tr>
        <tr>
            <td>Riboflavin</td>
            <td>mg</td>
            <td>{{ Form::text('riboflavin_value',null,['placeholder'=> 'riboflavin Value']) }}</td>
            <td>{{ Form::text('riboflavin_tbsp',null,['placeholder'=> 'riboflavin tbsp']) }}</td>
        </tr>
        <tr>
            <td>Niacin</td>
            <td>mg</td>
            <td>{{ Form::text('niacin_value',null,['placeholder'=> 'niacin Value']) }}</td>
            <td>{{ Form::text('niacin_tbsp',null,['placeholder'=> 'niacin tbsp']) }}</td>
        </tr>
        <tr>
            <td>Vitamin B-6</td>
            <td>mg</td>
            <td>{{ Form::text('vitaminb6_value',null,['placeholder'=> 'vitaminb6 Value']) }}</td>
            <td>{{ Form::text('vitaminb6_tbsp',null,['placeholder'=> 'vitaminb6 tbsp']) }}</td>
        </tr>
        <tr>
            <td>Folate, DFE</td>
            <td>µg</td>
            <td>{{ Form::text('folate_value',null,['placeholder'=> 'folate Value']) }}</td>
            <td>{{ Form::text('folate_tbsp',null,['placeholder'=> 'folate tbsp']) }}</td>
        </tr>
        <tr>
            <td>Vitamin B-12</td>
            <td>µg</td>
            <td>{{ Form::text('vitaminb12_value',null,['placeholder'=> 'vitaminb12 Value']) }}</td>
            <td>{{ Form::text('vitaminb12_tbsp',null,['placeholder'=> 'vitaminb12 tbsp']) }}</td>
        </tr>
        <tr>
            <td>Vitamin A, RAE</td>
            <td>µg</td>
            <td>{{ Form::text('vitaminar_value',null,['placeholder'=> 'vitaminar Value']) }}</td>
            <td>{{ Form::text('vitaminar_tbsp',null,['placeholder'=> 'vitaminar tbsp']) }}</td>
        </tr>
        <tr>
            <td>Vitamin A, IU</td>
            <td>IU</td>
            <td> {{ Form::text('vitaminai_value',null,['placeholder'=> 'vitaminai Value']) }}</td>
            <td>{{ Form::text('vitaminai_tbsp',null,['placeholder'=> 'vitaminai tbsp']) }}</td>
        </tr>
        <tr>
            <td>Vitamin E (alpha-tocopherol)</td>
            <td>mg</td>
            <td>{{ Form::text('vitamine_value',null,['placeholder'=> 'vitamine Value']) }}</td>
            <td>{{ Form::text('vitamine_tbsp',null,['placeholder'=> 'vitamine tbsp']) }}</td>
        </tr>
        <tr>
            <td>Vitamin D (D2+D3)</td>
            <td>µg</td>
            <td>{{ Form::text('vitamind2_value',null,['placeholder'=> 'vitamind2 Value']) }}</td>
            <td>{{ Form::text('vitamind2_tbsp',null,['placeholder'=> 'vitamind2 tbsp']) }}</td>
        </tr>
        <tr>
            <td>Vitamin D</td>
            <td>IU</td>
            <td>{{ Form::text('vitamind_value',null,['placeholder'=> 'vitamind_value Value']) }}</td>
            <td>{{ Form::text('vitamind_tbsp',null,['placeholder'=> 'vitamind_value tbsp']) }}</td>
        </tr>
        <tr>
            <td>Vitamin K (phylloquinone)</td>
            <td>µg</td>
            <td>{{ Form::text('vitamink_value',null,['placeholder'=> 'vitamink Value']) }}</td>
            <td>{{ Form::text('vitamink_tbsp',null,['placeholder'=> 'vitamink tbsp']) }}</td>
        </tr>
        <tr>
            <td colspan="4" bgcolor="#d6e3bc">Lipids</td>
        </tr>
        <tr>
            <td>Fatty acids, total saturated</td>
            <td>g</td>
            <td>{{ Form::text('fattysaturated_value',null,['placeholder'=> 'fattysaturated Value']) }}</td>
            <td>{{ Form::text('fattysaturated_tbsp',null,['placeholder'=> 'fattysaturated tbsp']) }}</td>
        </tr>
        <tr>
            <td>Fatty acids, total monounsaturated</td>
            <td>g</td>
            <td>{{ Form::text('fattymonoun_value',null,['placeholder'=> 'fattymonoun Value']) }}</td>
            <td>{{ Form::text('fattymonoun_tbsp',null,['placeholder'=> 'fattymonoun tbsp']) }}</td>
        </tr>
        <tr>
            <td>Fatty acids, polyunsaturated</td>
            <td>g</td>
            <td>{{ Form::text('fattypoly_value',null,['placeholder'=> 'fattypoly Value']) }}</td>
            <td>{{ Form::text('fattypoly_tbsp',null,['placeholder'=> 'fattypoly tbsp']) }}</td>
        </tr>
        <tr>
            <td>Cholesterol</td>
            <td>mg</td>
            <td>{{ Form::text('cholesterol_value',null,['placeholder'=> 'cholesterol Value']) }}</td>
            <td>{{ Form::text('cholesterol_tbsp',null,['placeholder'=> 'cholesterol tbsp']) }}</td>
        </tr>
        <tr>
            <td colspan="4" bgcolor="#d6e3bc">Other</td>
        </tr>
        <tr>
            <td>Caffeine</td>
            <td>mg</td>
            <td>{{ Form::text('caffeine_value',null,['placeholder'=> 'caffeine Value']) }}</td>
            <td>{{ Form::text('caffeine_tbsp',null,['placeholder'=> 'caffeine tbsp']) }}</td>
        </tr>
    </table>