<h2>Typical Analysis</h2>
<div class="form-group">
    {{ Form::label('typical_fatty_acid', 'Free Fatty Acid') }}
    {{ Form::text('typical_fatty_acid', null,['class' => 'form-control', 'placeholder' => 'Free Fatty Acid']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_moisture2', 'Moisture') }}
    {{ Form::text('typical_moisture2', null,['class' => 'form-control', 'placeholder' => 'Moisture']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_peroxide', 'Peroxide Value') }}
    {{ Form::text('typical_peroxide', null,['class' => 'form-control', 'placeholder' => 'Peroxide Value']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_iodine', 'Iodine Value') }}
    {{ Form::text('typical_iodine', null,['class' => 'form-control', 'placeholder' => 'Iodine Value']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_saponification', 'Saponification Value') }}
    {{ Form::text('typical_saponification', null,['class' => 'form-control', 'placeholder' => 'Saponification Value']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_anisidine', 'Anisidine Value') }}
    {{ Form::text('typical_anisidine', null,['class' => 'form-control', 'placeholder' => 'Anisidine Value']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_cold', 'Cold Test') }}
    {{ Form::text('typical_cold', null,['class' => 'form-control', 'placeholder' => 'Cold Test']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_refractive', 'Refractive Index') }}
    {{ Form::text('typical_refractive', null,['class' => 'form-control', 'placeholder' => 'Refractive Index']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_gravity', 'Specific Gravity') }}
    {{ Form::text('typical_gravity', null,['class' => 'form-control', 'placeholder' => 'Specific Gravity']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_stability', 'Oil Stability Index(OSI)') }}
    {{ Form::text('typical_stability', null,['class' => 'form-control', 'placeholder' => 'Oil Stability Index(OSI)']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_smoke', 'Smoke Point') }}
    {{ Form::text('typical_smoke', null,['class' => 'form-control', 'placeholder' => 'Smoke Point']) }}
</div>
<div class="form-group">
    {{ Form::label('typical_additives', 'Additives') }}
    {{ Form::text('typical_additives', null,['class' => 'form-control', 'placeholder' => 'Additives']) }}
</div>