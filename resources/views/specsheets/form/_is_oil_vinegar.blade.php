<div class="form-group">
    {{ Form::label('is_oil_vinegar', 'Oil or Vinegar?') }}
    {{ Form::select('is_oil_vinegar', ['' => 'Choose', '1' => 'Oil', '2' => 'Vinegar'], '', ['class' => 'form-control']) }}
</div>