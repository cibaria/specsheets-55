<div class="form-group">
    {{ Form::label('title', 'Specsheet Title') }}
    {{ Form::text('title', null,['class' => 'form-control', 'placeholder' => 'Title']) }}
</div>
<div class="form-group">
    {{ Form::label('sku', 'Sku') }}
    {{ Form::text('sku', null,['class' => 'form-control', 'placeholder' => 'Sku']) }}
</div>
<div class="form-group">
    {{ Form::label('revised', 'Revised') }}
    {{ Form::text('revised', null,['class' => 'form-control', 'placeholder' => 'Revised']) }}
</div>
<div class="form-group">
    {{ Form::label('description', 'Product Description') }}
    {{ Form::text('description', null,['class' => 'form-control', 'placeholder' => 'Product Description']) }}
</div>
<div class="form-group">
    {{ Form::label('bullet_point1', 'Bullet Point 1') }}
    {{ Form::text('bullet_point1', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 1']) }}
</div>
<div class="form-group">
    {{ Form::label('bullet_point2', 'Bullet Point 2') }}
    {{ Form::text('bullet_point2', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 2']) }}
</div>
<div class="form-group">
    {{ Form::label('bullet_point3', 'Bullet Point 3') }}
    {{ Form::text('bullet_point3', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 3']) }}
</div>
<div class="form-group">
    {{ Form::label('bullet_point4', 'Bullet Point 4') }}
    {{ Form::text('bullet_point4', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 4']) }}
</div>
<div class="form-group">
    {{ Form::label('bullet_point5', 'Bullet Point 5') }}
    {{ Form::text('bullet_point5', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 5']) }}
</div>
<div class="form-group">
    {{ Form::label('bullet_point6', 'Bullet Point 6') }}
    {{ Form::text('bullet_point6', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 6']) }}
</div>
<div class="form-group">
    {{ Form::label('bullet_point7', 'Bullet Point 7') }}
    {{ Form::text('bullet_point7', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 7']) }}
</div>
<div class="form-group">
    {{ Form::label('bullet_point8', 'Bullet Point 8') }}
    {{ Form::text('bullet_point8', null,['class' => 'form-control', 'placeholder' => 'Bullet Point 8']) }}
</div>
<div class="form-group">
    {{ Form::label('storage', 'Storage') }}
    {{ Form::text('storage', null,['class' => 'form-control', 'placeholder' => 'Storage']) }}
</div>
<div class="form-group">
    {{ Form::label('shelf_life', 'Shelf Life') }}
    {{ Form::text('shelf_life', null,['class' => 'form-control', 'placeholder' => 'Shelf Life']) }}
</div>
<div class="form-group">
    {{ Form::label('sewer', 'Sewer Sludge and Irradiation Statement:') }}
    {{ Form::text('sewer', null,['class' => 'form-control', 'placeholder' => 'Sewer Sludge and Irradiation Statement']) }}
</div>
<div class="form-group">
    {{ Form::label('applications', 'Applications for the Product') }}
    {{ Form::text('applications', null,['class' => 'form-control', 'placeholder' => 'Applications for the Product']) }}
</div>
<div class="form-group">
    {{ Form::label('country', 'Country of Origin:') }}
    {{ Form::text('country', null,['class' => 'form-control', 'placeholder' => 'Country']) }}
</div>