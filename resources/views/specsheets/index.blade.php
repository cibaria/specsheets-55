@extends('layouts.admin')

@section('content')

<div id="specsheets">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Specsheets
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Specsheets
                </li>
            </ol>
        </div>
    </div>

    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
            @if(Auth::user()->group != 20)
                {{ link_to_route('specsheets.create', 'Create A New Specsheet',null, ['class' => 'btn btn-primary navbar-btn']) }}
            @endif                
                {{--search

                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class=" search form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                </form>--}}
            </nav>
        </div>
    </div>

        <table id="datatable" class="table table-stripped">
        <thead>
        <tr>
           <td><strong>Done?</strong></td>
           <td><strong>Website</strong></td>
           <td><strong>Title</strong></td>
           <td><strong>Description</strong></td>
           <td><strong>Reviewed</strong></td>
           <td class="no-sort"></td>
        </tr>
        </thead>
        <tbody class="list">
        @foreach($specsheets as $specsheet)
            <tr>
             <td class="isdone">{{ $specsheet->isdone }}</td>
             <td>{{ $specsheet->website }}</td>
             <td class="specsheet">{{ $specsheet->title }}</td>
             <td>{{ $specsheet->description }}</td>
             <td>{{ $specsheet->reviewed }}</td>
             <td>
                 <div class="btn-group" role="group" aria-label="">
                     {{ link_to_route('specsheets.show', 'View', [$specsheet->id],['class' => 'btn btn-success']) }}
                     @if(Auth::user()->group == 100)
                     {{ link_to_route('specsheets.edit', 'Edit', [$specsheet->id],['class' => 'btn btn-warning']) }}

                     {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('specsheets.destroy', $specsheet->id))) }}
                     {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                     {{ Form::close() }}
                     @endif
                 </div>
             </td>
            </tr>
        @endforeach

        </tbody>
    </table>


<ul class="pagination"></ul>
</div>
    @stop

{{--end content--}}


{{--footer--}}
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop

