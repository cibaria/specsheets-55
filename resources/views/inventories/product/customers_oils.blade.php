@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Customers Oils
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Customers Oils
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to('inventories/count/customers_oils', 'New Customer Oil Count', ['class' => 'btn btn-primary navbar-btn']) }}
                     {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-warning navbar-btn']) }}
                    @if(Auth::user()->group == 100)

                    {{ link_to('inventories/discontinued/customers_oils', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/customers_oils', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                    @endif
                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>
        <div style="color:"></div>
        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>Total Val</strong></td>
                <td><strong>EXP</strong></td>
                <td><strong></strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($customers_oils as $customers_oil)
                @if($customers_oil->discontinued != 1)
                <tr>
                    <td class="lot">{{ $customers_oil->size }}</td>
                    <td class="so">{{ $customers_oil->oil_type }}</td>
                    <td>
                    @foreach($customers_oil->history as $history)
                        @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                        {{ $history->count }}</td>
                        @endif
                    @endforeach
                    </td>
                    <td class="originallot">{{ $customers_oil->lot }}</td>
                    <td>{{ $customers_oil->area }}</td>
                    <td class="supplier">
                    <?php
                        //calculate total value
                    foreach ($customers_oil->history as $history) {
                        if($history == $customers_oil->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year ) {
                        $total_value = $history->count;
                        echo number_format((float)$total_value, 3);
                        }
                    }
                     ?>
                    </td>
                    <td class="origin">{{ $customers_oil->expiration_date }}</td>

                    <td>
                        <div class="btn-group" role="group" aria-label="">
                                <a href="/inventories/edit/customers_oils/<?php echo $customers_oil->id ?>" class="btn btn-warning">Edit</a>
                            @if(Auth::user()->group == 100 || Auth::user()->group == 50)

                                {{ Form::model($customers_oil, array('method' => 'PATCH', 'route' => array('customers_oilsupdates.update', $customers_oil->id))) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('id', $customers_oil->id) }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('expiration_date') }}
                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                                {{ Form::close() }}
                            @endif
                        </div>
                </tr>
                @endif
            @endforeach
            <tr>
            <td><h3>Total</h3></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>
            </strong></td>
            </tr>
            </tbody>
        </table>
    </div>
@stop
{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
