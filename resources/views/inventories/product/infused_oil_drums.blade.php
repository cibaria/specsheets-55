@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Infused Oil Drums
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Infused Oil Drums
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                    {{ link_to('inventories/count/infused_oil_drums', 'New Infused Oil Drum Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-danger navbar-btn']) }}
                    @if(Auth::user()->group == 100)
                    {{ link_to('inventories/cost/infused_oil_drums', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}}
                    {{ link_to('inventories/discontinued/infused_oil_drums', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/infused_oil_drums', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                    @endif


                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                <td><strong>EXP</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($infused_oil_drums as $infused_oil_drum)
                @if($infused_oil_drum->discontinued != 1)

                <tr>
                    <td class="lot">{{ $infused_oil_drum->size }}</td>
                    <td class="so">{{ $infused_oil_drum->oil_type }}</td>
                    <td>
                    @foreach($infused_oil_drum->history as $history)
                    @if($history->month === Carbon::now()->month && $history->year === Carbon::now()->year)
                    {{ $history->count }}</td>
                    @endif
                    @endforeach
                    </td>
                    <td class="originallot">{{ $infused_oil_drum->lot }}</td>
                    <td class="originallot">{{ $infused_oil_drum->area }}</td>
                    <td class="oiltype">{{ number_format((float)$infused_oil_drum->infused_oil_drums_cost_per, 3) }}</td>
                    <td class="supplier">
                        <?php
                        //calculate total value
                        foreach ($infused_oil_drum->history as $history) {
                            if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year){
                                $total_value = $history->count * $infused_oil_drum->infused_oil_drums_cost_per;
                                echo number_format($total_value, 3);
                            } 
                        }
                        ?>
                    </td>
                    <td class="origin">{{ $infused_oil_drum->expiration_date }}</td>

                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            <a href="/inventories/edit/infused_oil_drums/<?php echo $infused_oil_drum->id ?>" class="btn btn-warning">Edit</a>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                          {{ Form::model($infused_oil_drum, array('method' => 'PATCH', 'route' => array('infused_oil_drumssupdates.update', $infused_oil_drum->id))) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('id', $infused_oil_drum->id) }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('infused_oil_drums_cost_per', "0") }}
                                {{ Form::hidden('expiration_date') }}
                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        @endif
                        </div>
                </tr>
                @endif
            @endforeach
            <tr>
                <td><h3>Total</h3></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>{{ number_format($total->get_total_infused_oil_drums(), 3) }}</strong></td>
            </tr>
            </tbody>
        </table>

        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
