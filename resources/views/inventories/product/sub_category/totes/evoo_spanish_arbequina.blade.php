@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    {{ $title }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >{{ $title }}
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
            {{-- nav --}}
                @include('inventories/product/sub_category/partials/nav')
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td>
                  <strong>Area</strong>
                </td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                <td><strong>EXP</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($data as $item)
                @if($item->discontinued != 1)

                <tr>
                    <td class="lot">{{ $item->size }}</td>
                    <td class="so">{{ $item->oil_type }}</td>
                    <td>
                    @foreach($item->history as $history)
                    @if($history == $item->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                    {{ $history->count }}</td>
                    @endif
                    @endforeach
                    </td>
                    <td class="originallot">{{ $item->lot }}</td>
                    <td>
                      {{ $item->area }}
                    </td>

                    <td class="oiltype"></td>
                    <td class="supplier">
                        <?php
                        //calculate total value
                        foreach ($item->history as $history) {
                            if($history == $item->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year){
                                $total_value = $history->count * $item->totes_cost_per;
                                echo number_format($total_value, 3);
                            }
                        }
                        ?>
                    </td>
                    <td class="origin">{{ $item->expiration_date }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                                <a href="/inventories/edit/totes/<?php echo $item->id ?>" class="btn btn-warning">Edit</a>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)

                       {{ Form::model($item, array('method' => 'PATCH', 'route' => array('toteupdates.update', $item->id))) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('id', $item->id) }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('totes_cost_per',"0") }}
                                {{ Form::hidden('expiration_date') }}
                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        @endif
                        </div>


                </tr>
                @endif
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong></strong></td>
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
