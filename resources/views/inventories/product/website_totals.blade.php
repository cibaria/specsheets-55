@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Website Totals
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories/">Inventory</a> >
                        Website Totals
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                    <a href="/inventories/print/website_totals/" class="btn btn-success navbar-btn">Print</a>
                    {{ link_to('inventories/pdf/website_totals', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}

                </nav>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <table class="table  table-responsive table-hover">
                            <thead>
                            <tr>
                                <td><span style="font-size:32px;">Website Totals</span></td>

                                <td></td>
                            </tr>
                            </thead>
                            <tbody class="list">

                            <tr>
                                <td><strong>Olive Oils</strong></td>
                                <td class="lot">${{ number_format($total->get_total_website_olive_oils(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Infused</strong></td>
                                <td>${{ number_format($total->get_total_web_flavored_and_infused(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Vinegars</strong></td>
                                <td>${{ number_format($total->get_total_website_vinegars(), 3) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Other</strong></td>
                                <td>${{ number_format($total->get_total_website_other(), 3) }}</td>
                            </tr>

                            <hr />
                            <tr>
                                <td><span style="font-size:32px;">Total</span></td>
                                <td><span style="font-size:32px;">${{ number_format($total->get_website_totals(), 3) }}</span></td>
                            </tr>


                            </tbody>
                        </table>

                    </div>
                </div>
                {{-- second column --}}
                <div class="col-md-6">
            
                <ul class="list-group">


              
                </ul>

                {{-- incoming product qc form --}}
               

            
                </div><!--end col-md-6-->
            
            </div>
        </div>
@stop