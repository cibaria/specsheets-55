@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Totes
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Totes
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                @include('inventories/product/sub_category/partials/nav')
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
            <td>id</td>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td>
                  <strong>Area</strong>
                </td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                <td><strong>EXP</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($totes as $tote)
                @if($tote->discontinued != 1)

                <tr>
                <td>{{ $tote->id }}</td>
                    <td class="lot">{{ $tote->size }}</td>
                    <td class="so">{{ $tote->oil_type }}</td>
                    <td>
                    @foreach($tote->history as $history)
                    @if($history == $tote->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                    {{ $history->count }}</td>
                    @endif
                    @endforeach
                    </td>
                    <td class="originallot">{{ $tote->lot }}</td>
                    <td>
                      {{ $tote->area }}
                    </td>

                    <td class="oiltype">{{ number_format((float)$tote->totes_cost_per, 3) }}</td>
                    <td class="supplier">
                        <?php
                        //calculate total value
                        foreach ($tote->history as $history) {
                            if($history == $tote->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year){
                                $total_value = $history->count * $tote->totes_cost_per;
                                echo number_format($total_value, 3);
                            }
                        }
                        ?>
                    </td>
                    <td class="origin">{{ $tote->expiration_date }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                                <a href="/inventories/edit/totes/<?php echo $tote->id ?>" class="btn btn-warning">Edit</a>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)

                       {{ Form::model($tote, array('method' => 'PATCH', 'route' => array('toteupdates.update', $tote->id))) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('id', $tote->id) }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('totes_cost_per',"0") }}
                                {{ Form::hidden('expiration_date') }}
                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        @endif
                        </div>


                </tr>
                @endif
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>{{ number_format($total->get_total_totes(), 3) }}</strong></td>
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
