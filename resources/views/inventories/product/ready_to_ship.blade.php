@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Ready To Ship
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Ready To Ship
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                    {{ link_to('inventories/count/ready_to_ship', 'New Ready To Ship Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-danger navbar-btn']) }}
                    @if(Auth::user()->group == 100)
                    {{ link_to('inventories/cost/ready_to_ship', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}}
                    {{ link_to('inventories/discontinued/ready_to_ship', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/ready_to_ship', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                    @endif


                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Order Number</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($ready_to_ship as $item)
                @if($item->discontinued != 1)

                <tr>
                    <td class="lot">{{ $item->size }}</td>
                    <td class="so">{{ $item->oil_type }}</td>
                    <td>
                    @foreach($item->history as $history)
                    @if($history->month === Carbon::now()->month && $history->year === Carbon::now()->year)
                    {{ $history->count }}</td>
                    @endif
                    @endforeach
                    </td>
                    <td>{{ $item->order_number }}</td>
                    <td class="originallot">{{ $item->lot }}</td>
                    <td>{{ $item->area }}</td>
                    <td class="oiltype">{{ number_format((float)$item->ready_to_ship_cost_per, 3) }}</td>
                    <td class="supplier">
                        <?php
                        //calculate total value
                        foreach ($item->history as $history) {
                            if($history == $item->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year){
                                $total_value = $history->count * $item->ready_to_ship_cost_per;
                                echo number_format($total_value, 3);
                            }
                        }
                        ?>
                    </td>
                    <td class="origin">{{ $item->expiration_date }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                                <a href="/inventories/edit/ready_to_ship/<?php echo $item->id ?>" class="btn btn-warning">Edit</a>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)

 {{ Form::model($item, array('method' => 'PATCH', 'route' => array('ready_to_shipupdates.update', $item->id))) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('id', $item->id) }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('order_number') }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('ready_to_ship_cost_per', "0") }}
                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        @endif
                        </div>
                </tr>
                @endif
            @endforeach
            <tr>
                <td><h3>Total</h3></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>{{ number_format($total->get_total_ready_to_ship(), 3) }}</strong></td>
            </tr>
            </tbody>
        </table>
        <ul class="pagination"></ul>
    </div>
@stop
{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
