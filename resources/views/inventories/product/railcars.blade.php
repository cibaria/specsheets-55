@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Railcars
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Railcars
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                    {{ link_to('inventories/count/railcars', 'New railcar Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/create', 'Create A Product', ['class' => 'btn btn-danger navbar-btn']) }}
                    @if(Auth::user()->group == 100)
                    {{ link_to('inventories/cost/railcars', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}}
                    {{ link_to('inventories/discontinued/railcars', 'Discontinued', ['class' => 'btn btn-danger navbar-btn'])}}
                    {{ link_to('inventories/pdf/railcars', 'Print PDF', ['class' => 'btn btn-primary navbar-btn']) }}
                    @endif

                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Car Recieved</strong></td>
                <td><strong>Inventory Date</strong></td>
                <td><strong>Inventory Count</strong></td>
                <td><strong>Loads Taken</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Per LB Price</strong></td>
                <td><strong>Total Val</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($railcars as $railcar)
                @if($railcar->discontinued != 1)

                <tr>
                    {{--replaced size with car recieved--}}
                    <td class="lot">{{ $railcar->size }}</td>

                    {{--replaced description inventory date--}}
                    <td class="so">{{ $railcar->oil_type }}</td>

                    {{--replaced qty with inventory count--}}
                    <td>
                    @foreach($railcar->history as $history)
                    @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                    {{ $history->count }}</td>
                    @endif
                    @endforeach
                    </td>
                    <td>{{ $railcar->loads_taken }}</td>

                    <td class="originallot">{{ $railcar->lot }}</td>
                    <td class="oiltype">{{ number_format((float)$railcar->railcars_per_lb_price, 3) }}</td>
                    <td class="supplier">
                        <?php
                        //calculate total value
                        foreach ($railcar->history as $history) {
                            if($history == $railcar->history->last() && $history->month == Carbon::now()->month && $history->year == Carbon::now()->year){
                                $total_value = $history->count * $railcar->railcars_per_lb_price;
                                echo number_format($total_value, 3);
                            }
                        }
                        ?>

                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                                <a href="/inventories/edit/railcars/<?php echo $railcar->id ?>" class="btn btn-warning">Edit</a>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)

                        {{ Form::model($railcar, array('method' => 'PATCH', 'route' => array('railcarupdates.update', $railcar->id))) }}
                                {{ Form::hidden('discontinued',1) }}
                                {{ Form::hidden('id', $railcar->id) }}
                                {{ Form::hidden('count', "0") }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('railcars_per_lb_price', "0") }}
                                {{ Form::hidden('loads_taken') }}
                                {{ Form::submit('Discontinue', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        @endif
                        </div>
                </tr>
                @endif
            @endforeach
            <tr>
                <td><h3>Total</h3></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>{{ number_format($total->get_railcars_per_lb_price(), 3) }}</strong></td>
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
