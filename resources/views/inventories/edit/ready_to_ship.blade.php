@extends('layouts.admin')

@section('content')
    <div id="receivingappointmentlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span> ready_to_ship
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories/product/ready_to_ship/">ready_to_ship</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($ready_to_ship, array('method' => 'PATCH', 'route' =>
                                                         array('ready_to_shipupdates.update', $ready_to_ship->id))) }}
                          {{ Form::hidden('update', 'true') }}                               
                          {{ Form::hidden('redirect_to_product', 1)}}   
                          @foreach($ready_to_ship->history as $history)
                          @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                            {{ Form::hidden('count', $history->count) }}
                            {{ Form::hidden('ready_to_ship_cost_per', $ready_to_ship->ready_to_ship_cost_per) }}
                          @endif
                          @endforeach
                                                                                
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('size', 'Size') }}
                                {{ Form::text('size',null,['class' => 'form-control', 'placeholder' => 'Size']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oil_type', 'Description') }}
                                {{ Form::text('oil_type',null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('order_number', 'Order Number') }}
                                {{ Form::text('order_number', null, ['class' => 'form-control', 'placeholder' => 'Order Number']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('lot', 'Lot#') }}
                                {{ Form::text('lot', null, ['class' => 'form-control', 'placeholder' => 'Lot#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('expiration_date', 'Expiration Date') }}
                                {{ Form::text('expiration_date', null, ['class' => 'form-control', 'placeholder' => 'Expiration Date', 'id' => 'datepicker1']) }}
                            </div>

                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            	<a href="/inventories/product/ready_to_ship" class="btn btn-primary" style="float:right">Cancel</a>
                            {{-- {{ link_to_route('inventories.product.ready_to_ship', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }} --}}
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop