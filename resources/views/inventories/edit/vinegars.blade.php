@extends('layouts.admin')

@section('content')
    <div id="receivingappointmentlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span> vinegar
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories/product/vinegars/">vinegars</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($vinegar, array('method' => 'PATCH', 'route' =>
                                                         array('vinegarupdates.update', $vinegar->id))) }}
                          {{ Form::hidden('update', 'true') }}                               
                          {{ Form::hidden('redirect_to_product', 1)}}    
                          @foreach($vinegar->history as $history)
                          @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                            {{ Form::hidden('count', $history->count) }}
                            {{ Form::hidden('vinegars_cost_per', $vinegar->vinegars_cost_per) }}
                          @endif
                          @endforeach                    
                        <fieldset>
                        <div class="form-group">
                            {{ Form::label('sub_product_category', 'Sub Category') }}
                            {{ Form::select('sub_product_category', [
                                '' => 'Choose',
                                '4_star_dark' => '4 Star Dark',
                                '4_star_white' => '4 Star White',
                                '8_star_dark' => '8 Star Dark',
                                '25_star_dark' => '25 Star Dark',
                                '25_star_white' => '25 Star White',
                                'lambrusco' => 'Lambrusco',
                                'cocoa_butter_deodorized' => 'Cocoa Butter, Deodorized',
                                'cocoa_butter_natural' => 'Cocoa Butter, Natural',
                                'sesame_clear' => 'Sesame, Clear',
                                'sesame_toasted' => 'Sesame, Toasted',
                                 ],$vinegar->sub_product_category, ['class' => 'form-control', 'id' => 'sub_product_category']) }}
    
                            </div>

                            <div class="form-group">
                                {{ Form::label('size', 'Size') }}
                                {{ Form::text('size',null,['class' => 'form-control', 'placeholder' => 'Size']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oil_type', 'Description') }}
                                {{ Form::text('oil_type',null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('lot', 'Lot#') }}
                                {{ Form::text('lot', null, ['class' => 'form-control', 'placeholder' => 'Lot#']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('expiration_date', 'Expiration Date') }}
                                {{ Form::text('expiration_date', null, ['class' => 'form-control', 'placeholder' => 'Expiration Date', 'id' => 'datepicker1']) }}
                            </div>

                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            	<a href="/inventories/product/vinegars" class="btn btn-primary" style="float:right">Cancel</a>
                            {{-- {{ link_to_route('inventories.product.vinegars', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }} --}}
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop