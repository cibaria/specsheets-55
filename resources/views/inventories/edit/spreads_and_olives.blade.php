@extends('layouts.admin')

@section('content')
    <div id="receivingappointmentlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span> spreads_and_olive
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories/product/spreads_and_olives/">spreads_and_olives</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($spreads_and_olive, array('method' => 'PATCH', 'route' =>
                                                         array('spreads_and_olivesupdates.update', $spreads_and_olive->id))) }}
                          {{ Form::hidden('update', 'true') }}                               
                          {{ Form::hidden('redirect_to_product', 1)}} 
                          @foreach($spreads_and_olive->history as $history)
                          @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year)
                            {{ Form::hidden('count', $history->count) }}
                            {{ Form::hidden('spreads_and_olives_cost_per', $spreads_and_olive->spreads_and_olives_cost_per) }}
                          @endif
                          @endforeach
                          {{ Form::hidden('update', 'true') }}                             
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('size', 'Size') }}
                                {{ Form::text('size',null,['class' => 'form-control', 'placeholder' => 'Size']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('oil_type', 'Description') }}
                                {{ Form::text('oil_type',null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('cases', 'Cases') }}
                                {{ Form::text('cases',null,['class' => 'form-control', 'placeholder' => 'Cases']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('jars', 'Jars') }}
                                {{ Form::text('jars',null,['class' => 'form-control', 'placeholder' => 'Jars']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('lot', 'Lot#') }}
                                {{ Form::text('lot', null, ['class' => 'form-control', 'placeholder' => 'Lot#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('expiration_date', 'Expiration Date') }}
                                {{ Form::text('expiration_date', null, ['class' => 'form-control', 'placeholder' => 'Expiration Date', 'id' => 'datepicker1']) }}
                            </div>

                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            	<a href="/inventories/product/spreads_and_olives" class="btn btn-primary" style="float:right">Cancel</a>
                            {{-- {{ link_to_route('inventories.product.spreads_and_olives', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }} --}}
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop