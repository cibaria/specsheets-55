@extends('layouts.admin')

@section('content')

    <div id="specsheets">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span>A Product
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories/">Product</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10">
                    <div class="col-lg-20">
                        {{ Form::open(['image/save', 'route' => 'inventories.store','files' => true, 'class' => 'form-horizontal']) }}
                        {{ Form::hidden('active',1)}}


                        <script>
                            $(document).ready(function () {
                                $('.defaultinventory').hide();

                                $('#form_category').change(function () {
                                    $(this).find("option").each(function () {
                                        $("#" + $(this).val()).hide();
                                        console.log($(this));
                                    });
                                    $("#" + $(this).val()).show();
                                    // hide input fields when they are hidden
                                    $("div").filter(":hidden").children("input[type='text']").attr("disabled", "disabled");
                                    $("div").filter(":hidden").children("textarea[class='form-control']").attr("disabled", "disabled");
                                        $("div").filter(":hidden").children("select[class='form-control']").attr("disabled", "disabled");
                                });
                            });
                        </script>

                        <fieldset>
    						<div class="form-group">
                                {{ Form::label('product_category', 'Product Category') }}
                                {{ Form::select('product_category', ['' => 'Choose', 'drums' => 'Drums', 'totes' => 'Totes', 'organic' => 'Organic', 'tanks' => 'Tanks', 'vinegars' => 'Vinegars', 'railcars' => 'Rail Cars', 'specialty' => 'Specialty', 'infused_oil_drums' => 'Infused Oil Drums', 'spreads_and_olives' => 'Spreads and Olives', 'website_olive_oils' => 'Website Olive Oils', 'web_flavor_and_infused' => 'Web Flavor and Infused', 'website_vinegars' => 'Website Vinegars', 'website_other' => 'Website Other', 'finished_product' => 'Finished Product', 'sprays' => 'Sprays', 'ready_to_ship' => 'Ready To Ship', 'rework' => 'Rework', 'labels_in_house' => 'Labels In House','labels_in_labelroom' => 'Labels In Labelroom', 'customers_labels' => 'Customers Labels', 'bottles' => 'Bottles', 'etched_bottles' => 'Etched Bottles', 'fustis' => 'Fustis', 'caps' => 'Caps', 'cardboard_blank' => 'Cardboard Blank','cardboard_printed' => 'Cardboard Printed','supplies' => 'Supplies', 'materials' => 'Materials', 'follmer' => 'Follmer', 'flavors' => 'Flavors', 'flavors_for_samples' => 'Flavors For Samples', 'customers_oils' => 'Customer Oils'], '', ['class' => 'form-control', 'id' => 'form_category']) }}
                            </div>
                        



                          
                            {{-- end main --}}
                            <hr/>
                        
                            
                      		{{-- drums --}}
                            <div id="drums" class="defaultinventory">
                                @include('inventories.form._drums')
                            </div>
                            {{-- totes --}}
                            <div id="totes" class="defaultinventory">
                                @include('inventories.form._totes')
                            </div>
                            {{-- organic --}}
                            <div id="organic" class="defaultinventory">
                                @include('inventories.form._organic')
                            </div>
                            {{-- tanks --}}
                            <div id="tanks" class="defaultinventory">
                                @include('inventories.form._tanks')
                            </div>
                            {{-- vinegars --}}
                            <div id="vinegars" class="defaultinventory">
                                @include('inventories.form._vinegars')
                            </div>
                            {{-- railcars --}}
                            <div id="railcars" class="defaultinventory">
                                @include('inventories.form._railcars')
                            </div>
                            {{-- specialty --}}
                            <div id="specialty" class="defaultinventory">
                                @include('inventories.form._specialty')
                            </div>
                            {{-- infused oil drums --}}
                            <div id="infused_oil_drums" class="defaultinventory">
                                @include('inventories.form._infused_oil_drums')
                            </div>
                            {{-- spreads and olives --}}
                            <div id="spreads_and_olives" class="defaultinventory">
                                @include('inventories.form._spreads_and_olives')
                            </div>
                            {{-- website alive oils --}}
                            <div id="website_olive_oils" class="defaultinventory">
                                @include('inventories.form._website_olive_oils')
                            </div>
                            {{-- web flavor and infused --}}
                            <div id="web_flavor_and_infused" class="defaultinventory">
                                @include('inventories.form._web_flavor_and_infused')
                            </div>
                            {{-- website vinegars --}}
                            <div id="website_vinegars" class="defaultinventory">
                                @include('inventories.form._website_vinegars')
                            </div>
                            {{-- website other --}}
                            <div id="website_other" class="defaultinventory">
                                @include('inventories.form._website_other')
                            </div>
                            {{-- finished product --}}
                            <div id="finished_product" class="defaultinventory">
                                @include('inventories.form._finished_product')
                            </div>
                            {{-- sprays --}}
                            <div id="sprays" class="defaultinventory">
                                @include('inventories.form._sprays')
                            </div>
                            {{-- ready to ship --}}
                            <div id="ready_to_ship" class="defaultinventory">
                                @include('inventories.form._ready_to_ship')
                            </div>
                            {{-- rework --}}
                            <div id="rework" class="defaultinventory">
                                @include('inventories.form._rework')
                            </div>
                            {{-- labels in house --}}
                            <div id="labels_in_house" class="defaultinventory">
                                @include('inventories.form._labels_in_house')
                            </div>
                            {{--labels in labelroom--}}
                            <div id="labels_in_labelroom" class="defaultinventory">
                                @include('inventories.form._labels_in_labelroom')
                            </div>
                            {{-- Customer Labels --}}
                            <div id="customers_labels" class="defaultinventory">
                                @include('inventories.form._customers_labels')
                            </div>
                            {{-- bottles --}}
                            <div id="bottles" class="defaultinventory">
                                @include('inventories.form._bottles')
                            </div>
                            {{-- etched bottles --}}
                            <div id="etched_bottles" class="defaultinventory">
                                @include('inventories.form._etched_bottles')
                            </div>
                            {{-- fustis --}}
                            <div id="fustis" class="defaultinventory">
                                @include('inventories.form._fustis')
                            </div>
                            {{-- caps --}}
                            <div id="caps" class="defaultinventory">
                                @include('inventories.form._caps')
                            </div>
                            {{-- cardboard blank --}}
                            <div id="cardboard_blank" class="defaultinventory">
                                @include('inventories.form._cardboard_blank')
                            </div>
                            {{--cardboard printed--}}
                            <div id="cardboard_printed" class="defaultinventory">
                                @include('inventories.form._cardboard_printed')
                            </div>
                            {{-- supplies --}}
                            <div id="supplies" class="defaultinventory">
                                @include('inventories.form._supplies')
                            </div>
                            {{-- materials --}}
                            <div id="materials" class="defaultinventory">
                                @include('inventories.form._materials')
                            </div>
                            {{-- follmer --}}
                            <div id="follmer" class="defaultinventory">
                                @include('inventories.form._follmer')
                            </div>
                            {{-- flavors --}}
                            <div id="flavors" class="defaultinventory">
                                @include('inventories.form._flavors')
                            </div>
                            {{-- flavors for samples --}}
                            <div id="flavors_for_samples" class="defaultinventory">
                                @include('inventories.form._flavors_for_samples')
                            </div>
                            {{-- cusotmers oils --}}
                            <div id="customers_oils" class="defaultinventory">
                                @include('inventories.form._customers_oils')
                            </div>



                        </fieldset>
                        <hr/>
                        <div class="form-group">
                        {{ Form::submit('Create', array('class' => 'btn btn-default')) }}
                        </div>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end specsheets-->
    @stop