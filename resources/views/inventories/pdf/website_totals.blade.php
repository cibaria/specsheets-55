@extends('layouts.pdf')

@section('content')


<div>
    <div style="text-align:center">
<img src="http://cibariaspecsheets.com/images/logo.jpg" width="200px" height="200px">
<br />
        <strong>705 Columbia Ave<br/>
            Riverside, CA 92507<br/>
            (P) (951) 823-8490<br/>
            (F) (951) 823-8495<br/>
            (E) <a href="mailto:info@cibaria-intl.com">info@cibaria-intl.com</a>
        </strong>
    </div>
    <br />
    <h1 style="text-align:center;">Website Totals Inventory</h1>
    <br />
    </div>


<table width="100%"  border="1" cellpadding="0" cellspacing="0">
            <thead>
            <tr style="border:1px solid black">
                <td><strong>Olive Oils</strong></td>
                <td><strong>Infused</strong></td>
                <td><strong>Vinegars</strong></td>
                <td><strong>Other</strong></td>
                <td><strong>Total</strong></td>
            </tr>
            </thead>
            <tbody class="list">
           <tr>
               
               <td>{{ number_format($website_totals->get_total_website_olive_oils(), 3) }}</td>
               <td>{{ number_format($website_totals->get_total_web_flavored_and_infused(), 3) }}</td>
               <td>{{ number_format($website_totals->get_total_website_vinegars(), 3) }}</td>
               <td>{{ number_format($website_totals->get_total_website_other(), 3) }}</td>
               <td>{{ number_format($website_totals->get_website_totals(), 3) }}</td>
           </tr>
              

            </tbody>
        </table>

@stop