@extends('layouts.pdf')

@section('content')



<div>
    <div style="text-align:center">
<img src="http://cibariaspecsheets.com/images/logo.jpg" width="200px" height="200px">
<br />
        <strong>705 Columbia Ave<br/>
            Riverside, CA 92507<br/>
            (P) (951) 823-8490<br/>
            (F) (951) 823-8495<br/>
            (E) <a href="mailto:info@cibaria-intl.com">info@cibaria-intl.com</a>
        </strong>
    </div>
    <br />
    <h1 style="text-align:center;">Inventory Totals</h1>
    <br />
    </div>


<table width="100%"  border="1" cellpadding="0" cellspacing="0">
            
            
                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Drums</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_drums(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Totes</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_totes(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Organic</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_organics(), 3) }}</td>

                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Vinegar</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_vinegars(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Tanks</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_tanks(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Railcars</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_railcars_per_lb_price(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Specialty</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_specialties(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Infused</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_infused_oil_drums(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Website</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_website_totals(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Labels</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_label_totals(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Bottles</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_bottles(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Caps</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_caps(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Cardboard Blank</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_cardboard_blank(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Cardboard Printed</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_cardboard_printed(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Ready To Ship</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_ready_to_ship(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Finished Product</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_finished_product(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Spray</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_sprays(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Rework</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_rework(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Etched Bottles</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_etched_bottles(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Flavors</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_flavor_totals(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Materials</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_materials(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Follmer</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_follmer(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Pantry Items</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_pantry_items(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Fustis</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_fustis(), 3) }}</td>
                </tr>

                <tr style="border:1px solid black">
                <td style="font-size:14px"><strong>Supplies</strong></td>
                <td style="font-size:14px">${{ number_format($inventory_totals->get_total_supplies(), 3) }}</td>
                </tr>
            
            
            
            <tr>
            <td style="font-size:23px"><strong>Total</strong></td>
            <td style="font-size:23px"><strong>${{ number_format($inventory_totals->total(), 3) }}</strong></td>
            </tr>
            
            

            
        </table>

@stop