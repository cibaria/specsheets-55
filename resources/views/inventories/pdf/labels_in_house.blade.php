@extends('layouts.pdf')

@section('content')


<div>
    <div style="text-align:center">
<img src="http://cibariaspecsheets.com/images/logo.jpg" width="200px" height="200px">
<br />
        <strong>705 Columbia Ave<br/>
            Riverside, CA 92507<br/>
            (P) (951) 823-8490<br/>
            (F) (951) 823-8495<br/>
            (E) <a href="mailto:info@cibaria-intl.com">info@cibaria-intl.com</a>
        </strong>
    </div>
    <br />
    <h1 style="text-align:center;">Labels In House Inventory</h1>
    <br />
    </div>


<table width="100%"  border="1" cellpadding="0" cellspacing="0">
            <thead>
            <tr style="border:1px solid black">
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                <td><strong>EXP</strong></td>
                <td><strong>Total</strong></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($labels_in_houses as $labels_in_house)
                @if($labels_in_house->discontinued != 1)
                <tr>
                    <td class="lot">{{ $labels_in_house->size }}</td>
                    <td class="so">{{ $labels_in_house->oil_type }}</td>
                   <td>
                    <?php
                    foreach($labels_in_house->history as $history){
                        if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year){
                            echo  number_format((float)$history->count,3);
                        }else {
                            echo " ";
                        }
                    }

                    ?>
                    </td>
                    <td class="originallot">{{ $labels_in_house->lot }}</td>
                    <td>{{ $labels_in_house->area }}</td>
                    <td class="oiltype">{{ $labels_in_house->labels_in_house_cost_per }}</td>

                    <td class="supplier">
                        <?php
                        //calculate total value
                        foreach($labels_in_house->history as $history) {
                    if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year) {
                        $total_value = $history->count * $labels_in_house->labels_in_house_cost_per;
                        echo number_format((float)$total_value,  3);
                    } else {
                        $total_value = "0";
                    }
                }
                        ?>



                    </td>
                    <td border="1">{{ $labels_in_house->expiration_date }}</td>
                    <td></td>
                </tr>
                @endif
            @endforeach
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>{{ number_format($total->get_total_labels_in_house(), 3) }}</strong></td>

            </tr>

            </tbody>
        </table>

@stop
