@extends('layouts.print')


@section('content')
    <div>
        <a href="javascript:window.print()">
            {{ HTML::image('images/print_icon.jpg', 'Print',['width' => '100', 'height' => '100', 'class' => 'noPrint', 'style' => 'border:none']) }}</a>
    </div>
    <table cellspacing=0 cellpadding=0>
        <thead>
        <tr><th colspan=14><h2>CIBARIA INTERNATIONAL, INC<br />Totals For Bank</h2></th></tr>
        <tr>
            <th style="border-left: 3px solid black; border-top: 3px solid black;border-right: 1px solid black; border-bottom:3px solid black;">Drums</th>
            <th style="border-top: 3px solid black; border-right:1px solid black; border-bottom: 3px solid black;">Totes</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Organic</th>
            <th style="border-top:3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Vinegar</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Tanks</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Railcars</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Specialty</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Infused</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Website</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Labels</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Bottles</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Caps</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Cardboard Blank</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Cardboard Printed</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Ready To Ship</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Finished Product</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Spray</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Supplies</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Rework</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Etched Bottles</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Flavors</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Materials</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Follmer</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Pantry Items</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Fustis</th>
            <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Totals</th>

        </tr>
        </thead>
        <tbody style="border-bottom: 3px solid black;">
        <tr>
            <td style="border-left: 3px solid black; border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_drums(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_totes(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_organics(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_vinegars(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_tanks(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_railcars_per_lb_price(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_specialties(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_infused_oil_drums(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;">$0</td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_label_totals(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_bottles(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_caps(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_cardboard_blank(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_cardboard_printed(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_ready_to_ship(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_finished_product(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_sprays(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_supplies(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;">$0</td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_etched_bottles(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_flavors(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;">$0</td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_follmer(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;">$0</td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_spreads_and_olives(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->get_total_fustis(); ?></td>
            <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $total->bank_totals(); ?></td>
        </tr>
        </tbody>
    </table>

@stop