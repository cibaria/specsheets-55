@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Bottles
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> > <a href="/inventories/product/bottles">bottles</a>
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to('inventories/print/bottles', 'Print', ['class' => 'btn btn-primary navbar-btn']) }}
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>Supplier</strong></td>
                <td><strong>Manufacturer Part #</strong></td>
                <td></td>
                {{-- <td><strong>Cost Per</strong></td> --}}
                {{-- <td><strong>Total Val</strong></td> --}}
                {{-- <td><strong>EXP</strong></td> --}}
                {{--<td><strong>QTY</strong></td>--}}
                {{--<td><strong>Size</strong></td>--}}
                {{--<td><strong>Customer</strong></td>--}}
                {{--<td><strong>Date</strong></td>--}}

                {{-- <td>Total</td> --}}
            </tr>
            </thead>
            <tbody class="list">



             @foreach($bottles as $bottle)
             @if($bottle->discontinued != "1")

                <tr>

                    <td class="lot">{{ $bottle->size }}</td>
                    <td class="so">{{ $bottle->oil_type }}</td>
                    {{ Form::model($bottle, array('method' => 'PATCH', 'route' => array('bottlesupdates.update', $bottle->id))) }}
                    @forelse($bottle->history as $historys)
                    @if($historys == $bottle->history->last() && $historys->month == Carbon::now()->month && $historys->year == Carbon::now()->year)
                    {{ Form::hidden('historyid', $historys->id) }}
                    {{ Form::hidden('update', "true") }}
                  @elseif($historys == $bottle->history->first() && $historys->month != Carbon::now()->month && $historys->year != Carbon::now()->year)
                    {{ Form::hidden('update', "false") }}
                    {{ Form::hidden('id', $bottle->id) }}
                  @else
                    {{ Form::hidden('update', "false") }}
                    {{ Form::hidden('id', $bottle->id) }}
                  @endif
                @empty
                  {{ Form::hidden('update', "false") }}
                  {{ Form::hidden('id', $bottle->id) }}


                    @endforelse





                    {{ Form::hidden('username',Auth::user()->username) }}
                    {{ Form::hidden('bottles_cost_per')}}
                    {{ Form::hidden('size') }}
                    {{ Form::hidden('oil_type') }}
                    {{ Form::hidden('lot') }}
                    {{ Form::hidden('area') }}
                    {{ Form::hidden('in_bulk') }}
                    {{ Form::hidden('in_carton') }}
                    {{ Form::hidden('pcs_plt') }}
                    {{ Form::hidden('supplier') }}
                    {{ Form::hidden('manufacturer_part_number') }}


 <td>
                        <input class="form-control" name="count" type="number" step="any" value="<?php
                            foreach($bottle->history as $history):

                               if($history->year == Carbon::now()->year && $history->month == Carbon::now()->month)
                               {
                                echo $history->count;
                               }
                                endforeach;
                        ?>">

                    </td>


                    <td class="originallot">{{ $bottle->lot }}</td>
                    <td>{{ $bottle->area }}</td>
                    <td>{{ $bottle->supplier }}</td>
                    <td>{{ $bottle->manufacturer_part_number }}</td>
              {{-- @forelse($bottle->history as $history)
                    @if($history->month == Carbon::now()->month && $history->year == Carbon::now()->year )

                    <td>{{ Form::submit('Update', ['class' => 'btn btn-update']) }}</td>
                    @endif
                @empty
                    <td>{{ Form::submit('Add', ['class' => 'btn btn-danger']) }}</td>
                @endforelse --}}

                <td>{{ Form::submit('Insert', ['class' => 'btn btn-danger']) }}</td>

                </tr>
                {{ Form::close() }}

                @endif
                 @endforeach
            <tr>
              <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>
                        <?php

                        //$total = 0;
                        //foreach($bottles as $bottlem)
                        //{
                        //    $total_values = $bottlem->qty * $bottlem->bottles_cost_per;
                        // $total += $total_values;
                        // }
                        //echo $total;

                        ?>

                    </strong></td>
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
