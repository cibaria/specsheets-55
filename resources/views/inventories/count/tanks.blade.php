@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Tanks
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> > <a href="/inventories/product/tanks">Tanks</a>
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to('inventories/print/tanks', 'Print', ['class' => 'btn btn-primary navbar-btn']) }}
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>GAL/Inch</strong></td>
                <td><strong>Total</strong></td>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td></td>
                {{-- <td><strong>Cost Per</strong></td> --}}
                {{-- <td><strong>Total Val</strong></td> --}}
                {{-- <td><strong>EXP</strong></td> --}}
                {{--<td><strong>QTY</strong></td>--}}
                {{--<td><strong>Size</strong></td>--}}
                {{--<td><strong>Customer</strong></td>--}}
                {{--<td><strong>Date</strong></td>--}}

                {{-- <td>Total</td> --}}
            </tr>
            </thead>
            <tbody class="list">

            @foreach($tanks as $tank)
                <tr>
                    <td>{{ $tank->gal_inch }}</td>
                    <td>{{ $tank->tank_total }}</td>
                    <td class="lot">{{ $tank->size }}</td>
                    <td class="so">{{ $tank->oil_type }}</td>
                 {{ Form::model($tank, array('method' => 'PATCH', 'route' => array('tankupdates.update', $tank->id))) }}

                @forelse($tank->history as $historys)
                    @if($historys == $tank->history->last() && $historys->month == Carbon::now()->month && $historys->year == Carbon::now()->year)
                    {{ Form::hidden('historyid', $historys->id) }}
                    {{ Form::hidden('update', "true") }}

                  @elseif($historys == $tank->history->first() && $historys->month != Carbon::now()->month && $historys->year != Carbon::now()->year)
                    {{ Form::hidden('update', "false") }}
                    {{ Form::hidden('id', $tank->id) }}
                  @else
                    {{ Form::hidden('update', "false") }}
                    {{ Form::hidden('id', $tank->id) }}
                  @endif
                @empty
                  {{ Form::hidden('update', "false") }}
                  {{ Form::hidden('id', $tank->id) }}
                    @endforelse

                    {{ Form::hidden('id', $tank->id) }}
                    {{ Form::hidden('username',Auth::user()->username) }}

                    {{ Form::hidden('lot') }}
                    {{ Form::hidden('area') }}
                    {{ Form::hidden('size') }}
                    {{ Form::hidden('oil_type') }}
                    {{ Form::hidden('tanks_cost_per') }}

                    <td>
                        <input class="form-control" name="count" type="text" value="<?php
                            foreach($tank->history as $history):

                               if($history == $tank->history->last() && $history->year === Carbon::now()->year || $history->month === Carbon::now()->month)
                               {
                                echo $history->count;
                               }
                                endforeach;
                        ?>">

                    </td>


                    <td>{{ Form::submit('Insert', ['class' => 'btn btn-danger']) }}</td>




                </tr>
                {{ Form::close() }}
            @endforeach



            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>
                        <?php

                        //$total = 0;
                        //foreach($tankss as $tanksm)
                        //{
                        //    $total_values = $tanksm->qty * $tanksm->tankss_cost_per;
                        // $total += $total_values;
                        // }
                        //echo $total;

                        ?>

                    </strong></td>
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
