@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    spreads_and_olives
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> > <a href="/inventories/product/spreads_and_olives">spreads_and_olives</a>
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            
            @foreach($spreads_and_olives as $spreads_and_olive)
                @if($spreads_and_olive->discontinued != 1)

                <tr>
                    <td class="lot">{{ $spreads_and_olive->size }}</td>
                    <td class="so">{{ $spreads_and_olive->oil_type }}</td>
                    {{ Form::model($spreads_and_olive, array('method' => 'PATCH', 'route' => array('spreads_and_olivesupdates.update', $spreads_and_olive->id))) }}
                    {{ Form::hidden('id', $spreads_and_olive->id) }}
                    {{ Form::hidden('update', 'true') }}
                    {{ Form::hidden('size') }}
                    {{ Form::hidden('oil_type') }}
                    {{ Form::hidden('lot') }}
                    {{ Form::hidden('area') }}
                    {{ Form::hidden('expiration_date') }}
                    {{ Form::hidden('username',Auth::user()->username) }}

                    <td>{{ Form::text('spreads_and_olives_cost_per', $spreads_and_olive->spreads_and_olives_cost_per,['class' => 'form-control'])}}</td>
                    <td class="originallot">{{ $spreads_and_olive->lot }}</td>
                    <td>{{ $spreads_and_olive->area }}</td>
                    <td>{{ Form::submit('Update', ['class' => 'btn btn-update']) }}</td>
                </tr>
                {{ Form::close() }}
                @endif
            @endforeach
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>
            </strong></td>
            </tr>
            </tbody>
        </table>


    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            // page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop