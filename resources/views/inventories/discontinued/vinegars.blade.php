@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                     <span class="alert alert-dismissible alert-danger" style="padding-right:15px;">Discontinued</span> Vinegars
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Vinegars
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                  {{--   {{ link_to('inventories/count/vinegars', 'New Vinegar Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/cost/vinegars', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}} --}}
                     {{ link_to('inventories/product/vinegars', 'Active Inventory', ['class' => 'btn btn-success navbar-btn'])}}


                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td>
                  <strong>Area</strong>
                </td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                <td><strong>EXP</strong></td>
                {{--<td><strong>QTY</strong></td>--}}
                {{--<td><strong>Size</strong></td>--}}
                {{--<td><strong>Customer</strong></td>--}}
                {{--<td><strong>Date</strong></td>--}}

            </tr>
            </thead>
            <tbody class="list">
            @foreach($vinegars as $vinegar)
                @if($vinegar->discontinued === 1)

                <tr>
                    <td class="lot">{{ $vinegar->size }}</td>
                    <td class="so">{{ $vinegar->oil_type }}</td>
                    <td>{{ $vinegar->qty }}</td>
                    <td class="originallot">{{ $vinegar->lot }}</td>
                    <td>
                      {{ $vinegar->area }}
                    </td>
                    <td class="oiltype">{{ $vinegar->vinegars_cost_per }}</td>
                    <td class="supplier">
                        <?php
                        //calculate total value
                        $total_value = $vinegar->qty * $vinegar->vinegars_cost_per
                        ?>

                        {{ $total_value }}

                    </td>
                    <td class="origin">{{ $vinegar->expiration_date }}</td>
                    {{--<td class="qty">{{ $materiallotlogs->qty }}</td>--}}
                    {{--<td class="size">{{ $materiallotlogs->size }}</td>--}}
                    {{--<td class="customer">{{ $materiallotlogs->customer }}</td>--}}
                    {{--<td class="date">{{ $materiallotlogs->date }}</td>--}}

                    <td>
                        <div class="btn-group" role="group" aria-label="">
{{--                        {{ link_to_route('materiallotlogs.show', 'View', [$materiallotlogs->id],['class' => 'btn btn-success']) }}--}}
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                        <a href="/inventories/edit/vinegars/<?php echo $vinegar->id ?>" class="btn btn-warning">Edit</a>

{{--                        {{ link_to_route('materiallotlogs.edit', 'Edit', [$materiallotlogs->id],['class' => 'btn btn-warning']) }}--}}
                       {{ Form::model($vinegar, array('method' => 'PATCH', 'route' => array('vinegarupdates.update', $vinegar->id))) }}
                                {{ Form::hidden('discontinued',0) }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('vinegars_cost_per', "0") }}
                                {{ Form::hidden('expiration_date') }}

                                {{ Form::submit('Put In Inventory', ['class' => 'btn btn-success']) }}
                        {{ Form::close() }}
                        @endif
                        </div>


                </tr>
                @endif
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
