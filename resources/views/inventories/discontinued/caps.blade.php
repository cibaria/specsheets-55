@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Caps
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Caps
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                     {{ link_to('inventories/product/caps', 'Active Inventory', ['class' => 'btn btn-success navbar-btn'])}}


                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
              <td><strong>Bottle(s) Used In</strong></td>
              <td><strong>Description</strong></td>
              <td><strong>Per Case</strong></td>
              <td><strong>Count</strong></td>
              <td><strong>Lot#</strong></td>
              <td><strong>Area</strong></td>
              <td><strong>Supplier</strong></td>
              <td><strong>Manufacturer Part #</strong></td>
              <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($caps as $cap)
                @if($cap->discontinued == 1)

                <tr>
                    <td class="lot">{{ $cap->size }}</td>
                    <td class="so">{{ $cap->oil_type }}</td>
                    <td class="originallot">{{ $cap->lot }}</td>
                    <td>{{ $cap->area }}</td>
                    <td>{{ $cap->per_case }}</td>
                    <td>{{ $cap->supplier }}</td>
                    <td class="oiltype">{{ $cap->caps_price }}</td>
                    <td class="supplier">
                        <?php
                        //calculate total value
                        $total_value = $cap->qty * $cap->caps_price
                        ?>

                        {{ $total_value }}

                    </td>

                    <td>
                        <div class="btn-group" role="group" aria-label="">
                          {{--{{ link_to_route('materiallotlogs.show', 'View', [$materiallotlogs->id],['class' => 'btn btn-success']) }}--}}
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                                <a href="/inventories/edit/caps/<?php echo $cap->id ?>" class="btn btn-warning">Edit</a>

                        {{--{{ link_to_route('materiallotlogs.edit', 'Edit', [$materiallotlogs->id],['class' => 'btn btn-warning']) }}--}}
                         {{ Form::model($cap, array('method' => 'PATCH', 'route' => array('capsupdates.update', $cap->id))) }}
                                {{ Form::hidden('discontinued',0) }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('per_case') }}
                                {{ Form::hidden('supplier') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('caps_price', "0") }}
                                {{ Form::hidden('expiration_date') }}

                                {{ Form::submit('Put In Inventory', ['class' => 'btn btn-success']) }}
                        {{ Form::close() }}
                        @endif
                        </div>


                </tr>
                @endif
            @endforeach
            <tr>
              <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>
                        <?php

                        $total = 0;
                        foreach($caps as $capm)
                        {
                            $total_values = $capm->qty * $capm->caps_price;
                            $total += $total_values;
                        }
                        echo $total;

                        ?>

                    </strong></td>
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
