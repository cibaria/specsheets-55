@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                   <span class="alert alert-dismissible alert-danger" style="padding-right:15px;">Discontinued</span> Drums
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Drums
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

{{--                     {{ link_to('inventories/count/drums', 'New Drum Count', ['class' => 'btn btn-primary navbar-btn']) }}
                    {{ link_to('inventories/cost/drums', 'Edit Cost Per', ['class' => 'btn btn-success navbar-btn'])}} --}}
                     {{ link_to('inventories/product/drums', 'Active Inventory', ['class' => 'btn btn-success navbar-btn'])}}
                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>EXP</strong></td>
                {{--<td><strong>QTY</strong></td>--}}
                {{--<td><strong>Size</strong></td>--}}
                {{--<td><strong>Customer</strong></td>--}}
                {{--<td><strong>Date</strong></td>--}}

            </tr>
            </thead>
            <tbody class="list">
            @foreach($drums as $drum)
                @if($drum->discontinued == 1)
                <tr>
                    <td class="lot">{{ $drum->size }}</td>
                    <td class="so">{{ $drum->oil_type }}</td>
                    <td>{{ $drum->qty }}</td>
                    <td class="originallot">{{ $drum->lot }}</td>
                    <td>{{ $drum->area }}</td>
                    <td class="oiltype">{{ $drum->drums_cost_per }}</td>
                    
                    <td class="origin">{{ $drum->expiration_date }}</td>
                    {{--<td class="qty">{{ $materiallotlogs->qty }}</td>--}}
                    {{--<td class="size">{{ $materiallotlogs->size }}</td>--}}
                    {{--<td class="customer">{{ $materiallotlogs->customer }}</td>--}}
                    {{--<td class="date">{{ $materiallotlogs->date }}</td>--}}

                    <td>
                        <div class="btn-group" role="group" aria-label="">
{{--                            {{ link_to_route('materiallotlogs.show', 'View', [$materiallotlogs->id],['class' => 'btn btn-success']) }}--}}
                            @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                                <a href="/inventories/edit/drums/<?php echo $drum->id ?>" class="btn btn-warning">Edit</a>
                                {{-- {{ link_to_route('inventories.edit.drums', 'Edit', [$drum->id],['class' => 'btn btn-warning']) }} --}}

                                {{ Form::model($drum, array('method' => 'PATCH', 'route' => array('drumupdates.update', $drum->id))) }}
                                {{ Form::hidden('discontinued',0) }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('qty', "0") }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('drums_cost_per', "0") }}
                                {{ Form::hidden('expiration_date') }}
                                {{ Form::hidden('id', $drum->id) }}

                                {{ Form::submit('Put In Inventory', ['class' => 'btn btn-success']) }}

                                {{ Form::close() }}
                            @endif
                        </div>
                        

                </tr>
                @endif
            @endforeach
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
           
            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop