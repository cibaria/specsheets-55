@extends('layouts.admin')

@section('content')

    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Bottles
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/inventories">Inventory</a> >Bottles
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

                     {{ link_to('inventories/product/bottles', 'Active Inventory', ['class' => 'btn btn-success navbar-btn'])}}

                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Size</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Lot#</strong></td>
                <td><strong>Area</strong></td>
                <td><strong>In Bulk</strong></td>
                <td><strong>In Carton</strong></td>
                <td><strong>PCS/PLT</strong></td>
                <td><strong>Cost Per</strong></td>
                <td><strong>Total Val</strong></td>
                {{--<td><strong>QTY</strong></td>--}}
                {{--<td><strong>Size</strong></td>--}}
                {{--<td><strong>Customer</strong></td>--}}
                {{--<td><strong>Date</strong></td>--}}

            </tr>
            </thead>
            <tbody class="list">
            @foreach($bottles as $bottle)
                @if($bottle->discontinued == 1)

                <tr>
                    <td class="lot">{{ $bottle->size }}</td>
                    <td class="so">{{ $bottle->oil_type }}</td>
                    @foreach($bottle->history as $bottles)
                    <td>{{ $bottles->count }}</td>
                    @endforeach
                    <td class="originallot">{{ $bottle->lot }}</td>
                    <td>{{ $bottle->area }}</td>
                    <td>{{ $bottle->in_bulk }}</td>
                    <td>{{ $bottle->in_carton }}</td>
                    <td>{{ $bottle->pcs_plt }}</td>
                    <td class="oiltype">{{ $bottle->bottles_cost_per }}</td>

                    <td>
                        <div class="btn-group" role="group" aria-label="">
                        {{--{{ link_to_route('materiallotlogs.show', 'View', [$materiallotlogs->id],['class' => 'btn btn-success']) }}--}}
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                                <a href="/inventories/edit/bottles/<?php echo $bottle->id ?>" class="btn btn-warning">Edit</a>

                        {{--{{ link_to_route('materiallotlogs.edit', 'Edit', [$materiallotlogs->id],['class' => 'btn btn-warning']) }}--}}
                       {{ Form::model($bottle, array('method' => 'PATCH', 'route' => array('bottlesupdates.update', $bottle->id))) }}
                                {{ Form::hidden('id',$bottle->id) }}
                                {{ Form::hidden('username',Auth::user()->username) }}
                                {{ Form::hidden('discontinued',0) }}
                                {{ Form::hidden('size') }}
                                {{ Form::hidden('oil_type') }}
                                {{ Form::hidden('lot') }}
                                {{ Form::hidden('area') }}
                                {{ Form::hidden('bottles_cost_per', "0") }}
                                {{ Form::hidden('expiration_date') }}

                                {{ Form::submit('Put In Inventory', ['class' => 'btn btn-success']) }}
                        {{ Form::close() }}
                        @endif
                        </div>


                </tr>
                @endif
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

            </tr>

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'so', 'originallot', 'oiltype', 'supplier', 'origin', 'qty', 'size', 'customer', 'date'],
            // pagination
            page: 10,
            plugins: [
                ListPagination({})
            ]
        };

        var lotlogList = new List('lotlogs', options);
    </script>
@stop
