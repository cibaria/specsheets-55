<div class="form-group">
    {{ Form::label('size', 'Size') }}
    {{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Size']) }}
</div>

<div class="form-group">
    {{ Form::label('oil_type', 'Description') }}
    {{ Form::textarea('oil_type', null,['class' => 'form-control', 'placeholder' => 'Description', 'rows' => '8', 'cols' => '4']) }}
</div>
<div class="form-group">
    {{ Form::label('expiration_date', 'Expiration Date') }}
    {{ Form::text('expiration_date',null,['class' => 'form-control', 'placeholder' => 'Expiration Date', 'id' => 'datepicker']) }}
</div>
<div class="form-group">
    {{ Form::label('gal_inch', 'Gal/Inch') }}
    {{ Form::text('gal_inch',null,['class' => 'form-control', 'placeholder' => 'Gal/Inch']) }}
</div>

<div class="form-group">
    {{ Form::label('tank_total', 'Total') }}
    {{ Form::text('tank_total',null,['class' => 'form-control', 'placeholder' => 'Total']) }}
</div>


<div class="form-group">
    {{ Form::label('lot1', 'Lot 1') }}
    {{ Form::text('lot1',null,['class' => 'form-control', 'placeholder' => 'Lot 1']) }}
</div>

<div class="form-group">
    {{ Form::label('lot2', 'Lot 2') }}
    {{ Form::text('lot2',null,['class' => 'form-control', 'placeholder' => 'Lot 2']) }}
</div>