<div class="form-group">
    {{ Form::label('size', 'Size') }}
    {{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Size']) }}
</div>

<div class="form-group">
    {{ Form::label('oil_type', 'Description') }}
    {{ Form::textarea('oil_type', null,['class' => 'form-control', 'placeholder' => 'Description', 'rows' => '8', 'cols' => '4']) }}
</div>

<div class="form-group">
    {{ Form::label('in_bulk', 'In Bulk') }}
    {{ Form::text('in_bulk', null,['class' => 'form-control', 'placeholder' => 'In Bulk']) }}
</div>
<div class="form-group">
    {{ Form::label('in_carton', 'In Carton') }}
    {{ Form::text('in_carton', null,['class' => 'form-control', 'placeholder' => 'In Carton']) }}
</div>

<div class="form-group">
    {{ Form::label('pcs_plt', 'PCS/PLT') }}
    {{ Form::text('pcs_plt', null,['class' => 'form-control', 'placeholder' => 'PCS/PLT']) }}
</div>
<div class="form-group">
    {{ Form::label('supplier', 'Supplier') }}
    {{ Form::text('supplier', null,['class' => 'form-control', 'placeholder' => 'Supplier']) }}
</div>
<div class="form-group">
    {{ Form::label('lot', 'Lot Number') }}
    {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot Number']) }}
</div>
<div class="form-group">
    {{ Form::label('manufacturer_part_number', 'manufacturer part number') }}
    {{ Form::text('manufacturer_part_number', null, ['class' => 'form-control', 'placeholder' => 'manufacturer part number']) }}
</div>