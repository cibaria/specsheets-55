<div class="form-group">
    {{ Form::label('size', 'Size') }}
    {{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Size']) }}
</div>

<div class="form-group">
    {{ Form::label('oil_type', 'Description') }}
    {{ Form::textarea('oil_type', null,['class' => 'form-control', 'placeholder' => 'Description', 'rows' => '8', 'cols' => '4']) }}
</div>
<div class="form-group">
    {{ Form::label('lot', 'U/M') }}
    {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'U/M']) }}
</div>
<div class="form-group">
    {{ Form::label('placement', 'Placement') }}
    {{ Form::text('placement',null,['class' => 'form-control', 'placeholder' => 'Placement']) }}
</div>
