<div class="form-group">
	{{ Form::label('sub_product_category', 'Sub Category') }}
	{{ Form::select('sub_product_category', [
		'' => 'Choose',
		'canola_rbd' => 'Canola, RBD',
		'canola_non_gmo' => 'Canola, NON-GMO',
		'soy_rbd' => 'Soy, RBD',
		'soy_non_gmo' => 'Soy, NON-GMO',
		'soy_winterized' => 'Soy, Winterized',
		'per_90_can_10_evoo' => '90% CAN/10% EVOO',
		'per_75_can_25_evoo' => '75% CAN/25% EVOO',
		'per_90_soy_10_evoo' => '90% SOY/10% EVOO',
		'evoo_italian_coratina' => 'EVOO, Italian, Coratina',
		'evoo_italian_umbrian' => 'EVOO, Italian, Umbrian',
		'evoo_spanish_arbequina' => 'EVOO, Spanish Arbequina',
		'evoo_spanish_hojiblanca' => 'EVOO, Spanish, Hojiblanca',
		'evoo_chilean_frantoio' => 'EVOO, Chilean, Frantoio',
		'pomace_olive_oil' => 'Pomace Olive Oil',
		'pure_olive_oil' => 'Pure Olive Oil',
		 ],'', ['class' => 'form-control', 'id' => 'sub_product_category']) }}
	
</div>


<div class="form-group">
	{{ Form::label('size', 'Size') }}
	{{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Size']) }}
</div>

<div class="form-group">
	{{ Form::label('oil_type', 'Description') }}
	{{ Form::textarea('oil_type', null,['class' => 'form-control', 'placeholder' => 'Description', 'rows' => '8', 'cols' => '4']) }}
</div>


<div class="form-group">
	{{ Form::label('lot', 'Lot Number') }}
	{{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot Number']) }}
</div>
<div class="form-group">
	{{ Form::label('expiration_date', 'Expiration Date') }}
	{{ Form::text('expiration_date',null,['class' => 'form-control', 'placeholder' => 'Expiration Date', 'id' => 'datepicker']) }}
</div>
