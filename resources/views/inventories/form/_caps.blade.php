<div class="form-group">
    {{ Form::label('size', 'Bottle(s) Used In') }}
    {{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Bottle(s) Used In']) }}
</div>

<div class="form-group">
    {{ Form::label('oil_type', 'Description') }}
    {{ Form::textarea('oil_type', null,['class' => 'form-control', 'placeholder' => 'Description', 'rows' => '8', 'cols' => '4']) }}
</div>
<div class="form-group">
    {{ Form::label('per_case', 'Per Case') }}
    {{ Form::text('per_case', null,['class' => 'form-control', 'placeholder' => 'Per Case']) }}
</div>
<div class="form-group">
    {{ Form::label('supplier', 'Supplier') }}
    {{ Form::text('supplier', null,['class' => 'form-control', 'placeholder' => 'Supplier']) }}
</div>
<div class="form-group">
    {{ Form::label('lot', 'Lot Number') }}
    {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot Number']) }}
</div>
<div class="form-group">
    {{ Form::label('manufacturer_part_number', 'manufacturer part number') }}
    {{ Form::text('manufacturer_part_number', null, ['class' => 'form-control', 'placeholder' => 'manufacturer part number']) }}
</div>