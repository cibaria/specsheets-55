@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Recipes
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/recipes/">Recipes</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Active on Website?:</strong> {{ $recipe->activated }}
                            </li>
                            <li class="list-group-item">
                                <strong>Title:</strong> {{ $recipe->title }}
                            </li>                            
                            <li class="list-group-item">
                                <strong>Main Image:</strong> <img src="{{ url('/') . '/images/recipes/' . $recipe->hero_image }}" width="150px" height="80px" />
                            </li>
                            <li class="list-group-item">
                                <strong>Front Image:</strong> <img src="{{ url('/') . '/images/recipes/' . $recipe->front_image }}" width="150px" height="80px" />
                            </li>
                                                        <li class="list-group-item">
                                <strong>Back Image:</strong> <img src="{{ url('/') . '/images/recipes/' . $recipe->front_image }}" width="150px" height="80px" />
                            </li>

                            <li class="list-group-item">
                                <strong>Category:</strong>

                                @foreach($recipe->categories as $category)
                                    @if($category->category)
                                    {{ $category->category }},
                                    @endif
                                @endforeach
                            </li>
                                                        <li class="list-group-item">
                                <strong>Product Category:</strong>

                                @foreach($recipe->product_categories as $product_category)
                                @if($product_category->product_category)
                                    {{ $product_category->product_category }},
                                @endif
                                @endforeach
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
@stop