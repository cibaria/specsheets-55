@extends('layouts.admin')

@section('content')

<div id="rmas">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Recipes
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Recipes
                </li>
            </ol>
        </div>
    </div>

    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
                {{ link_to_route('recipes.create', 'Create A New Recipe',null, ['class' => 'btn btn-primary navbar-btn']) }}                {{--search--}}
            </nav>
        </div>
    </div>

    <table id="datatable" class="table table-stripped table-responsive table-hover">
        <thead>
        <tr>
           <td><strong>Activated</strong></td>
           <td><strong>Main Image</strong></td>
           <td><strong>Front Recipe Image</strong></td>
           <td><strong>Back Recipe Image</strong></td>
           <td><strong>Title</strong></td>
           <td><strong>Category</strong></td>
           <td><strong>Product Category</strong></td>
           <td></td>
        </tr>
        </thead>
        <tbody class="list">
        @foreach($recipes as $recipe)
            <tr>
             <td>{{ $recipe->activated }}</td>
             <td><img src="{{ url('/') . '/images/recipes/' . $recipe->hero_image }}" width="150px" height="80px" /></td>
             <td><img src="{{ url('/') . '/images/recipes/' . $recipe->front_image }}" width="150px" height="80px" /></td>
             <td><img src="{{ url('/') . '/images/recipes/' . $recipe->back_image }}" width="150px" height="80px" /></td>
             <td>{{ $recipe->title }}</td>
             <td>
             @foreach($recipe->categories as $category)
             @if($category->category)
             @switch($category->category)
                     @case("appetizers")
                     Appetizers, 
                     @break
                     
                     @case("deserts")
                     Deserts, 
                     @break

                     @case("dips")
                     Dips, 
                     @break

                     @case("drinks")
                     Drinks, 
                     @break

                     @case("fish")
                     Fish, 
                     @break

                     @case("fruit")
                     Fruit, 
                     @break

                     @case("main_courses")
                     Main Courses, 
                     @break

                     @case("meat")
                     Meat, 
                     @break

                     @case("pastas")
                     Pastas, 
                     @break

                     @case("salad")
                     Salad, 
                     @break

                     @case("sandwiches")
                     Sandwiches, 
                     @break

                     @case("sides")
                     Sides, 
                     @break

                     @case("soups")
                     Soups, 
                     @break

             @default
             @endswitch
             @endif
             @endforeach
             </td>
             <td>
             @foreach($recipe->product_categories as $product_category)
             @if($product_category->product_category)

                @switch($product_category->product_category)

                    @case("arbequina")
                    Arbequina, 
                    @break

                    @case("arbosana")
                    Arbosana, 
                    @break

                    @case("balsamic_vinegar_of_modena")
                    Balsamic Vinegar of Modena, 
                    @break

                    @case("carolea")
                    Carolea, 
                    @break

                    @case("coratina")
                    Coratina, 
                    @break

                    @case("dipping_oils")
                    Dipping Oils, 
                    @break

                    @case("extra_virgin_olive_oil")
                    Extra Virgin Olive Oil, 
                    @break

                    @case("flavored_balsamic_vinegar")
                    Flavored Balsamic Vinegar, 
                    @break

                    @case("flavored_extra_virgin_olive_oils")
                    Flavored Extra Virgin Olive Oils, 
                    @break

                    @case("flavored_white_balsamic_vinegar")
                    Flavored White Balsamic Vinegar, 
                    @break

                    @case("frantoio")
                    Frantoio, 
                    @break

                    @case("fused_olive_oils")
                    Fused Olive Oils, 
                    @break

                    @case("hojiblanca")
                    Hojiblanca, 
                    @break

                    @case("honey_serrano_chili_vinegar")
                    Honey Serrano Chili Vinegar, 
                    @break

                    @case("infused_olive_oils")
                    Infused Olive Oils, 
                    @break

                    @case("kalamata")
                    Kalamata, 
                    @break

                    @case("koroneiki")
                    Koroneiki, 
                    @break

                    @case("leccino")
                    Leccino, 
                    @break

                    @case("mission")
                    Mission, 
                    @break

                    @case("ogliarola")
                    Ogliarola, 
                    @break

                    @case("pantry_products")
                    Pantry Products, 
                    @break

                    @case("picual")
                    Picual, 
                    @break

                    @case("sevillano")
                    Sevillano, 
                    @break

                    @case("specialty_oils")
                    Specialty Oils, 
                    @break

                    @case("umbrian")
                    Umbrian, 
                    @break

                    @case("varietal_blends")
                    Varietal Blends, 
                    @break

                    @case("white_balsamic_vinegar_of_modena")
                    White Balsamic Vinegar of Modena, 
                    @break
                @default
                @endswitch
             @endif
             @endforeach
             </td>
             <td class="no-sort">
                 <div class="btn-group" role="group" aria-label="">
                     {{ link_to_route('recipes.show', 'View', [$recipe->id],['class' => 'btn btn-success']) }}
                     @if(Auth::user()->group == 100)
                     {{ link_to_route('recipes.edit', 'Edit', [$recipe->id],['class' => 'btn btn-warning']) }}

                     {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('recipes.destroy', $recipe->id))) }}
                     {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                     {{ Form::close() }}
                     @endif
                 </div>
             </td>
            </tr>
        @endforeach

        </tbody>
    </table>


<ul class="pagination"></ul>
</div>
    @stop

{{--end content--}}


{{--footer--}}
@section('footer')
<script>
          $(function() {
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],
            });
        });
    </script>
    @stop

