@extends('layouts.admin')

@section('content')
    <div id="spotchecks">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>Recipe
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/recipes/">Recipes</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
 {{ Form::model($recipe, array('method' => 'PATCH', 'files' => true, 'route' => array('recipes.update', $recipe->id))) }}           
                        <fieldset>

                            <div class="form-group">
                            {{ Form::label('activated', 'Active on Website') }}
                            {{ Form::select('activated',[$recipe->activated, 'yes' => 'Yes', 'no' => 'No'], $recipe->activated, ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('title', 'Title') }}
                                {{ Form::text('title',$recipe->title,['class' => 'form-control', 'placeholder' => 'Title']) }}
                            </div>
                           <div class="form-group">
                                {{ Form::label('hero_image', 'Main Image') }}
                                {{ Form::file('hero_image', ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('front_image', 'Front Recipe Image') }}
                                {{ Form::file('front_image', ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('back_image', 'Back Recipe Image') }}
                                {{ Form::file('back_image', ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                                {{ Form::close() }}
                            </div>
                    </div><!--end row-->
                </div>
                {{-- right --}}

                <div class="col-md-6">
                        <h2>Category</h2>
                        <div class="form-group">
                            <br>
                            @foreach($recipe->categories as $category)
                                @switch($category->category)
                                     @case("appetizers")
                                     Appetizers
                                     @break
                                     
                                     @case("deserts")
                                     Deserts
                                     @break

                                     @case("dips")
                                     Dips
                                     @break

                                     @case("drinks")
                                     Drinks
                                     @break

                                     @case("fish")
                                     Fish
                                     @break

                                     @case("fruit")
                                     Fruit
                                     @break

                                     @case("main_courses")
                                     Main Courses
                                     @break

                                     @case("meat")
                                     Meat
                                     @break

                                     @case("pastas")
                                     Pastas
                                     @break

                                     @case("salad")
                                     Salad
                                     @break

                                     @case("sandwiches")
                                     Sandwiches
                                     @break

                                     @case("sides")
                                     Sides
                                     @break

                                     @case("soups")
                                     Soups
                                     @break
                                 @default
                                 @endswitch
                            {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('categories.destroy', $category->id))) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger', 'style' => 'float:right']) }}
                                {{ Form::close() }}<br>
                                <hr>
                            @endforeach
                            </div>
                            <div class="form-group">
                                {{ Form::open(array('method' => 'PUT', 'style' => 'display:inline','route' => array('categories.update', $recipe->id))) }}
                                {{ Form::label('category', 'Add A Category') }}
                                {{ Form::select('category',[
                                    ''             => 'Please Select',
                                    'appetizers' => 'Appetizers',
                                    'deserts' => 'Deserts ',
                                    'dips' => 'Dips',
                                    'drinks' => 'Drinks',
                                    'fish' => 'Fish',
                                    'fruit' => 'Fruit',
                                    'main_courses' => 'Main Courses',
                                    'meat' => 'Meat',
                                    'pastas' => 'Pastas',
                                    'salad' => 'Salad',
                                    'sandwiches' => 'Sandwiches',
                                    'sides' => 'Sides',
                                    'soups' => 'Soups',
                                    ], '', ['class' => 'form-control']) }}
                                
                                <br>
                                {{ Form::submit('Update', ['class' => 'btn btn-success']) }}
                                 {{ Form::close() }}
                            </div>

                 <h2>Product Category</h2>
                        <div class="form-group">
                             <br>
                            @foreach($recipe->product_categories as $product)
                            @if(!empty($product->product_category) || $product->product_category != ' ')
                            @switch($product->product_category)

                    @case("arbequina")
                    Arbequina
                    @break

                    @case("arbosana")
                    Arbosana
                    @break

                    @case("balsamic_vinegar_of_modena")
                    Balsamic Vinegar of Modena
                    @break

                    @case("carolea")
                    Carolea
                    @break

                    @case("coratina")
                    Coratina
                    @break

                    @case("dipping_oils")
                    Dipping Oils
                    @break

                    @case("extra_virgin_olive_oil")
                    Extra Virgin Olive Oil 
                    @break

                    @case("flavored_balsamic_vinegar")
                    Flavored Balsamic Vinegar
                    @break

                    @case("flavored_extra_virgin_olive_oils")
                    Flavored Extra Virgin Olive Oils
                    @break

                    @case("flavored_white_balsamic_vinegar")
                    Flavored White Balsamic Vinegar
                    @break

                    @case("frantoio")
                    Frantoio
                    @break

                    @case("fused_olive_oils")
                    Fused Olive Oils
                    @break

                    @case("hojiblanca")
                    Hojiblanca
                    @break

                    @case("honey_serrano_chili_vinegar")
                    Honey Serrano Chili Vinegar
                    @break

                    @case("infused_olive_oils")
                    Infused Olive Oils
                    @break

                    @case("kalamata")
                    Kalamata
                    @break

                    @case("koroneiki")
                    Koroneiki
                    @break

                    @case("leccino")
                    Leccino
                    @break

                    @case("mission")
                    Mission
                    @break

                    @case("ogliarola")
                    Ogliarola
                    @break

                    @case("pantry_products")
                    Pantry Products
                    @break

                    @case("picual")
                    Picual
                    @break

                    @case("sevillano")
                    Sevillano
                    @break

                    @case("specialty_oils")
                    Specialty Oils
                    @break

                    @case("umbrian")
                    Umbrian
                    @break

                    @case("varietal_blends")
                    Varietal Blends
                    @break

                    @case("white_balsamic_vinegar_of_modena")
                    White Balsamic Vinegar of Modena 
                    @break
                @default
                @endswitch
                            @endif
                            {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('product_categories.destroy', $product->id))) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger', 'style' => 'float:right']) }}
                                {{ Form::close() }}<br>
                                <hr>
                            @endforeach
                            </div>
                            <div class="form-group">
                                {{ Form::open(array('method' => 'PUT', 'style' => 'display:inline','route' => array('product_categories.update', $recipe->id))) }}
                                {{ Form::label('product_category', 'Add A Product Category') }}
                                {{ Form::select('product_category',[
                                    ''             => 'Please Select',
                                'arbequina' => 'Arbequina',
                                'arbosana' => 'Arbosana',
                                'balsamic_vinegar_of_modena' => 'Balsamic Vinegar of Modena',
                                'carolea' => 'Carolea',
                                'coratina' => 'Coratina',
                                'dipping_oils' => 'Dipping Oils',
                                'extra_virgin_olive_oil' => 'Extra Virgin Olive Oil',
                                'flavored_balsamic_vinegar' => 'Flavored Balsamic Vinegar',
                                'flavored_extra_virgin_olive_oils' => 'Flavored Extra Virgin Olive Oils',
                                'flavored_white_balsamic_vinegar' => 'Flavored White Balsamic Vinegar',
                                'frantoio' => 'Frantoio',
                                'fused_olive_oils' => 'Fused Olive Oils',
                                'hojiblanca' => 'Hojiblanca',
                                'honey_serrano_chili_vinegar' => 'Honey Serrano Chili Vinegar',
                                'infused_olive_oils' => 'Infused Olive Oils',
                                'kalamata' => 'Kalamata',
                                'koroneiki' => 'Koroneiki',
                                'leccino' => 'Leccino',
                                'mission' => 'Mission',
                                'ogliarola' => 'Ogliarola',
                                'pantry_products' => 'Pantry Products',
                                'picual' => 'Picual',
                                'sevillano' => 'Sevillano',
                                'specialty_oils' => 'Specialty Oils',
                                'umbrian' => 'Umbrian ',
                                'varietal_blends' => 'Varietal Blends',
                                'white_balsamic_vinegar_of_modena' => 'White Balsamic Vinegar of Modena',
                                    ], '', ['class' => 'form-control']) }}
                                <br>
                                {{ Form::submit('Update', ['class' => 'btn btn-success']) }}
                                 {{ Form::close() }}
                            </div>
                        </div>
                </div>
            </div>
            </div><!--end col lg 8 -->
                        </fieldset>
        </div><!--end container fluid-->
    </div><!--end blends-->
@stop
