@extends('layouts.admin')

@section('content')
    <div id="recipes">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span>A Recipe
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/recipes/">Recipes</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'recipes.store','files' => true, 'class' => 'form-horizontal']) }}

                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('activated', 'Active on Website?') }}
                                {{ Form::select('activated',['' => 'Please Select', 'yes' => 'Yes', 'no' => 'No'], '', ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('hero_image', 'Main Image') }}
                                {{ Form::file('hero_image', ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('front_image', 'Front Recipe Image') }}
                                {{ Form::file('front_image', ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('back_image', 'Back Recipe Image') }}
                                {{ Form::file('back_image', ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Title') }}
                                {{ Form::text('title', null,['class' => 'form-control', 'placeholder' => 'Title']) }}
                            </div>
                            {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop

@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>

    @stop