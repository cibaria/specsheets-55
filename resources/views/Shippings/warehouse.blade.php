@extends('layouts.publicwarehouse')


@section('content')

    <?php
    $refresh = 60000;
    ?>
    <script type="text/JavaScript">
        setTimeout("location.reload(true);",<?php echo $refresh; ?>);
    </script>


    <h2><span>Shipping Logs</span></h2>
    <br />
    <pre>
   <strong>Date: <?php echo Date("m/d/Y"); ?></strong>
</pre>




    @if($shippings)

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Order#</th>
            <th>Carrier</th>
            <th>Status</th>
            <th>Ship Date</th>
            <th>Notes</th>
        </tr>
        </thead>
        <tbody>
       @foreach ($shippings as $shipping)

       @if($shipping->active == '2')

        @if(Date("m/d/Y") == $shipping->ship_date && $shipping->status == 'Hold')
        <tr style="background-color: pink;">

        @elseif(Date("m/d/Y") == $shipping->ship_date && $shipping->status == 'Paid')
        <tr style="background-color: lightgreen;">
        @else

        <tr>
            @endif
            @endif
                <td>{{  $shipping->order_number }}</td>
                <td>{{  $shipping->carrier }}</td>
                <td>{{ $shipping->status }}</td>
            <td>{{  $shipping->ship_date }}</td>
            <td>{{  $shipping->notes }}</td>
        </tr>
       @endforeach



        </tbody>
    </table>

    @else
    <p>No Shipping Logs.</p>

    @endif

@stop