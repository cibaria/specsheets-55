@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Shipping Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/shippings/">Shipping Logs</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <a href="{{ url() . "/public/shipping/" . $shipping->id }}" class="btn btn-primary navbar-btn">View Public Shipping Log</a>

                </nav>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Order#:</strong> {{ $shipping->order_number}}
                            </li>
                            <li class="list-group-item">
                                <strong>Carrier</strong> {{ $shipping->carrier }}
                            </li>
                            <li class="list-group-item">
                                <strong>Status</strong> {{ $shipping->status }}
                            </li>
                            <li class="list-group-item">Ship Date {{ $shipping->ship_date }}</li>
                            <li class="list-group-item">Notes {{ $shipping->notes }}</li>

                        </ul>

                        <hr/>
                        <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                            <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($shipping->created_at))->diffForHumans(); ?>
                        </div>
                        <div class="alert alert-dismissible alert-warning">
                            <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($shipping->updated_at))->diffForHumans(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@stop