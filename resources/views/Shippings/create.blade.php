@extends('layouts.admin')

@section('content')
    <div id="shipping">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span> A New Shipping Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/shippings/">Shipping Logs </a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
    @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
    @endif


        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'shippings.store', 'class' => 'form-horizontal']) }}
                        <fieldset>
                            {{--<div class="form-group">--}}
                                {{--{{ Form::label('status', 'Status') }}--}}
                                {{--{{ Form::select('status',['' => 'Please Select', 'not_shipped' => 'Not Shipped', 'shipped' => 'Shipped', 'canceled' => 'Canceled'], '', ['class' => 'form-control']) }}--}}
                            {{--</div>--}}
                            <div class="form-group" style="background-color: lightgrey;">
                                {{ Form::label('date', 'Date') }}
                                <p>The Date must be in the following format 01/10/2018</p>
                                {{ Form::text('date', null, ['class' => 'form-control', 'id' => 'datepicker', 'autocomplete' => 'off', 'placeholder' => 'Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('order_number','Order Number') }}
                                {{ Form::text('order_number', null, ['class' => 'form-control', 'placeholder' => 'Order Number'] )  }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('company', 'Company') }}
                                {{ Form::text('company', null, ['class' => 'form-control', 'placeholder' => 'Company']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('time_in', 'Time In') }}
                                {{ Form::text('time_in', null, ['class' => 'form-control', 'id' => 'timepicker', 'placeholder' => 'Time In']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('time_out', 'Time Out') }}
                                {{ Form::text('time_out', null, ['class' => 'form-control', 'id' => 'timepicker1', 'placeholder' => 'Time Out']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('seal_number', 'Seal#') }}
                                {{ Form::text('seal_number', null, ['class' => 'form-control', 'placeholder' => 'Seal#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('will_call', 'Will Call') }}
                                {{ Form::text('will_call', null, ['class' => 'form-control', 'placeholder' => 'Will Call']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('door_number', 'Door Number') }}
                                {{ Form::text('door_number', null, ['class' => 'form-control', 'placeholder' => 'Door Number']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes', 'Notes') }}
                                {{ Form::textarea('notes', null, ['class' => 'form-control', 'placeholder' => 'Notes', 'cols' => '8', 'rows' => '4']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::submit('Create', ['class' => 'btn btn-default']) }}
                            </div>
                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop

  @section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $('#timepicker').timepicker();
            $('#timepicker1').timepicker();
        });
     </script>

    @stop