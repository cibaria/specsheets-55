@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Blends
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Blends
                    </li>
                </ol>
            </div>
        </div>
        <!--sessions-->
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <!--sub nav-->
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to_route('blends.index', '<< Back To Blends',null, array('class' => 'btn btn-danger navbar-btn')) }}
                </nav>
            </div>
        </div>
        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Lot</strong></td>
                <td><strong>SO#</strong></td>
                <td><strong>Oil Used</strong></td>
                <td><strong>Oil Used</strong></td>
                <td><strong>Customer</strong></td>
                <td><strong>Date</strong></td>
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($blends as $blend)
                <tr>
                    <td class="lot">{{ $blend->lot }}</td>
                    <td class="so">{{ $blend->sales_order }}</td>
                    <td class="oilsused1">{{ $blend->oil_used1 }}</td>
                    <td class="oilsused2">{{ $blend->oil_used2 }}</td>
                    <td class="customer">{{ $blend->customer }}</td>
                    <td class="date">{{ $blend->date }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                        {{ link_to_route('blends.show', 'View', [$blend->id],['class' => 'btn btn-success']) }}
                        {{ link_to_route('blends.edit', 'Edit', [$blend->id],['class' => 'btn btn-warning']) }}
                        {{ Form::open(array('method' => 'DELETE','style' => 'display:inline', 'route' => array('blends.destroy', $blend->id))) }}
                        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                        </div>
                </tr>
            @endforeach
            </tbody>
        </table>
        <ul class="pagination"></ul>
    </div>
@stop
<!--end content-->
<!--footer-->
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 6, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ]
            });
        });
    </script>
    @stop