@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Samples
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Samples
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to_route('samples.create', 'Add A New Sample',null, array('class' => 'btn btn-primary navbar-btn')) }}
                    {{--search--}}

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="sample">Submit</button>
                    </form>
                </nav>
            </div>
        </div>

        <table class="table table-stripped table-responsive table-hover">
            <thead>
            <tr>
                <td><strong>Lot #</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>Receiving Date</strong></td>
                <td><strong>Location</strong></td>
                <td></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($samples as $sample)
                <tr>
                    <td class="lot">{{ $sample->lot }}</td>
                    <td class="description">{{ $sample->description}}</td>
                    <td class="receiving_log">{{ $sample->receiving_date }}</td>
                    <td class="location">{{ $sample->location }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            {{ link_to_route('samples.show', 'View', [$sample->id],['class' => 'btn btn-success']) }}
                            {{ link_to_route('samples.edit', 'Edit', [$sample->id],['class' => 'btn btn-warning']) }}
                            {{ Form::open(array('method' => 'DELETE','style' => 'display:inline', 'route' => array('samples.destroy', $sample->id))) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                        </div>

                </tr>
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
    @stop


@section('footer')
    <script type="text/javascript">
        var options = {
            valueNames: [ 'lot', 'description', 'receiving_log', 'location'],
            // pagination
            page: 15,
            plugins: [
                ListPagination({})
            ]
        };

        var blendList = new List('samples ', options);
    </script>
    @stop