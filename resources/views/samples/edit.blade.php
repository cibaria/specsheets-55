@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>Sample {{
                    $sample->lot }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/samples/">Samples</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if($errors->has())
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($sample, array('method' => 'PATCH', 'route' => array('samples.update', $sample->id))) }}
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('lot', 'Lot') }}
                                {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description', 'Description') }}
                                {{ Form::textarea('description', null,['rows' => '3', 'cols' => '3', 'class' => 'form-control', 'placeholder' => 'Description']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('receiving_date', 'Receiving Date') }}
                                {{ Form::text('receiving_date', null,['id' => 'datepicker', 'class' => 'form-control', 'placeholder' =>
                                'Receiving Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('location', 'location') }}
                                {{ Form::text('location', null,['class' => 'form-control', 'placeholder' => 'Location']) }}
                            </div>

                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            {{ link_to_route('samples.index', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop