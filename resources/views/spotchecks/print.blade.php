@extends('layouts.print')


@section('content')
<div>
    <a href="javascript:window.print()">
        {{ HTML::image('images/print_icon.jpg', 'Print',['width' => '100', 'height' => '100', 'class' => 'noPrint', 'style' => 'border:none']) }}</a>
</div>
<table cellspacing=0 cellpadding=0>
    <thead>
    <tr><th colspan=14><h2>CIBARIA INTERNATIONAL, INC<br />Spot Check Form</h2></th></tr>
    <tr>
        <th style="border-left: 3px solid black; border-top: 3px solid black;border-right: 1px solid black; border-bottom:3px solid black;">DATE</th>
        <th style="border-top: 3px solid black; border-right:1px solid black; border-bottom: 3px solid black;">Product Description</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Lot#</th>
        <th style="border-top:3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Product Label</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Seals</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Exp. Date</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Box Label</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Required Weight</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">Actual Weight</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 1px solid black;">By</th>
        <th style="border-top: 3px solid black; border-bottom: 3px solid black; border-right: 3px solid black;">Resolution</th>
    </tr>
    </thead>
    <tbody style="border-bottom: 3px solid black;">
    @foreach($spotchecks as $spotcheck)
    <tr>
        <td style="border-left: 3px solid black; border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $spotcheck->date; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $spotcheck->description; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $spotcheck->lot; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $spotcheck->product_label; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $spotcheck->seals; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black; width:300px;"><?php echo $spotcheck->exp_date; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $spotcheck->box_label; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $spotcheck->required_weight; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $spotcheck->actual_weight; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 1px solid black;"><?php echo $spotcheck->by; ?></td>
        <td style="border-bottom: 1px solid black; border-right: 3px solid black;"><?php echo $spotcheck->resolution; ?></td>

    </tr>
    @endforeach
    </tbody>
</table>

    @stop