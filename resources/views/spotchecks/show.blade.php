@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Spotcheck Form - Lot# {{ $spotcheck->lot }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/spotchecks/">Spotcheck Form</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Date:</strong> {{ $spotcheck->date }}
                            </li><li class="list-group-item">
                                <strong>Description:</strong> {{ $spotcheck->description }}
                            </li>
                            <li class="list-group-item">
                                <strong>Lot#:</strong> {{ $spotcheck->lot }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product Label:</strong> {{ $spotcheck->product_label }}
                            </li>
                            <li class="list-group-item">
                                <strong>Seals</strong> {{ $spotcheck->seals }}
                            </li>
                            <li class="list-group-item">
                                <strong>EXP. Date</strong> {{ $spotcheck->exp_date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Box Label</strong> {{ $spotcheck->box_label }}
                            </li>
                            <li class="list-group-item">
                                <strong>Required Weight:</strong> {{ $spotcheck->required_weight }}
                            </li>
                            <li class="list-group-item">
                                <strong>Actual Weight:</strong> {{ $spotcheck->actual_weight }}
                            </li>
                            <li class="list-group-item">
                                <strong>By:</strong> {{ $spotcheck->by }}
                            </li>
                            <li class="list-group-item">
                                <strong>Date:</strong> {{ $spotcheck->date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Resolution</strong> {{ $spotcheck->resolution }}
                            </li>
                        </ul>

                        <hr/>
                        <div class="alert alert-dismissible alert-success" style="margin-bottom:0px">
                            <strong>Created: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($spotcheck->created_at))->diffForHumans(); ?>
                        </div>
                        <div class="alert alert-dismissible alert-warning">
                            <strong>Updated: </strong><?php echo \Carbon\Carbon::createFromTimestamp(strtotime($spotcheck->updated_at))->diffForHumans(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@stop