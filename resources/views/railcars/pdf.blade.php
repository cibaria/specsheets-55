@extends('layouts.pdf_specsheets')

@section('content')

<div style="width:750px">

<!--title-->
<div style="border: 2px solid black; padding:5px;">
<h1 style="text-align:center;font-size:32px">RAILCAR NOTICE</h1></td>
</div>


<!--main info-->
<div style="margin-top:15px;border: 2px solid black;padding:5px;">
	<h2>CAR#: {{ $railcar->car_number }}</h2>
	<h2>DATE: {{ $railcar->date }}</h2>
	<h2>VENDOR: {{ $railcar->vendor }}</h2>
	<h2>PRODUCT: {{ $railcar->product }}</h2>
	<h2>SHIPPED AMOUNT: {{ $railcar->shipped_amount }}</h2>
</div>

@foreach($railcar->railcarload as $load)
	<div style="margin-top:15px; border:2px solid black; padding:5px;">
		<h2>LOAD#: {{ $load->load_number}}</h2>
		<h2>LBS: {{ $load->load_lbs}}</h2>
		<h2>DATE: {{ $load->load_date }}</h2>
	</div>
@endforeach


<?php 
	//Total delivered Amount
	$total_delivered_amount = "";
	foreach($railcar->railcarload as $load) {
	$total_delivered_amount += $load->load_lbs;
	} 
?>
 
<div style="margin-top:15px; border:2px solid black; padding:5px;">
<h2>TOTAL DELIVERED AMOUNT: {{ $total_delivered_amount }} LBS</h2>
<span style="text-align:center;font-weight:bold;">REPORT ANY VARIANCE OF 200 LBS OR MORE TO ACCOUNTING IMMEDIATELY</span>
</div>

<!--release info-->
<div style="margin-top:15px; border:2px solid black; padding:5px;">
<h2>RELEASE DATE: {{ $railcar->released_date }}</h2>
<h2>REQUESTED BY: {{ $railcar->requested_by }}</h2>
</div>

</div><!--end div-->

@stop
