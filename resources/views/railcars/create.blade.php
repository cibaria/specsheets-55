@extends('layouts.admin')

@section('content')
    <div id="receivingappointmentlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span> A New Receiving Railcar Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/receiving/railcars/">Receiving Railcar Logs</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <form method="POST" action="/receiving/railcars/store" accept-charset="UTF-8" class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="category" value="railcars" />
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('active', 'Received?') }}
                                {{ Form::select('active', ['1' => 'Not Received', '2' => 'Received'],null,['class' => 'form-control','id' => 'select']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('car_number', 'Car#') }}
                                {{ Form::text('car_number', null,['class' => 'form-control', 'placeholder' => 'Car#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                {{ Form::text('date', null,['class' => 'form-control','id' => 'datepicker', 'placeholder' => 'Car#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('vendor', 'Vendor') }}
                                {{ Form::text('vendor', null,['class' => 'form-control', 'placeholder' => 'Vendor']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('product', 'Product') }}
                                {{ Form::text('product', null,['class' => 'form-control', 'placeholder' => 'Product']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('shipped_amount', 'Shipped Amount') }}
                                {{ Form::text('shipped_amount', null,['class' => 'form-control', 'placeholder' => 'Shipped Amount']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('released_date', 'Released Date') }}
                                {{ Form::text('released_date', null,['class' => 'form-control', 'id' => 'datepicker1', 'placeholder' => 'Released Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('requested_by', 'Requested By') }}
                                {{ Form::text('requested_by', null,['class' => 'form-control', 'placeholder' => 'Requested By']) }}
                            </div>

                            {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop

    @section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
        });
     </script>

    @stop