
@extends('layouts.admin')

@section('content')

    <div id="receivingappointments">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Active</span>Receiving Railcar Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Receiving Railcar Logs
                    </li>
                </ol>
            </div>
        </div>
        <div class="alert alert-dismissible alert-info">
            <h1>Date: <?php echo Date("m/d/Y");?></h1>
        </div>

        <!--sessions-->
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <!--sub nav-->
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                  <a href="/receiving/railcars/create" class="btn btn-primary navbar-btn">Create A New Railcar Log</a>
                    <a href="/receiving/railcars/received" class="btn btn-warning navbar-btn">Recieved</a>
                </nav>
            </div>
        </div>
        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Car#</strong></td>
                <td><strong>Date</strong></td>
                <td><strong>Vendor</strong></td>
                <td><strong>Product</strong></td>
                <td><strong>Shipped Amount</strong></td>
                <td><strong>Release Date</strong></td>
                <td><strong>Requested By</strong></td>
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody>
            @foreach($railcars as $railcar)
                @if($railcar->active == 1)
                    @if(Date("m/d/Y") === $railcar->date || Date("n/d/Y") === $railcar->date)
                        <tr class="danger">
                        <!--customer product-->
                        @elseif(Date("m/d/Y") === $railcar->date && $railcar->customer_product === '1')
                        <tr style="background-color:#d9edf7;">
                    @else
                        <tr>
                @endif
                    <td>{{ $railcar->car_number }}</td>
                    <td>{{ $railcar->date }}</td>
                    <td>{{ $railcar->vendor }}</td>
                    <td>{{ $railcar->product }}</td>
                    <td>{{ $railcar->shipped_amount }}</td>
                    <td>{{ $railcar->released_date }}</td>
                    <td>{{ $railcar->requested_by }}</td>
                    <td>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                        <div class="btn-group" role="group" aria-label="">
                            <a href="/receiving/railcars/pdf/{{ $railcar->id }}" class="btn btn-primary">Print Railcar Notice</a>
                            {{ link_to_route('railcars.show', 'View', [$railcar->id],['class' => 'btn btn-success']) }}
                            {{ link_to_route('railcars.edit', 'Edit', [$railcar->id],['class' => 'btn btn-warning']) }}
                            {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('railcars.destroy', $railcar->id))) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                        </div>
                            @endif
                    </td>
                </tr>
                    @endif
            @endforeach
            </tbody>
        </table>
        <ul class="pagination"></ul>
    </div>
@stop