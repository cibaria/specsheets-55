
@extends('layouts.admin')

@section('content')

    <div id="receivingappointments">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-danger" style="padding-right:15px;">Received</span>Receiving Railcar Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/receiving/railcars/">Receiving Railcar Logs</a> > Received
                    </li>
                </ol>
            </div>
        </div>

        <!--sub nav-->
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <a href="/receiving/railcars/" class="btn btn-warning navbar-btn">Active Receiving Railcar Logs</a>
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Car#</strong></td>
                <td><strong>Date</strong></td>
                <td><strong>Vendor</strong></td>
                <td><strong>Product</strong></td>
                <td><strong>Shipped Amount</strong></td>
                <td><strong>Release Date</strong></td>
                <td><strong>Requested By</strong></td>
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">
  @foreach($railcars as $railcar)
                @if($railcar->active == 2)
                    @if(Date("m/d/Y") === $railcar->date || Date("n/d/Y") === $railcar->date)
                        <tr class="danger">
                        <!--customer product-->
                        @elseif(Date("m/d/Y") === $railcar->date && $railcar->customer_product === '1')
                        <tr style="background-color:#d9edf7;">
                    @else
                        <tr>
                @endif
                    <td>{{ $railcar->car_number }}</td>
                    <td>{{ $railcar->date }}</td>
                    <td>{{ $railcar->vendor }}</td>
                    <td>{{ $railcar->product }}</td>
                    <td>{{ $railcar->shipped_amount }}</td>
                    <td>{{ $railcar->released_date }}</td>
                    <td>{{ $railcar->requested_by }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            {{ link_to_route('railcars.edit', 'Edit', [$railcar->id],['class' => 'btn btn-warning']) }}
                        </div>
                    </td>
                </tr>
                    @endif
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

<!--end content-->


@section('footer')
   <script>
          $(function() {
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 1, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ]

            });
        });
    </script>

    @stop
