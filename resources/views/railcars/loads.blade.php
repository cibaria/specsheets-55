@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span> Railcars
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/receiving/railcars/">Receiving Railcar Logs</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <a href="{{ url('/')  . "/receiving/railcars"}}" class="btn btn-danger navbar-btn"><< Back</a>
                </nav>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
              <div class="col-md-6">
                    <div class="col-lg-8">
                    <h2>Receiving Railcars</h2>
                        <ul class="list-group">
                            {{--main--}}
                            <li class="list-group-item">
                                <strong>Car#</strong> {{ $railcar->car_number }}
                            </li>
                            <li class="list-group-item">
                                <strong>Date</strong> {{ $railcar->date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Vendor</strong> {{ $railcar->vendor }}
                            </li>
                            <li class="list-group-item">
                                <strong>Product</strong> {{ $railcar->product }}
                            </li>
                            <li class="list-group-item">
                                <strong>Release Date</strong> {{ $railcar->release_date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Requested By</strong> {{ $railcar->requested_by }}
                            </li>
                          </ul>



                    </div>
                </div>

                {{--right--}}
                <div class="col-md-6">
                    <h2>Loads</h2>
                    <table class="table table-stripped table-responsive table-hover">
                    <thead>
                        <tr>
                            <td><strong>Load</strong></td>
                            <td><strong>Date</strong></td>
                            <td><strong>LBS</strong></td>
                        </tr>
                    </thead>
                        @foreach($railcar->railcarload as $load)
                            <tr>
                                <td>{{ $load->load_number }}</td>
                                <td>{{ $load->load_date }}</td>
                                <td>{{ $load->load_lbs }}</td>
                                <td>
                                {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('railcarloads.destroy', $load->id))) }}

                                {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                {{ Form::close() }}
                            </td>
                            </tr>
                        @endforeach
                    </table>
                    <hr>
                    <h2>Add A Load</h2>
                    <form method="POST" action="/receiving/railcars/{{ $railcar->id }}/loads">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $railcar->id }}">
                        <div class="form-group">
                            <label>Load#</label>
                            <input type="text" name="load_number" class="form-control" placeholder="Load#">
                        </div>                        
                        <div class="form-group">
                            <label>Date</label>
                            <input type="text" name="load_date" class="form-control" id="datepicker" placeholder="Load Date">
                        </div>
                        <div class="form-group">
                            <label>LBS</label>
                            <input type="text" name="load_lbs" class="form-control" placeholder="LBS">
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>  
                    </form>
                 </div><!--end right-->
                </div>
            <hr/>
        </div>
@stop
    @section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
        });
     </script>
    @stop
