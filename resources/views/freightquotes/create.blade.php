@extends('layouts.admin')

@section('content')
    <div id="spotchecks">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span> A New Freight Quote
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/freightquotes/">Freight Quotes</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'freightquotes.store', 'class' => 'form-horizontal']) }}
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                {{ Form::text('date', null,['class' => 'form-control','id' => 'datepicker', 'placeholder' => 'Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('vendor', 'Vendor') }}
                                {{ Form::text('vendor', null,['class' => 'form-control', 'placeholder' => 'Vendor']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('po', 'P.O.') }}
                                {{ Form::text('po', null,['class' => 'form-control', 'placeholder' => 'P.O.']) }}
                            </div>
							 <div class="form-group">
                                {{ Form::label('pallets', 'Pallets') }}
                                {{ Form::text('pallets', null,['class' => 'form-control', 'placeholder' => 'Pallets']) }}
                            </div>
							 <div class="form-group">
                                {{ Form::label('freight_company', 'Freight Company') }}
                                {{ Form::text('freight_company', null,['class' => 'form-control', 'placeholder' => 'Freight Company']) }}
                            </div>
							 <div class="form-group">
                                {{ Form::label('freight_rate', 'Freight Rate') }}
                                {{ Form::text('freight_rate', null,['class' => 'form-control', 'placeholder' => 'Freight Rate']) }}
                            </div>
                          
                            {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop
