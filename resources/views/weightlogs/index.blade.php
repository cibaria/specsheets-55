@extends('layouts.admin')

@section('content')

<div id="weightlogs">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <span class="alert alert-dismissible alert-success" style="padding-right:15px;">In Progress</span> Weight Logs
            </h1>
            <p>If the Weight Log Date is past today the background of the Weight Log will be red.</p>

            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Weight Logs
                </li>
            </ol>
        </div>
    </div>

    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">

                    {{ link_to_route('weightlogs.create', 'Create A New Weight Log',null, ['class' => 'btn btn-primary navbar-btn']) }}
                    <a href="/weightlogs/complete/" class="btn btn-info navbar-btn">Weight is Complete</a>
            </nav>


        </div>
    </div>

        <table id="datatable" class="table table-stripped">
        <thead>
        <tr>
            <td><strong>Order#</strong></td>
            <td><strong>Customer Name</strong></td>
            <td><strong>Requested Ship Date</strong></td>
            <td><strong>Weight</strong></td>
            <td><strong>Height</strong></td>
            <td><strong>Notes</strong></td>
            @if(Auth::user()->username != 'warehouse')
            <td><strong>Weight Added Date</strong></td>
            <td><strong>Weight Updated Date</strong></td>
            @endif
           <td class="no-sort"></td>
        </tr>
        </thead>
        <tbody class="list">
        @foreach($weightlogs as $weightlog)
            <?php

            //If the date is past today make the <tr> background red
            $weightlog_date = explode('/', $weightlog->date);
            $new_weightlog_date = implode("", $weightlog_date);

            $today = Date("m/d/Y");
            $new_today = explode('/', $today);
            $new_new_today = implode("", $new_today);

            if($new_weightlog_date < $new_new_today) :
            ?>
            <tr style="background-color: red;color:white;font-weight:bold;">
                <?php else: ?>
            <tr>
                <?php endif; ?>
             <td>{{ $weightlog->order_number }}</td>
             <td>{{ $weightlog->customer_name }}</td>
             <td>{{ $weightlog->date }}</td>
             <td>{{ $weightlog->weight}}</td>
                    <td>{{ $weightlog->height}}</td>
                    <td>{{ $weightlog->notes}}</td>
                    @if(Auth::user()->username != 'warehouse')
                    <td>{{ $weightlog->weight_added_date}}</td>
                    <td>{{ $weightlog->weight_updated_date}}</td>
                    @endif
             <td>
                 <div class="btn-group" role="group" aria-label="">
                     @if(Auth::user()->username != 'warehouse')
                     @if(isset($weightlog->weight) && isset($weightlog->height))
                         {{ Form::model($weightlog, array('method' => 'PATCH', 'route' => array('weightlogs.update', $weightlog->id))) }}
                         {{ Form::hidden('progress', 'complete') }}
                             {{ Form::hidden('date', $weightlog->date) }}
                             {{ Form::hidden('customer_name', $weightlog->customer_name) }}
                             {{ Form::hidden('order_number', $weightlog->order_number) }}
                             {{ Form::hidden('notes', $weightlog->notes) }}
                         {{ Form::submit('Complete', ['class' => 'btn btn-info']) }}
                         {{ Form::close() }}
                         @endif
                     @endif
{{--                     {{ link_to_route('weightlogs.show', 'View', [$weightlog->id],['class' => 'btn btn-success']) }}--}}
                     {{ link_to_route('weightlogs.edit', 'Edit', [$weightlog->id],['class' => 'btn btn-warning']) }}

                         @if(Auth::user()->group == 100)
                     {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('weightlogs.destroy', $weightlog->id))) }}
                     {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                     {{ Form::close() }}
                     @endif
                 </div>
             </td>
            </tr>
        @endforeach

        </tbody>
    </table>


<ul class="pagination"></ul>
</div>
    @stop
@section('footer')
   <script>
          $(function() {
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ]
            });
        });
    </script>

    @stop

