@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">View</span>Weight Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/weightlogs/">Weight Logs</a> >
                        View
                    </li>
                </ol>
            </div>
        </div>
<div class="row">
            <div class="col-lg-12">

            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Requested Ship Date:</strong> {{ $weightlog->date }}
                            </li>
                            <li class="list-group-item">
                                <strong>Order#:</strong> {{ $weightlog->order_number }}
                            </li>
                            <li class="list-group-item">
                                <strong>Customer Name:</strong> {{ $weightlog->customer_name }}
                            </li>
                            <li class="list-group-item">
                                <strong>Weight:</strong> {{ $weightlog->weight }}
                            </li>
                            <li class="list-group-item">
                                <strong>Height:</strong> {{ $weightlog->height}}
                            </li>
                            <li class="list-group-item">
                                <strong>Notes:</strong> {{ $weightlog->notes }}
                            </li>
                            <br />
                            <a href="/weightlogs" class="btn btn-info"><< Back To Weight Logs</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop