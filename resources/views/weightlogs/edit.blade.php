@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>Weight Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/weightlogs/">Weight Logs</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::model($weightlog, array('method' => 'PATCH', 'route' => array('weightlogs.update', $weightlog->id))) }}
                        {{ Form::hidden('progress', $weightlog->progress) }}

                    @if(isset($weightlog->weight_added_date))
                            {{ Form::hidden('weight_added_date', $weightlog->weight_added_date) }}
                            @endif
                        <fieldset>
                            {{--<div class="form-group">--}}
                                {{--{{ Form::label('progress', 'Weight Log Progress') }}--}}
                                {{--@if($weightlog->in_profress == 'in_progress')--}}
                                {{--{{ Form::select('progress', ['in_progress' => 'In Progress', 'complete' => 'Complete'], $weightlog->progress, ['class' => 'form-control']) }}--}}
                                    {{--@else--}}
                                    {{--{{ Form::select('progress', ['complete' => 'Complete', 'in_progress' => 'In Progress'], $weightlog->progress, ['class' => 'form-control']) }}--}}

                                {{--@endif--}}
                            {{--</div>--}}
                            <div class="form-group" style="background-color: lightgrey;">
                                {{ Form::label('date', 'Requested Ship Date') }}
                                <p>The Date must be in the following format 01/10/2018</p>
                                {{ Form::text('date', null,['class' => 'form-control', 'id' => 'datepicker', 'autocomplete' => 'off',  'placeholder' => 'Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('order_number', 'Order#') }}
                                {{ Form::text('order_number', null,['class' => 'form-control', 'placeholder' => 'Order#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('customer_name', 'Customer Name') }}
                                {{ Form::text('customer_name', null,['class' => 'form-control', 'placeholder' => 'Customer Name']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('weight', 'Weight') }}
                                {{ Form::text('weight', null,['class' => 'form-control', 'placeholder' => 'Weight']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('height', 'Height') }}
                                {{ Form::text('height', null,['class' => 'form-control', 'placeholder' => 'Height']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes', 'Notes') }}
                                {{ Form::textarea('notes', null,['class' => 'form-control', 'rows' => '4', 'cols' => '8', 'placeholder' => 'Notes']) }}
                            </div>
                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->

    </div><!--end blends-->
@stop
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>

    @stop