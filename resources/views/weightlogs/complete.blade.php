@extends('layouts.admin')

@section('content')

    <div id="weightlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-danger" style="padding-right:15px;">Complete</span> Weight Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Weight Logs
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    @if(Auth::user()->group != 20)
                        <a href="/weightlogs/" class="btn btn-success navbar-btn"><< Back To Active Weight Logs</a>

                    @endif
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Order#</strong></td>
                <td><strong>Customer Name</strong></td>
                <td><strong>Requested Ship Date</strong></td>
                <td><strong>Weight</strong></td>
                <td><strong>Height</strong></td>
                <td><strong>Notes</strong></td>
                <td><strong>Weight Added Date</strong></td>
                <td><strong>Weight Updated Date</strong></td>
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($weightlogs as $weightlog)
                <tr>
                    <td>{{ $weightlog->order_number }}</td>
                    <td>{{ $weightlog->customer_name }}</td>
                    <td>{{ $weightlog->date }}</td>
                    <td>{{ $weightlog->weight}}</td>
                    <td>{{ $weightlog->height}}</td>
                    <td>{{ $weightlog->notes}}</td>
                    <td>{{ $weightlog->weight_added_date}}</td>
                    <td>{{ $weightlog->weight_updated_date}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            {{ link_to_route('weightlogs.show', 'View', [$weightlog->id],['class' => 'btn btn-success']) }}
                            @if(Auth::user()->group == 100)
                                {{ link_to_route('weightlogs.edit', 'Edit', [$weightlog->id],['class' => 'btn btn-warning']) }}

                                {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('weightlogs.destroy', $weightlog->id))) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                {{ Form::close() }}
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop
@section('footer')
    <script>
        $(function() {
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                    { targets: 'no-sort', orderable: false }
                ]
            });
        });
    </script>

@stop

