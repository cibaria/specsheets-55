@extends('layouts.admin')

@section('content')
    <div id="weightlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span>A Weight Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a
                                href="/weightlogs/">Weight Logs</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>
        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'weightlogs.store', 'class' => 'form-horizontal']) }}
                        <fieldset>
                            <div class="form-group" style="background-color: lightgrey;">
                                {{ Form::label('date', 'Requested Ship Date') }}
                                <p>The Date must be in the following format 01/10/2018</p>
                                {{ Form::text('date', null,['class' => 'form-control', 'id' => 'datepicker', 'autocomplete' => 'off',  'placeholder' => 'Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('order_number', 'Order#') }}
                                {{ Form::text('order_number', null,['class' => 'form-control', 'placeholder' => 'Order#']) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('customer_name', 'Customer Name') }}
                                {{ Form::text('customer_name', null,['class' => 'form-control', 'placeholder' => 'Customer Name']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes', 'Notes') }}
                                {{ Form::textarea('notes', null,['class' => 'form-control', 'rows' => '4', 'cols' => '8', 'placeholder' => 'Notes']) }}
                            </div>
                   {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->

    </div><!--end blends-->
@stop

@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    @stop