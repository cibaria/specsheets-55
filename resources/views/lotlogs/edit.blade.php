@extends('layouts.admin')

@section('content')
    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>Lot Log - Lot# {{ $lotlog->lot }}
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/lotlogs/">Lot Logs</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                      

                                 {{ Form::model($lotlog, array('method' => 'PATCH','files' => true,'image/save' => 'image/save', 'route' => array('lotlogs.update', $lotlog->id))) }}
                        <fieldset>


                            <div class="form-group">
                            {{ Form::label('progress', 'Progress')}}
                                @if(!is_null($lotlog->progress))
                                    {{ Form::select('progress',[$lotlog->progress, 'in_progress' => 'In Progress', 'complete' => 'Complete'], $lotlog->progress, ['class' => 'form-control']) }}

                                @else
                                    {{ Form::select('progress',['' => 'Please Select', 'in_progress' => 'In Progress', 'complete' => 'Complete'], null, ['class' => 'form-control']) }}

                                @endif
                            </div>
                            <div class="form-group">
                                {{ Form::label('coa_active', 'COA Active') }}
                                @if($lotlog->coa_active == 'yes')
                                    {{ Form::checkbox('coa_active','yes', true)}}
                                    @else
                                    {{ Form::checkbox('coa_active','yes', false)}}
                                @endif
                            </div>
                            <div class="form-group" id="template">
                                {{ Form::label('coa_template', 'COA Template')}}
                                @if(!is_null($lotlog->coa_template))
                                    {{ Form::select('coa_template',[$lotlog->coa_template,
                                    'apricot_kernel' => 'Apricot Kernel',
                                    'almond' => 'Almond',
                                    'avocado' => 'Avocado',
                                    'babassu' => 'Babassu',
                                    'butters' => 'Butters',
                                    'canola' => 'Canola',
                                    'castor' => 'Castor',
                                    'coconut' => 'Coconut',
                                    'corn' => 'Corn',
                                    'cottonseed' => 'Cottonseed',
                                    'flaxseed' => 'Flaxseed',
                                    'hemp' => 'Hemp',
                                    'jojoba' => 'Jojoba',
                                    'macadamia' => 'Macadamia',
                                    'olive_oil' => 'Olive Oil',
                                    'olive_oil_extra_virgin_flavored' => 'Olive Oil Extra Virgin Flavored',
                                    'olive_oil_fused' => 'Olive Oil Fused',
                                    'olive_oil_pure_infused' => 'Olive Oil Pure Infused',
                                    'palm_kernel' => 'Palm Kernel',
                                    'peanut' => 'Peanut',
                                    'rice_bran' => 'Rice Bran',
                                    'safflower' => 'Safflower',
                                    'sesame' => 'Sesame',
                                    'soybean' => 'Soybean',
                                    'sunflower' => 'Sunflower'
                                    ], $lotlog->coa_template, ['class' => 'form-control']) }}
                                    @else
                                    {{ Form::select('coa_template',['' => 'Please Select',
                                    'apricot_kernel' => 'Apricot Kernel',
                                    'almond' => 'Almond',
                                    'avocado' => 'Avocado',
                                    'babassu' => 'Babassu',
                                    'butters' => 'Butters',
                                    'canola' => 'Canola',
                                    'castor' => 'Castor',
                                    'coconut' => 'Coconut',
                                    'corn' => 'Corn',
                                    'cottonseed' => 'Cottonseed',
                                    'flaxseed' => 'Flaxseed',
                                    'hemp' => 'Hemp',
                                    'jojoba' => 'Jojoba',
                                    'macadamia' => 'Macadamia',
                                    'olive_oil' => 'Olive Oil',
                                    'olive_oil_extra_virgin_flavored' => 'Olive Oil Extra Virgin Flavored',
                                    'olive_oil_fused' => 'Olive Oil Fused',
                                    'olive_oil_pure_infused' => 'Olive Oil Pure Infused',
                                    'palm_kernel' => 'Palm Kernel',
                                    'peanut' => 'Peanut',
                                    'rice_bran' => 'Rice Bran',
                                    'safflower' => 'Safflower',
                                    'sesame' => 'Sesame',
                                    'soybean' => 'Soybean',
                                    'sunflower' => 'Sunflower'
                                    ],null, ['class' => 'form-control']) }}
                                    @endif
                            </div>
                            <div class="form-group">
                                {{ Form::label('expiration_date', 'Expiration Date') }}
                                {{ Form::text('expiration_date', null,['class' => 'form-control', 'placeholder' => 'Expiration Date', 'id' => 'datepicker1']) }}
                            </div>
                                    {{-- <div class="form-group">
                            {{ Form::label('product_category', 'Inventory Category')}}
                            {{ Form::select('product_category',[$lotlog->product_category,
                            '' => 'Please Select',
                            'drums' => 'Drums',
                            'totes' => 'Totes',
                            'organic' => 'Organic',
                            'tanks' => 'Tanks',
                            'vinegars' => 'Vinegars',
                            'railcars' => 'Railcars',
                            'specialty' => 'Specialty',
                            'infused_oil_drums' => 'Infused Oil Drums',
                            'spreads_and_olives' => 'Spreads and Olives',
                            'website_olive_oil' => 'Website Olive Oil',
                            'web_flavor_and_infused' => 'Web Flavor and Infused',
                            'website_vinegars' => 'Website Vinegars',
                            'website_other' => 'Website Other',
                            'website_totals' => 'Website Totals',
                            'finished_product' => 'Finished Product',
                            'sprays' => 'Sprays',
                            'ready_to_ship' => 'Ready To Ship',
                            'rework' => 'Rework',
                            'labels_in_house' => 'Labels In House',
                            'labels_in_labelroom' => 'Labels In Labelroom',
                            'customer_labels' => 'Customers Labels',
                            'bottles' => 'Bottles',
                            'etched_bottles' => 'Etched Bottles',
                            'fustis' => 'Fustis',
                            'caps' => 'Caps',
                            'cardboard_blank' => 'Cardboard Blank',
                            'cardboard_printed' => 'Cardboard Printed',
                            'supplies' => 'Supplies',
                            'materials' => 'Materials',
                            'follmer' => 'Follmer',
                            'flavors' => 'Flavors',
                            'flavors_for_samples' => 'Flavors For Samples',

                             ], $lotlog->product_category, ['class' => 'form-control']) }}                                    
                            </div> --}}
                                   
                            <div class="form-group">
                                {{ Form::label('progress_notes', 'Progress Notes') }}
                                {{ Form::textarea('progress_notes', null,['class' => 'form-control', 'placeholder' => 'Progress Notes','rows' => '8', 'cols' => '4']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('lot', 'Lot') }}
                                {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description', 'Product Description') }}
                                {{ Form::text('description', null,['class' => 'form-control', 'placeholder' => 'Product Description']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('container_number','Container Number') }}
                                {{ Form::text('container_number',null,['class' => 'form-control', 'placeholder' => 'Container Number']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('supplier', 'Supplier') }}
                                {{ Form::text('supplier', null,['class' => 'form-control', 'placeholder' => 'Supplier']) }}
                            </div>
                             <div class="form-group">
                                {{ Form::label('carrier', 'Carrier')}}
                                {{ Form::text('carrier', null,['class' => 'form-control', 'placeholder' => 'Carrier' ]) }}
                            </div>
<div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                {{ Form::text('date', null,['class' => 'form-control', 'id' => 'datepicker', 'placeholder' => 'Date']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('so', 'SO#') }}
                                {{ Form::text('so', null,['class' => 'form-control', 'placeholder' => 'SO#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('original_lot', 'Original Lot') }}
                                {{ Form::text('original_lot', null,['class' => 'form-control', 'placeholder' => 'Original Lot']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('origin', 'Origin') }}
                                {{ Form::text('origin', null,['class' => 'form-control', 'placeholder' => 'Origin']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('qty', 'QTY') }}
                                {{ Form::text('qty', null,['class' => 'form-control', 'placeholder' => 'QTY']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('size', 'Size') }}
                                {{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Size']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('customer', 'Customer') }}
                                {{ Form::text('customer', null,['class' => 'form-control', 'placeholder' => 'Customer']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('sample_location', 'Sample Location') }}
                                {{ Form::text('sample_location', null,['class' => 'form-control', 'placeholder' => 'Sample Location']) }}
                            </div>

                            <hr/>
                            <h2>Notes</h2>
                            <div class="form-group">
                                {{ Form::label('notes1', 'Note') }}
                                {{ Form::textarea('notes1', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes2', 'Note') }}
                                {{ Form::textarea('notes2', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes3', 'Note') }}
                                {{ Form::textarea('notes3', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>

                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}
                            {{ link_to_route('lotlogs.index', 'Cancel',null,['class' => 'btn btn-primary', 'style' => 'float:right;']) }}

                        </fieldset>
                        

                    </div><!--end row-->

            </div><!--end col lg 8 -->
                <div class="col-md-6">
                 <script>
                                $(document).ready(function () {
                                    $('.lotlogtype').hide();

                                    $('#form_lotlogtype').change(function () {
                                        $(this).find("option").each(function () {
                                            $("#" + $(this).val()).hide();
                                            console.log($(this));
                                        });
                                        $("#" + $(this).val()).show();
                                        // hide input fields when they are hidden
                                        $("div").filter(":hidden").children("input[type='text']").attr("disabled", "disabled");
                                        $("div").filter(":hidden").children("textarea[class='form-control']").attr("disabled", "disabled");

                                    });
                                });
                            </script>

        
                @if(!empty($lotlog->im_approved_by) || $lotlog->ip_approved_by === NULL)
                      @include('lotlogs.form._edit_incoming_materials_qc_form')
                @elseif(!empty($lotlog->ip_approved_by) || $lotlog->im_approved_by === NULL)
                      @include('lotlogs.form._edit_incoming_product_qc_form')
                @else
    <div class="form-group">
                {{ Form::label('lotlogtype', 'Lot Log Type/Paperwork') }}
            <div class="controls">
                {{ Form::select('lotlogtype',['' => 'Choose', 'incoming_materials_qc_form' => 'Incoming Materials QC Form', 'incoming_product_qc_form' => 'Incoming Product QC Form'], '', ['class' => 'form-control', 'id' => 'form_lotlogtype']) }}

                </div>
                    </div>
                
                @endif
        
                </div>
        </div><!--end container fluid-->

                                

    </div><!--end blends-->
    {{ Form::close() }}


    <script>

        $(function() {

//check if checked, show/hide template dropdown
           if($('#coa_active').is(':checked')) {
               $('#template').show();
           } else {
               $('#template').hide();
           }


//if the coa active checkbox is clicked show/hide the template dropdown
           $('#coa_active').change(function() {
             if ($(this).is(':checked', true)) {
                 $('#template').show();
             } else {
                 $('#template').hide();
             }
           });
        });

    </script>
@stop