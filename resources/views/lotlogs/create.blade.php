@extends('layouts.admin')

@section('content')
    <div id="lotlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span> A New Lot Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/lotlogs/">Lot Logs</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['image/save','route' => 'lotlogs.store','files' => true, 'class' => 'form-horizontal']) }}
                        {{ Form::hidden('year', Date("Y")) }}
                        <fieldset>

                            <div class="form-group">
                            {{ Form::label('progress', 'Progress')}}
                            {{ Form::select('progress',['' => 'Please Select', 'in_progress' => 'In Progress', 'complete' => 'Complete'], '', ['class' => 'form-control']) }}                                    
                            </div>
                             {{-- <div class="form-group">
                            {{ Form::label('product_category', 'Inventory Category')}}
                            {{ Form::select('product_category',[
                            '' => 'Please Select',
                            'drums' => 'Drums',
                            'totes' => 'Totes',
                            'organic' => 'Organic',
                            'tanks' => 'Tanks',
                            'vinegars' => 'Vinegars',
                            'railcars' => 'Railcars',
                            'specialty' => 'Specialty',
                            'infused_oil_drums' => 'Infused Oil Drums',
                            'spreads_and_olives' => 'Spreads and Olives',
                            'website_olive_oil' => 'Website Olive Oil',
                            'web_flavor_and_infused' => 'Web Flavor and Infused',
                            'website_vinegars' => 'Website Vinegars',
                            'website_other' => 'Website Other',
                            'website_totals' => 'Website Totals',
                            'finished_product' => 'Finished Product',
                            'sprays' => 'Sprays',
                            'ready_to_ship' => 'Ready To Ship',
                            'rework' => 'Rework',
                            'labels_in_house' => 'Labels In House',
                            'labels_in_labelroom' => 'Labels In Labelroom',
                            'customer_labels' => 'Customers Labels',
                            'bottles' => 'Bottles',
                            'etched_bottles' => 'Etched Bottles',
                            'fustis' => 'Fustis',
                            'caps' => 'Caps',
                            'cardboard_blank' => 'Cardboard Blank',
                            'cardboard_printed' => 'Cardboard Printed',
                            'supplies' => 'Supplies',
                            'materials' => 'Materials',
                            'follmer' => 'Follmer',
                            'flavors' => 'Flavors',
                            'flavors_for_samples' => 'Flavors For Samples',

                             ], '', ['class' => 'form-control']) }}                                    
                            </div> --}}
                            <div class="form-group">
                                {{ Form::label('expiration_date', 'Expiration Date')}}
                                {{ Form::text('expiration_date',null,['class' => 'form-control', 'id' => 'datepicker1','placeholder' => 'Expiration Date'])}}
                            </div>
                            <div class="form-group">
                                {{ Form::label('progress_notes', 'Progress Notes') }}
                                {{ Form::textarea('progress_notes', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Progress Notes'])}}
                            </div>
                            <div class="form-group">
                                {{ Form::label('lot', 'Lot') }}
                                {{ Form::text('lot', null,['class' => 'form-control', 'placeholder' => 'Lot']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description', 'Product Description') }}
                                {{ Form::text('description', null,['class' => 'form-control', 'placeholder' => 'Product Description']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('container_number','Container Number') }}
                                {{ Form::text('container_number',null,['class' => 'form-control', 'placeholder' => 'Container Number']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('supplier', 'Supplier') }}
                                {{ Form::text('supplier', null,['class' => 'form-control', 'placeholder' => 'Supplier']) }}
                            </div>
                             <div class="form-group">
                                {{ Form::label('carrier', 'Carrier')}}
                                {{ Form::text('carrier', null,['class' => 'form-control', 'placeholder' => 'Carrier' ]) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('date', 'Date') }}
                                {{ Form::text('date', null,['class' => 'form-control', 'id' => 'datepicker', 'placeholder' => 'Date']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('so', 'SO#') }}
                                {{ Form::text('so', null,['class' => 'form-control', 'placeholder' => 'SO#']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('original_lot', 'Original Lot') }}
                                {{ Form::text('original_lot', null,['class' => 'form-control', 'placeholder' => 'Original Lot']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('origin', 'Origin') }}
                                {{ Form::text('origin', null,['class' => 'form-control', 'placeholder' => 'Origin']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('qty', 'QTY') }}
                                {{ Form::text('qty', null,['class' => 'form-control', 'placeholder' => 'QTY']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('size', 'Size') }}
                                {{ Form::text('size', null,['class' => 'form-control', 'placeholder' => 'Size']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('customer', 'Customer') }}
                                {{ Form::text('customer', null,['class' => 'form-control', 'placeholder' => 'Customer']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('sample_location', 'Sample Location') }}
                                {{ Form::text('sample_location', null,['class' => 'form-control', 'placeholder' => 'Sample Location']) }}
                            </div>
                            <hr/>
                            <h2>Notes</h2>
                            <div class="form-group">
                                {{ Form::label('notes1', 'Note') }}
                                {{ Form::textarea('notes1', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes2', 'Note') }}
                                {{ Form::textarea('notes2', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes3', 'Note') }}
                                {{ Form::textarea('notes3', null,['cols' => '3', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Note']) }}
                            </div>
  {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->


                </div>
                {{-- second column --}}
                <div class="col-md-6">

                            <script>
                                $(document).ready(function () {
                                    $('.lotlogtype').hide();

                                    $('#form_lotlogtype').change(function () {
                                        $(this).find("option").each(function () {
                                            $("#" + $(this).val()).hide();
                                            console.log($(this));
                                        });
                                        $("#" + $(this).val()).show();
                                        // hide input fields when they are hidden
                                        $("div").filter(":hidden").children("input[type='text']").attr("disabled", "disabled");
                                        $("div").filter(":hidden").children("textarea[class='form-control']").attr("disabled", "disabled");


                                    });
                                });
                            </script>

{{--             <div class="form-group">
                {{ Form::label('lotlogtype', 'Lot Log Type/Paperwork') }}
            <div class="controls">
                {{ Form::select('lotlogtype',['' => 'Choose', 'incoming_materials_qc_form' => 'Incoming Materials QC Form', 'incoming_product_qc_form' => 'Incoming Product QC Form'], '', ['class' => 'form-control', 'id' => 'form_lotlogtype']) }}

                </div>
                    </div> --}}


                        {{-- lotlogtype partials --}}
                            {{-- <div id="incoming_materials_qc_form" class="lotlogtype">
                                @include('lotlogs.form._incoming_materials_qc_form')
                            </div> --}}

                           {{--  <div id="incoming_product_qc_form" class="lotlogtype">
                                @include('lotlogs.form._incoming_product_qc_form')
                            </div> --}}

                               {{--  <div id="rma" class="lotlogtype">
                                @include('lotlogs.form._rma')
                            </div> --}}


               
                {{-- <div class="form-group">
                    {{ Form::label('po', 'PO')}}
                    {{ Form::text('po', null,['class' => 'form-control', 'placeholder' => 'PO' ]) }}
                </div>
                 <div class="form-group">
                    {{ Form::label('packslip', 'Packslip')}}
                    {{ Form::text('packslip', null,['class' => 'form-control', 'placeholder' => 'Packslip' ]) }}
                </div>
                 <div class="form-group">
                    {{ Form::label('container', 'Container')}}
                    {{ Form::text('container', null,['class' => 'form-control', 'placeholder' => 'Container' ]) }}
                </div>
                 <div class="form-group">
                    {{ Form::label('product_on_hold', 'Product On Hold')}}
                    {{ Form::text('product_on_hold', null,['class' => 'form-control', 'placeholder' => 'Product On Hold' ]) }}
                </div>
                 <div class="form-group">
                    {{ Form::label('product_contract', 'Product Contract')}}
                    {{ Form::text('product_contract', null,['class' => 'form-control', 'placeholder' => 'Product Contract' ]) }}
                </div>
                 <div class="form-group">
                    {{ Form::label('kosher_cert_file', 'Kosher Cert File')}}
                    {{ Form::text('kosher_cert_file', null,['class' => 'form-control', 'placeholder' => 'Kosher Cert File' ]) }}
                </div>
                --}}


                          

                </div><!--end col-md-6-->
            </div><!--end col lg 8 -->
            </div><!--end container fluid-->



    </div><!--end blends-->
@stop

@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            // $('#datatable').DataTable({
            //     paging:true,
            //     "order": [[ 10, "asc" ]],
            //     // scrollY:400,
            //     deferRender:true,
            //     // scroller:true,
            //     columnDefs: [
            //                     { targets: 'no-sort', orderable: false }
            //                 ],

            // });
        });
     </script>

    @stop