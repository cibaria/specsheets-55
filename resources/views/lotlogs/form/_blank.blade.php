<div style="border:1px solid lightgrey;background-color:#DDC; padding-right:5px;padding-left:5px; padding-bottom:5px;margin-bottom:25px;">

<h2>Items Received Without Paperwork</h2>

<div class="form-group">
{{ Form::label('bp_date', 'Date') }}
{{ Form::text('bp_date', null, ['class' => 'form-control', 'placeholder' => 'Date', 'id' => 'datepicker1']) }}	
</div>

<div class="form-group">
{{ Form::label('bp_from', 'From') }}
{{ Form::text('bp_from', null, ['class' => 'form-control', 'placeholder' => 'From']) }}	
</div>

<div class="form-group">
{{ Form::label('bp_description', 'Description(Quantity and Size)') }}
{{ Form::textarea('bp_description', null, ['class' => 'form-control', 'placeholder' => 'Description(Quantity and Size)', 'rows' => '4', 'cols' => '8']) }}	
</div>

<div class="form-group">
{{ Form::label('bp_reason_for_return', 'Reason For Return/Receipt') }}
{{ Form::textarea('bp_reason_for_return', null, ['class' => 'form-control', 'placeholder' => 'Reason For Return/Receipt', 'rows' => '4', 'cols' => '8']) }}	
</div>

<div class="form-group">
{{ Form::label('bp_condition', 'Condition') }}
{{ Form::textarea('bp_condition', null, ['class' => 'form-control', 'placeholder' => 'Condition', 'rows' => '4', 'cols' => '8']) }}	
</div>

<div class="form-group">
{{ Form::label('bp_method_of_delivery', 'Method of Delivery') }}
{{ Form::textarea('bp_method_of_delivery', null, ['class' => 'form-control', 'placeholder' => 'Method of Delivery', 'rows' => '4', 'cols' => '8']) }}	
</div>

<div class="form-group">
{{ Form::label('bp_received_and_reviewed_by', 'Received and Reviewed by') }}
{{ Form::text('bp_received_and_reviewed_by', null, ['class' => 'form-control', 'placeholder' => 'Received and Reviewed by']) }}	
</div>
<div class="form-group">
{{ Form::label('bp_action_required', 'Action Required') }}
{{ Form::textarea('bp_action_required', null, ['class' => 'form-control', 'placeholder' => 'Action Required', 'rows' => '4', 'cols' => '8']) }}	
</div>



</div><!--end border-->
<hr />