@extends('layouts.label_print')

@section('content')

<div style="width:800px; text-align: center;">
<span style="font-size:70px;font-weight:bold;">{{ $label['oil_type'] }}</span><br />
			<span style="font-size:40px;">Supplier: {{ $label['supplier'] }}</span><br />
			<span style="font-size:70px;font-weight:bold;">Lot#: {{ $label['lot'] }}</span><br />
			<span style="font-size:40px;">Recv date: {{ $label['date'] }}</span><br />
			<a href="javascript:window.print()" class="noPrint" style="margin-top:50px; background-color:#2780e3; padding-left:15px; padding-right:15px; padding-top:5px; padding-bottom:5px;color:white;">Print</a>
</div><!--end width-->

@stop