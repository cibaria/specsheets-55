@extends('layouts.admin')

@section('content')

    <div id="specsheets">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-info" style="padding-right:15px;">Canceled</span> Shipping Logs
                </h1>

                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Shipping Logs
                    </li>
                </ol>
            </div>
        </div>

        {{--sessions--}}
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        {{--sub nav--}}
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    @if(Auth::user()->group != 20)
                        <a href="/shippings/" class="btn btn-success navbar-btn"><< Back To Not Shipped</a>
                        <a href="/shippings/shipped/" class="btn btn-danger navbar-btn">Shipped</a>
                    @endif
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Date</strong></td>
                <td><strong>Order#</strong></td>
                <td><strong>Company</strong></td>
                <td><strong>Time In</strong></td>
                <td><strong>Time Out</strong></td>
                <td><strong>Seal#</strong></td>
                <td><strong>Will Call</strong></td>
                <td><strong>Door Number</strong></td>
                <td><strong>Notes</strong></td>
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($shippings  as $shipping)

                <?php

                //If the date is past today make the <tr> background red
                $shipping_date = explode('/', $shipping->date);
                $new_shipping_date = implode("", $shipping_date);

                $today = Date("m/d/Y");
                $new_today = explode('/', $today);
                $new_new_today = implode("", $new_today);

                if($new_shipping_date < $new_new_today) :
                ?>
                <tr style="background-color: red;color:white;font-weight:bold;">
                    <?php else: ?>
                <tr>
                    <?php endif; ?>


                    <td>{{ $shipping->date }}</td>
                    <td>{{ $shipping->order_number }}</td>
                    <td>{{ $shipping->company }}</td>
                    <td>{{ $shipping->time_in }}</td>
                    <td>{{ $shipping->time_out}}</td>
                    <td>{{ $shipping->seal_number }}</td>
                    <td>{{ $shipping->will_call }}</td>
                    <td>{{ $shipping->door_number }}</td>
                    <td>{{ $shipping->notes }}</td>
                    <td>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                            <div class="btn-group" role="group" aria-label="">
                                {{ link_to_route('shippings.edit', 'Edit', [$shipping->id],['class' => 'btn btn-warning']) }}
                                {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('shippings.destroy', $shipping->id))) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                {{ Form::close() }}
                            </div>
                        @endif
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

{{--end content--}}


{{--footer--}}
@section('footer')
    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                    { targets: 'no-sort', orderable: false }
                ],

            });
        });
    </script>

@stop

