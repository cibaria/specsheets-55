@extends('layouts.admin')

@section('content')

<div id="specsheets">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Complete</span>Customer Complaints {{ Date("Y") }}
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Customer Complaints
                </li>
            </ol>
        </div>
    </div>

    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
                {{ link_to_route('customercomplaints.index', 'Back To Customer Complaints',null, ['class' => 'btn btn-primary navbar-btn']) }}                {{--search--}}

                {{-- <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class=" search form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                </form> --}}
            </nav>
        </div>
    </div>

    <table class="table table-stripped table-responsive table-hover" id="datatable">
        <thead>
        <tr>
           <td><strong>Case Number</strong>
           <td><strong>Date Of Complaint</strong></td>
           <td><strong>Company Name</strong></td>
           <td><strong>Customer Contact</strong></td>
           <td><strong>Order Number</strong></td>
           <td class="no-sort"></td>
        </tr>
        </thead>
        <tbody class="list">
        @foreach($customercomplaints as $customercomplaint)
        @if($customercomplaint->status == 'complete')
            <tr>
            <td class="casenumber">
            <?php $case_number = $customercomplaint->id + 200 ?>
            {{ $case_number }}
            </td>
             <td class="isdone">{{ $customercomplaint->customer_name }}</td>
             <td>{{ $customercomplaint->date_of_complaint }}</td>
             <td>{{ $customercomplaint->customer_contact }}</td>
             <td class="ordernumber">{{ $customercomplaint->order_number }}</td>
             <td>
                 <a href="/customercomplaints/{{ $customercomplaint->id }}/products" class="btn btn-primary">Products</a>
             </td>
            </tr>
            @endif
        @endforeach

        </tbody>
    </table>


<ul class="pagination"></ul>
</div>
    @stop

{{--end content--}}


{{--footer--}}
@section('footer')
<script>
              $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });

    </script>
    @stop
