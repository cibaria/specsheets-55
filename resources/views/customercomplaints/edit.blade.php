@extends('layouts.admin')

@section('content')
     <div id="specsheets">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>A Customer Complaint
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/customercomplaints/">Customer Complaint</a> >
                    Edit
                </li>
            </ol>
        </div>
    </div>

    {{--errors--}}
        @if ( count( $errors ) > 0 )
        <div class="alert alert-dismissible alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

    {{--form--}}
    <div class="container-fluid">
        <div class="row">
           <div class="col-md-6">
               <div class="col-lg-8">

                    {{ Form::model($customercomplaint, array('method' => 'PATCH','files' => true,'image/save' => 'image/save', 'route' => array('customercomplaints.update', $customercomplaint->id))) }}
                  <fieldset>

                        <div class="form-group">
                                {{ Form::label('status', 'Status?') }}
                                @if($customercomplaint->status == 'in_progress')
                                {{ Form::select('status',['in_progress' => 'In Progress', 'complete' => 'Complete'], '', ['class' => 'form-control']) }}
                                @elseif($customercomplaint->status == 'complete')
                                {{ Form::select('status',['complete' => 'Complete', 'in_progress' => 'In Progress'], '', ['class' => 'form-control']) }}
                                @else
                                {{ Form::select('status',['' => 'Status', 'in_progress' => 'In Progress', 'complete' => 'Complete'], '', ['class' => 'form-control']) }}
                                @endif
                            </div>
                            <div class="form-group">
                                {{ Form::label('received_by', 'Received By') }}
                                {{ Form::text('received_by', null,['class' => 'form-control', 'placeholder' => 'Received By']) }}
                            </div>
                            <div class="form-group">
                                 {{ Form::label('date_closed', 'Date Closed') }}
                                 {{ Form::text('date_closed', null,['class' => 'form-control', 'placeholder' => 'Date Closed', 'id' => 'datepicker4']) }}
                             </div>
                            <h2>Customer Details</h2>
                            <div class="form-group">
                                {{ Form::label('customer_name', 'Company Name') }}
                                {{ Form::text('customer_name', null,['class' => 'form-control', 'placeholder' => 'Company Name']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('customer_contact', 'Customer Contact') }}
                                {{ Form::text('customer_contact', null,['class' => 'form-control', 'placeholder' => 'Customer Contact']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('customer_email', 'Customer Email') }}
                                {{ Form::text('customer_email', null,['class' => 'form-control', 'placeholder' => 'Customer Email']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('customer_phone', 'Customers Phone Number') }}
                                {{ Form::text('customer_phone', null,['class' => 'form-control', 'placeholder' => 'Customers Phone Number']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('order_number', 'Order Number') }}
                                {{ Form::text('order_number', null,['class' => 'form-control', 'placeholder' => 'Order Number']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('ship_date', 'Ship Date') }}
                                {{ Form::text('ship_date', null,['class' => 'form-control', 'placeholder' => 'Ship Date' , 'id' => 'datepicker3']) }}
                            </div>
                            <h2>Complaint Information</h2>
                            <div class="form-group">
                                {{ Form::label('details_of_complaint', 'Details of Complaint') }}
                                {{ Form::textarea('details_of_complaint', null,['class' => 'form-control', 'placeholder' => 'Details of Complaint', 'rows' => '8', 'cols' => '8']) }}
                            </div>                            <div class="form-group">
                                {{ Form::label('date_of_complaint', 'Date Of Complaint') }}
                                {{ Form::text('date_of_complaint', null,['class' => 'form-control', 'placeholder' => 'Date Of Complaint', 'id' => 'datepicker']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('time_of_complaint', 'Time of Complaint') }}
                                {{ Form::text('time_of_complaint', null,['class' => 'form-control', 'placeholder' => 'Time of Complaint', 'id' => 'timepicker']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('complaint_received_via', 'Complaint Received Via') }}

                                <br />

                                <div class="radio">
                                    <label>{{ Form::radio('complaint_received_via', 'phone') }}Phone</label>
                                </div>
                                <div class="radio">
                                    <label>{{ Form::radio('complaint_received_via', 'fax') }}Fax</label>
                                </div>
                                <div class="radio">
                                    <label>{{ Form::radio('complaint_received_via', 'email') }}Email</label>
                                </div>
                                <div class="radio">
                                    <label>{{ Form::radio('complaint_received_via', 'verbal') }}Verbal</label>
                                </div>
                            </div>

                            <h2>1st Action</h2>
                            <div class="form-group">
                                {{ Form::label('first_action_taken_by', 'Action Taken By') }}
                                {{ Form::text('first_action_taken_by', null,['class' => 'form-control', 'placeholder' => 'Action Taken By']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('first_action_taken_date', 'Action Taken Date') }}
                                {{ Form::text('first_action_taken_date', null,['class' => 'form-control', 'placeholder' => 'Action Taken Date', 'id' => 'datepicker1']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('first_action_taken_time', 'Action Taken Time') }}
                                {{ Form::text('first_action_taken_time', null,['class' => 'form-control', 'placeholder' => 'Action Taken Time', 'id' => 'timepicker1']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('first_action_taken_notes', 'Action Taken Notes') }}
                                {{ Form::textarea('first_action_taken_notes', null,['class' => 'form-control', 'placeholder' => 'Action Taken Notes', 'rows' => '8', 'cols' => '8']) }}
                            </div>

                            <h2>Corrective Action</h2>
                            <div class="form-group">
                                {{ Form::label('corrective_action_by', 'Corrective Action By') }}
                                {{ Form::select('corrective_action_by',['Customer Service' => 'Customer Service', 'Technology' => 'Technology', 'Production' => 'Production', 'QC' => 'QC'], $customercomplaint->corrective_action_by, ['class' => 'form-control']) }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('corrective_action_date', 'Corrective Action Date') }}
                                {{ Form::text('corrective_action_date', null,['class' => 'form-control', 'placeholder' => 'Corrective Action Date', 'id' => 'datepicker2']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('corrective_action_time', 'Corrective Action Time') }}
                                {{ Form::text('corrective_action_time', null,['class' => 'form-control', 'placeholder' => 'Corrective Action Time', 'id' => 'timepicker2']) }}
                            </div>

                            <h2>Product Information</h2>
                        
                            <div class="form-group">
                                {{ Form::label('fifteen_restocking_fee', "15% Restocking Fee") }}

                                <div class="radio">
                                    <label>{{ Form::radio('fifteen_restocking_fee', 'Yes' ) }} Yes</label>
                                </div>
                                <div class="radio">
                                    <label>{{ Form::radio('fifteen_restocking_fee', 'No' ) }} No</label>
                                </div>
                                <label>$ {{ Form::text('fifteen_restocking_fee_amount', null, ['placeholder' => 'Enter Dollar Amount'])}}</label>
                            </div>
                            <hr>
                            <div class="form-group">
                                {{ Form::label('ups_cost_to_re_ship', 'UPS or Freight Cost To re-ship') }}
                                {{ Form::text('ups_cost_to_re_ship', null,['class' => 'form-control', 'placeholder' => 'UPS or Freight Cost To re-ship', 'id' => 'timepicker2']) }}
                            </div>
                            <hr>
                            <div class="form-group">
                                {{ Form::label('return_received', "Return Received") }}

                                <div class="radio">
                                    <label>{{ Form::radio('return_received', 'Yes' ) }} Yes</label>
                                </div>
                                <div class="radio">
                                    <label>{{ Form::radio('return_received', 'No' ) }} No</label>
                                </div>
                                <label>By {{ Form::text('return_received_by', null, ['placeholder' => 'Return Received'])}}</label>
                            </div>


                            {{-- rma --}}
                            {{-- @include('customercomplaints.rma.index') --}}



                        </fieldset>
                        </div><!--end row-->
           </div>
           {{--right--}}
           <div class="col-md-6">

             <div class="form-group">
                 {{ Form::label('freight_damage_ups', 'Freight Damage UPS') }}
                 {{ Form::text('freight_damage_ups', null,['class' => 'form-control', 'placeholder' => '$']) }}
             </div>
             <div class="form-group">
                 {{ Form::label('freight_damage_other', 'Freight Damage Other') }}
                 {{ Form::text('freight_damage_other', null,['class' => 'form-control', 'placeholder' => '$']) }}
             </div>
             <div class="form-group">
                 {{ Form::label('quality_of_product', 'Quality Of Product') }}
                 {{ Form::text('quality_of_product', null,['class' => 'form-control', 'placeholder' => '$']) }}
             </div>
             <div class="form-group">
                 {{ Form::label('wrong_missing_product', 'Wrong Missing Product') }}
                 {{ Form::text('wrong_missing_product', null,['class' => 'form-control', 'placeholder' => '$']) }}
             </div>
             <div class="form-group">
                 {{ Form::label('labeling_documentation', 'Labeling Documentation') }}
                 {{ Form::text('labeling_documentation', null,['class' => 'form-control', 'placeholder' => '$']) }}
             </div>
             <div class="form-group">
                 {{ Form::label('other', 'Other') }}
                 {{ Form::text('other', null,['class' => 'form-control', 'placeholder' => '$']) }}
             </div>
             <div class="form-group">
                 {{ Form::label('ups_freight_refund', 'UPS Freight Refund') }}
                 {{ Form::text('ups_freight_refund', null,['class' => 'form-control', 'placeholder' => '$']) }}
             </div>
             <div class="form-group">
                 {{ Form::label('lot_number', 'Lot Number') }}
                 {{ Form::text('lot_number', null,['class' => 'form-control', 'placeholder' => 'Lot Number']) }}
             </div>
             <div class="form-group">
                 {{ Form::label('notes', 'Notes') }}
                 {{ Form::textarea('notes', null,['class' => 'form-control', 'placeholder' => 'Notes', 'rows' => '6', 'cols' => '8']) }}
             </div>

           </div>
           {{--end right--}}
       </div><!--end col lg 8 -->
        <hr/>
        <div class="form-group">
            {{ Form::submit('Update', array('class' => 'btn btn-default')) }}
        </div>
        {{ Form::close() }}
    </div><!--end container fluid-->




</div><!--end specsheets-->
@stop
    @section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $( "#datepicker2" ).datepicker();
            $( "#datepicker3" ).datepicker();
            $( "#datepicker4" ).datepicker();
            $('#timepicker').timepicker();
            $('#timepicker1').timepicker();
            $('#timepicker2').timepicker();
        });
     </script>

    @stop