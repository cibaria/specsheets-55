@extends('layouts.admin')

@section('content')

    <div id="blends">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Customer Complaint Totals
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/customercomplaints/">Customer Complaints</a>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">

{{--                     <a href="" class="btn btn-success navbar-btn">Print</a>
                    <a href="" class="btn btn-primary navbar-btn">Print PDF</a> --}}
                </nav>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        <table class="table  table-responsive table-hover">
                            <thead>
                            <tr>
                                <td><span style="font-size:27px;">Customer Complaint Totals {{ Date("Y") }}</span></td>

                                <td></td>
                            </tr>
                            </thead>
                            <tbody class="list">

                            <tr>
                                <td><strong>Freight Damage UPS Total</strong></td>
                                <td class="lot">$ 
                                <?php 
                                    $sum = 0;
                                    foreach($report as $key => $value)
                                    {
                                     $sum += $value->freight_damage_ups;
                                    }
                                    echo number_format($sum,2);
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Freight Damage Other</strong></td>
                                <td>$
                                <?php 
                                    $sum = 0;
                                    foreach($report as $key => $value)
                                    {
                                     $sum += $value->freight_damage_other;
                                    }
                                    echo number_format($sum,2);
                                   
                                ?>
 
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Quality Of Product</strong></td>
                                <td>$
                                <?php 
                                    $sum = 0;
                                    foreach($report as $key => $value)
                                    {
                                     $sum += $value->quality_of_product;
                                    }
                                    echo number_format($sum,2);
                                    
                                ?>
  
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Wrong Missing Product</strong></td>
                                <td>$
                                <?php 
                                    $sum = 0;
                                    foreach($report as $key => $value)
                                    {
                                     $sum += $value->wrong_missing_product;
                                    }
                                    echo number_format($sum,2);
                                   
                                ?>

                                </td>
                            </tr>
                            <tr>
                                <td><strong>Labeling/Documentation</strong></td>
                                <td>$
                                <?php 
                                    $sum = 0;
                                    foreach($report as $key => $value)
                                    {
                                     $sum += $value->labeling_documentation;
                                    }
                                    echo number_format($sum,2);
                                    
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Ups or Freight Refund</strong></td>
                                <td>$
                               <?php 
                                    $sum = 0;
                                    foreach($report as $key => $value)
                                    {
                                     $sum += $value->ups_freight_refund;
                                    }
                                    echo number_format($sum,2);
                                    
                                ?>
  
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Ups Cost To re-ship</strong></td>
                                <td>$
                               <?php 
                                    $sum = 0;
                                    foreach($report as $key => $value)
                                    {
                                     $sum += $value->ups_cost_to_re_ship;
                                    }
                                    echo number_format($sum,2);
                                    
                                ?>
  
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Other</strong></td>
                                <td>$
                                  <?php 
                                    $sum = 0;
                                    foreach($report as $key => $value)
                                    {
                                     $sum += $value->other;
                                    }
                                    echo number_format($sum,2);
                                    
                                ?>
 
                                </td>
                            </tr>

                            <hr />
                            <tr>
                                <td><span style="font-size:32px;">Total</span></td>
                                <td><strong>
                              <?php 
                                    $total = 0;
                                    foreach($report as $key => $value)
                                    {
                                     $total += $value->ups_freight_refund +
                                              $value->other +
                                              $value->labeling_documentation +
                                              $value->wrong_missing_product +
                                              $value->quality_of_product +
                                              $value->freight_damage_other +
                                              $value->ups_cost_to_re_ship +
                                              $value->freight_damage_ups  ;
                                    }
                                    echo "$" . number_format($total,2);
                                ?></strong>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- second column --}}
                <div class="col-md-6">
                    <div class="col-lg-8">
                       <table class="table  table-responsive table-hover">
                           <thead>
                           <tr>
                               <td><span style="font-size:26px;">Customer Complaint Totals {{ Date("Y") - 1 }}</span></td>

                               <td></td>
                           </tr>
                           </thead>
                           <tbody class="list">

                           <tr>
                               <td><strong>Freight Damage UPS Total</strong></td>
                               <td class="lot">$ 
                               <?php 
                                   $sum = 0;
                                   foreach($last_year_report as $key => $value)
                                   {
                                    $sum += $value->freight_damage_ups;
                                   }
                                   echo number_format($sum,2);
                               ?>
                               </td>
                           </tr>
                           <tr>
                               <td><strong>Freight Damage Other</strong></td>
                               <td>$
                               <?php 
                                   $sum = 0;
                                   foreach($last_year_report as $key => $value)
                                   {
                                    $sum += $value->freight_damage_other;
                                   }
                                   echo number_format($sum,2);
                                  
                               ?>

                               </td>
                           </tr>
                           <tr>
                               <td><strong>Quality Of Product</strong></td>
                               <td>$
                               <?php 
                                   $sum = 0;
                                   foreach($last_year_report as $key => $value)
                                   {
                                    $sum += $value->quality_of_product;
                                   }
                                   echo number_format($sum,2);
                                   
                               ?>
 
                               </td>
                           </tr>
                           <tr>
                               <td><strong>Wrong Missing Product</strong></td>
                               <td>$
                               <?php 
                                   $sum = 0;
                                   foreach($last_year_report as $key => $value)
                                   {
                                    $sum += $value->wrong_missing_product;
                                   }
                                   echo number_format($sum,2);
                                  
                               ?>

                               </td>
                           </tr>
                           <tr>
                               <td><strong>Labeling/Documentation</strong></td>
                               <td>$
                               <?php 
                                   $sum = 0;
                                   foreach($last_year_report as $key => $value)
                                   {
                                    $sum += $value->labeling_documentation;
                                   }
                                   echo number_format($sum,2);
                                   
                               ?>
                               </td>
                           </tr>
                           <tr>
                               <td><strong>Ups or Freight Refund</strong></td>
                               <td>$
                              <?php 
                                   $sum = 0;
                                   foreach($last_year_report as $key => $value)
                                   {
                                    $sum += $value->ups_freight_refund;
                                   }
                                   echo number_format($sum,2);
                                   
                               ?>
 
                               </td>
                           </tr>
                           <tr>
                               <td><strong>Ups Cost To re-ship</strong></td>
                               <td>$
                              <?php 
                                   $sum = 0;
                                   foreach($last_year_report as $key => $value)
                                   {
                                    $sum += $value->ups_cost_to_re_ship;
                                   }
                                   echo number_format($sum,2);
                                   
                               ?>
 
                               </td>
                           </tr>
                           <tr>
                               <td><strong>Other</strong></td>
                               <td>$
                                 <?php 
                                   $sum = 0;
                                   foreach($last_year_report as $key => $value)
                                   {
                                    $sum += $value->other;
                                   }
                                   echo number_format($sum,2);
                                   
                               ?>

                               </td>
                           </tr>

                           <hr />
                           <tr>
                               <td><span style="font-size:32px;">Total</span></td>
                               <td><strong>
                             <?php 
                                   $total = 0;
                                   foreach($last_year_report as $key => $value)
                                   {
                                    $total += $value->ups_freight_refund +
                                             $value->other +
                                             $value->labeling_documentation +
                                             $value->wrong_missing_product +
                                             $value->quality_of_product +
                                             $value->freight_damage_other +
                                             $value->ups_cost_to_re_ship +
                                             $value->freight_damage_ups  ;
                                   }
                                   echo "$" . number_format($total,2);
                               ?></strong>
                               </td>
                           </tr>
                           </tbody>
                       </table>
                   </div>
                <ul class="list-group">
                </ul>
                </div><!--end col-md-6-->
            
            </div>
        </div>
@stop