@extends('layouts.admin')

@section('content')
    <div id="spotchecks">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Edit</span>Recieving Label Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/receiving/labels/">Recieving Label Logs</a> >
                        Edit
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
 {{ Form::model($label, array('method' => 'PATCH', 'route' => array('labels.update', $label->id))) }}                        
 <fieldset>
                        <div class="form-group">
                                {{ Form::label('active', 'Received?') }}
                                {{ Form::select('active', ['1' => 'Not Received', '2' => 'Received'],null,['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                                {{ Form::label('customer', 'Customer') }}
                                {{ Form::text('customer', null, ['class' => 'form-control', 'placeholder' => 'Customer' ] ) }}
                            </div>
                             <div class="form-group">
                                {{ Form::label('qty', 'QTY') }}
                                {{ Form::text('qty', null, ['class' => 'form-control', 'placeholder' => 'QTY' ] ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('date', 'Date Received') }}
                                {{ Form::text('date', null, ['class' => 'form-control', 'id' => 'datepicker1', 'placeholder' => 'Date' ] ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('qc_date', 'QC Date') }}
                                {{ Form::text('qc_date', null, ['class' => 'form-control', 'id' => 'datepicker', 'placeholder' => 'QC Date' ] ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('inventory_location', 'Inventory Location') }}
                                {{ Form::text('inventory_location', null, ['class' => 'form-control', 'placeholder' => 'Inventory Location' ] ) }}
                            </div>
                          
                            {{ Form::submit('Update',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div>
@stop
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ]
            });
        });
    </script>

    @stop
