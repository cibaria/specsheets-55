@extends('layouts.admin')

@section('content')
    <div id="receivingappointmentlogs">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Create</span> Recieving Label Log
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/receiving/labels/">Recieving Label Log</a> >
                        Create
                    </li>
                </ol>
            </div>
        </div>

        {{--errors--}}
        @if ( count( $errors ) > 0 )
            <div class="alert alert-dismissible alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

        {{--form--}}
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-lg-8">
                        {{ Form::open(['route' => 'labels.store', 'class' => 'form-horizontal']) }}
                        {{ Form::hidden('category', 'labels') }}
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('customer', 'Customer') }}
                                {{ Form::text('customer', null, ['class' => 'form-control', 'placeholder' => 'Customer' ] ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('qty', 'QTY') }}
                                {{ Form::text('qty', null, ['class' => 'form-control', 'placeholder' => 'QTY' ] ) }}
                            </div>                            
                            <div class="form-group">
                                {{ Form::label('date', 'Date Received') }}
                                {{ Form::text('date', null, ['class' => 'form-control', 'id' => 'datepicker1', 'placeholder' => 'Date' ] ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('qc_date', 'QC Date') }}
                                {{ Form::text('qc_date', null, ['class' => 'form-control', 'id' => 'datepicker', 'placeholder' => 'QC Date' ] ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('inventory_location', 'Inventory Location') }}
                                {{ Form::text('inventory_location', null, ['class' => 'form-control', 'placeholder' => 'Inventory Location' ] ) }}
                            </div>


                            {{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}

                        </fieldset>
                        {{ Form::close() }}

                    </div><!--end row-->
                </div>
            </div><!--end col lg 8 -->
        </div><!--end container fluid-->



    </div><!--end blends-->
@stop

    @section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
        });
     </script>

    @stop