@extends('layouts.admin')

@section('content')

<div id="coas">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Active</span>Receiving Label Log
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Labels
                </li>
            </ol>
        </div>
    </div>
        <div class="alert alert-dismissible alert-info">
            <h1>Date: <?php echo Date("m/d/Y");?></h1>
        </div>
    {{--sessions--}}
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    {{--sub nav--}}
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default">
            @if(Auth::user()->group != 20)
                {{ link_to_route('labels.create', 'Create A New Receiving Label Log',null, ['class' => 'btn btn-primary navbar-btn']) }}
                <a href="/receiving/labels/received" class="btn btn-warning navbar-btn">Received</a>            
            @endif
            </nav>
        </div>
    </div>

        <table id="datatable" class="table table-stripped">
        <thead>
        <tr>
           <td><strong>Customer</strong></td>
           <td><strong>QTY</strong></td>
           <td><strong>Date Recieved</strong></td>
           <td><strong>QC Date</strong></td>
           <td><strong>Inventory Location</strong></td>
           <td class="no-sort"></td>
        </tr>
        </thead>
        <tbody class="list">
        @foreach($labels as $label)

          @if($label->active == 1)
                    @if(Date("m/d/Y") === $label->date || Date("n/d/Y") === $label->date)
                        <tr class="danger">
                            @endif
                    @else
                        <tr>
                @endif
             <td>{{ $label->customer }}</td>
             <td>{{ $label->qty }}</td>
             <td>{{ $label->date }}</td>
             <td>{{ $label->qc_date }}</td>
             <td>{{ $label->inventory_location }}</td>
            
             <td>
                 <div class="btn-group" role="group" aria-label="">
                     @if(Auth::user()->group == 100)
                     {{ link_to_route('labels.edit', 'Edit', [$label->id],['class' => 'btn btn-warning']) }}

                     {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('labels.destroy', $label->id))) }}
                     {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                     {{ Form::close() }}
                     @endif
                 </div>
             </td>
            </tr>
        @endforeach

        </tbody>
    </table>


<ul class="pagination"></ul>
</div>
    @stop
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 0, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ]
            });
        });
    </script>

    @stop

