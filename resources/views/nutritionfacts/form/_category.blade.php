<div class="form-group">
    {{ Form::label('category', 'Category') }}
    <div class="controls">
        {{ Form::select('category',['' => 'Choose', 'blank' => 'Blank', 'olive_oils' => 'Olive Oils', 'vinegars' => 'Vinegars', 'pantry_items' => 'Pantry Items', '25_star_white' => '25 Star White Balsamic Vinegar', '25_star_dark' => '25 Star Dark Balsamic Vinegar'], '', ['class' => 'form-control', 'id' => 'form_category']) }}

    </div>
</div>