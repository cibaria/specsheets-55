<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title',null,['class' => 'form-control', 'placeholder' => 'Title']) }}
</div>

<div class="form-group">
    {{ Form::label('active', 'Active') }}
    {{ Form::select('active', ['' => 'Choose', '1' => 'Active', '2' => 'Disable'],'',['class' => 'form-control', 'placeholder' => 'Title']) }}
</div>

<div class="form-group">
    {{ Form::label('serving_size', 'Serving Size') }}
    {{ Form::text('serving_size',null, ['class' => 'form-control', 'placeholder' => 'Serving Size']) }}
</div>

<div class="form-group">
    {{ Form::label('servings_per_container', 'Servings Per Container') }}
    {{ Form::text('servings_per_container', null, ['class' => 'form-control', 'placeholder' => 'Servings Per Container']) }}
</div>

<div class="form-group">
    {{ Form::label('calories', 'Calories') }}
    {{ Form::text('calories', null, ['class' => 'form-control', 'placeholder' => 'Calories']) }}
</div>

<div class="form-group">
    {{ Form::label('calories_from_fat', 'Calories From Fat') }}
    {{ Form::text('calories_from_fat', null, ['class' => 'form-control', 'placeholder' => 'Calories From Fat']) }}
</div>


<div class="form-group">
    {{ Form::label('total_fat_grams', 'Total Fat Grams') }}
    {{ Form::text('total_fat_grams', null, ['class' => 'form-control', 'placeholder' => 'Total Fat Grams']) }}
</div>

<div class="form-group">
    {{ Form::label('total_fat_percent', 'Total Fat Percent') }}
    {{ Form::text('total_fat_percent', null, ['class' => 'form-control', 'placeholder' => 'Total Fat Percent']) }}
</div>

<div class="form-group">
    {{ Form::label('saturated_fat_grams', 'Saturated Fat Grams') }}
    {{ Form::text('saturated_fat_grams', null, ['class' => 'form-control', 'placeholder' => 'Saturated Fat Grams']) }}
</div>

<div class="form-group">
    {{ Form::label('saturated_fat_percentage', 'Saturated Fat Percentage') }}
    {{ Form::text('saturated_fat_percentage', null, ['class' => 'form-control', 'placeholder' => 'Saturated Fat Percentage']) }}
</div>

<div class="form-group">
    {{ Form::label('trans_fat', 'Trans fat') }}
    {{ Form::text('trans_fat', null, ['class' => 'form-control', 'placeholder' => 'Trans fat']) }}
</div>

<div class="form-group">
    {{ Form::label('cholesterol_grams', 'Cholesterol grams') }}
    {{ Form::text('cholesterol_grams', null, ['class' => 'form-control', 'placeholder' => 'Cholesterol grams']) }}
</div>

<div class="form-group">
    {{ Form::label('cholesterol_percentage', 'Cholesterol percentage') }}
    {{ Form::text('cholesterol_percentage', null, ['class' => 'form-control', 'placeholder' => 'Cholesterol percentage']) }}
</div>

<div class="form-group">
    {{ Form::label('sodium_grams', 'Sodium grams') }}
    {{ Form::text('sodium_grams', null, ['class' => 'form-control', 'placeholder' => 'Sodium grams']) }}
</div>

<div class="form-group">
    {{ Form::label('sodium_percentage', 'Sodium percentage') }}
    {{ Form::text('sodium_percentage', null, ['class' => 'form-control', 'placeholder' => 'Sodium percentage']) }}
</div>

<div class="form-group">
    {{ Form::label('total_carbohydrates_grams', 'Total carbohydrates grams') }}
    {{ Form::text('total_carbohydrates_grams', null, ['class' => 'form-control', 'placeholder' => 'Total carbohydrates grams']) }}
</div>

<div class="form-group">
    {{ Form::label('total_carbohydrates_percentage', 'Total carbohydrates percentage') }}
    {{ Form::text('total_carbohydrates_percentage', null, ['class' => 'form-control', 'placeholder' => 'Total carbohydrates percentage']) }}
</div>

<div class="form-group">
    {{ Form::label('dietary_fiber_grams', 'Dietary fiber grams') }}
    {{ Form::text('dietary_fiber_grams', null, ['class' => 'form-control', 'placeholder' => 'Dietary fiber grams']) }}
</div>

<div class="form-group">
    {{ Form::label('dietary_fiber_percentage', 'Dietary fiber percentage') }}
    {{ Form::text('dietary_fiber_percentage', null, ['class' => 'form-control', 'placeholder' => 'Dietary fiber percentage']) }}
</div>

<div class="form-group">
    {{ Form::label('sugars_grams', 'Sugars grams') }}
    {{ Form::text('sugars_grams', null, ['class' => 'form-control', 'placeholder' => 'Sugars grams']) }}
</div>

<div class="form-group">
    {{ Form::label('protein_grams', 'Protein grams') }}
    {{ Form::text('protein_grams', null, ['class' => 'form-control', 'placeholder' => 'Protein grams']) }}
</div>
<div class="form-group">
    {{ Form::label('added_sugars', 'Added Sugars') }}
    {{ Form::text('added_sugars', null, ['class' => 'form-control', 'placeholder' => 'Added Sugars']) }}
</div>

<div class="form-group">
    {{ Form::label('protein_grams', 'Protein grams') }}
    {{ Form::text('protein_grams', null, ['class' => 'form-control', 'placeholder' => 'Protein grams']) }}
</div>

 <div class="form-group">
      {{ Form::label('vitamin_d_grams', 'Vitamin D grams') }}
      {{ Form::text('vitamin_d_grams', null, ['class' => 'form-control', 'placeholder' => 'Vitamin D grams']) }}
  </div>

 <div class="form-group">
      {{ Form::label('vitamin_d_percentage', 'Vitamin D percentage') }}
      {{ Form::text('vitamin_d_percentage', null, ['class' => 'form-control', 'placeholder' => 'Vitamin D percentage']) }}
  </div>

 <div class="form-group">
      {{ Form::label('calcium_grams', 'Calcium Grams') }}
      {{ Form::text('calcium_grams', null, ['class' => 'form-control', 'placeholder' => 'Calcium Grams']) }}
  </div>

 <div class="form-group">
      {{ Form::label('calcium_percentage', 'Calcium Percentage') }}
      {{ Form::text('calcium_percentage', null, ['class' => 'form-control', 'placeholder' => 'Calcium Percentage']) }}
  </div>

 <div class="form-group">
      {{ Form::label('iron_grams', 'Iron Grams') }}
      {{ Form::text('iron_grams', null, ['class' => 'form-control', 'placeholder' => 'Iron Grams']) }}
  </div>

 <div class="form-group">
      {{ Form::label('iron_percentage', 'Iron Percentage') }}
      {{ Form::text('iron_percentage', null, ['class' => 'form-control', 'placeholder' => 'Iron Percentage']) }}
  </div>

 <div class="form-group">
      {{ Form::label('potassium_grams', 'Potassium Grams') }}
      {{ Form::text('potassium_grams', null, ['class' => 'form-control', 'placeholder' => 'Potassium Grams']) }}
  </div>

 <div class="form-group">
      {{ Form::label('potassium_percentage', 'Potassium Percentage') }}
      {{ Form::text('potassium_percentage', null, ['class' => 'form-control', 'placeholder' => 'Potassium Percentage']) }}
 </div>

{{-- <div class="form-group">
    {{ Form::label('blurb', 'Blurb') }}
    {{ Form::textarea('blurb',null,['class' => 'form-control', 'rows' => '4', 'cols' => '8', 'placeholder' => 'Blurb']) }}
</div> --}}

<div class="form-group">
    {{ Form::label('ingredients', 'Ingredients') }}
    {{ Form::textarea('ingredients',null,['class' => 'form-control', 'rows' => '4', 'placeholder' => 'Ingredients']) }}
</div>

{{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}