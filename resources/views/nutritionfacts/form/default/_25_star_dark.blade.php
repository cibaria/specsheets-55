<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title','25 Star Balsamic Vinegar',['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('active', 'Active') }}
    {{ Form::select('active', ['' => 'Choose', '1' => 'Active', '2' => 'Disable'],'',['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('serving_size', 'Serving Size') }}
    {{ Form::text('serving_size','1 tbsp', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('servings_per_container', 'Servings Per Container') }}
    {{ Form::text('servings_per_container', '??', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('calories', 'Calories') }}
    {{ Form::text('calories', '36', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('calories_from_fat', 'Calories From Fat') }}
    {{ Form::text('calories_from_fat', '0', ['class' => 'form-control']) }}
</div>


<div class="form-group">
    {{ Form::label('total_fat_grams', 'Total Fat Grams') }}
    {{ Form::text('total_fat_grams', '0', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('total_fat_percent', 'Total Fat Percent') }}
    {{ Form::text('total_fat_percent', '0', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('saturated_fat_grams', 'Saturated Fat Grams') }}
    {{ Form::text('saturated_fat_grams', '0', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('saturated_fat_percentage', 'Saturated Fat Percentage') }}
    {{ Form::text('saturated_fat_percentage', '0', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('trans_fat', 'Trans fat') }}
    {{ Form::text('trans_fat', '0', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('cholesterol_grams', 'Cholesterol grams') }}
    {{ Form::text('cholesterol_grams', '0', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('cholesterol_percentage', 'Cholesterol percentage') }}
    {{ Form::text('cholesterol_percentage', '0', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('sodium_grams', 'Sodium grams') }}
    {{ Form::text('sodium_grams', '2', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('sodium_percentage', 'Sodium percentage') }}
    {{ Form::text('sodium_percentage', '0', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('total_carbohydrates_grams', 'Total carbohydrates grams') }}
    {{ Form::text('total_carbohydrates_grams', '8', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('total_carbohydrates_percentage', 'Total carbohydrates percentage') }}
    {{ Form::text('total_carbohydrates_percentage', '3', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('dietary_fiber_grams', 'Dietary fiber grams') }}
    {{ Form::text('dietary_fiber_grams', '0', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('dietary_fiber_percentage', 'Dietary fiber percentage') }}
    {{ Form::text('dietary_fiber_percentage', '0', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('sugars_grams', 'Sugars grams') }}
    {{ Form::text('sugars_grams', '7', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('protein_grams', 'Protein grams') }}
    {{ Form::text('protein_grams', '0', ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('added_sugars', 'Added Sugars') }}
    {{ Form::text('added_sugars', null, ['class' => 'form-control', 'placeholder' => 'Added Sugars']) }}
</div>

<div class="form-group">
    {{ Form::label('protein_grams', 'Protein grams') }}
    {{ Form::text('protein_grams', null, ['class' => 'form-control', 'placeholder' => 'Protein grams']) }}
</div>

 <div class="form-group">
      {{ Form::label('vitamin_d_grams', 'Vitamin D grams') }}
      {{ Form::text('vitamin_d_grams', null, ['class' => 'form-control', 'placeholder' => 'Vitamin D grams']) }}
  </div>

 <div class="form-group">
      {{ Form::label('vitamin_d_percentage', 'Vitamin D percentage') }}
      {{ Form::text('vitamin_d_percentage', null, ['class' => 'form-control', 'placeholder' => 'Vitamin D percentage']) }}
  </div>

 <div class="form-group">
      {{ Form::label('calcium_grams', 'Calcium Grams') }}
      {{ Form::text('calcium_grams', null, ['class' => 'form-control', 'placeholder' => 'Calcium Grams']) }}
  </div>

 <div class="form-group">
      {{ Form::label('calcium_percentage', 'Calcium Percentage') }}
      {{ Form::text('calcium_percentage', null, ['class' => 'form-control', 'placeholder' => 'Calcium Percentage']) }}
  </div>

 <div class="form-group">
      {{ Form::label('iron_grams', 'Iron Grams') }}
      {{ Form::text('iron_grams', null, ['class' => 'form-control', 'placeholder' => 'Iron Grams']) }}
  </div>

 <div class="form-group">
      {{ Form::label('iron_percentage', 'Iron Percentage') }}
      {{ Form::text('iron_percentage', null, ['class' => 'form-control', 'placeholder' => 'Iron Percentage']) }}
  </div>

 <div class="form-group">
      {{ Form::label('potassium_grams', 'Potassium Grams') }}
      {{ Form::text('potassium_grams', null, ['class' => 'form-control', 'placeholder' => 'Potassium Grams']) }}
  </div>

 <div class="form-group">
      {{ Form::label('potassium_percentage', 'Potassium Percentage') }}
      {{ Form::text('potassium_percentage', null, ['class' => 'form-control', 'placeholder' => 'Potassium Percentage']) }}
 </div>

{{-- <div class="form-group">
    {{ Form::label('blurb', 'Blurb') }}
    {{ Form::textarea('blurb',
    '* Percent daily values based on a 2000 calorie diet. Not a significant source of Vitamin A, Vitamin C, Calcium or Iron. Contains naturally occurring sulfites from raw materials.',
    ['class' => 'form-control', 'rows' => '4', 'cols' => '8']) }}
</div> --}}

<div class="form-group">
    {{ Form::label('ingredients', 'Ingredients') }}
    {{ Form::textarea('ingredients',
     '25 Star Balsamic Vinegar (Wine Vinegar, Concentrated and Cooked Grape Must, Natural Caramel).',
     ['class' => 'form-control', 'rows' => '4']) }}
</div>

{{ Form::submit('Create',['class' => 'btn btn-default', 'style' => 'float:right']) }}