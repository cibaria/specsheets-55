<div class="form-group {{ $errors->has('product_category') ? 'has-error' : ''}}">
    <label for="product_category" class="col-md-4 control-label">{{ 'Product Category' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="product_category" type="text" id="product_category" value="{{ $recipeproductcategory->product_category or ''}}" >
        {!! $errors->first('product_category', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
