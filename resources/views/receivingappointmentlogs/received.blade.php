
@extends('layouts.admin')

@section('content')

    <div id="receivingappointments">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-danger" style="padding-right:15px;">Received</span>Receiving Appointment Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > <a href="/receivingappointmentlogs/">Receiving Appointment Logs</a> > Received
                    </li>
                </ol>
            </div>
        </div>

        <!--sub nav-->
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <a href="/receivingappointmentlogs/" class="btn btn-warning navbar-btn">Active Receiving Appointment Logs</a>
                    <!--search-->

                    <!--<form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>-->
                </nav>
            </div>
        </div>

        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Conf.#</strong></td>
                <td><strong>PO#</strong></td>
                <td><strong>Lot</strong></td>
<<<<<<< HEAD
                <td><strong>Location</strong></td>
=======
               <td><strong>Location</strong></td>
>>>>>>> coa_update_v2
                <td><strong>QTY</strong></td>
                <td><strong>Vendor</strong></td>
                <td><strong>Carrier</strong></td>
                <td><strong>Product</strong></td>
                <td><strong>Size</strong></td>
                <td><strong>In/Out</strong></td>
                <td><strong>Date</strong></td>
                <td><strong>Time</strong></td>
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($receivingappointmentlogs as $appointment)

                @if($appointment->active == 2)
                    @if(Date("m/d/Y") === $appointment->date)
                        <tr class="danger">
                        @else
                <tr>
                    @endif
                    <td class="appointment">{{ $appointment->id }} {{ $appointment->conf_name }}</td>
                    <td>{{ $appointment->po }}</td>
                    <td>{{ $appointment->lot }}</td>
                    <td>{{ $appointment->location }}</td>
                    <td class="qty">{{ $appointment->qty }}</td>
                    <td class="vendor">{{ $appointment->vendor }}</td>
                    <td class="carrier">{{ $appointment->carrier }}</td>
                    <td class="product">{{ $appointment->product }}</td>
                    <td class="size">{{ $appointment->size }}</td>
                    <td>{{ $appointment->in_out }}</td>
                    <td class="date">{{ $appointment->date }}</td>
                    <td class="time">{{ $appointment->time }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            {{ link_to_route('receivingappointmentlogs.edit', 'Edit', [$appointment->id],['class' => 'btn btn-warning']) }}
                            {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('receivingappointmentlogs.destroy', $appointment->id))) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                        </div>
                    </td>
                </tr>
                    @endif
            @endforeach

            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

<!--end content-->


@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 9, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop
