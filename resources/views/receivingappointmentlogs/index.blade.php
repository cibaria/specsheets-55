
@extends('layouts.admin')

@section('content')

    <div id="receivingappointments">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="alert alert-dismissible alert-success" style="padding-right:15px;">Active</span>Receiving Appointment Logs
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="/admin">Dashboard</a> > Receiving Appointment Logs
                    </li>
                </ol>
            </div>
        </div>
        <div class="alert alert-dismissible alert-info">
            <h1>Date: <?php echo Date("m/d/Y");?></h1>
        </div>

        <!--sessions-->
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <!--sub nav-->
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    {{ link_to_route('receivingappointmentlogs.create', 'Create A New Receiving Log',null, ['class' => 'btn btn-primary navbar-btn']) }}
                    <a href="/receivingappointmentlogs/received" class="btn btn-warning navbar-btn">Recieved</a>
                    <a href="/warehouse" class="btn btn-danger navbar-btn">Warehouse</a>
                    <!--search-->

                    <!--<form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class=" search form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-primary" data-sort="nutritionfact">Submit</button>
                    </form>-->
                </nav>
            </div>
        </div>
        <table id="datatable" class="table table-stripped">
            <thead>
            <tr>
                <td><strong>Conf.#</strong></td>
                <td><strong>PO#</strong></td>
                <td><strong>Lot</strong></td>
                <td><strong>Location</strong></td>
                <td><strong>QTY</strong></td>
                <td><strong>Vendor</strong></td>
                <td><strong>Carrier</strong></td>
                <td><strong>Product</strong></td>
                <td><strong>Size</strong></td>
                <td><strong>In/Out</strong></td>
                <td><strong>Date</strong></td>
                <td><strong>Time</strong></td>
                <td class="no-sort"></td>
            </tr>
            </thead>
            <tbody>
            @foreach($receivingappointmentlogs as $appointment)

                @if($appointment->active == 1)
                    @if(Date("m/d/Y") === $appointment->date || Date("n/d/Y") === $appointment->date)
                        <tr class="danger">
                        <!--customer product-->
                        @elseif(Date("m/d/Y") === $appointment->date && $appointment->customer_product === '1')
                        <tr style="background-color:#d9edf7;">
                    @else
                        <tr>
                @endif

                    <td>{{ $appointment->id }} {{ $appointment->conf_name }}</td>
                    <td>{{ $appointment->po }}</td>
                    <td>{{ $appointment->qty }}</td>
                    <td>{{ $appointment->vendor }}</td>
                    <td>{{ $appointment->carrier }}</td>
                    <td>{{ $appointment->product }}</td>
                    <td>{{ $appointment->size }}</td>
                    <td>{{ $appointment->in_out }}</td>
                    <td>{{ $appointment->date }}</td>
                    <td>{{ $appointment->time }}</td>
                    <td>
                        @if(Auth::user()->group == 100 || Auth::user()->group == 50)
                        <div class="btn-group" role="group" aria-label="">
                            {{ link_to_route('receivingappointmentlogs.edit', 'Edit', [$appointment->id],['class' => 'btn btn-warning']) }}
                            {{ Form::open(array('method' => 'DELETE', 'style' => 'display:inline','route' => array('receivingappointmentlogs.destroy', $appointment->id))) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                        </div>
                            @endif
                    </td>
                </tr>
                    @endif
            @endforeach
            </tbody>
        </table>


        <ul class="pagination"></ul>
    </div>
@stop

<!--end content-->
@section('footer')
   <script>
          $(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker1" ).datepicker();
            $('#timepicker').timepicker();
            $('#datatable').DataTable({
                paging:true,
                "order": [[ 9, "desc" ]],
                // scrollY:400,
                deferRender:true,
                // scroller:true,
                columnDefs: [
                                { targets: 'no-sort', orderable: false }
                            ],

            });
        });
    </script>

    @stop