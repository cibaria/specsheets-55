<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFrontImageToRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipes', function (Blueprint $table) {
            $table->string('front_image');
            $table->string('back_image');
            $table->string('title');
            $table->string('category');
            $table->boolean('activated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipes', function (Blueprint $table){
            $table->dropColumn('front_image');
            $table->dropColumn('back_image');
            $table->dropColumn('title');
            $table->dropColumn('category');
            $table->dropColumn('activated');
        });
    }
}
