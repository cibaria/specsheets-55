===Almond====
Appearance
Flavor 
Odor
Peroxide Value
FFA
C16:0
C18:0
C18:1
C18:2
C18:3
-------------
===Apricot Kernel===
Acid Value
Peroxide Value
Relative Density
Saponification Value
C16:0
C16:1
C18:0
C18:1
C18:2
C18:3
---------------
=====Avocado Oil====
Acid Value
Peroxide Value
Iodine Value
Saponification Value
Refractive Index 20ºC
C:16:0
C:16:1
C:18:0
C:18:1
C:18:2
C:18:3
--------------------
===Babassu=====
Refractive Index (40 deg C)
Acid Value
Iodine Value
Saponification Value
Color Lovibond 5 1/4"
C6:0
C 8:0
C10:0
C12:0
C14:0
C16:0
C18:0
C18:1
C18:2
C18:3
C20:0
C20:1
C22:0
C22:1
C24:0
-----------------------------
===Balsamic Vinegars===
SPECIFIC GRAVITY @ 20 DEG:
POUNDS PER GALLON
DEVELOPED ALCOHOL DEGREE:
TOTAL ACIDITY AS ACETIC ACID:
Ph / Ph:
TOTAL DRY EXTRACT:
TOTAL SULPHUROUS ANHYDRIDE (SO2):
FREE SULPHUROUS ANHYDRIDE:
RIFRACTOMETRIC AT 20 DEG
SPECIFIC WEIGHT AS BAUMIC DEGREES
LEAD
COPPER
ZINC
IRON
--------------------------
===Canola===
Free Fatty Acid (Oleic Acid)
Flavor
Peroxide Value
Color Lovibond
Iodine Value - Ri
Moisture %
--------------------------
===Castor Oil===
Specific Gravity
Odor
Color (Lovibond Red)
Free Fatty Acid
Hydroxyl Value
Iodine Value
Saponification Value
-------------------------
===Cocoa Butter===
Free Fatty Acid
Color
Refractive Index
Melt Point
Additives
------------------------
===Coconut RBD===
FREE FATTY ACID
M % I (%)
IODINE VALUE
PEROXIDE VALUE, meg./kg
SLIP MELTING POINT
--------------------
===Coconut Organic===
Flavor		
Color		
Aroma		
FFA		
Moisture & Impurities		
Peroxide Value		
Iodine Value		
Melting point		Deg C
-----------------------
===Coconut Organic RBD===
Flavor
Color
Ordor
FFA
Moisture & Impurities
Peroxide Value
Iodine Value
Melting point
Saponification Value
Unsaponifiable Matter
C6: Capronic acid
C8: Caprylic acid
C10:Capric acid
C12: Lauric acid
C14: Myristic acid
C16: Palmitic acid
C18: Stearic acid
C18:1 Oleic acid
C18:2 Linoleic acid
C18.3 Linolenic acid
---------------------------
===Corn Oil===
ACID VALUE
PEROXIDE VALUE
ODOR
COLOR RED
APPERANCE
===Cotton seed===
% FFA
APPEARANCE CLEAR
COLOR
ODOR
FLAVOR
PV
ADDITIVE
------------------
===Cotton Seed===
% FFA
APPEARANCE CLEAR
COLOR
ODOR
FLAVOR
PV
ADDITIVE
----------------
===Flax Organic===
FFA
PEROXIDE VALUE meq/kg
C16:
C18:
C'18:
C''18:
C'''18:
------------------
===Flaxseed===
Appearance @ 25 degree C		
Color	Lovibond	Red
	Lovibond	Yellow
Flavor and Odor		
Free Fatty Acids		
Iodine Value		
Lead		
Moisture		
Peroxide Value		
Refractive Index		
Unsaponifiable Matter		
		
	C16:0	
	C18:0	
	C18:1	
	C18:2	
	C18:3	
--------------------
===Grapeseed===
Acid Value
Peroxide Value
C14:0
C16:0
C16:1
C17:0
C17:1
C18:0
C18:1
C18:2
C18:3
C20:0
--------------------
===Hemp===
Acid Value
Peroxide Value
Iodine Value (Wijs)
C16:0
C18:0
C18:1
C18:2
C18:3
C20:0
Minor
--------------------
===Jojoba Oil===
Refractive Index (20 deg C)
Acid Value
Iodine Value
Saponification Value
Color Lovibond 5 1/4"
C12:0
C14:0
C14:1
C16:0
C16:1
C17:0
C17:1
C18:0
C18:1
C18:2
C18:3
C18:4
C20:0
C20:1
C20:2
C20:4
C22:0
C22:1
C24:0
C24:1
------------------------
===Macadamia===
Acid Value
PV
Palmitic 
Palmitoleic 
Stearic 
Oleic 
Linoleic 
Linoleic 
Arachidic
Gadoleic
------------------------
===Macadamia Virgin===
Acid Value
PV
C16:0
C16:1
C18:0
C18:1
C18:2
C 20:0
C 20:1
C 22:0
-------------------------
===Mango Butter===
Free Fatty Acid	
Peroxide Value	
Iodine Value	
Melt Point	Deg C
Additives	
Fatty Acid Composition	
	C16:0
	C18:0
	C18:1
	C18:2
	C18:3
	C20:0
	Other
--------------------------
===Olive Oil, Extra Virgin - Black Truffle===
Free Acidity is % Oleic Acid
Peroxide Value meg gr O/kg
Delta K
K270
K232
---------------------------
===Olive Oil, Extra Virgin - White Truffle===
Free Acidity is % Oleic Acid
Peroxide Value meg gr O/kg
Absorbency in UV
ΔK
K270
K232
-----------------------------
===Olive Oil, Extra Virgin - Organic===
Free acidity (% m/m expressed in oleic acid)
Peroxide value
C16:0
C16:1
C17:0
C17:1 
C18:0
C18:1
C18:2
C18:3
Cholesterol
Brassicasterol
Campestanol+Campesterol
Stigmasterol
------------------------------
===Olive Oil, Extra Virgin===
Free acidity (% m/m expressed in oleic acid)			
Peroxide value			
		C14	
		C16	
		C16:1	
		C17	
		C17:1	
		C18	
		C18:1	
		C18:2	
		C20	
		C18:3	
		C20:1	
		C22
-------------------------------------
===Olive Oil, Extra Virgin - Italian===
Free acidity (% m/m expressed in oleic acid)	
Absorption in ultra-violet:	
	delta k
	K232
	K268
	K270
Peroxide value
-----------------------------------
===Olive Oil, Fused - Classic Italy===
Free acidity (% m/m expressed in oleic acid)			
Peroxide value			
Refractive Index			
			
C14:0	Myristic acid 		
C16:0	Palmitic acid		
C16:1	Palmitoleic acid		
C17:0	Heptadacanoic acid		
C18:0	Stearic acid		
C18:1	Oleic acid		
C18:2	Linoleic acid		
C18:3	Linolenic acid		
C20:0	Arachidic acid		
C22:0	Behenic acid
----------------------------------
===Olive Oil, Garlic Cilantro===
Acidity - % Oleic Acid	
Peroxide Value meg gr O/kg
Palmitic	
Palmitoleic	
Stearic	
Oleic	
Linoleic	
Linolenic
---------------------------------
===Olive Oil, Herbs De Provence - Fused===
Acidity - % Oleic Acid
Peroxide Value meg gr O/kg
Palmitic
Palmitoleic
Stearic
Oleic
Linoleic
Linolenic
--------------------------------
===Olive Oil, Pure - Basil Lemon Grass===
Acidity - % Oleic Acid	
Peroxide Value meg gr O/kg	
K-270	
Delta K		
Palmitic	
Palmitoleic	
Steric	
Oleic	
Linoleic	
Linolenic	
Gadoleic		
Cholesterol	
Campesterol	
Stigmasterol	
Delta7 - Stigmastenol
----------------------------------
===Olive Oil, Pure - Basil===
Free acidity (% m/m expressed in oleic acid)		
Peroxide value		
Saponification Value		
C14:0	Myristic acid 	
C16:0	Palmitic acid	
C16:1	Palmitoleic acid	
C17:0	Heptadacanoic acid	
C18:0	Stearic acid	
C18:1	Oleic acid	
C18:2	Linoleic acid	
C18:3	Linolenic acid	
C20:0	Arachidic acid	
C22:0	Behenic acid
-----------------------------------
===Olive Oil, Pure - Black Pepper Infused===
Free acidity (% m/m expressed in oleic acid)		
Peroxide 		
Refractive Index		
Saponification Value		
		
C14:0	Myristic acid 	
C16:0	Palmitic acid	
C16:1	Palmitoleic acid	
C18:0	Stearic acid	
C18:1	Oleic acid	
C18:2	Linoleic acid	
C18:3	Linolenic acid	
C20:0	Arachidic acid	
C22:0	Behenic acid
-----------------------------------
===Oil Oil, Pure Chipotle Infused===
Free acidity (% m/m expressed in oleic acid)	
K-270	
K-232	
Peroxide value	
Refractive Index	
Iodine Value	
Saponification Value	
Delta-K	
C16:0	Palmitic acid
C16:1	Palmitoleic acid
C17:0	Heptadacanoic acid
C18:0	Stearic acid
C18:1	Oleic acid
C18:2	Linoleic acid
C18:3	Linolenic acid
Cholesterol	
Brassicasterol	
Campesterol
--------------------------------
===Olive Oil, Pure - Garlic Infused===
Free acidity (% m/m expressed in oleic acid)	
Peroxide value	
Saponification Value	
C14:0	Myristic acid 
C16:0	Palmitic acid
C16:1	Palmitoleic acid
C18:0	Stearic acid
C18:1	Oleic acid
C18:2	Linoleic acid
C18:3	Linolenic acid
C20:0	Arachidic acid
C22:0	Behenic acid
----------------------------------
===Olive Oil, Pure - Habanero Infused===
Free acidity (% m/m expressed in oleic acid)		
K-270		
K-232		
Peroxide value		
Saponification Value		
Delta-K		
C16:0	Palmitic acid	
C16:1	Palmitoleic acid	
C17:0	Heptadacanoic acid	
C18:0	Stearic acid	
C18:1	Oleic acid	
C18:2	Linoleic acid	
C18:3	Linolenic acid	
Cholesterol		
Brassicasterol		
Campesterol
-----------------------------------
===Olive Oil, Pure - Lemon Pepper Infused===
Free acidity (% m/m expressed in oleic acid)		
K-270		
K-232		
Peroxide value		
Refractive Index		
Iodine Value		
Saponification Value		
Delta-K			
C16:0	Palmitic acid	
C16:1	Palmitoleic acid	
C17:0	Heptadacanoic acid	
C18:0	Stearic acid	
C18:1	Oleic acid	
C18:2	Linoleic acid	
C18:3	Linolenic acid		
Cholesterol		
Brassicasterol		
Campesterol
---------------------------------
===Olive Oil, Pure - Oregano Infused===
Free acidity (% m/m expressed in oleic acid)
AppearanceFlavor
Appearance
-------------------------------------
===Olive Oil, Pure - Roasted Chili===
Free acidity (% m/m expressed in oleic acid)		
Peroxide value		
Saponification Value		
C14:0	Myristic acid 	
C16:0	Palmitic acid	
C16:1	Palmitoleic acid	
C17:0	Heptadacanoic acid	
C18:0	Stearic acid	
C18:1	Oleic acid	
C18:2	Linoleic acid	
C18:3	Linolenic acid	
C20:0	Arachidic acid	
C22:0	Behenic acid
-------------------------------------
===Olive Oil, Pure - Rosemary Infused===
Free acidity (% m/m expressed in oleic acid)	
Peroxide value	
Saponification Value 	
C14:0	Myristic acid 
C16:0	Palmitic acid
C16:1	Palmitoliec acid
C17:0	Heptadacanoic acid
C18:0	Stearic acid
C18:1	Oleic acid
C18:2	Linoleic acid
C18:3	Linolenic acid
---------------------------------------
===Olive Oil, Pure - Scallion Infused===
Free acidity (% m/m expressed in oleic acid)	
Absorption in ultra-violet:	 270 nm
Peroxide value(in millieq. Peroxide	oxygen per kg/oil)
C16:0	Palmitic acid
C16:1	Palmitoliec acid
C18:0	Stearic acid
C18:1	Oleic acid
C18:2	Linoleic acid
C18:3	Linolenic acid
C20:0	Arachidic acid
Cholesterol	
Campesterol	
Stigmasterol
---------------------------------------
===Olive Oil, Pure===
Free acidity (% m/m expressed in oleic acid)		
Absorption in ultra-violet:	 K270	
Peroxide value(in millieq. Peroxide	oxygen per kg/oil)	
C14:0	Myristic Acid	
C16:0	Palmitic acid	
C16:1	Palmitoliec acid	
C17:0	Heptadecanoic Acid	
C17:1	Heptadecenoic Acid	
C18:0	Stearic acid	
C18:1	Oleic acid	
C18:2	Linoleic acid	
C18:3	Linolenic acid	
C20:0	Arachidic acid	
C22:0	Behenic Acid
---------------------------------------
===Olive Oil, Tuscan Herb Fused===
Acidity - % Oleic Acid
Peroxide Value meg gr O/kg
Palmitic
Palmitoleic
Stearic
Oleic
Linoleic
Linolenic
---------------------------------
===Olive Oil, Virgin===
Free acidity (% m/m expressed in oleic acid)
Peroxide value
K232
K270
DELTA K
---------------------------------
===Olive Pomace Oil===
F.F.A (% OLEIC ACID)
Peroxide Value (meg 0/kg) 
K 270 nm
Miristic acid
Palmitic (C16:0)
Palmitoleic (C16:1)
Heptadecanoic acid
Heptadecenoic acid
Stearic (C18:0)
Oleic (C18:1)
Linoleic (C18:2)
Linolenic (C18:3)
Arachidic (C20:0)
Gadoleic
Behenic (C22:0)
Trans Oleic
Trans Linoleic + Linolenic
Cholesterol
Brassicasterol
Campesterol
Stigmasterol
-------------------------------------
===Palm Kernel===
COLOR, LOVIBOND	RED
FLAVOR			
FREE FATTY ACID			
PEROXIDE VALUE			
IODINE VALUE			
ADDITIVES			
MELTING POINT 			
SFC @	50 deg F		
	70 deg F		
	80 deg F		
	104 deg F
----------------------------------
===Palm Oil, RBD===
COLOR, LOVIBOND 5 1/4"			YEL
			RED
FREE FATTY ACID, % OLEIC			
IODINE VALUE			
PEROXIDE VALUE			
MELT POINT, DEG C			
M % I (%)			
---------------------------------
===Peanut===
FFA	
COLOR RED	
PEROXIDE	
IODINE	
---------------------------------
===Rice Bran===
ACID VALUE			
PEROXIDE VALUE			
MOISTURE			
IODINE VALUE			
COLOR - LOVIBOND Red
			Yellow
---------------------------------
===Safflower, HO - EXP - RBD - Organic===
APPEARANCE 		
FLAVOR		
FREE FATTY ACID, % AS OLAIC		
PEROXIDE VALUE, meq/L		
C16-0 Palmitic		
C18-0 Stearic		
C18-1 Oleic		
C18-2 Linoleic		
C18-3 Alpha Linolenic
--------------------------------
===Safflower, HO - EXP===		
APPEARANCE 		
FLAVOR		
FREE FATTY ACID, % AS OLEIC		
PEROXIDE VALUE, meq/L		
C16-0 Palmitic		
C18-0 Stearic		
C18-1 Oleic		
C18-2 Linoleic		
C18-3 Alpha Linolenic
------------------------------
===Sesame Oil, Refined===
COLOR		LOVIBOND
FREE FATTY ACID		(%)
IODINE VALUE		
ADDITIVES		
PEROXIDE VALUE		(meq/Kg oil)
C:16		
C:18		
C:18:1		
C:18:2		
C:18:3							
-------------------------------
===Sesame Oil, Toasted===
Color Lovibond (visual)
Iodine Value
Free Fatty Acid (as oleic)
Peroxide Value (meq/Kg oil)
Refractive Index @ 40 Degrees C
Saponification Value
Unsaponifiable Matter
Sediment
C16
C18
C18:1
C18:2
C18:3
-----------------------------
===Shea Butter===
Peroxid Value (meq/kg)		
Free Fatty Acid (as % Oleic Acid)		
Iodine Value		
Melting Point (degree C.)			
C16:0	Palmitic acid	
C18:0	Stearic acid	
C18:1	Oleic acid	
C18:2	Linoleic	
C20:0	Arachidic acid	
Minor		
-----------------------------
===Shea Butter, Organic===
Refractive Index (40 degree C.)		
Acid Value		
Iodine Value		
Saponification Value		
Melting Point (degree C.)		
Color Lovibond 5 1/4"		
Color Gardner			
C12:0	Lauric acid	
C14:0	Myristic acid	
C16:0	Palmitic acid	
C16:1	Palmitoleic acid	
C17:0	Margaric acid	
C18:0	Stearic acid	
C18:1	Oleic acid	
C18:3	Linolenic acid	
C20:0	Arachidic acid	
C20:1	Eicosenoic acid	
C22:0	Behenic acid	
C24:0	Lignoceric acid
-------------------------------
===Soybean, Non GMO===
Appearance:	
Odor:	
FFA ( OLEIC )	
COLOR - Y	
COLOR - R	
Intial Peroxide Value (meq/kg)	
% Moisture
----------------------------
===Soybean, Organic===
COLOR - Y	
COLOR - R	
Free Fatty Acids	
Peroxide Value (meq/kg)	
Refractive Index	
Odor:	
---------------------------------
===Soybean, RBD===
Color Yel	
Color Red	
Free Fatty Acids	
Peroxide Value	
Iodine Value	
Cold Test (Hours)	
Allergens:	
------------------------------
===Soybean, Wint===
LOVIBOND RED COLOR		
LOVIBOND YELLOW COLOR		
COLD TEST @ 0 C (HRS.)		
% FFA 		
% MOISTURE		
FLAVOR 		
PEROXIDE VALUE		
I.V.		
AOM		
SMOKE POINT 		
ADDITIVE
----------------------------
===Sunflower, HO - Organic===
Free Fatty Acid (as oleic)
Peroxide Value (meq/Kg oil)
Odor
Taste
----------------------------
===Sunflower, HO - Winterized===
Free Fatty Acid (as oleic)
Peroxide Value (meq/Kg oil)
Color
Iodine Value
Appearance
Organoleptic Characteristics
C16:0
C18:0
C18:1
C18:2
C18:3	
-------------------------------
===Sunflower, HO===
Free Fatty Acid (as oleic)
Peroxide Value (meq/Kg oil)
Odor
Taste
-----------------------------
===Sunflower, MO - RBD Non GMO===
FREE FATTY ACID, %	
PEROXIDE VALUE MEG/KG	
OSI AT 110ºC	
C 18:1 TOTAL	
ADDITIVES	
FLAVOR	
COLOR	RED
-------------------------
===Sunflower, MO - RBDD===
COLOR	RED
FREE FATTY ACID, %
PEROXIDE VALUE MEG/KG
I.V.
ADDITIVES
------------------------
===Sunflower, MO - RBWD - Non GMO===
COLOR	RED	
FREE FATTY ACID, %		
PEROXIDE VALUE MEG/KG		
IODINE VALUE		
FLAVOR		
OSI AT 110ºC		
C 18:1 TOTAL		
ADDITIVES		
-----------------------------------
===Sunflower, MO - RBWD===
COLOR	RED	
FREE FATTY ACID, %		
PEROXIDE VALUE MEG/KG		
IODINE VALUE		
FLAVOR		
OSI AT 110ºC		
C 18:1 TOTAL		
----------------------------
===Sunflower, MO===
COLOR	RED	
FREE FATTY ACID, %		
PEROXIDE VALUE MEG/KG		
IODINE VALUE		
FLAVOR		
OSI AT 110ºC		
C 18:1 TOTAL		
-------------------------
===Tapioca Glucose===
BRIX @ 25 C		
STARCH		
DEXTROS EQUIVALENT		
pH (in 50% aq. Sol.)		
So₂		
ASH		
GLUCOSE		
MALTOSE
-------------------------
===Vinegar, Apple Cider, 50 Grain===
Acidity	
Grains	
Brix	
Salt	
Alcohol	
Color	
Free SO2	
Total SO2	
pH	
Odor/Aroma/Flavor
------------------------------------
===Vinegar, Red Wine - 50 Grain===
Acidity
Grains
Brix
Salt
Alcohol
Color
Free SO2
Total SO2
pH
Odor/Aroma/Flavor
---------------------------------
===Vinegar, Rice - 100 Grain===
Acidity
Grain
Alcohol
Color
FSO2
Turbidity
-------------------------------
===Vinegar, Rice - 50 Grain===
Acidity		5.04
Grain		50.40
Alcohol		0.14
Color		Slight Amber
Total SO2		NA
-----------------------------
===Vinegar, Serrano Honey===
ACIDITY
PH
BRIX
--------------------------
===Vinegar, White Dist - 200 Grain===
Acidity
Grain
Alcohol
Color
Odor
FSO2
Turbidity
---------------------------
===Vinegar, White Wine - Organic 9%===
SPECIFIC GRAVITY @ 20 DEG:
DEVELOPED ALCOHOL DEGREE:
TOTAL ACIDITY AS ACETIC ACID:
Ph / Ph:
TOTAL DRY EXTRACT:
TOTAL SULPHUROUS ANHYDRIDE (SO2):
FREE SULPHUROUS ANHYDRIDE:
LEAD
COPPER
ZINC
IRON
-------------------------------------
===Walnut - Refined===
FFA	
Peroxide Value	
Fatty Acid Composition in % (Detection Limit 0.05%)	
C16:0	Palmitic
C18:0	Stearic
C18:1	Oleic
C18:2	Linoleic
C18:3	Linolenic
-------------------------------------
//Do this to mysql before it goes live
[mysqld]

innodb_log_file_size = 512M

innodb_strict_mode = 0
ubuntu 16.04 edit path:

sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf