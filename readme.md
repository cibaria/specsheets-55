## Laravel 5.5 Upgrade

This is the repository for the 4.2 to 5.5 Laravel upgrade

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
