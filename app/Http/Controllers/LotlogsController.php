<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class LotlogsController extends BaseController
{

    /**
     * Display a listing of lotlogs
     *
     * @return Response
     */
    public function index()
    {
     //ex. 2017
     $full_year = Date('Y');
     $current_year_full_like = '%/' . $full_year;
     //ex. 17
     $year = Date('y');
     $current_year_like = '%/' . $year;

        $lotlogs = \DB::table('lotlogs')
            ->where('date', 'LIKE', $current_year_full_like )
            ->orWhere('date', 'LIKE', $current_year_like )
            ->orderBy('lot', 'desc')
            ->get();

        return View::make('lotlogs.index', compact('lotlogs'));
    }
    public function last_year()
    {

        //ex. 2017
     $full_year = Date('Y');
     $last_year_full = $full_year - 1;
     $last_year_full_like = '%/' . $last_year_full;
     //ex. 17
     $year = Date('y');
     $last_year = $year - 1;
     $last_year_like = '%/' . $last_year;

        $lotlogs = \DB::table('lotlogs')
            ->where('date', 'LIKE', $last_year_full_like )
            ->orWhere('date', 'LIKE', $last_year_like )
            ->orderBy('lot', 'desc')
            ->get();

        return View::make('lotlogs.last_year', compact('lotlogs'));   
    }

    public function archive()
    {

        /* Current Year */
      //ex. 2017
     $full_year = Date('Y');
     $current_year_full_like = '%/' . $full_year;
     //ex. 17
     $year = Date('y');
     $current_year_like = '%/' . $year;
     /* Last Year */
         //ex. 2017
     $full_year = Date('Y');
     $last_year_full = $full_year - 1;
     $last_year_full_like = '%/' . $last_year_full;
     //ex. 17
     $year = Date('y');
     $last_year = $year - 1;
     $last_year_like = '%/' . $last_year;


        $lotlogs = \DB::table('lotlogs')
            ->where('date', 'NOT LIKE', $current_year_like )
            ->where('date', 'NOT LIKE', $current_year_full_like )
            ->where('date', 'NOT LIKE', $last_year_full_like )
            ->where('date', 'NOT LIKE', $last_year_like )
            ->orderBy('lot', 'desc')
            ->get();

        return View::make('lotlogs.archive', compact('lotlogs'));   
    }

    /**
     * Show the form for creating a new lotlog
     *
     * @return Response
     */
    public function create()
    {
        return View::make('lotlogs.create');
    }

    /**
     * Store a newly created lotlog in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data = new \Lotlog();
        /*
		*-------------------------------------------
		*FILE UPLOADS
		*--------------------------------------------
		*/

        //file upload Incoming Materials QC form BOL
        if (Input::hasFile('im_bol')) {
            $file = Input::file('im_bol');

            $destinationPath = '/images/lotlogs/forms/';
            $filename = Str::random(10) . "_" . $file->getClientOriginalName();

            $success = Input::file('im_bol')->move(public_path() . $destinationPath, $filename);
            if ($success) {
                $data->im_bol = $filename;
            }
        }
        //end BOL File upload
        //file upload Incoming Product QC form BOL
        //Certificate of Analysis
        if (Input::hasFile('ip_certificate_of_analysis')) {
            $file = Input::file('ip_certificate_of_analysis');
            
            $destinationPath = '/images/lotlogs/forms/';
            
            $filename = Str::random(10) . "_" . $file->getClientOriginalName();


            $success = Input::file('ip_certificate_of_analysis')->move(public_path() . $destinationPath, $filename);

            if ($success) {
                $data->ip_certificate_of_analysis = $filename;
            }
        }
        //end Certificate of Analysis File upload

        //Weight Certificate
        if (Input::hasFile('ip_weight_certificate')) {
            $file = Input::file('ip_weight_certificate');
            

            $destinationPath = '/images/lotlogs/forms/';
            $filename = Str::random(10) . "_" . $file->getClientOriginalName();

            $success = Input::file('ip_weight_certificate')->move(public_path() . $destinationPath, $filename);

            if ($success) {
                $data->ip_weight_certificate = $filename;
            }
        }
        //end Weight Certificate File upload

        //Wash Certificate
        if (Input::hasFile('ip_wash_certificate')) {
            $file = Input::file('ip_wash_certificate');
            

            $destinationPath = '/images/lotlogs/forms/';
            $filename = Str::random(10) . "_" . $file->getClientOriginalName();

            $success = Input::file('ip_wash_certificate')->move(public_path() . $destinationPath, $filename);

            if ($success) {
                $data->ip_wash_certificate = $filename;
            }
        }
        //end Wash Certificate File upload

        //Organic Certificate
        if (Input::hasFile('ip_organic_certificate')) {
            $file = Input::file('ip_organic_certificate');
            

            $destinationPath = '/images/lotlogs/forms/';
            $filename = Str::random(10) . "_" . $file->getClientOriginalName();

            $success = Input::file('ip_organic_certificate')->move(public_path() . $destinationPath, $filename);

            if ($success) {
                $data->ip_organic_certificate = $filename;
            }
        }
        //end Organic Certificate File upload




        //$_GET
        $data->active = Input::get('active');
        $data->lot = Input::get('lot');
        $data->so = Input::get('so');
        $data->original_lot = Input::get('original_lot');
        $data->description = Input::get('description');
        $data->supplier = Input::get('supplier');
        $data->origin = Input::get('origin');
        $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->customer = Input::get('customer');
        $data->date = Input::get('date');
        $data->notes1 = Input::get('notes1');
        $data->notes2 = Input::get('notes2');
        $data->notes3 = Input::get('notes3');
        $data->created_at = Input::get('created_at');
        $data->updated_at = Input::get('updated_at');
        $data->sample_location = Input::get('sample_location');
        $data->po = Input::get('po');
        $data->packslip = Input::get('packslip');
        $data->container = Input::get('container');
        $data->container_number = Input::get('container_number');
        $data->expiration_date = Input::get('expiration_date');
        $data->carrier = Input::get('carrier');
        $data->vendor = Input::get('vendor');
        $data->product_on_hold = Input::get('product_on_hold');
        $data->product_contract = Input::get('product_contract');
        $data->kosher_cert_file = Input::get('kosher_cert_file');
        $data->year = Input::get('year');
        // $data->spotchecked = Input::get('spotchecked');
        $data->lotlogtype = Input::get('lotlogtype');
        $data->progress = Input::get('progress');
        $data->progress_notes = Input::get('progress_notes');

        //incoming product qc form
            $data->ip_seals = Input::get('ip_seals');
            $data->ip_truck_tanker = Input::get('ip_truck_tanker');
            $data->ip_caps_and_seals = Input::get('ip_caps_and_seals');
            $data->ip_cartons = Input::get('ip_cartons');
            $data->ip_drum_condition = Input::get('ip_drum_condition');
            $data->ip_labels = Input::get('ip_labels');
            $data->ip_product_type = Input::get('ip_product_type');
            $data->ip_vendor_lot_number = Input::get('ip_vendor_lot_number');
            $data->ip_notes = Input::get('ip_notes');
            $data->ip_approved_for_use = Input::get('ip_approved_for_use');
            $data->ip_if_no_explain_why = Input::get('ip_if_no_explain_why');
            $data->ip_date_received = Input::get('ip_date_received');
            $data->ip_approved_by = Input::get('ip_approved_by');
            $data->ip_kosher_shed_a = Input::get('ip_kosher_shed_a');

        //incoming materials qc form
            $data->im_trucker_inspection = Input::get('im_trucker_inspection');
            $data->im_date_received = Input::get('im_date_received');
            $data->im_labeling = Input::get('im_labeling');
            $data->im_condition = Input::get('im_condition');
            $data->im_cartons = Input::get('im_cartons');
            $data->im_primary_secondary = Input::get('im_primary_secondary');
            $data->im_approved_for_use = Input::get('im_approved_for_use');
            $data->im_notes = Input::get('im_notes');
            $data->im_date_approved = Input::get('im_date_approved');
            $data->im_seal = Input::get('im_seal');
            $data->im_not_approved_why = Input::get('im_not_approved_why');
            $data->im_approved_by = Input::get('im_approved_by');
            $data->approved_by = Input::get('approved_by');
            $data->product_category = Input::get('product_category');
        $data->save();

        Session::flash('message', 'Your Lotlog Was Created');
        Session::flash('alert-class', 'alert-success');
        return Redirect::route('lotlogs.index');
    }

    /**
     * Display the specified lotlog.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $lotlog = \Lotlog::findOrFail($id);

        return View::make('lotlogs.show', compact('lotlog'));
    }

    /**
     * Show the form for editing the specified lotlog.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $lotlog = \Lotlog::find($id);

        return View::make('lotlogs.edit', compact('lotlog'));
    }

    /**
     * Update the specified lotlog in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

            $data = \Lotlog::with('coa')->findOrFail($id);

            //file upload Incoming Materials QC form BOL
        if (Input::hasFile('im_bol')) {
            $file = Input::file('im_bol');
            

            $destinationPath = '/images/lotlogs/forms/';
            $filename = Str::random(10) . "_" . $file->getClientOriginalName();

            $success = Input::file('im_bol')->move(public_path() . $destinationPath, $filename);

            if ($success) {
                $data->im_bol = $filename;
            }
        }
        //end BOL File upload

        //file upload Incoming Product QC form BOL


        //Certificate of Analysis
        if (Input::hasFile('ip_certificate_of_analysis')) {
            $file = Input::file('ip_certificate_of_analysis');
            
            $destinationPath = '/images/lotlogs/forms/';
            
            $filename = Str::random(10) . "_" . $file->getClientOriginalName();


            $success = Input::file('ip_certificate_of_analysis')->move(public_path() . $destinationPath, $filename);

            if ($success) {
                $data->ip_certificate_of_analysis = $filename;
            }
        }
        //end Certificate of Analysis File upload

        //Weight Certificate
        if (Input::hasFile('ip_weight_certificate')) {
            $file = Input::file('ip_weight_certificate');
            

            $destinationPath = '/images/lotlogs/forms/';
            $filename = Str::random(10) . "_" . $file->getClientOriginalName();

            $success = Input::file('ip_weight_certificate')->move(public_path() . $destinationPath, $filename);

            if ($success) {
                $data->ip_weight_certificate = $filename;
            }
        }
        //end Weight Certificate File upload

        //Wash Certificate
        if (Input::hasFile('ip_wash_certificate')) {
            $file = Input::file('ip_wash_certificate');
            

            $destinationPath = '/images/lotlogs/forms/';
            $filename = Str::random(10) . "_" . $file->getClientOriginalName();

            $success = Input::file('ip_wash_certificate')->move(public_path() . $destinationPath, $filename);

            if ($success) {
                $data->ip_wash_certificate = $filename;
            }
        }
        //end Wash Certificate File upload

        //Organic Certificate
        if (Input::hasFile('ip_organic_certificate')) {
            $file = Input::file('ip_organic_certificate');
            

            $destinationPath = '/images/lotlogs/forms/';
            $filename = Str::random(10) . "_" . $file->getClientOriginalName();

            $success = Input::file('ip_organic_certificate')->move(public_path() . $destinationPath, $filename);

            if ($success) {
                $data->ip_organic_certificate = $filename;
            }
        }
        //end Organic Certificate File upload


	    /*
	     * COA
	     *
	     */
	    //COA active?
	    if(is_null(Input::get('coa_active'))) {
		    $data->coa_active = 'no';
	    } else {
		    $data->coa_active = Input::get('coa_active');
		    $data->coa()->updateOrCreate([
			    'master_template' => 'yes',
		    ]);
	    }
	    //
	    $data->coa_template = Input::get('coa_template');







		    //$_GET
        $data->active = Input::get('active');
        $data->lot = Input::get('lot');
        $data->so = Input::get('so');
        $data->original_lot = Input::get('original_lot');
        $data->description = Input::get('description');
        $data->supplier = Input::get('supplier');
        $data->origin = Input::get('origin');
        $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->customer = Input::get('customer');
        $data->date = Input::get('date');
        $data->notes1 = Input::get('notes1');
        $data->notes2 = Input::get('notes2');
        $data->notes3 = Input::get('notes3');
        $data->created_at = Input::get('created_at');
        $data->updated_at = Input::get('updated_at');
        $data->sample_location = Input::get('sample_location');
        $data->po = Input::get('po');
        $data->packslip = Input::get('packslip');
        $data->container = Input::get('container');
        $data->container_number = Input::get('container_number');
        $data->expiration_date = Input::get('expiration_date');
        $data->carrier = Input::get('carrier');
        $data->vendor = Input::get('vendor');
        $data->product_on_hold = Input::get('product_on_hold');
        $data->product_contract = Input::get('product_contract');
        $data->kosher_cert_file = Input::get('kosher_cert_file');
        $data->year = Input::get('year');
        $data->spotchecked = Input::get('spotchecked');
        $data->lotlogtype = Input::get('lotlogtype');
        //incoming product qc form
            $data->ip_seals = Input::get('ip_seals');
            $data->ip_truck_tanker = Input::get('ip_truck_tanker');
            // $data->ip_kosher_shed_a = Input::get('ip_kosher_shed_a');
            // $data->ip_certificate_of_analysis = Input::get('ip_certificate_of_analysis');
            // $data->ip_weight_certificate = Input::get('ip_weight_certificate');
            // $data->ip_wash_certificate = Input::get('ip_wash_certificate');
            // $data->ip_organic_certificate = Input::get('ip_organic_certificate');
            $data->ip_caps_and_seals = Input::get('ip_caps_and_seals');
            $data->ip_cartons = Input::get('ip_cartons');
            $data->ip_drum_condition = Input::get('ip_drum_condition');
            $data->ip_labels = Input::get('ip_labels');
            $data->ip_product_type = Input::get('ip_product_type');
            $data->ip_vendor_lot_number = Input::get('ip_vendor_lot_number');
            $data->ip_notes = Input::get('ip_notes');
            $data->ip_approved_for_use = Input::get('ip_approved_for_use');
            $data->ip_if_no_explain_why = Input::get('ip_if_no_explain_why');
            $data->ip_date_received = Input::get('ip_date_received');

        //incoming materials qc form
            $data->im_trucker_inspection = Input::get('im_trucker_inspection');
            $data->im_date_received = Input::get('im_date_received');
            $data->im_labeling = Input::get('im_labeling');
            // $data->im_bol = Input::get('im_bol');
            $data->im_condition = Input::get('im_condition');
            $data->im_cartons = Input::get('im_cartons');
            $data->im_primary_secondary = Input::get('im_primary_secondary');
            $data->im_approved_for_use = Input::get('im_approved_for_use');
            $data->im_notes = Input::get('im_notes');
            $data->im_date_approved = Input::get('im_date_approved');
            $data->im_seal = Input::get('im_seal');
            $data->im_not_approved_why = Input::get('im_not_approved_why');
            $data->ip_kosher_shed_a = Input::get('ip_kosher_shed_a');
            $data->progress_notes = Input::get('progress_notes');

            $data->approved_by = Input::get('approved_by');
            $data->product_category = Input::get('product_category');
            $data->progress = Input::get('progress');


        $data->save();

        Session::flash('message', 'Your Lotlog Was Updated Successfully');
        Session::flash('alert-class', 'alert-success');
  
            return Redirect::route('lotlogs.index');
    }

    /**
     * Remove the specified lotlog from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        \Lotlog::destroy($id);
        Session::flash('message', 'Your Lot Log Was Deleted');
        Session::flash('alert-class', 'alert-danger');
        return Redirect::route('lotlogs.index');
    }

    public function in_progress()
    {

        // $lotlogs = Lotlog::all();
        $lotlogs = \DB::table('lotlogs')
            ->where('progress', 'in_progress')
            ->orderBy('lot', 'desc')
            ->get();

        return View::make('lotlogs.in_progress', compact('lotlogs'));

        // return "hello";
    }

    public function label($id)
    {
        $label = \Lotlog::findOrFail($id);

        return View::make('lotlogs.label', compact('label'));

    }

    public function label_store()
    {
        
        $label = Input::all();
        // return $data;
         return View::make('lotlogs.label_print', compact('label'));
        
    }

    public function label_print()
    {
        $label = Input::all();
        // return $label;
         return View::make('lotlogs.label_print', compact('label'));
    }

    public function all()
    {
         $lotlogs = \DB::table('lotlogs')
            ->orderBy('lot', 'desc')
            ->get();

        return View::make('lotlogs.all', compact('lotlogs'));
    }
}
