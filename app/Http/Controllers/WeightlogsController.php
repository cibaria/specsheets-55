<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class WeightlogsController extends BaseController {

	/**
	 * Display a listing of weightlogs
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$weightlogs = \DB::table('weightlogs')
						->where('progress', 'in_progress')
						->get();
//		$weightlogs = \Weightlog::all();

		return View::make('weightlogs.index', compact('weightlogs'));
	}

	/**
	 * Show the form for creating a new Weightlog
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('weightlogs.create');
	}

	/**
	 * Store a newly created Weightlog in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), \Weightlog::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		\Weightlog::create($data);

		return Redirect::route('weightlogs.index');
	}

	/**
	 * Display the specified Weightlog.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$weightlog = \Weightlog::findOrFail($id);

		return View::make('weightlogs.show', compact('weightlog'));
	}

	/**
	 * Show the form for editing the specified Weightlog.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$weightlog = \Weightlog::find($id);

		return View::make('weightlogs.edit', compact('weightlog'));
	}

	/**
	 * Update the specified Weightlog in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = \Weightlog::findOrFail($id);

		$data->progress = Input::get('progress');
		$data->date = Input::get('date');
		$data->customer_name = Input::get('customer_name');
		$data->order_number = Input::get('order_number');

		//if weight and height set progress to complete and add the weight_added_date
		if(!is_null(Input::get('weight')) && !is_null(Input::get('height')) ) {
//			$data->progress = 'complete';
			$data->weight_added_date = Date("m/d/Y");
			$data->weight = Input::get('weight');
			$data->height = Input::get('height');


			/**** EMAIL *****/
			//email customer service when a new weight logs height and weight is added

//			 $email_to = "bsiegel@cibaria-intl.com";
			$email_to = "freight@cibaria-intl.com";

			$email_subject = "Weight Log Height & Weight Added";


			$email_message = "Your New Weight Log Height and Weight Was added or updated.\n\n";

			$email_message .= "Order Number: " . $data->order_number . "\n\n";
			$email_message .= "Customer Name: " . $data->customer_name . "\n\n";
			$email_message .= "Weight: " . $data->weight . "\n\n";
			$email_message .= "Height: " . $data->height . "\n\n";
			$email_message .= "Click Here To View " .url('/') . "/weightlogs/" . $data->id;
//			$email_message .= 'test';
// create email headers

			$headers = 'From: customerservice@cibaria-intl.com'."\r\n".

			           'Reply-To: customerservice@cibaria-intl.com'."\r\n" .

			           'X-Mailer: PHP/' . phpversion();

			@mail($email_to, $email_subject, $email_message, $headers);
/**** END EMAIL *****/
		}
		if(Input::has('weight_added_date')) {
			$data->weight_updated_date = Date("m/d/Y");
		}
		$data->notes = Input::get('notes');


//		$weightlog->update($data);
		$data->save();
		Session::flash('message', 'Your Weight Log Was Updated Successfully');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('weightlogs.index');
	}

	/**
	 * Remove the specified Weightlog from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		\Weightlog::destroy($id);

		return Redirect::route('weightlogs.index');
	}

	public function complete()
	{
		//
		$weightlogs = \DB::table('weightlogs')
		                 ->where('progress', 'complete')
		                 ->get();
//		$weightlogs = \Weightlog::all();

		return View::make('weightlogs.complete', compact('weightlogs'));
	}


}
