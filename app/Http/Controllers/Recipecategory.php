<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;
use Illuminate\Http\Request;

class RecipecategoryController extends BaseController {


public function destroy($id)
{

\Recipecategory::destroy($id);

	// \Recipe::destroy($id);
	Session::flash('message', 'Your Recipe Was Deleted');
	Session::flash('alert-class', 'alert-danger');

	// return Redirect::route('recipes.index');

return Redirect::back()->withInput();

}


public function update(Request $request, $id)
{
	$recipe = \Recipe::findOrFail($id);

//main image
	if($request->hasFile('hero_image'))
	{
		$file = $request->file('hero_image');
		$filename = rand() . $file->getClientOriginalName();
		$destinationPath = '/images/recipes/';
		$file->move(public_path() . $destinationPath, $filename);
		$recipe->hero_image = $filename;

	}

//front image
		if($request->hasFile('front_image'))
	{
		$file = $request->file('front_image');
		$filename = rand() . $file->getClientOriginalName();
		$destinationPath = '/images/recipes/';
		$file->move(public_path() . $destinationPath, $filename);
		$recipe->front_image = $filename;

	}

//back image
		if($request->hasFile('back_image'))
	{
		$file = $request->file('back_image');
		$filename = rand() . $file->getClientOriginalName();
		$destinationPath = '/images/recipes/';
		$file->move(public_path() . $destinationPath, $filename);
		$recipe->back_image = $filename;

	}

//get the rest
if(!empty(Input::get('title'))) {
$recipe->title = Input::get('title');
}

if(!empty(Input::get('activated'))) {
$recipe->activated = Input::get('activated');
}

if(!empty(Input::get('category'))){
$recipe->categories()->create([
'category' => Input::get('category')
]);

}

if(!empty(Input::get('product_category'))) {
$recipe->product_categories()->create([
'product_category' => Input::get('product_category')
]);
}


// dd($recipe->categories());


		$recipe->save();
        Session::flash('message', 'Your Recipe Was Updated Successfully');
        Session::flash('alert-class', 'alert-success');
		// return Redirect::route('recipes.index');

		return Redirect::back()->withInput();
}

}