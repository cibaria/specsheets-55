<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class MateriallotlogsController extends BaseController {

	/**
	 * Display a listing of materiallotlogs
	 *
	 * @return Response
	 */
	public function index()
	{
		$materiallotlogs = \DB::table('materiallotlogs')
			->orderby('id', 'desc')
			->get();

		return View::make('materiallotlogs.index', compact('materiallotlogs'));
	}

	/**
	 * Show the form for creating a new materiallotlog
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('materiallotlogs.create');
	}

	/**
	 * Store a newly created materiallotlog in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), \Materiallotlog::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		\Materiallotlog::create($data);

		return Redirect::route('materiallotlogs.index');
	}

	/**
	 * Display the specified materiallotlog.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$materiallotlog = \Materiallotlog::findOrFail($id);

		return View::make('materiallotlogs.show', compact('materiallotlog'));
	}

	/**
	 * Show the form for editing the specified materiallotlog.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$materiallotlog = \Materiallotlog::find($id);

		return View::make('materiallotlogs.edit', compact('materiallotlog'));
	}

	/**
	 * Update the specified materiallotlog in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$materiallotlog = \Materiallotlog::findOrFail($id);

		$validator = Validator::make($data = Input::all(), \Materiallotlog::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$materiallotlog->update($data);

		return Redirect::route('materiallotlogs.index');
	}

	/**
	 * Remove the specified materiallotlog from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		\Materiallotlog::destroy($id);

		return Redirect::route('materiallotlogs.index');
	}

	    public function label($id)
    {
        $label = \Materiallotlog::findOrFail($id);

        return View::make('materiallotlogs.label', compact('label'));

    }

//labels
    public function label_store()
    {
        
        $label = Input::all();
        // return $data;
         return View::make('materiallotlogs.label_print', compact('label'));
        
    }

    public function label_print()
    {
        $label = Input::all();
        // return $label;
         return View::make('materiallotlogs.label_print', compact('label'));
    }

}
