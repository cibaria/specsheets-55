<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;
use App\User;
use Hash;

class UserController extends BaseController {

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Index.
     *
     * @return Response
     */

	public function index()
	{
		$users = User::all();

        return View::make('user.index', compact('users'));
	}

    /**
     * create.
     *
     * @return Response
     */


	public function create()
	{
        return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = new User;
			$user->username = Input::get('username');
			$user->email = Input::get('email');
			$user->password = Hash::make(Input::get('password'));
			$user->group = Input::get('group');
        $user->save();

        return Redirect::to('user');
	}


	public function edit($id)
	{
		$user = User::find($id);

        return View::make('user.edit', compact('user'));
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::find($id);

			$user->username = Input::get('username');
			$user->email = Input::get('email');
			$user->password = Hash::make(Input::get('password'));
			$user->group = Input::get('group');
        $user->save();

        return Redirect::to('user');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::destroy($id);

        return Redirect::to('user');
	}


}
