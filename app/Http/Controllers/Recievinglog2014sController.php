<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class Recievinglog2014sController extends BaseController {

	/**
	 * Display a listing of recievinglog2014s
	 *
	 * @return Response
	 */
	public function index() {
		$recievinglog2014s = \DB::table('receivinglogs2014s')
			->orderBy('date', 'desc')
			->get();

		return View::make('recievinglog2014s.index', compact('recievinglog2014s'));
	}

	/**
	 * Show the form for creating a new recievinglog2014
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('recievinglog2014s.create');
	}

	/**
	 * Store a newly created recievinglog2014 in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$validator = Validator::make($data = Input::all(), \Recievinglog2014::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		\Recievinglog2014::create($data);
		Session::flash('message', 'Your Receiving Log Was Created');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('recievinglog2014s.index');
	}

	/**
	 * Display the specified recievinglog2014.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$recievinglog2014 = \Recievinglog2014::findOrFail($id);

		return View::make('recievinglog2014s.show', compact('recievinglog2014'));
	}

	/**
	 * Show the form for editing the specified recievinglog2014.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$recievinglog2014 = \Recievinglog2014::find($id);

		return View::make('recievinglog2014s.edit', compact('recievinglog2014'));
	}

	/**
	 * Update the specified recievinglog2014 in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$recievinglog2014 = \Recievinglog2014::findOrFail($id);

		$validator = Validator::make($data = Input::all(), \Recievinglog2014::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$recievinglog2014->update($data);
		Session::flash('message', 'Your Receiving Log Was Updated Successfully');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('recievinglog2014s.index');
	}

	/**
	 * Remove the specified recievinglog2014 from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		\Recievinglog2014::destroy($id);
		Session::flash('message', 'Your Receiving Log Was Deleted');
		Session::flash('alert-class', 'alert-danger');
		return Redirect::route('recievinglog2014s.index');
	}

	/**
	 * Print all of Recievinglog2015 from storage
	 *
	 * @return Response
	 */

	public function print_page() {
		$receivinglogs = \Recievinglog2014::all();

		return View::make('recievinglog2014s.print', compact('receivinglogs'));
	}

}
