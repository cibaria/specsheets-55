<?php

namespace App\Http\Controllers;
// namespace App\Http\Controllers\Input;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use View;


class HomeController extends Controller {

public function index()
{
    //
}

    public function getIndex()
    {
        return View::make('home.index');
    }

    public function postIndex()
    {
        $username = Input::get('username');
        $password = Input::get('password');

        if(Auth::attempt(['username' => $username, 'password' => $password]))
        {
            return Redirect::intended('/admin');
        }

        return Redirect::back()
            ->withInput()
            ->withErrors('The username/password is incorrect');
    }

    public function getLogin()
    {
        return Redirect::to('/');
    }

    public function getLogout()
    {
        Auth::logout();

        return Redirect::to('/');
    }

}
