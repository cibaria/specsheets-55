<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;

class PublicnutritionfactsController extends BaseController {

    public function show($id){
        // $id = '23';
        $nutritionfact = \Nutritionfact::findOrFail($id);

//        return var_dump($id);
//        return dd($nutritionfact);
        return View::make('public.nutritionfacts.show', compact('nutritionfact'));
    }

    public function download(){
        $nutritionfact = \Nutritionfact::all();

        return View::make('public.nutritionfacts.download');
//        return "hello";
    }



    public function update(){
        return View::make('public.nutritionfacts.download');
    }
}