<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class RailcarsreceivingController extends BaseController {

	public function index()
	{

		$railcars = \DB::table('receivingappointmentlogs')
					->where('category', 'railcars')
					->get();

		return View::make('railcars.index', compact('railcars'));
	}

	public function create()
	{
		return View::make('railcars.create');
	}

	public function edit($id)
	{
		$railcar = \Receivingappointmentlog::findOrFail($id);
		return View::make('railcars.edit', compact('railcar'));
	}

	public function show($id)
	{
		$railcar = \Receivingappointmentlog::findOrFail($id);
		// dd($railcar);
		return View::make('railcars.show', compact('railcar'));

	}

	public function store()
	{
		$validator = Validator::make($data = Input::all(), \Receivingappointmentlog::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		\Receivingappointmentlog::create($data);

		return Redirect::route('railcars.index');
	}

		public function update($id) {
		$railcar = \Receivingappointmentlog::findOrFail($id);

		$validator = Validator::make($data = Input::all(), \Receivingappointmentlog::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$railcar->update($data);
		Session::flash('message', 'Your Railcar Appointment Log Was Updated Successfully');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('railcars.index');
	}

	public function destroy($id)
	{
		\Receivingappointmentlog::destroy($id);
				Session::flash('message', 'Your Railcar Appointment Log Was Deleted');
		Session::flash('alert-class', 'alert-danger');
		return Redirect::route('railcars.index');
	}

	//railcar load
	public function loads($id)
	{
		if(Input::get('id') != NULL)
		{
		$railcar = \Receivingappointmentlog::find(Input::get('id'));

            $railcar->railcarload()->insert([
                    'load_number' => Input::get('load_number'),
                    'load_date' => Input::get('load_date'),
                    'load_lbs' => Input::get('load_lbs'),
                    'receivingappointmentlog_id' => $railcar->id
                ]);	
		} else {
			$railcar = \Receivingappointmentlog::findOrFail($id);
			$railcarloads = '';
		}
		

		return View::make('railcars.loads', compact(['railcarloads', 'railcar']));
	}

	//railcars received
	public function received()
	{
		$railcars = \DB::table('receivingappointmentlogs')
					->where('category', 'railcars')
					->get();

		return View::make('railcars.received', compact('railcars'));
	}

	//pdf

	public function pdf($id)
	{
		$railcar = \Receivingappointmentlog::findOrFail($id);

		$pdf = \PDF::loadView('railcars.pdf', compact('railcar'));
		return $pdf->download('railcar-' . Date("d/m/y") . '.pdf');

		// return View::make('railcars.pdf', compact('railcar'));
	}

}