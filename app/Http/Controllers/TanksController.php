<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class TanksController extends BaseController
{



    public function update($id)
    {


        $update = Input::get('update');

        if($update == "true") {

        $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/tanks
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->tanks_cost_per = Input::get('tanks_cost_per');

                 $data->id = Input::get('id');

        $count = Input::get('count');

        $username = Input::get('username');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->update([
                'count' => $count,
                'lotlog_id' => $id,
                'month' => Carbon::now()->month,
                'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        
        $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->gal_inch = Input::get('gal_inch');
        $data->tank_total = Input::get('tank_total');
        $data->extension = Input::get('extension');
        $data->lot1 = Input::get('lot1');
        $data->lot2 = Input::get('lot2');
        $data->expiration_date = Input::get('expiration_date');

        $data->update();
    } else {
               $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/tanks
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->tanks_cost_per = Input::get('tanks_cost_per');

                 $data->id = Input::get('id');

        $count = Input::get('count');

        $username = Input::get('username');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->insert([
                'count' => $count,
                'lotlog_id' => $id,
                'month' => Carbon::now()->month,
                'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        
        $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->gal_inch = Input::get('gal_inch');
        $data->tank_total = Input::get('tank_total');
        $data->extension = Input::get('extension');
        $data->lot1 = Input::get('lot1');
        $data->lot2 = Input::get('lot2');
        $data->expiration_date = Input::get('expiration_date');

        $data->save(); 
    }

        if(Input::get('redirect_to_product') === "1" || Input::get('discontinued') === "1")
        {

        return Redirect::to('inventories/product/tanks');

    } else
    {
        return Redirect::back();
        
    }


    }

}
