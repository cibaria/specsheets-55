<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class NutritionfactsController extends BaseController {

	/**
	 * Display a listing of nutritionfacts
	 *
	 * @return Response
	 */
	public function index()
	{
		$nutritionfacts = \Nutritionfact::all();

		return View::make('nutritionfacts.index', compact('nutritionfacts'));
	}

	/**
	 * Show the form for creating a new nutritionfact
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('nutritionfacts.create');
	}

	/**
	 * Store a newly created nutritionfact in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), \Nutritionfact::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		\Nutritionfact::create($data);
        Session::flash('message', 'Your Nutrition Fact Was Created');
        Session::flash('alert-class', 'alert-success');
		return Redirect::route('nutritionfacts.index');
	}

	/**
	 * Display the specified nutritionfact.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$nutritionfact = \Nutritionfact::findOrFail($id);

		return View::make('nutritionfacts.show', compact('nutritionfact'));
	}

	/**
	 * Show the form for editing the specified nutritionfact.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$nutritionfact = \Nutritionfact::find($id);

		return View::make('nutritionfacts.edit', compact('nutritionfact'));
	}

	/**
	 * Update the specified nutritionfact in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$nutritionfact = \Nutritionfact::findOrFail($id);

		$validator = Validator::make($data = Input::all(), \Nutritionfact::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$nutritionfact->update($data);
        Session::flash('message', 'Your Nutrition Fact Was Updated Successfully');
        Session::flash('alert-class', 'alert-success');
		return Redirect::route('nutritionfacts.index');
	}

	/**
	 * Remove the specified nutritionfact from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		\Nutritionfact::destroy($id);
        Session::flash('message', 'Your Nutrition Fact Was Deleted');
        Session::flash('alert-class', 'alert-danger');
		return Redirect::route('nutritionfacts.index');
	}

	public function download()
	{
		return "hi";
	}

/*
Nutrition Labels API
*/

/* TODO restrict to only images */
public function api()
{
	$nutritionfacts = \Nutritionfact::all();

	return response()
			->json($nutritionfacts);
}

//oils
	public function api_oils()
	{
		$nutritionfacts = \DB::table('nutritionfacts')
						->where('category', '=', 'olive_oils')
		 				->orderBy('title', 'asc')
		 				->get();

		return response()
				->json($nutritionfacts);
	}

//vinegars
	public function api_vinegars()
	{
		$nutritionfacts = \DB::table('nutritionfacts')
						->where('category', '=', 'vinegars')
		 				->orderBy('title', 'asc')
		 				->get();

		return response()
				->json($nutritionfacts);
	}

//white 25 star
	public function api_white_25_star()
	{
		$nutritionfacts = \DB::table('nutritionfacts')
						->where('category', '=', '25_star_white')
		 				->orderBy('title', 'asc')
		 				->get();

		return response()
				->json($nutritionfacts);
	}

//dark 25 star
	public function api_dark_25_star()
	{
		$nutritionfacts = \DB::table('nutritionfacts')
						->where('category', '=', '25_star_dark')
		 				->orderBy('title', 'asc')
		 				->get();

		return response()
				->json($nutritionfacts);
	}

//specialty items
	public function api_specialty_items()
	{
		$nutritionfacts = \DB::table('nutritionfacts')
						->where('category', '=', 'blank')
		 				->orderBy('title', 'asc')
		 				->get();

		return response()
				->json($nutritionfacts);
	}

//pantry items
	public function api_pantry_items()
	{
		$nutritionfacts = \DB::table('nutritionfacts')
						->where('category', '=', 'pantry_items')
		 				->orderBy('title', 'asc')
		 				->get();

		return response()
				->json($nutritionfacts);
	}
}
