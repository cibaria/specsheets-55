<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class TotesController extends BaseController
{



    public function update($id)
    {

        $update = Input::get('update');

        if($update == "true") {

        $data = Lotlog::findOrFail($id);
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->totes_cost_per = Input::get('totes_cost_per');

        //get the tote id
        // $data->id = Input::get('id');
        $count = Input::get('count');
        $username = Input::get('username');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $historyid = Input::get('historyid');
        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->where('id', $historyid)->update([
                'count' => $count,
                'lotlog_id' => $id,
                // 'month' => Carbon::now()->month,
                // 'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        $data->expiration_date = Input::get('expiration_date');
        $data->sub_product_category = Input::get('sub_product_category');

        $data->update();
        
    } else {
        $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/totes
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->totes_cost_per = Input::get('totes_cost_per');

        //get the tote id
        $data->id = Input::get('id');

        $count = Input::get('count');

        $username = Input::get('username');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->insert([
                'count' => $count,
                'lotlog_id' => $id,
                'month' => Carbon::now()->month,
                'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }



        // $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->expiration_date = Input::get('expiration_date');
        $data->sub_product_category = Input::get('sub_product_category');

        $data->save(); 
    }

        if(Input::get('redirect_to_product') === "1" || Input::get('discontinued') === "1")
        {
        return Redirect::to('inventories/product/totes');
    } else
    {
        return Redirect::back();

    }
}

//sub categories

public function canola_oil()
{
    $data = Lotlog::where('sub_product_category', 'canola_rbd')
                    ->orWhere('sub_product_category', 'canola_non_gmo')
                    ->orderBy('oil_type', 'asc')
                    ->get();
    $title = 'Canola Oil';
    //Inventory Category
    $category = 'totes';

    return View::make('inventories/product/sub_category/totes/canola_oil', compact('data', 'title', 'category') );
}

public function canola_rbd()
{
        $data = Lotlog::where('sub_product_category', 'canola_rbd')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Canola, RBD';            
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/canola_rbd', compact('data', 'title', 'category'));
}

public function canola_non_gmo()
{
        $data = Lotlog::where('sub_product_category', 'canola_non_gmo')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Canola, NON-GMO';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/canola_non_gmo', compact('data', 'title', 'category'));
}

public function soybean_oil()
{
        $data = Lotlog::where('sub_product_category', 'soy_rbd')
                    ->orWhere('sub_product_category', 'soy_non_gmo')
                    ->orWhere('sub_product_category', 'soy_winterized')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Soybean Oil';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/soybean_oil', compact('data', 'title', 'category'));
}

public function soy_rbd()
{
        $data = Lotlog::where('sub_product_category', 'soy_rbd')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Soy, RBD';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/soy_rbd', compact('data', 'title', 'category'));
}

public function soy_non_gmo()
{
        $data = Lotlog::where('sub_product_category', 'soy_non_gmo')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Soy, NON-GMO';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/soy_non_gmo', compact('data', 'title', 'category'));
}

public function soy_winterized()
{
        $data = Lotlog::where('sub_product_category', 'soy_winterized')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Soy, Winterized';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/soy_winterized', compact('data', 'title', 'category'));
}

public function blended_oils()
{
        $data = Lotlog::where('sub_product_category', 'per_90_can_10_evoo')
                    ->orWhere('sub_product_category', 'per_75_can_25_evoo')
                    ->orWhere('sub_product_category', 'per_90_soy_10_evoo')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Blended Oils';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/blended_oils', compact('data', 'title', 'category'));
}

public function per_90_can_10_evoo()
{
        $data = Lotlog::where('sub_product_category', 'per_90_can_10_evoo')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = '90% CAN/10% EVOO';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/90_can_10_evoo', compact('data', 'title', 'category'));
}

public function per_75_can_25_evoo()
{
        $data = Lotlog::where('sub_product_category', 'per_75_can_25_evoo')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = '75% CAN/25% EVOO';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/90_can_10_evoo', compact('data', 'title', 'category'));
}

public function per_90_soy_10_evoo()
{
        $data = Lotlog::where('sub_product_category', 'per_90_soy_10_evoo')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = '90% SOY/10% EVOO';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/90_soy_10_evoo', compact('data', 'title', 'category'));
}

public function olive_oil()
{
        $data = Lotlog::where('sub_product_category', 'evoo_italian_coratina')
                    ->orWhere('sub_product_category', 'evoo_italian_umbrian')
                    ->orWhere('sub_product_category', 'evoo_chilean_frantoio')
                    ->orWhere('sub_product_category', 'evoo_spanish_arbequina')
                    ->orWhere('sub_product_category', 'evoo_spanish_hojiblanca')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Olive Oil';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/olive_oil', compact('data', 'title', 'category'));
}

public function extra_virgin_olive_oil()
{
        $data = Lotlog::where('sub_product_category', 'evoo_italian_coratina')
                    ->orWhere('sub_product_category', 'evoo_chilean_frantoio')
                    ->orWhere('sub_product_category', 'evoo_italian_umbrian')
                    ->orWhere('sub_product_category', 'evoo_spanish_arbequina')
                    ->orWhere('sub_product_category', 'evoo_spanish_hojiblanca')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Extra Virgin Olive Oil';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/extra_virgin_olive_oil', compact('data', 'title', 'category'));
}


public function extra_virgin_olive_oil_italian()
{
        $data = Lotlog::where('sub_product_category', 'evoo_italian_coratina')
                    ->orWhere('sub_product_category', 'evoo_italian_umbrian')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Italian';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/italian', compact('data', 'title', 'category'));
}

public function evoo_italian_coratina()
{
        $data = Lotlog::where('sub_product_category', 'evoo_italian_coratina')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'EVOO, Italian, Coratina';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/evoo_italian_coratina', compact('data', 'title', 'category'));
}

public function evoo_italian_umbrian()
{
        $data = Lotlog::where('sub_product_category', 'evoo_italian_umbrian')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'EVOO, Italian, Umbrian';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/evoo_italian_umbrian', compact('data', 'title', 'category'));
}

public function spanish()
{
        $data = Lotlog::where('sub_product_category', 'evoo_spanish_arbequina')
                    ->orWhere('sub_product_category', 'evoo_spanish_hojiblanca')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Spanish';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/spanish', compact('data', 'title', 'category'));
}

public function evoo_spanish_arbequina()
{
        $data = Lotlog::where('sub_product_category', 'evoo_spanish_arbequina')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'EVOO, Spanish, Arbequina';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/evoo_spanish_arbequina', compact('data', 'title', 'category'));
}

public function evoo_spanish_hojiblanca()
{
        $data = Lotlog::where('sub_product_category', 'evoo_spanish_hojiblanca')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'EVOO, Spanish, Hojiblanca';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/evoo_spanish_hojiblanca', compact('data', 'title', 'category'));
}

public function chilean()
{
        $data = Lotlog::where('sub_product_category', 'evoo_chilean_frantoio')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Chilean';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/chilean', compact('data', 'title', 'category'));
}

public function evoo_chilean_frantoio()
{
        $data = Lotlog::where('sub_product_category', 'evoo_chilean_frantoio')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'EVOO, Chilean, Frantoio';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/evoo_chilean_frantoio', compact('data', 'title', 'category'));
}

public function pomace_olive_oil()
{
        $data = Lotlog::where('sub_product_category', 'pomace_olive_oil')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Pomace Olive Oil';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/pomace_olive_oil', compact('data', 'title', 'category'));
}

public function pure_olive_oil()
{
        $data = Lotlog::where('sub_product_category', 'pure_olive_oil')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Pure Olive Oil';
        //Inventory Category
        $category = 'totes';


    return View::make('inventories/product/sub_category/totes/pure_olive_oil', compact('data', 'title', 'category'));
}

//end class
}