<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class RailcarloadController extends Controller
{
    
    public function destroy($id)
    {
        \Railcarload::destroy($id);

        return Redirect::back();
    }
}
