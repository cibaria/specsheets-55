<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class RailcarsController extends BaseController
{



    public function update($id)
    {

        $update = Input::get('update');

        if($update == "true") {


        $data = Lotlog::findOrFail($id);

        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->railcars_per_lb_price = Input::get('railcars_per_lb_price');

         // $data->id = Input::get('id');

        $count = Input::get('count');
        $historyid = Input::get('historyid');
        $username = Input::get('username');
                $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->loads_taken = Input::get('loads_taken');
        $data->expiration_date = Input::get('expiration_date');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->where('id', $historyid)->update([
                'count' => $count,
                'lotlog_id' => $id,
                // 'month' => Carbon::now()->month,
                // 'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        


        $data->update();
    } else {
                $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/tanks
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->railcars_per_lb_price = Input::get('railcars_per_lb_price');

         $data->id = Input::get('id');

        $count = Input::get('count');

        $username = Input::get('username');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->insert([
                'count' => $count,
                'lotlog_id' => $id,
                'month' => Carbon::now()->month,
                'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->loads_taken = Input::get('loads_taken');
        $data->expiration_date = Input::get('expiration_date');

        $data->save();
    }

        if(Input::get('redirect_to_product') === "1" || Input::get('discontinued') === "1")
        {

        return Redirect::to('inventories/product/railcars');

    } else
    {
        return Redirect::back();
        
    }


    }

}
