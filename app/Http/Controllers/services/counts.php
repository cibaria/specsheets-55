<?php

class Counts
{

    //counts

    //drums
    public function count_drums()
    {

        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all drum counts from this month and year
        $drums = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'drums' and histories.month = '$current_month' and histories.year = '$current_year'"));

        //get the count of all the drums from this month and year
        $value = 0;
        foreach ($drums as $drum) {
            $drum_count = $drum->count;
            $value += $drum_count;
        }
        return $value;
    }

    //totes
    public function count_totes()
    {
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the tote counts from this month and this year
        $totes = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'totes' and histories.month = '$current_month' and '$current_year'"));

        //get the count of all totes from this month and year
        $value = 0;
        foreach ($totes as $tote) {
            $tote_count = $tote->count;
            $value += $tote_count;
        }
        return $value;
    }

    //organic
    public function count_organics()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $organics = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'organic' and histories.month = '$current_month' and '$current_year'"));

        //get the count of all from this month and year



        $value = 0;
        foreach ($organics as $organic) {
            $organic_count = $organic->count;
            $value += $organic_count;
        }
        return $value;
    }

    //tanks
    public function count_tanks()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $tanks = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'tanks' and histories.month = '$current_month' and '$current_year'"));

        //get the count of all from this month and year

        $value = 0;
        foreach ($tanks as $tank) {
            $tank_count = $tank->count;
            $value += $tank_count;
        }
        return $value;
    }

    //vinegars
    public function count_vinegars()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $vinegars = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'vinegars' and histories.month = '$current_month' and '$current_year'"));

        //get the count of all from this month and year

        $value = 0;
        foreach ($vinegars as $vinegar) {
            $vinegar_count = $vinegar->count;
            $value += $vinegar_count;
        }
        return $value;
    }

    //website vinegars
    public function count_website_vinegars()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $website_vinegars = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'website_vinegars' and histories.month = '$current_month' and '$current_year'"));

        //get the count of all from this month and year

        $value = 0;
        foreach ($website_vinegars as $website_vinegar) {
            $website_vinegars_count = $website_vinegar->count;
            $value += $website_vinegars_count;
        }
        return $value;
    }

    //railcars
    public function count_railcars()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $railcars = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'railcars' and histories.month = '$current_month' and '$current_year'"));

        //get the count of all from this month and year

        $value = 0;
        foreach ($railcars as $railcar) {
            $railcars_count = $railcar->count;
            $value += $railcars_count;
        }
        return $value;
    }

    //specialties
    public function count_specialties()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $specialties = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'specialty' and histories.month = '$current_month' and '$current_year'"));



        $value = 0;
        foreach ($specialties as $specialty) {
            $specialties_count = $specialty->count;
            $value += $specialties_count;
        }
        return $value;
    }

    //infused_oil_drums
    public function count_infused_oil_drums()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $infused_oil_drums = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'infused_oil_drums' and histories.month = '$current_month' and '$current_year'"));

        $value = 0;
        foreach ($infused_oil_drums as $infused_oil_drum) {
            $infused_oil_drums_count = $infused_oil_drum->count;
            $value += $infused_oil_drums_count;
        }
        return $value;
    }

    //spreads_and_olives
    public function count_spreads_and_olives()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $spreads_and_olives = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'spreads_and_olives' and histories.month = '$current_month' and '$current_year'"));



        $value = 0;
        foreach ($spreads_and_olives as $spreads_and_olive) {
            $spreads_and_olives_count = $spreads_and_olive->count;
            $value += $spreads_and_olives_count;
        }
        return $value;
    }

    //web flavored and infused
    public function count_web_flavor_and_infused()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $web_flavored_and_infuseds = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'web_flavor_and_infused' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($web_flavored_and_infuseds as $web_flavored_and_infused) {
            $web_flavored_and_infused_count = $web_flavored_and_infused->count;
            $value += $web_flavored_and_infused_count;
        }
        return $value;
    }

    //website_olive_oils
    public function count_website_olive_oils()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $website_olive_oils = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'website_olive_oils' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($website_olive_oils as $website_olive_oil) {
            $website_olive_oil_count = $website_olive_oil->count;
            $value += $website_olive_oil_count;
        }
        return $value;
    }

    //website_other
    public function count_website_other()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $website_others = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'website_other' and histories.month = '$current_month' and '$current_year'"));

        $value = 0;
        foreach ($website_others as $website_other) {
            $website_other_count = $website_other->count;
            $value += $website_other_count;
        }
        return $value;
    }

    //finished_product
    public function count_finished_product()
    {
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $finished_products = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'finished_product' and histories.month = '$current_month' and '$current_year'"));

        $value = 0;
        foreach ($finished_products as $finished_product) {
            $finished_product_count = $finished_product->count;
            $value += $finished_product_count;
        }
        return $value;
    }

    //sprays
    public function count_sprays()
    {
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $sprays = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'sprays' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($sprays as $spray) {
            $sprays_count = $spray->count;
            $value += $sprays_count;
        }
        return $value;
    }

    //ready_to_ship
    public function count_ready_to_ship()
    {
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $ready_to_ships = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'ready_to_ship' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($ready_to_ships as $ready_to_ship) {
            $ready_to_ships_count = $ready_to_ship->count;
            $value += $ready_to_ships_count;
        }
        return $value;
    }

    //rework
    public function count_rework()
    {
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $reworks = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'rework' and histories.month = '$current_month' and '$current_year'"));

        $value = 0;
        foreach ($reworks as $rework) {
            $rework_count = $rework->count;
            $value += $rework_count;
        }
        return $value;
    }

    //labels_in_house
    public function count_labels_in_house()
    {

        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $labels_in_houses = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'labels_in_house' and histories.month = '$current_month' and '$current_year'"));

        $value = 0;
        foreach ($labels_in_houses as $labels_in_house) {
            $labels_in_house_count = $labels_in_house->count;
            $value += $labels_in_house_count;
        }
        return $value;
    }

    //labels_in_labelroom
    public function count_labels_in_labelroom()
    {

        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $labels_in_labelrooms = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'labels_in_labelroom' and histories.month = '$current_month' and '$current_year'"));

        $value = 0;
        foreach ($labels_in_labelrooms as $labels_in_labelroom) {
            $labels_in_labelroom_count = $labels_in_labelroom->count;
            $value += $labels_in_labelroom_count;
        }
        return $value;
    }

    //customers_labels
    public function count_customers_labels()
    {
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $customers_labels = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'customers_labels' and histories.month = '$current_month' and '$current_year'"));

        $value = 0;
        foreach ($customers_labels as $customers_label) {
            $customers_labels_count = $customers_label->count;
            $value += $customers_labels_count;
        }
        return $value;
    }

    //bottles
    public function count_bottles()
    {
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $bottles = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'bottles' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($bottles as $bottle) {
            $bottles_count = $bottle->count;
            $value += $bottles_count;
        }
        return $value;
    }

    //etched_bottles
    public function count_etched_bottles()
    {
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $etched_bottles = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'etched_bottles' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($etched_bottles as $etched_bottle) {
            $etched_bottles_count = $etched_bottle->count;
            $value += $etched_bottles_count;
        }
        return $value;
    }

    //fustis
    public function count_fustis()
    {
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $fustis = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'fustis' and histories.month = '$current_month' and '$current_year'"));



        $value = 0;
        foreach ($fustis as $fusti) {
            $fustis_count = $fusti->count;
            $value += $fustis_count;
        }
        return $value;
    }

    //caps
    public function count_caps()
    {
        // $caps = Lotlog::where('product_category', 'caps')->get();

        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $caps = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'caps' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($caps as $cap) {
            $caps_count = $cap->count;
            $value += $caps_count;
        }
        return $value;
    }

    //cardboard_blank
    public function count_cardboard_blank()
    {
        // $cardboard_blanks = Lotlog::where('product_category', 'cardboard_blank')->get();
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $cardboard_blanks = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'cardboard_blank' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($cardboard_blanks as $cardboard_blank) {
            $cardboard_blank_count = $cardboard_blank->count;
            $value += $cardboard_blank_count;
        }
        return $value;
    }

    //cardboard_blank
    public function count_cardboard_printed()
    {
        // $cardboard_printed = Lotlog::where('product_category', 'cardboard_printed')->get();
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $cardboard_printed = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'cardboard_printed' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($cardboard_printed as $cardboard_prints) {
            $cardboard_printed_count = $cardboard_prints->count;
            $value += $cardboard_printed_count;
        }
        return $value;
    }


    //supplies
    public function count_supplies()
    {
        // $supplies = Lotlog::where('product_category', 'supplies')->get();

        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $supplies = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'supplies' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($supplies as $supply) {
            $supplies_count = $supply->count;
            $value += $supplies_count;
        }
        return $value;
    }

    //materials
    public function count_materials()
    {
        // $materials = Lotlog::where('product_category', 'materials')->get();
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $materials = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'materials' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($materials as $material) {
            $materials_count = $material->count;
            $value += $materials_count;
        }
        return $value;
    }

    //follmer
    public function count_follmer()
    {
        // $follmers = Lotlog::where('product_category', 'follmer')->get();

        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $follmers = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'follmer' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($follmers as $follmer) {
            $follmer_count = $follmer->count;
            $value += $follmer_count;
        }
        return $value;
    }

    //flavors
    public function count_flavors()
    {
        // $flavors = Lotlog::where('product_category', 'flavors')->get();

        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $flavors = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'flavors' and histories.month = '$current_month' and '$current_year'"));


        $value = 0;
        foreach ($flavors as $flavor) {
            $flavors_count = $flavor->count;
            $value += $flavors_count;
        }
        return $value;
    }

    //flavors_for_samples
    public function count_flavors_for_samples()
    {
        // $flavors_for_samples = Lotlog::where('product_category', 'flavors_for_samples')->get();
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $flavors_for_samples = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'flavors_for_samples' and histories.month = '$current_month' and '$current_year'"));

        $value = 0;
        foreach ($flavors_for_samples as $flavors_for_sample) {
            $flavors_for_samples_count = $flavors_for_sample->count;
            $value += $flavors_for_samples_count;
        }
        return $value;
    }

    //customers oils
    public function count_customers_oils()
    {
        // $flavors_for_samples = Lotlog::where('product_category', 'flavors_for_samples')->get();
        
        $current_month = Carbon::now()->month;
        $current_year = Carbon::now()->year;

        //get all of the counts from this month and this year
        $customers_oils = DB::select(DB::raw("select lotlogs.id, histories.count, lotlogs.product_category, histories.month, histories.year from lotlogs inner join histories on lotlogs.id = histories.lotlog_id where lotlogs.product_category = 'customers_oils' and histories.month = '$current_month' and '$current_year'"));

        $value = 0;
        foreach ($customers_oils as $item) {
            $customers_oils_count = $item->count;
            $value += $customers_oils_count;
        }
        return $value;
    }

    //website_totals count
    public function count_website_totals()
    {
        return 
            $this->count_website_other() +
            $this->count_web_flavor_and_infused() +
            $this->count_website_vinegars() +
            $this->count_website_olive_oils();
    }

    //labels
    public function count_labels()
    {
        return 
            $this->count_labels_in_house() +
            $this->count_labels_in_labelroom() +
            $this->count_customers_labels();
    }

//end class
}
