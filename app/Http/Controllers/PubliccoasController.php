<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class PubliccoasController extends BaseController {

	
	/**
	 * Display the specified resource.
	 * GET /publiccoascontroller/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
//		$coa = \Coa::findOrFail($id);

		$coa = \Lotlog::with('coa')->findOrFail($id);

		return View::make('public.coas.show', compact('coa'));
	}

	

}