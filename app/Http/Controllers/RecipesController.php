<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;
use Illuminate\Http\Request;

class RecipesController extends BaseController {


public function index()
{
	$recipes = \Recipe::all();

	return \View::make('recipes.index', compact('recipes'));
}

public function create()
{
	return View::make('recipes.create');
}

public function show($id)
{
	$recipe = \Recipe::findOrFail($id);

	return \View::make('recipes.show', compact('recipe'));
}

public function store() {

$data = new \Recipe();

//hero image file upload
//this is the main image of the recipe
if (Input::hasFile('hero_image')) {
	$file = Input::file('hero_image');

	$destinationPath = '/images/recipes/';
	$filename = rand() . $file->getClientOriginalName();

	$success = Input::file('hero_image')->move(public_path() . $destinationPath, $filename);
if($success) {
$data->hero_image = $filename;
}

}


//front image file upload
if (Input::hasFile('front_image')) {
	$file = Input::file('front_image');

	$destinationPath = '/images/recipes/';
	$filename = rand() . $file->getClientOriginalName();

	$success = Input::file('front_image')->move(public_path() . $destinationPath, $filename);
if($success) {
$data->front_image = $filename;
}

}

//back image file upload
if (Input::hasFile('back_image')) {
	$file = Input::file('back_image');

	$destinationPath = '/images/recipes/';
	$filename = rand() . $file->getClientOriginalName();

	$success = Input::file('back_image')->move(public_path() . $destinationPath, $filename);
	if ($success) {
		$data->back_image = $filename;
	}
}


//get the rest
if(!empty(Input::get('title'))) {
$data->title = Input::get('title');
}

if(!empty(Input::get('activated'))) {
$data->activated = Input::get('activated');
}

//
$data->save();
Session::flash('message', 'Your Recipe Was Created');
Session::flash('alert-class', 'alert-success');
return Redirect::route('recipes.index');



}

public function edit($id)
{
	$recipe = \Recipe::find($id);

	return View::make('recipes.edit', compact('recipe'));
}

public function update(Request $request, $id)
{
	$recipe = \Recipe::findOrFail($id);

//main image
	if($request->hasFile('hero_image'))
	{
		$file = $request->file('hero_image');
		$filename = rand() . $file->getClientOriginalName();
		$destinationPath = '/images/recipes/';
		$file->move(public_path() . $destinationPath, $filename);
		$recipe->hero_image = $filename;

	}

//front image
		if($request->hasFile('front_image'))
	{
		$file = $request->file('front_image');
		$filename = rand() . $file->getClientOriginalName();
		$destinationPath = '/images/recipes/';
		$file->move(public_path() . $destinationPath, $filename);
		$recipe->front_image = $filename;

	}

//back image
		if($request->hasFile('back_image'))
	{
		$file = $request->file('back_image');
		$filename = rand() . $file->getClientOriginalName();
		$destinationPath = '/images/recipes/';
		$file->move(public_path() . $destinationPath, $filename);
		$recipe->back_image = $filename;

	}

//get the rest
if(!empty(Input::get('title'))) {
$recipe->title = Input::get('title');
}

if(!empty(Input::get('activated'))) {
$recipe->activated = Input::get('activated');
}

if(!empty(Input::get('category'))) {
$recipe->categories()->create([
'category' => Input::get('category')
]);	
}

if(!empty(Input::get('product_category'))) {
$recipe->product_categories()->create([
'product_category' => Input::get('product_category')
]);
}

// dd($recipe->categories());


		$recipe->save();
        Session::flash('message', 'Your Recipe Was Updated Successfully');
        Session::flash('alert-class', 'alert-success');
		return Redirect::route('recipes.index');

		// return Redirect::back()->withInput();
}


public function destroy($id)
{
	\Recipe::destroy($id);
	Session::flash('message', 'Your Recipe Was Deleted');
	Session::flash('alert-class', 'alert-danger');

	return Redirect::route('recipes.index');
}


//api
public function api()
{
	//grab category and product category relationships
	$recipes = \Recipe::with('categories', 'product_categories')->get();

	return response()
			->json($recipes);
}

//Recipe Download

public function download($id)
{

	$download = \Recipe::findOrFail($id);

	return View::make('public.recipes.download', compact('download'));
}



}