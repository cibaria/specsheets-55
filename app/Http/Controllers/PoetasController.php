<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class PoetasController extends BaseController {

	/**
	 * Display a listing of poetas
	 *
	 * @return Response
	 */
	public function index()
	{
		$poetas = \Poeta::where('received', 'no')->get();

		return View::make('poetas.index', compact('poetas'));
	}

		public function received()
	{
		$poetas = \Poeta::where('received', 'yes')->get();

		return View::make('poetas.received', compact('poetas'));
	}

	/**
	 * Show the form for creating a new poeta
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('poetas.create');
	}

	/**
	 * Store a newly created poeta in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

//		$data = Input::all();
//
//		dd($data);

		$validator = Validator::make($data = Input::all(), \Poeta::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		\Poeta::create($data);

		return Redirect::route('poetas.index');
	}

	/**
	 * Display the specified poeta.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$poeta = \Poeta::findOrFail($id);

		return View::make('poetas.show', compact('poeta'));
	}

	/**
	 * Show the form for editing the specified poeta.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$poeta = \Poeta::find($id);

		return View::make('poetas.edit', compact('poeta'));
	}

	/**
	 * Update the specified poeta in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$poeta = \Poeta::findOrFail($id);

		$validator = Validator::make($data = Input::all(), \Poeta::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$poeta->update($data);

		return Redirect::route('poetas.index');
	}

	/**
	 * Remove the specified poeta from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		\Poeta::destroy($id);

		return Redirect::route('poetas.index');
	}

}
