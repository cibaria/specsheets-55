<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class ProductholdsController extends BaseController {

	/**
	 * Display a listing of productholds
	 *
	 * @return Response
	 */
	public function index()
	{
		$productholds = \Producthold::all();

		return View::make('productholds.index', compact('productholds'));
	}

	/**
	 * Show the form for creating a new producthold
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('productholds.create');
	}

	/**
	 * Store a newly created producthold in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), \Producthold::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		\Producthold::create($data);

		return Redirect::route('productholds.index');
	}

	/**
	 * Display the specified producthold.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$producthold = \Producthold::findOrFail($id);

		return View::make('productholds.show', compact('producthold'));
	}

	/**
	 * Show the form for editing the specified producthold.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$producthold = \Producthold::find($id);

		return View::make('productholds.edit', compact('producthold'));
	}

	/**
	 * Update the specified producthold in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$producthold = \Producthold::findOrFail($id);

		$validator = Validator::make($data = Input::all(), \Producthold::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$producthold->update($data);

		return Redirect::route('productholds.index');
	}

	/**
	 * Remove the specified producthold from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		\Producthold::destroy($id);

		return Redirect::route('productholds.index');
	}

	public function released()
	{
		$release = \Producthold::where('released', 1)->get();

		return View::make('productholds.released', compact('release'));
	}

}
