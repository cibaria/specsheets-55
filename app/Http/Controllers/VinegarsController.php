<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class VinegarsController extends BaseController
{



    public function update($id)
    {

        $update = Input::get('update');

        if($update == "true") {


        $data = Lotlog::findOrFail($id);

        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->vinegars_cost_per = Input::get('vinegars_cost_per');
        // $data->id = Input::get('id');

        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $count = Input::get('count');
        $username = Input::get('username');
        $historyid = Input::get('historyid');
        

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->where('id', $historyid)->update([
                'count' => $count,
                'lotlog_id' => $id,
                // 'month' => Carbon::now()->month,
                // 'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        
        $data->expiration_date = Input::get('expiration_date');
        $data->sub_product_category = Input::get('sub_product_category');

        $data->update();
    } else {
                $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/tanks
        $data->discontinued = Input::get('discontinued');
        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->vinegars_cost_per = Input::get('vinegars_cost_per');

                 $data->id = Input::get('id');

        $count = Input::get('count');

        $username = Input::get('username');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->insert([
                'count' => $count,
                'lotlog_id' => $id,
                'month' => Carbon::now()->month,
                'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->expiration_date = Input::get('expiration_date');
        $data->sub_product_category = Input::get('sub_product_category');

        $data->save();
    }

        if(Input::get('redirect_to_product') === "1" || Input::get('discontinued') === "1")
        {

        return Redirect::to('inventories/product/vinegars');

    } else
    {
        return Redirect::back();
        
    }


    }

    public function honey_vinegar_serrano()
    {
        $data = Lotlog::where('sub_product_category', 'honey_vinegar_serrano')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Honey Vinegar, Serrano';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/honey_vinegar_serrano', compact('data', 'title', 'category'));
    }

    public function lambrusco()
    {
        $data = Lotlog::where('sub_product_category', 'lambrusco')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Lambrusco';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/lambrusco', compact('data', 'title', 'category'));
    }

    public function wine_vinegar()
    {
        $data = Lotlog::where('sub_product_category', 'wine_vinegar')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Wine Vinegar';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/wine_vinegar', compact('data', 'title', 'category'));
    }

    public function wine_vinegar_white()
    {
        $data = Lotlog::where('sub_product_category', 'wine_vinegar_white')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Wine Vinegar White';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/wine_vinegar_white', compact('data', 'title', 'category'));
    }

    public function wine_vinegar_red()
    {
        $data = Lotlog::where('sub_product_category', 'wine_vinegar_red')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Wine Vinegar Red';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/wine_vinegar_red', compact('data', 'title', 'category'));
        
    }

    public function rice_vinegar()
    {
        $data = Lotlog::where('sub_product_category', 'wine_vinegar_red')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Rice Vinegar';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/wine_vinegar_red', compact('data', 'title', 'category'));
        
    }

    public function _4_star_dark()
    {
        $data = Lotlog::where('sub_product_category', '4_star_dark')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = '4 Star Dark';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/4_star_dark', compact('data', 'title', 'category'));
        
    }

    public function _4_star_white()
    {
        $data = Lotlog::where('sub_product_category', '4_star_white')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = '4 Star White';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/4_star_white', compact('data', 'title', 'category'));
        
    }

    public function _25_star()
    {
        $data = Lotlog::where('sub_product_category', '25_star')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = '25 Star';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/25_star', compact('data', 'title', 'category'));
        
    }

    public function _25_star_dark()
    {
        $data = Lotlog::where('sub_product_category', '25_star_dark')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = '25 Star Dark';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/25_star_dark', compact('data', 'title', 'category'));
        
    }

       public function _8_star_dark()
    {
        $data = Lotlog::where('sub_product_category', '8_star_dark')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = '8 Star Dark';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/8_star_dark', compact('data', 'title', 'category'));
        
    }

    public function _25_star_white()
    {
        $data = Lotlog::where('sub_product_category', '25_star_white')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = '25 Star White';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/25_star_white', compact('data', 'title', 'category'));
        
    }


    public function balsamic_vinegar()
    {
        $data = Lotlog::where('sub_product_category', '4_star_dark')
                    ->orWhere('sub_product_category', '4_star_white')
                    ->orWhere('sub_product_category', '25_star_dark')
                    ->orWhere('sub_product_category', '25_star_white')
                    ->orderBy('oil_type', 'asc')
                    ->get();
        $title = 'Balsamic Vinegar';
        //Inventory Category
        $category = 'vinegars';


    return View::make('inventories/product/sub_category/vinegars/balsamic_vinegar', compact('data', 'title', 'category'));
        
    }


//end class
}
