<?php

class SaleschartsController extends \BaseController {

	/**
	 * Display a listing of salescharts
	 *
	 * @return Response
	 */
	public function index()
	{
		$salescharts = Saleschart::all();

		return View::make('salescharts.index', compact('salescharts'));
	}

	/**
	 * Show the form for creating a new saleschart
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('salescharts.create');
	}

	/**
	 * Store a newly created saleschart in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Saleschart::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Saleschart::create($data);

		return Redirect::route('salescharts.index');
	}

	/**
	 * Display the specified saleschart.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$saleschart = Saleschart::findOrFail($id);

		return View::make('salescharts.show', compact('saleschart'));
	}

	/**
	 * Show the form for editing the specified saleschart.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$saleschart = Saleschart::find($id);

		return View::make('salescharts.edit', compact('saleschart'));
	}

	/**
	 * Update the specified saleschart in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$saleschart = Saleschart::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Saleschart::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$saleschart->update($data);

		return Redirect::route('salescharts.index');
	}

	/**
	 * Remove the specified saleschart from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Saleschart::destroy($id);

		return Redirect::route('salescharts.index');
	}

}
