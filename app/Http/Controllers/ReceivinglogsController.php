<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class ReceivinglogsController extends BaseController {

	/**
	 * Display a listing of receivinglogs
	 *
	 * @return Response
	 */
	public function index() {
		$receivinglogs = \DB::table('receivinglogs')
			->orderBy('date', 'desc')
			->get();

		return View::make('receivinglogs.index', compact('receivinglogs'));
	}

	/**
	 * Show the form for creating a new receivinglog
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('receivinglogs.create');
	}

	/**
	 * Store a newly created receivinglog in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$validator = Validator::make($data = Input::all(), \Receivinglog::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		\Receivinglog::create($data);
		Session::flash('message', 'Your Receiving Log Was Created');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('receivinglogs.index');
	}

	/**
	 * Display the specified receivinglog.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$receivinglog = \Receivinglog::findOrFail($id);

		return View::make('receivinglogs.show', compact('receivinglog'));
	}

	/**
	 * Show the form for editing the specified receivinglog.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$receivinglog = \Receivinglog::find($id);

		return View::make('receivinglogs.edit', compact('receivinglog'));
	}

	/**
	 * Update the specified receivinglog in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$receivinglog = \Receivinglog::findOrFail($id);

		$validator = Validator::make($data = Input::all(), \Receivinglog::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$receivinglog->update($data);
		Session::flash('message', 'Your Receiving Log Was Updated Successfully');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('receivinglogs.index');
	}

	/**
	 * Remove the specified receivinglog from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		\Receivinglog::destroy($id);
		Session::flash('message', 'Your Receiving Log Was Deleted');
		Session::flash('alert-class', 'alert-danger');
		return Redirect::route('receivinglogs.index');
	}

	/**
	 * Print all of Recievinglog2015 from storage
	 *
	 * @return Response
	 */

	public function print_page() {
		$receivinglogs = \Receivinglog::all();

		return View::make('receivinglogs.print', compact('receivinglogs'));
	}

}
