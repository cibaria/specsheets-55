<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class Receivinglog2012sController extends BaseController {

	/**
	 * Display a listing of receivinglog2012s
	 *
	 * @return Response
	 */
	public function index() {
		$receivinglog2012s = \DB::table('receivinglogs2012s')
			->orderBy('date', 'desc')
			->get();

		return View::make('receivinglog2012s.index', compact('receivinglog2012s'));
	}

	/**
	 * Show the form for creating a new receivinglog2012
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('receivinglog2012s.create');
	}

	/**
	 * Store a newly created receivinglog2012 in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$validator = Validator::make($data = Input::all(), \Receivinglog2012::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		\Receivinglog2012::create($data);
		Session::flash('message', 'Your Receiving Log Was Created');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('receivinglog2012s.index');
	}

	/**
	 * Display the specified receivinglog2012.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$receivinglog2012 = \Receivinglog2012::findOrFail($id);

		return View::make('receivinglog2012s.show', compact('receivinglog2012'));
	}

	/**
	 * Show the form for editing the specified receivinglog2012.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$receivinglog2012 = \Receivinglog2012::find($id);

		return View::make('receivinglog2012s.edit', compact('receivinglog2012'));
	}

	/**
	 * Update the specified receivinglog2012 in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$receivinglog2012 = \Receivinglog2012::findOrFail($id);

		$validator = Validator::make($data = Input::all(), \Receivinglog2012::$rules);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$receivinglog2012->update($data);
		Session::flash('message', 'Your Receiving Log Was Updated Successfully');
		Session::flash('alert-class', 'alert-success');
		return Redirect::route('receivinglog2012s.index');
	}

	/**
	 * Remove the specified receivinglog2012 from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		\Receivinglog2012::destroy($id);
		Session::flash('message', 'Your Receiving Log Was Deleted');
		Session::flash('alert-class', 'alert-danger');
		return Redirect::route('receivinglog2012s.index');
	}

	public function print_page() {
		$receivinglogs = \Receivinglog2012::all();

		return View::make('receivinglog2012s.print', compact('receivinglogs'));
	}

}
