<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

//Charts
// http://lavacharts.com/
use Khill\Lavacharts\Lavacharts;

class HistoriesController extends BaseController {

	/**
	 * Display history of each product_category from lotlogs and histories.
	 * GET /histories
	 *
	 * @return Chart/totals/counts
	 */
	public function index()
	{

//History
include('services/inventoryhistory.php');
$month = new InventoryHistory;
//Total
include('services/totals.php');
$total = new Totals;
//Counts
include('services/counts.php');
$count = new Counts;


//This month last month chart on histories.index
$lava = new Lavacharts;

$history = $lava->DataTable();

$history->addDateColumn('Month')
	->addNumberColumn('Count')
	->setDateTimeFormat('M')
	->addRow([$month->thisMonth(), $month->thisMonthCount()])
	->addRow([$month->lastMonth(), $month->lastMonthCount()])
	;

$lava->AreaChart('History', $history, [
    'title' => 'Inventory History',
    'titleTextStyle' => [
        'color'    => '#eb6b2c',
        'fontSize' => 10
    ],
    'legend' => [
    	'position' => 'in'
    ]
]);

		return View::make('inventories.history.index', compact('lava', 'month', 'total', 'count'));
	}


	public function drums()
	{

		//Drums History
		include('services/drums.php');
		$drums = new Drums;

		//History
		include('services/inventoryhistory.php');
		$month = new InventoryHistory;

		//Charts
		$lava = new Lavacharts;

		$history = $lava->DataTable();

		$history->addDateColumn('Month')
			->addNumberColumn('Count')
			->setDateTimeFormat('M')
			->addRow([$month->thisMonth(), $month->thisMonthCount()])
			->addRow([$month->lastMonth(), $month->lastMonthCount()])
			->addRow([$month->threeMonthsAgo(), $drums->threeMonthsAgoDrums()])
			->addRow([$month->fourMonthsAgo(), $drums->fourMonthsAgoDrums()])
			->addRow([$month->fiveMonthsAgo(), $drums->fiveMonthsAgoDrums()])
			->addRow([$month->sixMonthsAgo(), $drums->sixMonthsAgoDrums()])
			->addRow([$month->sevenMonthsAgo(), $drums->sevenMonthsAgoDrums()])
			->addRow([$month->eightMonthsAgo(), $drums->eightMonthsAgoDrums()])
			->addRow([$month->nineMonthsAgo(), $drums->nineMonthsAgoDrums()])
			->addRow([$month->tenMonthsAgo(), $drums->tenMonthsAgoDrums()])
			->addRow([$month->elevenMonthsAgo(), $drums->elevenMonthsAgoDrums()])
			->addRow([$month->twelveMonthsAgo(), $drums->twelveMonthsAgoDrums()])
			;

		$lava->AreaChart('History', $history, [
		    'title' => 'Drums History',
		    'titleTextStyle' => [
		        'color'    => '#eb6b2c',
		        'fontSize' => 10
		    ],
		    'legend' => [
		    	'position' => 'in'
		    ]
		]);


		return View::make('inventories.history.drums', compact('drums', 'lava', 'month'));
	}

	public function totes()
	{
		return View::make('inventories.history.totes');
	}

	public function organics()
	{
		return View::make('inventories.history.organics');
	}

	public function tanks()
	{
		return View::make('inventories.history.tanks');
	}

	public function vinegars()
	{
		return View::make('inventories.history.vinegars');
	}

	public function railcars()
	{
		return View::make('inventories.history.railcars');
	}

	public function specialty()
	{
		return View::make('inventories.history.specialty');
	}

	public function infused_oil_drums()
	{
		return View::make('inventories.history.infused_oil_drums');
	}

	public function spreads_and_olives()
	{
		return View::make('inventories.history.spreads_and_olives');
	}

	public function website_olive_oils()
	{
		return View::make('inventories.history.website_olive_oils');
	}

	public function web_flavor_and_infused()
	{
		return View::make('inventories.history.web_flavor_and_infused');
	}

	public function website_vinegars()
	{
		return View::make('inventories.history.website_vinegars');
	}

	public function website_other()
	{
		return View::make('inventories.history.website_other');
	}

	public function website_totals()
	{
		return View::make('inventories.history.website_totals');
	}

	public function finished_product()
	{
		return View::make('inventories.history.finished_product');
	}

	public function sprays()
	{
		return View::make('inventories.history.sprays');
	}

	public function ready_to_ship()
	{
		return View::make('inventories.history.ready_to_ship');
	}

	public function rework()
	{
		return View::make('inventories.history.rework');
	}

	public function labels_in_house()
	{
		return View::make('inventories.history.labels_in_house');
	}

	public function labels_in_labelroom()
	{
		return View::make('inventories.history.labels_in_labelroom');
	}

	public function customers_labels()
	{
		return View::make('inventories.history.customers_labels');
	}

	public function bottles()
	{
		return View::make('inventories.history.bottles');
	}

	public function etched_bottles()
	{
		return View::make('inventories.history.etched_bottles');
	}

	public function fustis()
	{
		return View::make('inventories.history.fustis');
	}

	public function caps()
	{
		return View::make('inventories.history.caps');
	}

	public function cardboard_blank()
	{
		return View::make('inventories.history.cardboard_blank');
	}

	public function cardboard_printed()
	{
		return View::make('inventories.history.cardboard_printed');
	}

	public function supplies()
	{
		return View::make('inventories.history.supplies');
	}

	public function materials()
	{
		return View::make('inventories.history.materials');
	}

	public function follmer()
	{
		return View::make('inventories.history.follmer');
	}

	public function flavors()
	{
		return View::make('inventories.history.flavors');
	}

	public function flavors_for_samples()
	{
		return View::make('inventories.history.flavors_for_samples');
	}

	public function customers_oils()
	{
		return View::make('inventories.history.customers_oils');

	}



//end class
}