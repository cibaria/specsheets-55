<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use View;

class AdminController extends BaseController {

    protected $layout = "layouts.master";

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{


        //counts
        $nutritionfacts = \DB::table('nutritionfacts')->count();
        $specsheets = \DB::table('specsheets')->count();
        $lotlogs = \DB::table('lotlogs')->count();
        $recievingappointmentlogs = \DB::table('receivingappointmentlogs')->whereActive(1)->count();
        $samples = \DB::table('samples')->count();
        $customer_complaints = \DB::table('customercomplaints')->count();
        $product_holds = \DB::table('productholds')->count();

        //last five of the newest
        $nutritions = \DB::table('nutritionfacts')->limit(5)->orderBy('created_at', 'desc')->get();
        $lots = \DB::table('lotlogs')->limit(5)->orderBy('created_at', 'desc')->get();
        $specs = \DB::table('specsheets')->limit(5)->orderBy('created_at', 'desc')->get();
        $appointments = \DB::table('receivingappointmentlogs')->limit(5)->orderBy('id', 'desc')->get();
        $sample = \DB::table('samples')->limit(5)->orderBy('created_at', 'desc')->get();
        $customer_complaint = \DB::table('customercomplaints')->limit(5)->orderBy('created_at', 'desc')->get();
        $product_hold = \DB::table('productholds')->limit(5)->orderBy('created_at', 'desc')->get();

		return View::make('admin.index', compact('specsheets', 'specs', 'lotlogs', 'lots', 'nutritionfacts', 'nutritions', 'recievingappointmentlogs', 'appointments', 'samples', 'sample', 'customer_complaints', 'customer_complaint', 'product_hold', 'product_holds'));
	}



}