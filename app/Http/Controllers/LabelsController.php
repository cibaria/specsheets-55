<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class LabelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $labels = \DB::table('receivingappointmentlogs')
                    ->where('category', 'labels')
                    ->where('active', 1)
                    ->get();

        return View::make('labels.index',compact('labels'));
    }

    public function create()
    {
        return View::make('labels.create');
    }

    public function store()
    {
        $validator = Validator::make($data = Input::all(), \Receivingappointmentlog::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        \Receivingappointmentlog::create($data);
        Session::flash('message', 'Your Label Was Created');
        Session::flash('alert-class', 'alert-success');
        return Redirect::route('labels.index');
    }

    public function edit($id)
    {
        $label = \Receivingappointmentlog::findOrFail($id);

        return View::make('labels.edit', compact('label'));
    }




    public function update($id)
    {
        $label = \Receivingappointmentlog::findOrFail($id);

        $validator = Validator::make($data = Input::all(), \Receivingappointmentlog::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $label->update($data);
        Session::flash('message', 'Your Label Was Updated Successfully');
        Session::flash('alert-class', 'alert-success');
        return Redirect::route('labels.index');
    }

    public function destroy($id)
    {
        \Receivingappointmentlog::destroy($id);
        Session::flash('message', 'Your Label Was Deleted');
        Session::flash('alert-class', 'alert-danger');
        return Redirect::route('labels.index');
    }

    public function received()
    {

        $labels = \DB::table('receivingappointmentlogs')
                    ->where('category', 'labels')
                    ->where('active', 2)
                    ->get();

        return View::make('labels.received', compact('labels'));
    }

}
