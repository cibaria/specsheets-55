<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Input;
use Session;
use Validator;

class CoasController extends BaseController {


	public function index()
	{

		$coas = \Lotlog::with('coa')
			->where('coa_active', 'yes')
			->get();

		

		return View::make('coas.index', compact('coas'));
	}

	public function show($id)
	{
		$coa = \Lotlog::with('coa')->findOrFail($id);

		return View::make('coas.show', compact('coa'));
	}

	public function edit($id)
	{
		$coa = \Lotlog::with('coa')->findOrFail($id);

		return View::make('coas.edit', compact('coa'));
	}

	public function create($id)
	{
		$coa = \Lotlog::with('coa')->findOrFail($id);

		return View::make('coas.create', compact('coa'));
	}

	public function create_coa($id)
	{
		$coas = \Lotlog::with('coa')->findOrFail($id);

		$new_coa = $coas->replicate();

		$new_coa->save();
//
		$new_coa->coa()->updateOrCreate([
			'coa_description' => Input::get('coa_description'),
			'appearance' => Input::get('appearance'),
			'brassicasterol' => Input::get('brassicasterol'),
			'campesterol' => Input::get('campesterol'),
			'stigmasterol' => Input::get('stigmasterol'),
			'color_red' => Input::get('color_red'),
			'color_yellow' => Input::get('color_yellow'),
			'cholesterol' => Input::get('cholesterol'),
			'flavor_and_odor' => Input::get('flavor_and_odor'),
			'free_fatty_acids' => Input::get('free_fatty_acids'),
			'iodine_value' => Input::get('iodine_value'),
			'lead' => Input::get('lead'),
			'moisture' => Input::get('moisture'),
			'peroxide_value' => Input::get('peroxide_value'),
			'refractive_index' => Input::get('refractive_index'),
			'unsaponifiable_matter' => Input::get('unsaponifiable_matter'),
			'C14_0' => Input::get('C14_0'),
			'C16_0' => Input::get('C16_0'),
			'C16_1' => Input::get('C16_1'),
			'C17_0' => Input::get('C17_0'),
			'C17_1' => Input::get('C17_1'),
			'C18_0' => Input::get('C18_0'),
			'C18_1' => Input::get('C18_1'),
			'C18_2' => Input::get('C18_2'),
			'C18_3' => Input::get('C18_3'),
			'C20_0' => Input::get('C20_0'),
			'C20_1' => Input::get('C20_1'),
			'C22_0' => Input::get('C22_0'),
			'C24_0' => Input::get('C24_0'),
			'C24_1' => Input::get('C24_1'),
			'flavor' => Input::get('flavor'),
			'aroma' => Input::get('aroma'),
			'ffa' => Input::get('ffa'),
			'moisture_and_impurities' => Input::get('moisture_and_impurities'),
			'melting_point' => Input::get('melting_point'),
			'additives' => Input::get('additives'),
			'cold_test' => Input::get('cold_test'),
			'master_template' => 'no',
		]);

		return Redirect::route('coas.index');

	}

	public function update($id)
	{
		$coa = \Lotlog::with('coa')->findOrFail($id);


		$data = [
			'coa_description' => Input::get('coa_description'),
			'appearance' => Input::get('appearance'),
			'brassicasterol' => Input::get('brassicasterol'),
			'campesterol' => Input::get('campesterol'),
			'stigmasterol' => Input::get('stigmasterol'),
			'color_red' => Input::get('color_red'),
			'color_yellow' => Input::get('color_yellow'),
			'cholesterol' => Input::get('cholesterol'),
			'flavor_and_odor' => Input::get('flavor_and_odor'),
			'free_fatty_acids' => Input::get('free_fatty_acids'),
			'iodine_value' => Input::get('iodine_value'),
			'lead' => Input::get('lead'),
			'moisture' => Input::get('moisture'),
			'peroxide_value' => Input::get('peroxide_value'),
			'refractive_index' => Input::get('refractive_index'),
			'unsaponifiable_matter' => Input::get('unsaponifiable_matter'),
			'C14_0' => Input::get('C14_0'),
			'C16_0' => Input::get('C16_0'),
			'C16_1' => Input::get('C16_1'),
			'C17_0' => Input::get('C17_0'),
			'C17_1' => Input::get('C17_1'),
			'C18_0' => Input::get('C18_0'),
			'C18_1' => Input::get('C18_1'),
			'C18_2' => Input::get('C18_2'),
			'C18_3' => Input::get('C18_3'),
			'C20_0' => Input::get('C20_0'),
			'C20_1' => Input::get('C20_1'),
			'C22_0' => Input::get('C22_0'),
			'C24_0' => Input::get('C24_0'),
			'C24_1' => Input::get('C24_1'),
			'flavor' => Input::get('flavor'),
			'aroma' => Input::get('aroma'),
			'ffa' => Input::get('ffa'),
			'moisture_and_impurities' => Input::get('moisture_and_impurities'),
			'melting_point' => Input::get('melting_point'),
			'additives' => Input::get('additives'),
			'cold_test' => Input::get('cold_test'),
			'manf_date' => Input::get('manf_date')
		];

		$coa->coa()->update($data);

		return Redirect::route('coas.index');
	}


	public function destroy($id)
	{
		$coa = \Lotlog::with('coa')->findOrFail($id);

		$coa->coa()->delete();

		return Redirect::route('coas.index');
	}

	/*
	 * COA Templates
	 */
	public function template()
	{
		$coas = \Lotlog::with('coa')
			->where('coa_active', 'yes')
			//TODO: needs to be refactored
			->orWhere('coa_template', 'apricot_kernel')
			->orWhere('coa_template', 'almond')
			->orWhere('coa_template', 'avocado')
			->orWhere('coa_template', 'babassu')
			->orWhere('coa_template', 'butters')
			->orWhere('coa_template', 'canola')
			->orWhere('coa_template', 'castor')
			->orWhere('coa_template', 'coconut')
			->orWhere('coa_template', 'corn')
			->orWhere('coa_template', 'cottonseed')
			->orWhere('coa_template', 'flaxseed')
			->orWhere('coa_template', 'hemp')
			->orWhere('coa_template', 'jojoba')
			->orWhere('coa_template', 'macadamia')
			->orWhere('coa_template', 'olive_oil')
			->orWhere('coa_template', 'olive_oil_extra_virgin_flavored')
			->orWhere('coa_template', 'olive_oil_fused')
			->orWhere('coa_template', 'olive_oil_pure_infused')
			->orWhere('coa_template', 'palm_kernel')
			->orWhere('coa_template', 'peanut')
			->orWhere('coa_template', 'rice_bran')
			->orWhere('coa_template', 'safflower')
			->orWhere('coa_template', 'sesame')
			->orWhere('coa_template', 'soybean')
			->orWhere('coa_template', 'sunflower')
			->get();
		return View::make('coas.template', compact('coas'));
	}

	public function template_edit($id)
	{
		$coa = \Lotlog::with('coa')->findOrFail($id);

		return View::make('coas.form.template.edit', compact('coa'));
	}


}
