<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class CapsController extends BaseController
{



    public function update($id)
    {

$update = Input::get('update');

        if($update == "true") {

        $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/drums
        $data->discontinued = Input::get('discontinued');

        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->caps_price = Input::get('caps_price');
        
         // $data->id = Input::get('id');
        $historyid = Input::get('historyid');

        $count = Input::get('count');

        $username = Input::get('username');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->where('id', $historyid)->update([
                'count' => $count,
                'lotlog_id' => $id,
                // 'month' => Carbon::now()->month,
                // 'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        // $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->per_case = Input::get('per_case');
        $data->supplier = Input::get('supplier');
        $data->manufacturer_part_number = Input::get('manufacturer_part_number');

        $data->update();
    } else {
         $data = Lotlog::findOrFail($id);

        //redirect to main product page
        //inventories/product/drums
        $data->discontinued = Input::get('discontinued');

        $data->redirect_to_product = Input::get('redirect_to_product');
        $data->caps_price = Input::get('caps_price');
        
         $data->id = Input::get('id');

        $count = Input::get('count');

        $username = Input::get('username');

        //add history information
        if(!empty($count))
        {
            Lotlog::find($id)->history()->insert([
                'count' => $count,
                'lotlog_id' => $id,
                'month' => Carbon::now()->month,
                'year' => Carbon::now()->year,
                'date' => Carbon::now(),
                'username' => $username, 
                ]);
        }
        // $data->qty = Input::get('qty');
        $data->size = Input::get('size');
        $data->oil_type = Input::get('oil_type');
        $data->lot = Input::get('lot');
        $data->per_case = Input::get('per_case');
        $data->supplier = Input::get('supplier');
        $data->manufacturer_part_number = Input::get('manufacturer_part_number');

        $data->save();
    }

        if (Input::get('redirect_to_product') === "1" || Input::get('discontinued') === "1") {
            return Redirect::to('inventories/product/caps');

        } else {
            return Redirect::back();
        
        }


    }
}
