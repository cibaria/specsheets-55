<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// use App\Passport\Passport;

use Laravel\Passport\PassportServiceProvider;
use Laravel\Passport\Passport;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    //     if ($this->app->environment() == 'local') {
    //     $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
    // }
    }
}
