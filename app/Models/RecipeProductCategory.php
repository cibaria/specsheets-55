<?php

// namespace App;

// use Illuminate\Database\Eloquent\Model;

class Recipeproductcategory extends \Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'recipe_product_categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['product_category'];


    public function recipe(){
        return $this->belongsTo('Recipe');
    }

    
}
