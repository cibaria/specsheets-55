<?php


class Weightlog extends \Eloquent {

	public static $rules = [
	'date' => 'required'
	];

    protected $fillable = ['date', 'order_number', 'weight', 'height', 'notes', 'customer_name', 'progress', 'freight_scheduled', 'weight_added_date', 'weight_updated_date'];
}
