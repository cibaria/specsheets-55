<?php 

class Recipecategory extends \Eloquent {


// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
		
	];

	public function recipe(){
		return $this->belongsTo('Recipe');
	}

	// Don't forget to fill this array
	protected $fillable = ['category', 'id'];

}