<?php

class Spotcheck extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'date' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['date', 'description', 'lot', 'product_label', 'seals', 'exp_date', 'box_label', 'required_weight', 'actual_weight', 'by', 'resolution'];

}