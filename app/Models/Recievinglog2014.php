<?php

class Recievinglog2014 extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

    protected $table = 'receivinglogs2014s';

	// Don't forget to fill this array
	protected $fillable = ['date', 'po', 'vendor', 'packslip', 'qty', 'product', 'lot', 'container', 'carrier', 'product_on_hold','product_description', 'product_contract', 'condition_truck', 'kosher_cert_file', 'notes'];

}