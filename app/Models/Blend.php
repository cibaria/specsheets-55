<?php

class Blend extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'lot' => 'required|integer'
	];

	// Don't forget to fill this array
	protected $fillable = ['lot', 'sales_order', 'oil_used1', 'oil_used2', 'oil_used3', 'blend_type', 'quantity', 'size', 'customer', 'date', 'note1', 'note2'];

}