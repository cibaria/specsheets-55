<?php

class Customercomplaintproduct extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [
		'product_description',
		'product_quantity',
		'credit_amount',
		'customercomplaint_id'
	];

	public function customercomplaints() {
		return $this->belongsTo('Customercomplaint');
	}

}