<?php

class Shipping extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'date' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['order_number', 'status', 'company', 'date', 'time_in', 'time_out', 'seal_number', 'will_call', 'door_number', 'notes'];

}