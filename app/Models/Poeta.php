<?php

class Poeta extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['vendor_name', 'order_date', 'item_description', 'po', 'delivery_request_date', 'quantity_ordered', 'eta', 'notes', 'price', 'amount', 'received'];

}