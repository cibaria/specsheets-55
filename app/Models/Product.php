<?php

class Product extends \Eloquent {
	protected $fillable = [
			'id',
			'customercomplaint_id',
			'description',
			'quantity',
			'credit_amount',

	];

	public function customercomplaint(){
		return $this->belongsTo('Customercomplaint');
	}
}
