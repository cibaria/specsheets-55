<?php

class Customercomplaint extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
		'date_of_complaint' => 'required'
	];

	public function products(){
		return $this->hasMany('Product');
	}

	// Don't forget to fill this array
	protected $fillable = [
		'id',
		'customercomplaint_id',
		'status',
		'case_number',
		'customer_name',
		'customer_contact',
		'customer_email',
		'order_number',
		'ship_date',
		'date_of_complaint',
		'time_of_complaint',
		'return_received_by',
		'complaint_received_by',
		'customer_phone',
		'customer_fax',
		'verbal',
		'details_of_complaint',
		'first_action_taken_notes',
		'first_action_taken_by',
		'first_action_taken_date',
		'first_action_taken_time',
		'corrective_action_notes',
		'corrective_action_by',
		'corrective_action_date',
		'corrective_action_time',
		'credit_return_replacement_authorization',
		'credit_return_replacement_authorization_by',
		'fifteen_restocking_fee',
		'fifteen_restocking_fee_amount',
		'ups_cost_to_re_ship',
		'return_received',
		'complaint_received_via',
		'received_by',
		'created_at',
		'updated_at',
		'freight_damage_ups',
		'freight_damage_other',
		'quality_of_product',
		'wrong_missing_product',
		'labeling_documentation',
		'other',
		'ups_freight_refund',
	];

}
