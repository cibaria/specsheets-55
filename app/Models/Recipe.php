<?php

class Recipe extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'hero_image' => 'required',
		// 'front_image' => 'required',
		// 'back_image' => 'required',
	];

	public function categories()
	{
		return $this->hasMany('Recipecategory');
	}

	public function product_categories()
	{
		return $this->hasMany('Recipeproductcategory');
	}

	// Don't forget to fill this array
    protected $fillable = [
    	'front_image', 'back_image', 'title','activated', 'hero_image'
    ];


}