<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//COA
//for intl
Route::get(
'coas', 'CoasController@api',
['except' => ['create', 'edit'] ]
);

//Specsheets
//for store supply and intl
Route::get(
'specsheets', 'SpecsheetsController@api',
['except' => ['create', 'edit'] ]
);

//Recipes
Route::get(
'recipes' , 'RecipesController@api',
['except' => ['create', 'edit'] ]
);

/*
Nutrition Facts
*/

//oil
Route::get(
'nutritionfacts/oils', 'NutritionfactsController@api_oils',
['except' => ['create', 'edit'] ] 
);

//vinegars
Route::get(
'nutritionfacts/vinegars', 'NutritionfactsController@api_vinegars',
['except' => ['create', 'edit'] ] 
);

//white 25 star
Route::get(
'nutritionfacts/white_25_star', 'NutritionfactsController@api_white_25_star',
['except' => ['create', 'edit'] ] 
);

//dark 25 star
Route::get(
'nutritionfacts/dark_25_star', 'NutritionfactsController@api_dark_25_star',
['except' => ['create', 'edit'] ] 
);

//pantry items
Route::get(
'nutritionfacts/pantry_items', 'NutritionfactsController@api_pantry_items',
['except' => ['create', 'edit'] ] 
);

//specialty items
Route::get(
'nutritionfacts/specialty_items', 'NutritionfactsController@api_specialty_items',
['except' => ['create', 'edit'] ] 
);