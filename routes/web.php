<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 *Public
 * no need to login to view
 */

//prepacked
// Route::group(['middleware' => 'cors'], function(){
//     Route::get('prepacked/', 'PrepackedController@index');
//     Route::post('prepacked/', 'PrepackedController@index');
// });


//main login
// Route::get('/', 'HomeController@index');
Route::get('/', 'HomeController@getIndex');
Route::get('/login', 'HomeController@getLogin');
Route::post('login', [ 'as' => 'login', 'uses' => 'HomeController@getLogin']);
Route::post('/', 'HomeController@postIndex');
Route::get('logout', 'HomeController@getLogout');

Route::get('public/nutritionfacts/{id} ', 'PublicnutritionfactsController@show');
Route::get('public/nutritionfacts/download', 'PublicnutritionfactsController@download');
Route::post('public/nutritionfacts/download', 'PublicnutritionfactsController@download');

Route::get('public/specsheets/{id} ', 'PublicspecsheetsController@show');
//appointment log for warehouse
Route::get('warehouse', 'ReceivingappointmentlogsController@warehouse');
//shipping log for warehouse
Route::get('shippings/warehouse', 'ShippingsController@warehouse');

Route::get('public/coas/{id}', 'PubliccoasController@show');

//public recipe download
Route::post('recipes/download/{id}', 'RecipesController@download');
Route::get('recipes/download/{id}', 'RecipesController@download');

/*
 * Auth
 * backend
 */
Route::group(['middleware' => 'auth'], function () {

    /*
    * Inventory History
    *
    */

    Route::get('history', 'HistoriesController@index');
    Route::get('history/drums', 'HistoriesController@drums');
    Route::get('history/totes', 'HistoriesController@totes');
    Route::get('history/organics', 'HistoriesController@organics');
    Route::get('history/tanks', 'HistoriesController@tanks');
    Route::get('history/vinegars', 'HistoriesController@vinegars');
    Route::get('history/railcars', 'HistoriesController@railcars');
    Route::get('history/specialty', 'HistoriesController@specialty');
    Route::get('history/infused_oil_drums', 'HistoriesController@infused_oil_drums');
    Route::get('history/spreads_and_olives', 'HistoriesController@spreads_and_olives');
    Route::get('history/website_olive_oils', 'HistoriesController@website_olive_oils');
    Route::get('history/web_flavor_and_infused', 'HistoriesController@web_flavor_and_infused');
    Route::get('history/website_vinegars', 'HistoriesController@website_vinegars');
    Route::get('history/website_other', 'HistoriesController@website_other');
    Route::get('history/website_totals', 'HistoriesController@website_totals');
    Route::get('history/finished_product', 'HistoriesController@finished_product');
    Route::get('history/sprays', 'HistoriesController@sprays');
    Route::get('history/ready_to_ship', 'HistoriesController@ready_to_ship');
    Route::get('history/rework', 'HistoriesController@rework');
    Route::get('history/labels_in_house', 'HistoriesController@labels_in_house');
    Route::get('history/labels_in_labelroom', 'HistoriesController@labels_in_labelroom');
    Route::get('history/customers_labels', 'HistoriesController@customers_labels');
    Route::get('history/bottles', 'HistoriesController@bottles');
    Route::get('history/etched_bottles', 'HistoriesController@etched_bottles');
    Route::get('history/fustis', 'HistoriesController@fustis');
    Route::get('history/caps', 'HistoriesController@caps');
    Route::get('history/cardboard_blank', 'HistoriesController@cardboard_blank');
    Route::get('history/cardboard_printed', 'HistoriesController@cardboard_printed');
    Route::get('history/supplies', 'HistoriesController@supplies');
    Route::get('history/materials', 'HistoriesController@materials');
    Route::get('history/follmer', 'HistoriesController@follmer');
    Route::get('history/flavors', 'HistoriesController@flavors');
    Route::get('history/flavors_for_samples', 'HistoriesController@flavors_for_samples');
    Route::get('history/customer_oils', 'HistoriesController@customer_oils');



    /*
    *Inventory
    *
    */

    //edit
    Route::get('inventories/edit/drums/{id}', 'InventoriesController@drum_edit');
    Route::get('inventories/edit/totes/{id}', 'InventoriesController@tote_edit');
    Route::get('inventories/edit/organics/{id}', 'InventoriesController@organic_edit');
    Route::get('inventories/edit/tanks/{id}', 'InventoriesController@tank_edit');
    Route::get('inventories/edit/vinegars/{id}', 'InventoriesController@vinegar_edit');
    Route::get('inventories/edit/railcars/{id}', 'InventoriesController@railcar_edit');
    Route::get('inventories/edit/specialties/{id}', 'InventoriesController@specialty_edit');
    Route::get('inventories/edit/infused_oil_drums/{id}', 'InventoriesController@infused_oil_drums_edit');
    Route::get('inventories/edit/spreads_and_olives/{id}', 'InventoriesController@spreads_and_olives_edit');
    Route::get('inventories/edit/website_olive_oils/{id}', 'InventoriesController@website_olive_oils_edit');
    Route::get('inventories/edit/web_flavor_and_infused/{id}', 'InventoriesController@web_flavor_and_infused_edit');
    Route::get('inventories/edit/website_vinegars/{id}', 'InventoriesController@website_vinegars_edit');
    Route::get('inventories/edit/website_other/{id}', 'InventoriesController@website_other_edit');
    Route::get('inventories/edit/finished_product/{id}', 'InventoriesController@finished_product_edit');
    Route::get('inventories/edit/sprays/{id}', 'InventoriesController@sprays_edit');
    Route::get('inventories/edit/ready_to_ship/{id}', 'InventoriesController@ready_to_ship_edit');
    Route::get('inventories/edit/rework/{id}', 'InventoriesController@rework_edit');
    Route::get('inventories/edit/labels_in_house/{id}', 'InventoriesController@labels_in_house_edit');
    Route::get('inventories/edit/labels_in_labelroom/{id}', 'InventoriesController@labels_in_labelroom_edit');
    Route::get('inventories/edit/customers_labels/{id}', 'InventoriesController@customers_labels_edit');
    Route::get('inventories/edit/bottles/{id}', 'InventoriesController@bottles_edit');
    Route::get('inventories/edit/etched_bottles/{id}', 'InventoriesController@etched_bottles_edit');
    Route::get('inventories/edit/fustis/{id}', 'InventoriesController@fustis_edit');
    Route::get('inventories/edit/caps/{id}', 'InventoriesController@caps_edit');
    Route::get('inventories/edit/cardboard_blank/{id}', 'InventoriesController@cardboard_blank_edit');
    Route::get('inventories/edit/cardboard_printed/{id}', 'InventoriesController@cardboard_printed_edit');
    Route::get('inventories/edit/supplies/{id}', 'InventoriesController@supplies_edit');
    Route::get('inventories/edit/materials/{id}', 'InventoriesController@materials_edit');
    Route::get('inventories/edit/follmer/{id}', 'InventoriesController@follmer_edit');
    Route::get('inventories/edit/flavors/{id}', 'InventoriesController@flavors_edit');
    Route::get('inventories/edit/flavors_for_samples/{id}', 'InventoriesController@flavors_for_samples_edit');
    Route::get('inventories/edit/customers_oils/{id}', 'InventoriesController@customers_oils_edit');

    //update
    Route::resource('drumupdates', 'DrumsController');
    Route::resource('toteupdates', 'TotesController');
    Route::resource('organicupdates', 'OrganicsController');
    Route::resource('tankupdates', 'TanksController');
    Route::resource('vinegarupdates', 'VinegarsController');
    Route::resource('railcarupdates', 'RailcarsController');
    Route::resource('specialtiesupdates', 'SpecialtiesController');
    Route::resource('infused_oil_drumssupdates', 'InfusedoildrumsController');
    Route::resource('spreads_and_olivesupdates', 'SpreadsandolivesController');
    Route::resource('website_olive_oilsupdates', 'WebsiteoliveoilsController');
    Route::resource('web_flavor_and_infusedupdates', 'WebflavorandinfusedController');
    Route::resource('website_vinegarsupdates', 'WebsitevinegarsController');
    Route::resource('website_otherupdates', 'WebsiteotherController');
    Route::resource('finished_productupdates', 'FinishedproductController');
    Route::resource('spraysupdates', 'SprayController');
    Route::resource('ready_to_shipupdates', 'ReadytoshipController');
    Route::resource('reworkupdates', 'ReworkController');
    Route::resource('labels_in_houseupdates', 'LabelsinhouseController');
    Route::resource('labels_in_labelroomupdates', 'LabelsinlabelroomController');
    Route::resource('customers_labelsupdates', 'CustomerslabelsController');
    Route::resource('bottlesupdates', 'BottlesController');
    Route::resource('bottlesupdates/edit', 'BottlesController@edit');
    Route::resource('etched_bottlesupdates', 'EtchedbottlesController');
    Route::resource('fustisupdates', 'FustisController');
    Route::resource('capsupdates', 'CapsController');
    Route::resource('cardboard_blankupdates', 'CardboardblankController');
    Route::resource('cardboard_printedupdates', 'CardboardprintedController');
    Route::resource('suppliesupdates', 'SuppliesController');
    Route::resource('materialsupdates', 'MaterialsController');
    Route::resource('follmerupdates', 'FollmerController');
    Route::resource('flavorsupdates', 'FlavorsController');
    Route::resource('flavors_for_samplesupdates', 'FlavorsforsamplesController');
    Route::resource('customers_oilsupdates', 'CustomersoilController');

    //cost
    Route::get('inventories/cost/drums', 'InventoriesController@drum_cost');
    Route::get('inventories/cost/totes', 'InventoriesController@tote_cost');
    Route::get('inventories/cost/organics', 'InventoriesController@organic_cost');
    Route::get('inventories/cost/tanks', 'InventoriesController@tank_cost');
    Route::get('inventories/cost/vinegars', 'InventoriesController@vinegar_cost');
    Route::get('inventories/cost/railcars', 'InventoriesController@railcar_cost');
    Route::get('inventories/cost/specialties', 'InventoriesController@specialty_cost');
    Route::get('inventories/cost/infused_oil_drums', 'InventoriesController@infused_oil_drums_cost');
    Route::get('inventories/cost/spreads_and_olives', 'InventoriesController@spreads_and_olives_cost');
    Route::get('inventories/cost/website_olive_oils', 'InventoriesController@website_olive_oils_cost');
    Route::get('inventories/cost/web_flavor_and_infused', 'InventoriesController@web_flavor_and_infused_cost');
    Route::get('inventories/cost/website_vinegars', 'InventoriesController@website_vinegars_cost');
    Route::get('inventories/cost/website_other', 'InventoriesController@website_other_cost');
    Route::get('inventories/cost/finished_product', 'InventoriesController@finished_product_cost');
    Route::get('inventories/cost/sprays', 'InventoriesController@sprays_cost');
    Route::get('inventories/cost/ready_to_ship', 'InventoriesController@ready_to_ship_cost');
    Route::get('inventories/cost/rework', 'InventoriesController@rework_cost');
    Route::get('inventories/cost/labels_in_house', 'InventoriesController@labels_in_house_cost');
    Route::get('inventories/cost/labels_in_labelroom', 'InventoriesController@labels_in_labelroom_cost');
    Route::get('inventories/cost/customers_labels', 'InventoriesController@customers_labels_cost');
    Route::get('inventories/cost/bottles', 'InventoriesController@bottles_cost');
    Route::get('inventories/cost/etched_bottles', 'InventoriesController@etched_bottles_cost');
    Route::get('inventories/cost/fustis', 'InventoriesController@fustis_cost');
    Route::get('inventories/cost/caps', 'InventoriesController@caps_cost');
    Route::get('inventories/cost/cardboard_blank', 'InventoriesController@cardboard_blank_cost');
    Route::get('inventories/cost/cardboard_printed', 'InventoriesController@cardboard_printed_cost');
    Route::get('inventories/cost/supplies', 'InventoriesController@supplies_cost');
    Route::get('inventories/cost/materials', 'InventoriesController@materials_cost');
    Route::get('inventories/cost/follmer', 'InventoriesController@follmer_cost');
    Route::get('inventories/cost/flavors', 'InventoriesController@flavors_cost');
    Route::get('inventories/cost/flavors_for_samples', 'InventoriesController@flavors_for_samples_cost');
    Route::get('inventories/cost/customers_oils', 'InventoriesController@customers_oils_cost');

    //print
    Route::get('inventories/print/website_totals', 'InventoriesController@website_totals_print');
    Route::get('inventories/print/inventory_totals', 'InventoriesController@inventory_totals');
    Route::get('inventories/print/totals_for_bank', 'InventoriesController@totals_for_bank');
    Route::get('inventories/print/drums','InventoriesController@drums_print');
    Route::get('inventories/print/totes','InventoriesController@totes_print');
    Route::get('inventories/print/organics','InventoriesController@organics_print');
    Route::get('inventories/print/tanks','InventoriesController@tanks_print');
    Route::get('inventories/print/vinegars','InventoriesController@vinegars_print');
    Route::get('inventories/print/railcars','InventoriesController@railcars_print');
    Route::get('inventories/print/specialty','InventoriesController@specialty_print');
    Route::get('inventories/print/infused_oil_drums','InventoriesController@infused_oil_drums_print');
    Route::get('inventories/print/spreads_and_olives','InventoriesController@spreads_and_olives_print');
    Route::get('inventories/print/website_olive_oils','InventoriesController@website_olive_oils_print');
    Route::get('inventories/print/web_flavor_and_infused','InventoriesController@web_flavor_and_infused_print');
    Route::get('inventories/print/website_vinegars','InventoriesController@website_vinegars_print');
    Route::get('inventories/print/website_other','InventoriesController@website_other_print');
    Route::get('inventories/print/finished_product','InventoriesController@finished_product_print');
    Route::get('inventories/print/sprays','InventoriesController@sprays_print');
    Route::get('inventories/print/ready_to_ship','InventoriesController@ready_to_ship_print');
    Route::get('inventories/print/rework','InventoriesController@rework_print');
    Route::get('inventories/print/labels_in_house','InventoriesController@labels_in_house_print');
    Route::get('inventories/print/labels_in_labelroom','InventoriesController@labels_in_labelroom_print');
    Route::get('inventories/print/customers_labels','InventoriesController@customers_labels_print');
    Route::get('inventories/print/bottles','InventoriesController@bottles_print');
    Route::get('inventories/print/etched_bottles','InventoriesController@etched_bottles_print');
    Route::get('inventories/print/fustis','InventoriesController@fustis_print');
    Route::get('inventories/print/caps','InventoriesController@caps_print');
    Route::get('inventories/print/cardboard_blank','InventoriesController@cardboard_blank_print');
    Route::get('inventories/print/cardboard_printed','InventoriesController@cardboard_printed_print');
    Route::get('inventories/print/supplies','InventoriesController@supplies_print');
    Route::get('inventories/print/materials','InventoriesController@materials_print');
    Route::get('inventories/print/follmer','InventoriesController@follmer_print');
    Route::get('inventories/print/flavors','InventoriesController@flavors_print');
    Route::get('inventories/print/flavors_for_samples','InventoriesController@flavors_for_samples_print');
    Route::get('inventories/print/customers_oils','InventoriesController@customers_oils_print');

    //totals
    Route::get('inventories/totals', 'InventoriesController@totals');
    Route::get('inventories/website_totals/', 'InventoriesController@website_totals');
    Route::get('inventories/product/product_totals', 'InventoriesController@product_totals');
    //counts
    Route::get('inventories/count/drums', 'InventoriesController@drum_count');
    Route::get('inventories/count/totes', 'InventoriesController@tote_count');
    Route::get('inventories/count/organics', 'InventoriesController@organics_count');
    Route::get('inventories/count/tanks', 'InventoriesController@tanks_count');

    Route::get('inventories/count/vinegars', 'InventoriesController@vinegars_count');
    Route::get('inventories/count/railcars', 'InventoriesController@railcars_count');
    Route::get('inventories/count/specialties', 'InventoriesController@specialty_count');
    Route::get('inventories/count/spreads_and_olives', 'InventoriesController@spreads_and_olives_count');
    Route::get('inventories/count/infused_oil_drums', 'InventoriesController@infused_oil_drums_count');
    Route::get('inventories/count/website_olive_oil', 'InventoriesController@website_olive_oil');
    Route::get('inventories/count/website_other', 'InventoriesController@website_other_count');
    Route::get('inventories/count/finished_product', 'InventoriesController@finished_product_count');
    Route::get('inventories/count/sprays', 'InventoriesController@sprays_count');
    Route::get('inventories/count/labels_in_house', 'InventoriesController@labels_in_houses_count');
    Route::get('inventories/count/labels_in_labelroom', 'InventoriesController@labels_in_labelroom_count');
    Route::get('inventories/count/customers_labels', 'InventoriesController@customer_labels_count');
    Route::get('inventories/count/bottles', 'InventoriesController@bottles_count');
    Route::get('inventories/count/etched_bottles', 'InventoriesController@etched_bottles_count');
    Route::get('inventories/count/caps', 'InventoriesController@caps_count');
    Route::get('inventories/count/supplies', 'InventoriesController@supplies_count');
    Route::get('inventories/count/materials', 'InventoriesController@materials_count');
    Route::get('inventories/count/follmer', 'InventoriesController@follmer_count');
    Route::get('inventories/count/flavors', 'InventoriesController@flavors_count');
    Route::get('inventories/count/flavors_for_samples', 'InventoriesController@flavors_for_samples_count');
    Route::get('inventories/count/website_olive_oils', 'InventoriesController@website_olive_oils_count');
    Route::get('inventories/count/web_flavor_and_infused', 'InventoriesController@web_flavor_and_infused_count');
    Route::get('inventories/count/website_vinegars', 'InventoriesController@website_vinegars_count');
    Route::get('inventories/count/ready_to_ship', 'InventoriesController@ready_to_ship_count');
    Route::get('inventories/count/rework', 'InventoriesController@rework_count');
    Route::get('inventories/count/fustis', 'InventoriesController@fustis_count');
    Route::get('inventories/count/cardboard_blank', 'InventoriesController@cardboard_blank_count');
    Route::get('inventories/count/cardboard_printed', 'InventoriesController@cardboard_printed_count');
    Route::get('inventories/count/customers_oils', 'InventoriesController@customers_oils_count');

    //product
    Route::get('inventories/product/flavors_for_samples', 'InventoriesController@flavors_for_samples');
    Route::get('inventories/product/flavors', 'InventoriesController@flavors');
    Route::get('inventories/product/follmer', 'InventoriesController@follmer');
    Route::get('inventories/product/materials', 'InventoriesController@materials');
    Route::get('inventories/product/supplies', 'InventoriesController@supplies');
    Route::get('inventories/product/cardboard_blank', 'InventoriesController@cardboard_blank');
    Route::get('inventories/product/cardboard_printed', 'InventoriesController@cardboard_printed');
    Route::get('inventories/product/caps', 'InventoriesController@caps');
    Route::get('inventories/product/fustis', 'InventoriesController@fustis');
    Route::get('inventories/product/etched_bottles', 'InventoriesController@etched_bottles');
    Route::get('inventories/product/bottles', 'InventoriesController@bottles');
    Route::get('inventories/product/customers_labels', 'InventoriesController@customers_labels');
    Route::get('inventories/product/labels_in_labelroom', 'InventoriesController@labels_in_labelroom');
    Route::get('inventories/product/labels_in_house', 'InventoriesController@labels_in_house');
    Route::get('inventories/product/rework', 'InventoriesController@rework');
    Route::get('inventories/product/ready_to_ship', 'InventoriesController@ready_to_ship');
    Route::get('inventories/product/sprays', 'InventoriesController@sprays');
    Route::get('inventories/product/finished_product', 'InventoriesController@finished_product');
    Route::get('inventories/product/website_totals', 'InventoriesController@website_totals');
    Route::get('inventories/product/website_other', 'InventoriesController@website_other');
    Route::get('inventories/product/website_vinegars', 'InventoriesController@website_vinegars');
    Route::get('inventories/product/web_flavor_and_infused', 'InventoriesController@web_flavor_and_infused');
    Route::get('inventories/product/website_olive_oils', 'InventoriesController@website_olive_oils');
    Route::get('inventories/product/spreads_and_olives', 'InventoriesController@spreads_and_olives');
    Route::get('inventories/product/infused_oil_drums', 'InventoriesController@infused_oil_drums');

    Route::get('inventories/product/specialty', 'InventoriesController@specialty');
        //Specialty Sub Category
    Route::get('inventories/product/specialties/truffle_oil', 'SpecialtiesController@truffle_oil');
    Route::get('inventories/product/specialties/truffle_oil_black', 'SpecialtiesController@truffle_oil_black');
    Route::get('inventories/product/specialties/truffle_oil_white', 'SpecialtiesController@truffle_oil_white');
    Route::get('inventories/product/specialties/cocoa_butter', 'SpecialtiesController@cocoa_butter');
    Route::get('inventories/product/specialties/cocoa_butter_deodorized', 'SpecialtiesController@cocoa_butter_deodorized');
    Route::get('inventories/product/specialties/cocoa_butter_natural', 'SpecialtiesController@cocoa_butter_natural');
    Route::get('inventories/product/specialties/sesame_oil', 'SpecialtiesController@sesame_oil');
    Route::get('inventories/product/specialties/sesame_clear', 'SpecialtiesController@sesame_clear');
    Route::get('inventories/product/specialties/sesame_toasted', 'SpecialtiesController@sesame_toasted');

    Route::get('inventories/product/railcars', 'InventoriesController@railcars');
    Route::get('inventories/product/vinegars', 'InventoriesController@vinegars');
    //Vinegars Sub Category
    Route::get('inventories/product/vinegars/balsamic_vinegar', 'VinegarsController@balsamic_vinegar');
    Route::get('inventories/product/vinegars/4_star_dark', 'VinegarsController@_4_star_dark');
    Route::get('inventories/product/vinegars/4_star_white', 'VinegarsController@_4_star_white');
    Route::get('inventories/product/vinegars/8_star_dark', 'VinegarsController@_8_star_dark');
    Route::get('inventories/product/vinegars/25_star', 'VinegarsController@_25_star');
    Route::get('inventories/product/vinegars/25_star_dark', 'VinegarsController@_25_star_dark');
    Route::get('inventories/product/vinegars/25_star_white', 'VinegarsController@_25_star_white');

    Route::get('inventories/product/vinegars/honey_vinegar_serrano', 'VinegarsController@honey_vinegar_serrano');
    Route::get('inventories/product/vinegars/lambrusco', 'VinegarsController@lambrusco');
    Route::get('inventories/product/vinegars/wine_vinegar', 'VinegarsController@wine_vinegar');
    Route::get('inventories/product/vinegars/wine_vinegar_white', 'VinegarsController@wine_vinegar_white');
    Route::get('inventories/product/vinegars/wine_vinegar_red', 'VinegarsController@wine_vinegar_red');
    Route::get('inventories/product/vinegars/rice_vinegar', 'VinegarsController@rice_vinegar');

    Route::get('inventories/product/tanks', 'InventoriesController@tanks');
    Route::get('inventories/product/organic', 'InventoriesController@organic');

    //totes
    Route::get('inventories/product/totes', 'InventoriesController@totes');
    //totes sub category
    Route::get('inventories/product/totes/canola_oil', 'TotesController@canola_oil');
    Route::get('inventories/product/totes/canola_oil/canola_rbd', 'TotesController@canola_rbd');
    Route::get('inventories/product/totes/canola_oil/canola_non_gmo', 'TotesController@canola_non_gmo');
    Route::get('inventories/product/totes/soybean_oil', 'TotesController@soybean_oil');
    Route::get('inventories/product/totes/soybean_oil/soy_rbd', 'TotesController@soy_rbd');
    Route::get('inventories/product/totes/soybean_oil/soy_non_gmo', 'TotesController@soy_non_gmo');
    Route::get('inventories/product/totes/soybean_oil/soy_winterized', 'TotesController@soy_winterized');
    Route::get('inventories/product/totes/blended_oils', 'TotesController@blended_oils');
    Route::get('inventories/product/totes/blended_oils/per_90_can_10_evoo', 'TotesController@per_90_can_10_evoo');
    Route::get('inventories/product/totes/blended_oils/per_75_can_25_evoo', 'TotesController@per_75_can_25_evoo');
    Route::get('inventories/product/totes/blended_oils/per_90_soy_10_evoo', 'TotesController@per_90_soy_10_evoo');
    Route::get('inventories/product/totes/olive_oil', 'TotesController@olive_oil');
    Route::get('inventories/product/totes/olive_oil/extra_virgin_olive_oil', 'TotesController@extra_virgin_olive_oil');
    Route::get('inventories/product/totes/olive_oil/extra_virgin_olive_oil/italian', 'TotesController@extra_virgin_olive_oil_italian');
    Route::get('inventories/product/totes/olive_oil/extra_virgin_olive_oil/italian/evoo_italian_coratina', 'TotesController@evoo_italian_coratina');
    Route::get('inventories/product/totes/olive_oil/extra_virgin_olive_oil/italian/evoo_italian_umbrian', 'TotesController@evoo_italian_umbrian');
    Route::get('inventories/product/totes/olive_oil/extra_virgin_olive_oil/spanish', 'TotesController@spanish');
    Route::get('inventories/product/totes/olive_oil/extra_virgin_olive_oil/spanish/evoo_spanish_arbequina', 'TotesController@evoo_spanish_arbequina');
    Route::get('inventories/product/totes/olive_oil/extra_virgin_olive_oil/spanish/evoo_spanish_hojiblanca', 'TotesController@evoo_spanish_hojiblanca');
    Route::get('inventories/product/totes/olive_oil/extra_virgin_olive_oil/chilean', 'TotesController@chilean');
    Route::get('inventories/product/totes/olive_oil/extra_virgin_olive_oil/chilean/evoo_chilean_frantoio', 'TotesController@evoo_chilean_frantoio');
    Route::get('inventories/product/totes/olive_oil/pomace_olive_oil', 'TotesController@pomace_olive_oil');
    Route::get('inventories/product/totes/olive_oil/pure_olive_oil', 'TotesController@pure_olive_oil');

    Route::get('inventories/product/drums', 'InventoriesController@drums');
    Route::get('inventories/product/customers_oils', 'InventoriesController@customers_oils');

    //discontinued
    // Route::get('inventories/discontinued/drums' , 'InventoriesController@drum_discontinued');
    // Route::get('inventories/discontinued/totes' , 'InventoriesController@tote_discontinued');
    // Route::get('inventories/discontinued/organics' , 'InventoriesController@organic_discontinued');
    // Route::get('inventories/discontinued/tanks' , 'InventoriesController@tank_discontinued');
    // Route::get('inventories/discontinued/vinegars' , 'InventoriesController@vinegar_discontinued');
    // Route::get('inventories/discontinued/railcars' , 'InventoriesController@railcar_discontinued');
    // Route::get('inventories/discontinued/specialty' , 'InventoriesController@specialty_discontinued');
    // Route::get('inventories/discontinued/infused_oil_drums' , 'InventoriesController@infused_oil_drums_discontinued');
    // Route::get('inventories/discontinued/spreads_and_olives' , 'InventoriesController@spreads_and_olives_discontinued');
    // Route::get('inventories/discontinued/website_olive_oils' , 'InventoriesController@website_olive_oils_discontinued');
    // Route::get('inventories/discontinued/web_flavor_and_infused' , 'InventoriesController@web_flavor_and_infused_discontinued');

    Route::get('inventories/discontinued/drums', 'InventoriesController@drum_discontinued');
    Route::get('inventories/discontinued/totes', 'InventoriesController@tote_discontinued');
    Route::get('inventories/discontinued/organics', 'InventoriesController@organic_discontinued');
    Route::get('inventories/discontinued/tanks', 'InventoriesController@tank_discontinued');
    Route::get('inventories/discontinued/vinegars', 'InventoriesController@vinegar_discontinued');
    Route::get('inventories/discontinued/railcars', 'InventoriesController@railcar_discontinued');
    Route::get('inventories/discontinued/specialty', 'InventoriesController@specialty_discontinued');
    Route::get('inventories/discontinued/infused_oil_drums', 'InventoriesController@infused_oil_drums_discontinued');
    Route::get('inventories/discontinued/spreads_and_olives', 'InventoriesController@spreads_and_olives_discontinued');
    Route::get('inventories/discontinued/website_olive_oils', 'InventoriesController@website_olive_oils_discontinued');
    Route::get('inventories/discontinued/web_flavor_and_infused', 'InventoriesController@web_flavor_and_infused_discontinued');
    Route::get('inventories/discontinued/website_vinegars', 'InventoriesController@website_vinegars_discontinued');
    Route::get('inventories/discontinued/website_other', 'InventoriesController@website_other_discontinued');
    Route::get('inventories/discontinued/finished_product', 'InventoriesController@finished_product_discontinued');
    Route::get('inventories/discontinued/sprays', 'InventoriesController@sprays_discontinued');
    Route::get('inventories/discontinued/ready_to_ship', 'InventoriesController@ready_to_ship_discontinued');
    Route::get('inventories/discontinued/rework', 'InventoriesController@rework_discontinued');
    Route::get('inventories/discontinued/labels_in_house', 'InventoriesController@labels_in_house_discontinued');
    Route::get('inventories/discontinued/labels_in_labelroom', 'InventoriesController@labels_in_labelroom_discontinued');
    Route::get('inventories/discontinued/customers_labels', 'InventoriesController@customers_labels_discontinued');
    Route::get('inventories/discontinued/bottles', 'InventoriesController@bottles_discontinued');
    Route::get('inventories/discontinued/etched_bottles', 'InventoriesController@etched_bottles_discontinued');
    Route::get('inventories/discontinued/fustis', 'InventoriesController@fustis_discontinued');
    Route::get('inventories/discontinued/caps', 'InventoriesController@caps_discontinued');
    Route::get('inventories/discontinued/cardboard_blank', 'InventoriesController@cardboard_blank_discontinued');
    Route::get('inventories/discontinued/cardboard_printed', 'InventoriesController@cardboard_printed_discontinued');
    Route::get('inventories/discontinued/supplies', 'InventoriesController@supplies_discontinued');
    Route::get('inventories/discontinued/materials', 'InventoriesController@materials_discontinued');
    Route::get('inventories/discontinued/follmer', 'InventoriesController@follmer_discontinued');
    Route::get('inventories/discontinued/flavors', 'InventoriesController@flavors_discontinued');
    Route::get('inventories/discontinued/flavors_for_samples', 'InventoriesController@flavors_for_samples_discontinued');
    Route::get('inventories/discontinued/customers_oils', 'InventoriesController@customers_oils_discontinued');

    //pdf
    Route::get('inventories/pdf/product_totals', 'InventoriesController@pdf_product_totals');
    Route::get('inventories/pdf/drums', 'InventoriesController@pdf_drum');
    Route::get('inventories/pdf/totes', 'InventoriesController@pdf_totes');
    Route::get('inventories/pdf/organics', 'InventoriesController@pdf_organic');
    Route::get('inventories/pdf/tanks', 'InventoriesController@pdf_tanks');
    Route::get('inventories/pdf/vinegars', 'InventoriesController@pdf_vinegars');
    Route::get('inventories/pdf/railcars', 'InventoriesController@pdf_railcars');
    Route::get('inventories/pdf/specialty', 'InventoriesController@pdf_specialty');
    Route::get('inventories/pdf/infused_oil_drums', 'InventoriesController@pdf_infused_oil_drum');
    Route::get('inventories/pdf/spreads_and_olives', 'InventoriesController@pdf_spreads_and_olive_oils');
    Route::get('inventories/pdf/website_olive_oils', 'InventoriesController@pdf_website_olive_oil');
    Route::get('inventories/pdf/web_flavor_and_infused', 'InventoriesController@pdf_web_flavor_and_infused');
    Route::get('inventories/pdf/website_vinegars', 'InventoriesController@pdf_website_vinegar');
    Route::get('inventories/pdf/website_other', 'InventoriesController@pdf_website_other');
    Route::get('inventories/pdf/website_totals', 'InventoriesController@pdf_website_totals');
    Route::get('inventories/pdf/finished_product', 'InventoriesController@pdf_finished_product');
    Route::get('inventories/pdf/sprays', 'InventoriesController@pdf_spray');
    Route::get('inventories/pdf/ready_to_ship', 'InventoriesController@pdf_ready_to_ship');
    Route::get('inventories/pdf/rework', 'InventoriesController@pdf_rework');
    Route::get('inventories/pdf/labels_in_house', 'InventoriesController@pdf_labels_in_house');
    Route::get('inventories/pdf/labels_in_labelroom', 'InventoriesController@pdf_labels_in_labelroom');
    Route::get('inventories/pdf/customers_labels', 'InventoriesController@pdf_customers_labels');
    Route::get('inventories/pdf/bottles', 'InventoriesController@pdf_bottle');
    Route::get('inventories/pdf/etched_bottles', 'InventoriesController@pdf_etched_bottle');
    Route::get('inventories/pdf/fustis', 'InventoriesController@pdf_fustis');
    Route::get('inventories/pdf/caps', 'InventoriesController@pdf_cap');
    Route::get('inventories/pdf/cardboard_blank', 'InventoriesController@pdf_cardboard_blank');
    Route::get('inventories/pdf/cardboard_printed', 'InventoriesController@pdf_cardboard_printed');
    Route::get('inventories/pdf/supply', 'InventoriesController@pdf_supply');
    Route::get('inventories/pdf/materials', 'InventoriesController@pdf_material');
    Route::get('inventories/pdf/follmer', 'InventoriesController@pdf_follmer');
    Route::get('inventories/pdf/flavors', 'InventoriesController@pdf_flavor');
    Route::get('inventories/pdf/flavors_for_samples', 'InventoriesController@pdf_flavors_for_sample');
    Route::get('inventories/pdf/inventory_totals', 'InventoriesController@pdf_inventory_totals');
    Route::get('inventories/pdf/totals_for_bank', 'InventoriesController@pdf_totals_for_bank');
    Route::get('inventories/pdf/customers_oils', 'InventoriesController@pdf_customers_oils');

    //specsheets pdf
    Route::get('specsheets/pdf/specsheets/{id}','SpecsheetsController@pdf_specsheets');

    Route::resource('inventories', 'InventoriesController');

    //

    /*
    *Product Hold
    *
    */

    Route::get('productholds/released', 'ProductholdsController@released');
    Route::resource('productholds', 'ProductholdsController');
    //Product Hold release

    /*
    *RMA Form
    *
    */
    Route::resource('rmas', 'RmasController');



    /*
    *Receiving Logs 2016
    *
    */
     //label
    // Route::get('receivinglogs2016/label/{id}', 'Receivinglog2016Controller@label');




    /*
     * Samples
     * Remove Per Josh
     */
//    Route::resource('samples', 'SamplesController');

    /*
    *Freight Quotes
    * per Leeanne
    */
    Route::resource('freightquotes', 'FreightquotesController');
    /*
     * Shipping
     */
	Route::get('shippings/canceled', 'ShippingsController@canceled');
    Route::get('shippings/shipped', 'ShippingsController@shipped');
    Route::resource('shippings', 'ShippingsController');
    /*
     * Appointment Logs
     *
     */
    Route::get('receivingappointmentlogs/received', 'ReceivingappointmentlogsController@received');
    Route::resource('receivingappointmentlogs', 'ReceivingappointmentlogsController');

//blends
    Route::get('blends/archive', 'BlendsController@archive');
    Route::resource('blends', 'BlendsController');

/*
* COA
*
*/
	Route::get('coas/create_coa/{id}', 'CoasController@create_coa');
	Route::post('coas/create_coa/{id}', 'CoasController@create_coa');
	Route::get('coas/create/{id}', 'CoasController@create');
	Route::post('coas/create/{id}', 'CoasController@create');
	Route::get('coas/template/edit/{id}', 'CoasController@template_edit');
Route::get('coas/templates', 'CoasController@template');
Route::resource('coas', 'CoasController');

/*
 * Lot Logs
 * Material Lot Logs
 */

    // Route::get('lotlogs/label_store/{id}', 'LotlogsController@label_store');

    Route::post('lotlogs/label/{id}', 'LotlogsController@label_store');
    Route::get('lotlogs/label/{id}', 'LotlogsController@label');

    Route::get('lotlogs/label/print', 'LotlogsController@label_store');
    Route::get('lotlogs/in_progress', 'LotlogsController@in_progress');
    Route::get('lotlogs/archive', 'LotlogsController@archive');
    Route::get('lotlogs/last_year', 'LotlogsController@last_year');
    Route::get('lotlogs/all', 'LotlogsController@all');
    Route::resource('lotlogs', 'LotlogsController');

//material lot logs
    Route::post('materiallotlogs/label/{id}', 'MateriallotlogsController@label_store');
    Route::get('materiallotlogs/label/{id}', 'MateriallotlogsController@label');

    Route::get('materiallotlogs/label/print', 'MateriallotlogsController@label_store');
    Route::resource('materiallotlogs', 'MateriallotlogsController');

//specsheets
    Route::resource('specsheets', 'SpecsheetsController');

//spotcheck
    Route::get('spotchecks/print', 'SpotchecksController@print_page');
    Route::resource('spotchecks', 'SpotchecksController');

//old app for Karen
//    Route::resource('salescharts', 'SaleschartsController');

//nutritionfacts
    Route::resource('nutritionfacts', 'NutritionfactsController');

    /**
     * Receiving Logs
     */

//2013 receiving log print
    Route::get('receivinglogs/print', 'ReceivinglogsController@print_page');
    Route::resource('receivinglogs', 'ReceivinglogsController');

//2012 receiving log print
    Route::get('recievinglog2012s/print', 'Receivinglog2012sController@print_page');
    Route::resource('receivinglog2012s', 'Receivinglog2012sController');

//2014 receiving log print
    Route::get('recievinglog2014s/print', 'Recievinglog2014sController@print_page');
    Route::resource('recievinglog2014s', 'Recievinglog2014sController');

//2015 receiving log print
    Route::get('recievinglog2015s/print', 'Recievinglog2015sController@print_page');
    Route::resource('recievinglog2015s', 'Recievinglog2015sController');

//2016 receiving log print
    Route::get('receivinglogs2016/print', 'Receivinglog2016Controller@print_page');
    Route::resource('receivinglogs2016', 'Receivinglog2016Controller');

//2017 receiving log
    Route::get('receivinglogs2017/print', 'Receivinglog2017Controller@print_page');
    Route::resource('receivinglogs2017', 'Receivinglog2017Controller');

//2018 receiving log
    Route::get('receivinglogs2018/print', 'Receivinglog2018Controller@print_page');
    Route::resource('receivinglogs2018', 'Receivinglog2018Controller');


//customer complaints
    //customer complaints api
    //form from store supply
    Route::post('api/customercomplaints', 'CustomercomplaintsController@api_store');
    //
    // qc report
    // Route::get('customercomplaints/concern_report', 'CustomercomplaintsController@concern_report');
    Route::get('customercomplaints/concern_report/{id}', 'CustomercomplaintsController@concern_report');

    //
    Route::get('customercomplaints/reports', 'CustomercomplaintsController@reports');
    Route::get('customercomplaints/qc', 'CustomercomplaintsController@qc');
    Route::get('customercomplaints/customer_service', 'CustomercomplaintsController@customer_service');
    Route::get('customercomplaints/production', 'CustomercomplaintsController@production');
    Route::get('customercomplaints/technology', 'CustomercomplaintsController@technology');
    Route::get('customercomplaints/{id}/products', 'CustomercomplaintsController@products');
    Route::post('customercomplaints/{id}/products', 'CustomercomplaintsController@products');
    Route::get('customercomplaints/complete', 'CustomercomplaintsController@complete');
    Route::get('customercomplaints/in_progress', 'CustomercomplaintsController@in_progress');
    Route::get('customercomplaints/last_year', 'CustomercomplaintsController@last_year');
    Route::resource('customercomplaints', 'CustomercomplaintsController');
    Route::resource('products', 'ProductsController');
//PO ETA's
    Route::get('poetas/received', 'PoetasController@received');
    Route::resource('poetas', 'PoetasController');

//orders
    Route::get('orders', 'OrdersController@index');

//Recipes api
    // Route::get('recipes/api', 'RecipesController@api');
//Recipes
//Recipes category update

Route::resource('categories', 'RecipecategoryController');
Route::resource('product_categories', 'RecipeProductCategoriesController');
Route::resource('recipes', 'RecipesController');

//Receiving Railcars
//railcar loads

Route::prefix('receiving')->group(function() {
Route::get('railcars/{id}/loads', 'RailcarsreceivingController@loads');
Route::get('railcars/{id}/loads', 'RailcarsreceivingController@loads');
Route::post('railcars/{id}/loads', 'RailcarsreceivingController@loads');
Route::get('railcars/received', 'RailcarsreceivingController@received');
Route::get('railcars/store', 'RailcarsreceivingController@store');
Route::post('railcars/store', 'RailcarsreceivingController@store');
Route::resource('railcars', 'RailcarsreceivingController');
//railcars pdf
Route::get('railcars/pdf/{id}','RailcarsreceivingController@pdf');
});

//destroy railcar loads
Route::resource('railcarloads', 'RailcarloadController');

// Route::get('receiving/railcars/store', 'RailcarsreceivingController@store');
// Route::post('receiving/railcars/store', 'RailcarsreceivingController@store');
// Route::resource('receiving/railcars/', 'RailcarsreceivingController');
// Route::get('receiving/railcars/{id}/loads', 'RailcarsreceivingController@loads');
// Route::get('receiving/railcars/received', 'RailcarsreceivingController@received');

//Recieving Labels
Route::get('receiving/labels/received', 'LabelsController@received');
Route::resource('receiving/labels', 'LabelsController');


//Weight Logs
Route::get('weightlogs/freight_scheduled', 'WeightlogsController@freight_scheduled');
Route::get('weightlogs/complete', 'WeightlogsController@complete');
Route::resource('weightlogs', 'WeightlogsController');


//user
    Route::resource('user', 'UserController');
//admin/backend section
    Route::resource('admin', 'AdminController');
});



Route::resource('recipe-product-categories', 'RecipeProductCategoriesController');